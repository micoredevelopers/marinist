<?php
namespace App\Sitemap;

use Illuminate\Support\Str;
use Spatie\Crawler\CrawlProfile;
use Psr\Http\Message\UriInterface;

class CustomProfile extends CrawlProfile
{
protected $disabledUrls = [];
protected $disabledWords = ['page='];
/** @var callable */
protected $profile;

public function shouldCrawlCallback(callable $callback)
{
$this->profile = $callback;
}

/*
* Determine if the given url should be crawled.
*/
public function shouldCrawl(UriInterface $url): bool
{
$notContainsWords = !Str::contains($url->getQuery(), $this->disabledWords);
return (($this->profile)($url)) AND $notContainsWords;
}
}
