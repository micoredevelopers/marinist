<?php

namespace App\Providers;

use App\Models\Gallery;
use App\Models\News;
use App\Models\Setting;
use App\Models\Translate;
use App\Observers\GalleryObserver;
use App\Observers\NewsObserver;
use App\Observers\SettingObserver;
use App\Observers\TranslateObserver;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
		Carbon::setLocale(config('app.locale'));
		Schema::defaultStringLength(191);
		if ($this->app->environment() !== 'production') {
			$this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
		}
	}

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->bootObservers();
		$this->bootBlade();
		$this->bootCollection();
	}

	private function bootObservers()
	{
		Translate::observe(TranslateObserver::class);
		Setting::observe(SettingObserver::class);
		News::observe(NewsObserver::class);
		Gallery::observe(GalleryObserver::class);
	}

	private function bootBlade()
	{
		Blade::if('superadmin', function () {
			return isSuperAdmin();
		});
		Blade::if('dev', function () {
			return env('APP_ENV') === 'local';
		});
	}

	private function bootCollection()
	{
		Collection::macro('whereActive', function ($active = 1) {
			return $this->filter(function ($item) use ($active) {
				return (int)Arr::get($item, 'active') === $active;
			});
		});
	}
}
