<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;

class CreateUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auth:user {email} {name} {password}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->argument('name');
        $email = $this->argument('email');
        $password = $this->argument('password');
        if (!$email OR !$name OR !$password){
            return $this->warn('Заполнены не все данные');
        }
        if (User::where('email', $email)->first()){
            return $this->warn('Пользователь с таким email уже существует');
        }
        $isCreated = User::create([
            'name' => $name,
            'password' => bcrypt($password),
            'email' => $email
        ]);
        if (!$isCreated){
            return $this->warn('Чет трабл, не получилось создать');
        }
        $this->info('Пользователь успешно создан');

    }
}
