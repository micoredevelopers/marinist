<?php

namespace App\Http\Requests;

class ContactRequest extends AbstractRequest
{
	public function rules()
	{
		return [
			'name' => ['required', 'string', 'max:255'],
			'phone' => ['required', 'string', 'max:255'],
			'text' => ['nullable', 'string'],
			'email' => ['nullable', 'string'],
		];
	}
}
