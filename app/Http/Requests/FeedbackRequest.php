<?php declare(strict_types=1);

namespace App\Http\Requests;

use App\Services\Phone\PhoneUkraineFormatter;

class FeedbackRequest extends AbstractRequest
{
	public function rules()
	{
		return [
			'name' => ['required', 'string', 'max:255'],
			'phone' => ['required', 'string', 'max:255'],
			'email' => ['nullable', 'string', 'email'],
		];
	}

	protected function prepareForValidation()
	{
		$this->merge([
			'phone' => PhoneUkraineFormatter::formatPhone($this->get('phone')),
		]);
	}
}
