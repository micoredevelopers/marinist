<?php declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AbstractRequest extends FormRequest
{
	public function authorize()
	{
		return true;
	}

	public function getFillableFields()
	{
		return $this->only(
			array_keys($this->rules())
		);
	}

	/***
	 * @return array
	 */
	public function rules()
	{
		return [];
	}
}