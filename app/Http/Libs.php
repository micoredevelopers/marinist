<?php
/**
 * Created by PhpStorm.
 * User: Cannabis
 * Date: 30.10.2017
 * Time: 22:54
 */

namespace App\Http;

use Image;
class Libs
{
	/**
	 * @param $imagePath
	 * @param int $cols
	 * @param int $rows
	 * @return string
	 */
	static function thumbnailImage($imagePath, $cols = 250, $rows = 250)
	{
		$ext = 'jpg';
		$image = Image::make($imagePath);
		$image->fit($cols, $rows);
		return $image->stream($ext);
	}

	public static function imgOrig($photoPath)
	{
		return str_replace('_s', '', $photoPath);
	}

	public static function imgThumb($photoPath)
	{
		return str_replace('.', '_s.' , $photoPath);
	}

	public static function makeThumbFromOrigin($storagePath, $filename)
	{
		$thumbImageFullPath = $storagePath . $filename;
		$fileNameOrigin = self::imgOrig($filename);
		copy($storagePath . $fileNameOrigin, $thumbImageFullPath);
		file_put_contents($thumbImageFullPath, Libs::thumbnailImage($thumbImageFullPath));
	}

	public static function alertMessage($str, $type = 3)
	{
		$type = self::bootstrapColors($type);
		return '<div class="alert alert-' . $type . '" role="alert">' . $str . '</div>';
	}

	public static function isLast($array = [], $key)
	{
		$last_key = key(array_slice($array, -1, 1, true));
		if ($last_key === $key) return true;
		return false;
	}

	public static function startRender()
	{
		ob_start();
	}

	public static function stopRender()
	{
		$contents = ob_get_contents();
		ob_end_clean();
		return $contents;
	}

	/**
	 * @param $obj
	 * @return mixed
	 * Временный костыль, пока в ядро лары не добавили bootstrap 4
	 */
	public static function _tmpRemakePaging($obj)
	{
		self::startRender();
		echo htmlspecialchars_decode($obj->links());
		$content = self::stopRender();
		$replace = [
			'<li ' => '<li class="page-item" ',
			'<a ' => '<a class="page-link" ',
			'<span>' => '<li class="page-item disabled"><a class="page-link" href="#" tabindex="-1">',
			'</span>' => '</a></li>'
		];
		return str_replace(array_keys($replace), array_values($replace), $content);
	}

	public static function bootstrapColors($offset)
	{
		$array = [
			0 => 'danger',
			1 => 'success',
			2 => 'warning',
			3 => 'info',
			4 => 'primary',
			5 => 'default',
			'default' => 'default'
		];
		return array_key_exists($offset, $array) ? $array[$offset] : $array['default'];
	}

	public static function makeEditBtn($url, $color = 0)
	{
		if (self::isUserAuth()){
			$btn = self::bootstrapColors($color);
			return '<a href="' . $url . '" id="adminButtonEdit" class="btn btn-' . $btn . '" target="_blank">Редактировать это</a>';
		}
		return '';
	}
}