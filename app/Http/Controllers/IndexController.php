<?php

namespace App\Http\Controllers;

use App\Models\Cats;
use App\Models\Infoblock;
use App\Models\Menu;
use App\Models\News;
use App\Models\Request;
use App\Models\Shows;
use App\Models\Slider;
use App\Models\Translate;

class IndexController extends SiteController
{

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->setTitle(getSetting('global.sitename'));
		$vars = [];
		$data = [];
		$data['content'] = view('main', $vars);

		return $this->main($data);
	}

	public function translates()
	{
		return Translate::GetLang($this->lang)->get([
			'key',
			'value',
			'comment',
		])->keyBy('key');
	}
}
