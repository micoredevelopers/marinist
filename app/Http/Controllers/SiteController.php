<?php

namespace App\Http\Controllers;

use App\Models\Language;
use App\Models\Menu;
use App\Models\Meta;
use App\Traits\Helpers;
use View;

abstract class SiteController extends BaseController
{
	use Helpers;
	protected $lang = 1;
	private $languages;
	private $languagesList;

	public function __construct()
	{

		parent::__construct();
		$this->languages = new Language();
		$this->languagesList = $this->getLanguages();
		$this->lang = getLang();
		$this->loadSettings();
		$this->loadTranslates(getLang());
		$this->checkForStylesAndScripts();
        $this->addBreadCrumb(getTranslate('global.general'), route('home'));
	}

	public function main($data = [])
	{
		$this->checkMetaData();
		$data['styles'] = $this->getStylesString();
		$data['stylesFooter'] = $this->getStylesString('footer');
		$data['scripts'] = $this->getScriptsString($this->getScripts());
		$data['scriptsDefer'] = $this->getScriptsString($this->getScripts(true), 'defer');
		$data['menus'] = Menu::Active(1)->GetLang($this->lang)->SortOrder()->get();
		$data['languages'] = $this->languagesList;
		$data['canonical']= $this->canonical;
		$data['localizedUrl']=$this->localizedUrl;
		$data['sections'] = [];
		if (!\Arr::has($data, 'breadcrumbs')) {
			$data['breadcrumbs'] = $this->getBreadCrumbs();
		}
		//Получаем секции дочерних шаблонов, поскольку шаблоны не наследуют друг друга
		if (\Arr::get($data, 'content') AND ($data['content'] instanceof \Illuminate\View\View)) {
			$data['sections'] = $data['content']->renderSections();
		}

		return view('layout.index', $data);
	}

	public function checkForStylesAndScripts()
	{
		$styles = [
//			'css/jquery.fancybox.min.css',
		];
		$stylesFooter = [
//			'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css',
		];
		$scripts = [
//			'js/lib/jquery.fancybox.min.js',
		];
		$scriptsDefer = [
//			'js/functions.js',
//			'js/triggers.js',
		];
		$this->addCss($styles);
		$this->addCss($stylesFooter, 'footer');
		$this->addScripts($scripts);
		$this->addScriptsDefer($scriptsDefer);

		return $this;
	}

	public function checkMetaData()
	{
		$metadata = Meta::getMetaData();
		if ($metadata) {
			if ($metadata->meta_title) $this->setTitle($metadata->meta_title, true);
			if ($metadata->meta_description) $this->setDescription($metadata->meta_description, true);
			if ($metadata->meta_keywords) $this->setKeywords($metadata->meta_keywords, true);
		} elseif ($meta_default = Meta::whereUrl('*')->GetLang($this->lang)->first()) {
			if (!$this->getTitle()) $this->setTitle($meta_default->meta_title);
			if (!$this->getDescription()) $this->setDescription($meta_default->meta_description);
			if (!$this->getKeywords()) $this->setKeywords($meta_default->meta_keywords);
		}
	}

	protected function getLanguages()
	{
		$languages = $this->languages->Active(1)->get();
		$languages = $languages->map(function ($item) {
			$item->current = ($item->key === getCurrentLocale()) ? 1 : 0;
			return $item;
		});
		return $languages;
	}
}











