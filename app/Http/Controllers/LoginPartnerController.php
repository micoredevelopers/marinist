<?php

namespace App\Http\Controllers;

use App\Models\PartnerUser;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

class LoginPartnerController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '';

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        //d(Auth::guard('partners')->check());
        if(Auth::guard('partners')->check())
        {
            return redirect()->route('partners.index');
        }
        $this->redirectTo = route('partners.index');


    }

    public function index()
    {
        if(Auth::guard('partners')->check())
        {
            return redirect()->route('partners.index');
        }
        return view('public.partners.login');
    }

    public function username(){
        return 'username';
    }
    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        return $this->guard()->attempt(
            $this->credentials($request)
        );
    }
    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('partners');
    }

    public function logout(Request $request)
    {
        Auth::guard('partner')->logout();
        $request->session()->invalidate();

        return $this->loggedOut($request) ?: redirect('/');
    }


    public function login(Request $request){
        $this->validateLogin($request);
        $data = $this->credentials($request);

        $user = PartnerUser::where([['username',$data['username']],['password',$data['password']]])->first();
        if(is_null($user)){
            return redirect()->back()->with('error','Не верный логин или пароль!');
        }
       // d($user);
        $this->guard()->login($user,    true);

        if($this->guard()->check()){
            return redirect()->to($this->redirectTo);
        }
        else {
            return redirect()->back()->with('error','Не верный логин или пароль!');
        }
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        $data = $request->only($this->username(), 'password');
        $data['password'] =base64_encode($data['password']);
        return $data;
    }

}
