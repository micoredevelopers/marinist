<?php

	namespace App\Http\Controllers;

	use App\Models\Infoblock;
	use App\Models\Language;
	use App\Models\Menu;
	use App\Models\Meta;
	use App\Models\Translate;
	use App\Traits\Helpers;
	use Illuminate\Http\Request;
    use View;

	abstract class BaseController extends Controller
	{
		use Helpers;
		protected $settings = [];
		protected $translate = [];
		protected $request;
		protected $canonical = '';
		protected $localizedUrl = [];

		public function __construct()
		{

			$this->request = request();
			$this->localizedUrl['ru']=\LaravelLocalization::getLocalizedURL('ru',request()->url());
            $this->localizedUrl['en']=\LaravelLocalization::getLocalizedURL('en',request()->url());
            $this->localizedUrl['uk']=\LaravelLocalization::getLocalizedURL('uk',request()->url());
			$this->canonical =request()->url();
		}

		protected function getTranslateAndSettings(&$vars)
		{
			if (isset($this->settings)) $vars['settings'] = $this->settings;
			if (isset($this->translate)) $vars['translate'] = $this->translate;
		}


	}











