<?php

namespace App\Http\Controllers;

use App\Helpers\PartnersChess;
use App\Helpers\ResponseHelper;
use App\Mail\NewsLetterView;
use App\Models\Flat;
use App\Models\Partner;
use App\Models\Section;
use App\Scopes\FlatsScope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class PartnersController extends SiteController
{
    public function __construct() {

        parent::__construct();
    }

    public function index(Request $request)
    {
        if(!Auth::guard('partners')->check())
        {
            return redirect()->route('partners.login.index');
        }
        $vars =[];


        $tmp = new PartnersChess(Section::all()->load(['floors.flats' => function($query){
            $query->withoutGlobalScope(new FlatsScope);
        }, 'floors.flats.status']));

        $vars['sections']=$tmp->getDataForChess();

        $vars['partners']=Partner::all();
        return view('public.partners.partners',$vars);

    }

    public function mail(Request $request) {
        $this->validate($request, [
            'name' => 'required|min:2',
            'phone' => 'required',
        ]);
        $data = $request->all();

        $data['referer'] = $request->server('HTTP_REFERER');
        $items = $request->except('_token');
        Mail::send(new NewsLetterView($items, $request));

        $res = [
            ResponseHelper::STATUS_KEY => ResponseHelper::SUCCESS_KEY,
            ResponseHelper::MESSAGE_KEY => getTranslate('request.send-success')
        ];
        return $res;


    }
}
