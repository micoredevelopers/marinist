<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ResponseHelper;
use App\Http\Libs;
use App\Models\Admin\Photo;
use App\Models\News;
use App\Models\NewsLang;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class NewsController extends AdminController
{
	protected $tb;
	private $name = 'Новости';

	private $key = 'news';

	public function __construct()
	{
		parent::__construct();
		$this->tb = 'news';
		$this->addBreadCrumb($this->name, route($this->tb . '.index'));
	}

	public function index()
	{
		$this->setTitle($this->name);
		$data['cardTitle'] = $this->name;
		$vars['table'] = $vars['controller'] = $this->tb;
		$vars['list'] = News::GetLang($this->langAdmin)->orderBy('date_pub', 'desc')->paginate();
		$data['content'] = view('admin.' . $this->tb . '.index', $vars);
		return $this->main($data);
	}

	/**
	 * @param Request $request
	 * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function create()
	{
		$data = [];
		$vars['controller'] = $this->tb;
		$title = 'Создание новости';
		$data['cardTitle'] = $title;
		$this->addBreadCrumb($title);
		$this->setTitle($title);
		$data['content'] = view('admin.news.create');
		return $this->main($data);
	}

	public function store(Request $request)
	{
		$message = '';
		if (!$request->get('url')) $request->merge(['url' => \Str::slug($request->get('name'))]);
		$input = $request->except('photo');
		$input['active'] = (int)$request->get('active');
		$input['date_pub'] = Carbon::parse($request->get('date_pub', now()));
		$this->validate($request, [
			'name' => 'max:255',
			'url'  => 'unique:news,url',
		]);
		if (($news = new News())->fillExisting($input)->save()) {
			$message = __('generic.successfully_added_new');
			$input[$news->getForeignKey()] = $news->id;
			foreach ($this->languagesList as $language) {
				/** @var $language \App\Models\Language */
				$input[$language->getForeignKey()] = $language->id;
				(new NewsLang())->fillExisting($input)->save();
			}
			//сохранить в базе имя фото если есть в реквесте
			if ($request->hasFile('photo')) {
				$photoPath = (new Photo())
					->setData('photo', $request->file('photo'))
					->setData('table', $this->tb)
					->setData('directory', $this->tb)
					->setData('id', $news->id)
					->saveImage(...$this->getThumbnailSizes());
				$news->update(['photo' => $photoPath]);
			}
		}
		if ($request->has('createOpen')) {
			return redirect(route($this->tb . '.edit', $news->id))->with('success', $message);
		}

		return redirect(route($this->tb . '.index'))->with('message', $message);
	}

	/**
	 * @param $id
	 * @param Request $request
	 * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
	 */
	public function edit($id, Request $request)
	{
		$vars = [];
		$data = [];
		$storage = Storage::disk('local');
		$vars['edit'] = News::GetLang($this->langAdmin)->findOrFail($id);
		$vars['photo_table'] = $this->tb;
		$vars['photosList'] = $vars['edit']->photos()->SortOrder()->get();
		$vars['edit']['photoExists'] = $storage->exists('public/' . $vars['edit']['photo']);
		$title = 'Редактирование ' . $vars['edit']['name'];
		$data['cardTitle'] = $title;
		$this->addBreadCrumb($title);
		$this->setTitle($title);

		$data['content'] = view('admin.news.edit', $vars);
		return $this->main($data);
	}

	public function update(Request $request, News $news)
	{
		$message = __('generic.update_failed');
		$input = $request->except('photo');
		$this->validate($request, [
			'name' => 'max:255',
		]);
		$input['active'] = (int)$request->get('active');
        $input['active_modal'] = (int)$request->get('active_modal');
		$input['date_pub'] = Carbon::parse($request->get('date_pub', now()));
		$input['video'] = $request->get('video');
		//
		if ($request->hasFile('photo')) {
			$input['photo'] = $this->photoModel
				->setData('photo', $request->file('photo'))
				->setData('table', $this->tb)
				->setData('directory', $this->tb)
				->setData('id', $news->id)
				->saveImage(...$this->getThumbnailSizes());
		}
		$this->photoModel->saveAdditionPhotos($news, $news->id);
		$news->fillExisting($input);
		if ($news->save()) {
			$news->langItem($this->langAdmin)->first()->fillExisting($input)->save();
			$message = __('generic.successfully_updated');
		}

		if ($request->has('saveClose')) {
			return redirect(route($this->tb . '.index'))->with(['success' => $message]);
		}

		return redirect()->back()->with(['success' => $message]);
	}

	public function destroy($id)
	{
		$status = ResponseHelper::ERROR_KEY;
		$message = __('generic.error_deleting');
		if ($delete = News::find($id)) {
			//todo on delete news, delete photos
			if ($delete->delete()) {
				$message = __('generic.successfully_deleted');
				$status = ResponseHelper::SUCCESS_KEY;
			}
		}

		return redirect(route('news.index'))->with([$status => $message]);
	}

	private function getThumbnailSizes(){
		return [
			getSetting($this->key . '.image.width', 405),
			getSetting($this->key. '.image.height', 250)
		];
	}

	public function normalizeDatePub()
	{
		News::all()->map(function(News $news){
			$news->setAttribute('date_pub', $news->getAttribute($news->getCreatedAtColumn()));
			$news->save();
		});

		return back();
	}
}
