<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ResponseHelper;
use Illuminate\Http\Request;
use \DB;
use Illuminate\Support\Facades\Schema;

class AjaxController
{
	/**
	 * @param Request $request
	 *
	 * @return array|\Illuminate\Http\RedirectResponse
	 * Включить/выключить запись в переданной таблице
	 */
	public function active(Request $request)
	{
		$data = [
			ResponseHelper::STATUS_KEY  => ResponseHelper::ERROR_KEY,
			ResponseHelper::MESSAGE_KEY => 'Переданы не все нужные данные',
		];
		if ($request->has('table') AND $request->has('id')) {
			$primary = $request->get('primary_key') ?? 'id';
			$table = $request->table;
			$id = $request->id;
			$record = DB::table($table)->where($primary, $id)->first();
			if (isset($record->active)) {
				$active = (int)!$record->active;
				if (Schema::hasTable($table) AND Schema::hasColumn($table, 'active')) {
					try {
						DB::table($table)->where($primary, $id)->update(['active' => $active]);
						$data[ResponseHelper::STATUS_KEY] = ResponseHelper::SUCCESS_KEY;
						$data[ResponseHelper::MESSAGE_KEY] = 'Активность записи обновлена';
					} catch (\Exception $e) {
						$data[ResponseHelper::MESSAGE_KEY] = $e->getMessage();
					}
				}
			}
		}

		return $data;
	}

	/**
	 * @param Request $request
	 *
	 * @return array
	 * Сортировка фотографий по переданным таблице и id записи
	 */
	public function sort(Request $request)
	{
		$data = [
			ResponseHelper::STATUS_KEY  => ResponseHelper::ERROR_KEY,
			ResponseHelper::MESSAGE_KEY => 'Переданы не все нужные данные',
		];
		$table = $request->table;
		$sort = json_decode($request->sort);
		$primary = $request->get('primary_key') ?? 'id';
		if ($table AND $sort) {
			try {
				if (Schema::hasTable($table) AND Schema::hasColumn($table, 'sort')) {
					foreach ($sort as $sort => $id) {
						\DB::table($table)->where($primary, $id)->update(['sort' => $sort]);
					}
					$data[ResponseHelper::STATUS_KEY] = ResponseHelper::SUCCESS_KEY;
					$data[ResponseHelper::MESSAGE_KEY] = 'Порядок сортировки успешно обновлен';
				}
			} catch (\Exception $e) {
				$data[ResponseHelper::MESSAGE_KEY] = $e->getMessage();
			}
		}

		return $data;
	}
}
