<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\Admin_menu;
use App\Models\Model;
use App\Traits\Authorizable;
use Illuminate\Http\Request;
use App\Http\Libs;
use Route;

class AdminMenuController extends AdminController
{
	use Authorizable;
	private $name = 'Админ Меню';

	protected $selfModelLang;
	protected $selfModel;
	protected $tb;

	public function __construct()
	{
		parent::__construct();
		$this->tb = 'admin-menus';
		$this->tbId = $this->tb . '_id';
		$this->selfModel = new Admin_menu();
		$this->addBreadCrumb($this->name, route('admin-menus.index'));
	}

	/**
	 * @param Request $request
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index()
	{
		$data = [];
		$vars['controller'] = $this->tb;
		$vars['table'] = 'admin_menus';

		$title = $this->name;
		$data['cardTitle'] = $title;
		$this->setTitle($title);

		$this->addScripts('js/lib/jquery.nestable.js');

		$vars['list'] = $this->selfModel->SortOrder()->get();
		if (view()->exists('admin.' . $this->tb . '.index')) {
			$data['content'] = view('admin.' . $this->tb . '.index', $vars);
		}

		return $this->main($data);
	}

	public function update(Request $request)
	{
		$message = 'Данные сохранены';
		$input = $request->except('_token');
		if (count($input['id'])) {
			foreach ($input['id'] as $id) {
				if (!$menu = Admin_menu::find($id)) continue;
				// Saving main table data
				$mainArr = [
					'url'              => $input['url'][$id] ?? null,
					'route'            => $input['route'][$id] ?? null,
					'active'           => $input['active'][$id] ?? 0,
					'parent_id'        => $input['parent_id'][$id],
					'name'             => $input['name'][$id],
					'icon_font'        => $input['icon_font'][$id],
					'gate_rule'        => $input['gate_rule'][$id],
					'content_provider' => $input['content_provider'][$id],
				];
				$menu->fill($mainArr)->save();
				//Saving language table data
				$vars['message'] = Libs::alertMessage($message, 1);
			}
			return redirect(route('admin-menus.index'))->with(['message' => $message]);
		}

		return redirect()->back();
	}

	/**
	 * @param Request $request
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
	 */
	public function create(Request $request)
	{
		$message = 'Запись не была создана, какая то блин ошибка';

		if ($this->selfModel->create([])) {
			$message = 'Запись успешно создана';
		}

		return redirect(route('admin-menus.index'))->with('message', $message);
	}


	public function destroy($id)
	{
		if ($this->selfModel->find($id)->delete()) $message = Libs::alertMessage('Запись удалена', 1);
		else $message = Libs::alertMessage('Запись не была удалена', 0);

		return redirect(route($this->tb))->with('message', $message);
	}

}
