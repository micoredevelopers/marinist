<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ResponseHelper;
use App\Models\PartnerUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RegisterPartnerController extends AdminController
{
    private $name = 'Регистрация партнера';

    protected $tb;

    public function __construct()
    {
        parent::__construct();
        $this->tb = 'admin.reg-partner';
        $this->addBreadCrumb($this->name, route($this->tb . '.index'));
    }

    public function index()
    {
        $this->setTitle($this->name);
        $data['cardTitle'] = $this->name;
        $vars['controller'] = $this->tb;
        $vars['list'] = PartnerUser::all();
        $data['content'] = view($this->tb . '.index', $vars);
        return $this->main($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $data =[];
        $data['cardTitle'] = 'Регистрация партнера';
        $data['content'] = view( $this->tb . '.create');
        return $this->main($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'username'=>'required|unique:partner_users',
            'password'=>'required|min:6'
        ]);
        $data = $request->except(['_method','_token']);

        $data['password']= base64_encode($data['password']);


        $partner = new PartnerUser();
        $partner->fill($data)->save();
        if ($request->has('createOpen')) {
            return redirect(route($this->tb . '.edit', $partner->id))->with('success', 'Партнер успешно зарегестрирован!');
        }

        return redirect(route($this->tb . '.index'))->with('message', 'Партнер успешно зарегестрирован!');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $vars = $data = [];
        $vars['edit'] = PartnerUser::findOrFail($id);
        $title = 'Редактирование';
        $data['cardTitle'] = $title;
        $this->addBreadCrumb($title);
        $this->setTitle($title);

        $data['content'] = view($this->tb .'.edit', $vars);
        return $this->main($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'username'=>'required',
            'password'=>'required|min:6'
        ]);
        $data = $request->except(['_method','_token']);

        $data['password']= base64_encode($data['password']);


        $partner = PartnerUser::findOrFail($id);
        $partner->fill($data)->save();
        if ($request->has('createOpen')) {
            return redirect(route($this->tb . '.edit', $partner->id))->with('success', 'Партнер успешно изменен!');
        }

        return redirect(route($this->tb . '.index'))->with('message', 'Партнер успешно изменен!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $status = ResponseHelper::ERROR_KEY;
        $message = __('generic.error_deleting');
        if ($delete =  PartnerUser::findOrFail($id)) {
            if ($delete->delete()) {
                $message = __('generic.successfully_deleted');
                $status = ResponseHelper::SUCCESS_KEY;
            }
        }

        return redirect(route($this->tb.'.index'))->with([$status => $message]);
    }
}
