<?php

namespace App\Http\Controllers\Admin;

use App\Permission;
use App\Role;
use App\Traits\Authorizable;
use Illuminate\Http\Request;

class RoleController extends AdminController
{
    use Authorizable;
    protected $name = 'Роли';

    public function __construct()
    {
        parent::__construct();
        $this->addBreadCrumb($this->name, route('roles.index'));
    }

    public function index()
    {
        $this->setTitle($this->name);
        $roles = Role::all();
        $permissions = Permission::all();
        $data['content'] = view('admin.role.index', compact('roles', 'permissions'));
        return $this->main($data);
    }

    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required|unique:roles']);

        if( Role::create($request->only('name')) ) {
            flash('Role Added');
        }

        return redirect()->back();
    }

    public function update(Request $request, $id)
    {
        if($role = Role::findOrFail($id)) {
            // admin role has everything
            if($role->name === 'Admin') {
                $role->syncPermissions(Permission::all());
                return redirect()->route('roles.index');
            }

            $permissions = $request->get('permissions', []);
            $role->syncPermissions($permissions);
            flash( $role->name . ' permissions has been updated.');
        } else {
            flash()->error( 'Role with id '. $id .' note found.');
        }

        return redirect()->route('roles.index');
    }
}
