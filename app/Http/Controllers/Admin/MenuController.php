<?php

namespace App\Http\Controllers\Admin;

use App\Models\Language;
use App\Models\Menu;
use App\Models\MenuLang;
use App\Traits\Authorizable;
use Illuminate\Http\Request;

class MenuController extends AdminController
{
	use Authorizable;
	private $name = 'Меню';

	protected $tb;

	public function __construct()
	{
		parent::__construct();
		$this->tb = 'menu';
		$this->addBreadCrumb($this->name, route($this->tb . '.index'));
	}

	/**
	 * @param Request $request
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index(Request $request)
	{
		$data = [];
		$vars['controller'] = $this->tb;
		// Сохраним данные
		$data['cardTitle'] = $this->name;
		$this->setTitle($this->name);
		$vars['list'] = Menu::GetLang($this->langAdmin)->SortOrder()->get();
		if (view()->exists('admin.' . $this->tb . '.index')) {
			$data['content'] = view('admin.' . $this->tb . '.index', $vars);
		}

		return $this->main($data);
	}

	/**
	 * @param Request $request
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 * @throws \Illuminate\Validation\ValidationException
	 */
	public function store(Request $request)
	{
		$this->validate($request, [
			'url' => 'required|unique:menu,url',
		]);
		$message = __('generic.successfully_added_new');

		$input = $request->all();
		$input['active'] = (int)$request->get('active');
		$menu = new Menu();

		if ($menu->fillExisting($input)->save()) {
			/** @var $language  Language */
			foreach ($this->languagesList as $language) {
				$input[$menu->getForeignKey()] = $menu->id;
				$input[$language->getForeignKey()] = $language->id;
				(new MenuLang())->fillExisting($input)->save();
			}
		}
		if ($request->hasFile('photo')) {
			$menu->photo = $this->photoModel
				->setData('photo', $request->file('photo'))
				->setData('table', $this->tb)
				->setData('directory', $this->tb)
				->setData('id', $menu->id)
				->saveImage(false, false);
			$menu->save();
		}
		if ($request->has('createOpen')) {
			return redirect(route($this->tb . '.edit', $menu->id))->with('success', $message);
		}

		return redirect(route($this->tb . '.index'))->with('message', $message);
	}

	public function create()
	{
		$data = [];
		$vars['controller'] = $this->tb;
		$title = 'Создание меню';

		$data['cardTitle'] = $title;
		$this->addBreadCrumb($title);
		if (view()->exists('admin.' . $this->tb . '.create')) {
			$data['content'] = view('admin.' . $this->tb . '.create', $vars);
		}

		return $this->main($data);
	}

	public function update(Request $request, Menu $menu)
	{
		$message = __('generic.update_failed');
		$input = $request->all();
		$input['active'] = (int)$request->get('active');
		$menu->fillExisting($input);
		if ($menu->save()) {
			$message = __('generic.successfully_updated');
			$menu->langItem($this->langAdmin)->first()->fillExisting($input)->save();
			if ($request->hasFile('photo')) {
				$menu->photo = $this->photoModel
					->setData('photo', $request->file('photo'))
					->setData('table', $this->tb)
					->setData('directory', $this->tb)
					->setData('id', $menu->id)
					->saveImage(false, false);
				$menu->save();
			}
		}
		if ($request->has('saveClose')) {
			return redirect(route($this->tb . '.index'))->with(['success' => $message]);
		}

		return redirect()->back()->with(['success' => $message]);
	}

	/**
	 * @param Request $request
	 * @param         $id
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function edit(Request $request, $id)
	{
		$data = [];
		$vars['edit'] = Menu::GetLang($this->langAdmin)->findOrFail($id);
		$vars['controller'] = $vars['photo_table'] = $this->tb;
		$title = 'Пункт меню ' . $vars['edit']['name'];
		$this->addBreadCrumb($title);
		$data['cardTitle'] = $title;
		$this->setTitle($title);
		if (view()->exists('admin.' . $this->tb . '.edit')) {
			$data['content'] = view('admin.' . $this->tb . '.edit', $vars);
		}

		return $this->main($data);
	}

	public function destroy(Request $request, Menu $menu)
	{

	}
}
