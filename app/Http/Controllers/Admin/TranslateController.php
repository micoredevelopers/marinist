<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ResponseHelper;
use App\Models\Language;
use App\Models\Translate;
use App\Models\TranslateLang;
use Illuminate\Http\Request;

class TranslateController extends AdminController
{
    private $name = 'Локализация';
    protected $tb;

    public function __construct()
    {
        parent::__construct();
        $this->tb = 'translate';
        $this->tbId = $this->tb . '_id';
        $this->tbLang = $this->tb . '_lang';
        $this->selfModel = new Translate();
        $this->selfModelLang = new TranslateLang();
        $this->addBreadCrumb($this->name, route($this->tb . '.index'));
		$this->addScripts([
			'js/lib/select2/select2.full.min.js',
			'js/lib/select2/ru.js',
		]);
		$this->addCss([
			'css/lib/select2.min.css',
		]);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $data = [];
        $vars['controller'] = $this->tb;

        $vars['list'] = Translate::GetLang($this->langAdmin)->latest()->get();
        $vars['groups'] = Translate::getGroups();

		$data['cardTitle'] = $this->name;
		$this->setTitle($this->name);
        if (view()->exists('admin.translate.index')) {
            $data['content'] = view('admin.translate.index', $vars);
        }

        return $this->main($data);
    }

    public function create()
    {
		$vars['groups'] = Translate::getGroups();
        $this->addBreadCrumb('Создание перевода');
		$vars['controller'] = $this->tb;
        $data['content'] = view('admin.translate.add')->with($vars);
        return $this->main($data);
    }

    public function update(Request $request)
    {
    	$status = ResponseHelper::ERROR_KEY;
        $message = __('generic.update_failed');
        if ($request->has('translate')) {
        	$ids = array_keys($request->get('translate'));
            $message = __('generic.successfully_updated');
			$status = ResponseHelper::SUCCESS_KEY;
            $translates = Translate::with('langItem')->find($ids);
            $translateRequest = $request->get('translate');
            foreach ($translates as $translate) {
				$id = $translate->id;
            	$data = \Arr::get($translateRequest, $id);
				$translate->fillExisting($data)->save();
				$translate->langItem->fillExisting($data)->save();
            }
        }
        return redirect(route($this->tb . '.index'))->with([$status => $message]);

    }

    public function ckeditor(Request $request){

        $translates = Translate::where('id',$request->get('id'))->first();

        if ($translates->isCKeditor()){
            $translates->fillExisting(['type'=>'textarea'])->save();

            return redirect()->back()->with('success','Добавлен CKEditor');
        }else if($translates->isTextarea()) {
            $translates->fillExisting(['type' =>'ckeditor'])->save();

            return redirect()->back()->with('success','Добавлен CKEditor');
        }
        return redirect('',500);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function store(Request $request)
    {
        $message = 'Запись не была создана, какая то ошибка';
        $data = $request->all();
		$translate = new Translate();
        if ($translate->fillExisting($data)->save()) {
            $message = 'Запись успешно создана';
			$data[$translate->getForeignKey()] = $translate->id;
            foreach ($this->languagesList as $lang) {
				/*** @var $lang Language */
				$data[$lang->getForeignKey()] = $lang->id;
				(new TranslateLang())->fillExisting($data)->save();
            }
        }

        return redirect(route('translate.index'))->with('message', $message);
    }
}
