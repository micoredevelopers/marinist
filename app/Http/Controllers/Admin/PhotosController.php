<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ResponseHelper;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use App\Models\Admin\Photo;

class PhotosController extends AdminController
{
	public function delete(Request $request)
	{
		$res[ResponseHelper::STATUS_KEY] = ResponseHelper::ERROR_KEY;
		$res[ResponseHelper::MESSAGE_KEY] = 'Переданны не все обязательные данные';
		$photoId = $request->id ?: false;
		$table = $request->table ?: false;
		$primaryId = $request->primary_id ?: false;
		if ($photoId AND \Schema::hasTable($table)) {
			$isDeleted = (new Photo())->deletePhoto($photoId, $table, $primaryId);
			if ($isDeleted) {
				$res[ResponseHelper::STATUS_KEY] = ResponseHelper::SUCCESS_KEY;
				$res[ResponseHelper::MESSAGE_KEY] = 'Изображение успешно удалено';
			}
		}
		return Response::create($res);
	}

	public function edit(Request $request)
	{
		$res[ResponseHelper::STATUS_KEY] = ResponseHelper::ERROR_KEY;
		$res[ResponseHelper::MESSAGE_KEY] = 'Изображение не было отредактировано, произошла ошибка';
		($photo = new Photo())
			->setData('photo', $request->get('base64'))
			->setData('table', $request->get('table'))
			->setData('id', $request->get('id'));
		if ($request->has('primary_id')) {
			$imagePath = $photo
				->setData('primary_id', $request->get('primary_id'))
				->saveAdditionalImageFromBase64();
		} else {
			$imagePath = $photo
				->setData('directory', $request->get('table'))
				->saveImageFromBase64();
		}
		if ($imagePath !== false) {
			$res['photo'] = $imagePath;
			$photo->updateImagePath($request, $imagePath);
			$res[ResponseHelper::STATUS_KEY] = ResponseHelper::SUCCESS_KEY;
			$res[ResponseHelper::MESSAGE_KEY] = 'Изображение успешно отредактировано';
		}
		return $res;
	}

	public function getPhotoCropper(Request $request)
	{
		$fail = [
			ResponseHelper::MESSAGE_KEY =>"Фото недоступно для редактирования",
			ResponseHelper::STATUS_KEY => ResponseHelper::ERROR_KEY,
		];
		$data = [];
		$this->dropListScripts();
		$this->addScripts([
			'js/admin/jquery-croppper-init.js',
			'js/lib/cropperjs/cropper.min.js',
			'js/lib/cropperjs/jquery-cropper.js',
		]);
		$this->addCss('css/cropperjs/cropper.css');
		$vars['styles'] = $this->getStylesString();
		$vars['scripts'] = $this->getScriptsString($this->getScripts());
		if ($request->has(['table', 'id'])) {
			if (!\Schema::hasTable($request->table) OR !$request->id) {
				return $fail;
			}
			$table = $request->has('primary_id') ? $request->get('table') . '_photo' : $request->get('table');
			$vars['photo'] = DB::table($table)->where('id', $request->get('id'))->first();
			if ($vars['photo']) {
				$vars['photo_table'] = $request->get('table');
				if ($request->has('primary_id')) $vars['primary_id'] = $request->get('primary_id');
				$data['content'] = view('admin.editPhotos', $vars)->render();
			}
		}
		return $data;
	}
}
