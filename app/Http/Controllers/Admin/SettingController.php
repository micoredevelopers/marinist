<?php

namespace App\Http\Controllers\Admin;

use App\ContentTypes\Checkbox;
use App\ContentTypes\Coordinates;
use App\ContentTypes\File;
use App\ContentTypes\MultipleImage;
use App\ContentTypes\SelectMultiple;
use App\ContentTypes\Text;
use App\ContentTypes\Timestamp;
use App\ContentTypes\Image as ContentImage;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Libs;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use App\ContentTypes\Password;

class SettingController extends AdminController
{
    private $name = 'Настройки';

    public function __construct()
    {
        parent::__construct();
        $this->tb = 'settings';
        $this->selfModel = new Setting();
        $this->addBreadCrumb($this->name, route($this->tb . '.index'));
        $this->setTitle('Настройки');
    }

    public function index()
    {
        $settings = Setting::getSettingsList();
        $groups = $settings->where('group')->pluck('group')->unique();
        $data['cardTitle'] = $this->name;

        $this->addScripts([
            'js/lib/select2/select2.full.min.js',
            'js/lib/select2/ru.js',
            'https://cdnjs.cloudflare.com/ajax/libs/ace/1.4.4/ace.js',
        ]);
        $this->addCss([
            'css/lib/select2.min.css',
        ]);
        $active = (request()->session()->has('setting_tab')) ? request()->session()->get('setting_tab') : old('setting_tab', key($settings));
        $data['content'] = view('admin.settings.index', compact('settings', 'groups', 'active'));

        return $this->main($data);
    }

    public function store(Request $request)
    {
        $key = $request->input('key');
        if (!Setting::isStaff($key)) {
            if ($request->has('group')) {
                $key = implode('.', [\Str::slug($request->input('group')), $key]);
            }
        }
        $key_check = Setting::where('key', $key)->get()->count();
        if ($key_check > 0) {
            return back()->with([
                'message' => __('settings.key_already_exists', ['key' => $key]),
                'alert-type' => 'error',
            ]);
        }
        $lastSetting = Setting::SortOrder()->first();
        $request->merge(['sort' => 0]);
        $request->merge(['value' => '']);
        $request->merge(['key' => $key]);
        Setting::create($request->except('setting_tab'));
        request()->flashOnly('setting_tab');

        return back()->with([
            'message' => __('settings.successfully_created'),
            'alert-type' => 'success',
        ]);
    }

    public function update(Request $request)
    {

        // Check permission
        $settings = Setting::all();
        foreach ($settings as $setting) {
            $content = $this->getContentBasedOnType($request, 'setting', (object)[
                'type' => $setting->type,
                'field' => str_replace('.', '_', $setting->key),
                'details' => $setting->details,
                'group' => $setting->group,
            ], json_decode($setting->details));

            if ($setting->type == 'image' && $content == null) {
                continue;
            }
            if (($setting->isTypeFile() OR $setting->isTypeFileMultiple()) && $content == json_encode([])) {
                continue;
            }
            $key = preg_replace('/^' . \Str::slug($setting->group) . './i', '', $setting->key);
            if ($request->has(str_replace('.', '_', $setting->key) . '_group')) {
                $setting->group = $request->input(str_replace('.', '_', $setting->key) . '_group');
            }
            $setting->value = $content;
            $setting->save();
        }
        request()->flashOnly('setting_tab');

        return back()->with([
            'message' => __('settings.successfully_saved'),
            'alert-type' => 'success',
        ]);
    }

    public function destroy(Setting $setting)
    {
        $this->delete_value($setting->id);
        $setting->delete();
        request()->session()->flash('setting_tab', $setting->group);

        return back()
            ->with([
                'message' => __('settings.successfully_deleted'),
                'alert-type' => 'success',
            ]);
    }

    public function move_up($id)
    {
        // Check permission
        $setting = Setting::find($id);
        // Check permission
        $swapOrder = $setting->sort;
        $previousSetting = model('Setting')
            ->where('order', '<', $swapOrder)
            ->where('group', $setting->group)
            ->orderBy('sort', 'DESC')->first();
        $data = [
            'message' => __('settings.already_at_top'),
            'alert-type' => 'error',
        ];
        if (isset($previousSetting->sort)) {
            $setting->sort = $previousSetting->sort;
            $setting->save();
            $previousSetting->sort = $swapOrder;
            $previousSetting->save();
            $data = [
                'message' => __('settings.moved_order_up', ['name' => $setting->display_name]),
                'alert-type' => 'success',
            ];
        }
        request()->session()->flash('setting_tab', $setting->group);

        return back()->with($data);
    }

    public function delete_value($id)
    {
        $setting = Setting::find($id);
        // Check permission
        if (isset($setting->id)) {
            // If the type is an image... Then delete it
            $deleteable = [
                'image',
                'file',
            ];
            if (in_array($setting->type, $deleteable)) {
                $files = [];
                if ($setting->type == 'image') {
                    $image = normalizePath($setting->value);
                    $files[] = $image;
                    $details = json_decode($setting->details);
                    if (isset($details->thumbnails)) {
                        $ext = getExtensionByName($setting->value);
                        $original = getFilenameWithoutExtension($image);
                        foreach ($details->thumbnails as $thumbnail) {
                            $files[] = $original . '-' . $thumbnail->name . '.' . $ext;
                        }
                    }
                } elseif ($setting->type == 'file') {
                    $files[] = \Arr::get(json_decode($setting->value, true)[0], 'download_link');
                }
                foreach ($files as $file) {
                    if (\Storage::disk()->exists($file)) {
                        \Storage::disk()->delete($file);
                    }
                }
            }
            $setting->value = '';
            $setting->save();
        }
        request()->session()->flash('setting_tab', $setting->group);

        return back()->with([
            'message' => __('settings.successfully_removed', ['name' => $setting->display_name]),
            'alert-type' => 'success',
        ]);
    }

    public function move_down($id)
    {
        // Check permission
        $setting = Setting::find($id);
        // Check permission
        $swapOrder = $setting->sort;
        $previousSetting = Setting::model('Setting')
            ->where('order', '>', $swapOrder)
            ->where('group', $setting->group)
            ->orderBy('sort', 'ASC')->first();
        $data = [
            'message' => __('settings.already_at_bottom'),
            'alert-type' => 'error',
        ];
        if (isset($previousSetting->sort)) {
            $setting->sort = $previousSetting->sort;
            $setting->save();
            $previousSetting->sort = $swapOrder;
            $previousSetting->save();
            $data = [
                'message' => __('settings.moved_order_down', ['name' => $setting->display_name]),
                'alert-type' => 'success',
            ];
        }
        request()->session()->flash('setting_tab', $setting->group);

        return back()->with($data);
    }

    public function action(Request $request, $action)
    {
        if ($action === 'watermark') {
            return $this->watermarkAction($request);
        }
    }

    public function watermarkAction($request)
    {
        $vars = $data = [];
        if ($request->isMethod('post')) {
            $input = $request->except('_token');
            $value = json_encode($input);
            $this->settings['_watermark'] = $value;
            $this->selfModel->where('key', '_watermark')->update(['value' => $value]);
        }
        $vars['controller'] = $this->tb;
        $vars['editable'] = [
            'cats' => 'Коты главное фото',
            'cats_photo' => 'Коты доп фото',
            'wallpapers' => 'Обои главное фото',
            'wallpapers_photo' => 'Обои доп фото',
            'news' => 'Новости главное фото',
            'news_photo' => 'Новости доп фото',
            'available' => 'Наличие главное фото',
            'sliders' => 'Слайдер главное фото',
            'sliders_photo' => 'Слайдер доп фото',
        ];
        $vars['config'] = json_decode($this->settings['_watermark'], true);
        $this->addBreadCrumb('Водный знак');
        if (view()->exists('admin.settings.watermark')) {
            $data['content'] = view('admin.settings.watermark', $vars);
        }

        return $this->main($data);
    }

    public function getContentBasedOnType(Request $request, $slug, $row, $options = null)
    {
        switch ($row->type) {
            case 'password':
                return (new Password($request, $slug, $row, $options))->handle();
            case 'checkbox':
                return (new Checkbox($request, $slug, $row, $options))->handle();
            case 'file':
                return (new File($request, $slug, $row, $options))->handle();
            case 'file_multiple':
                return (new File($request, $slug, $row, $options))->handle();
            case 'multiple_images':
                return (new MultipleImage($request, $slug, $row, $options))->handle();
            case 'select_multiple':
                return (new SelectMultiple($request, $slug, $row, $options))->handle();
            case 'image':
                return (new ContentImage($request, $slug, $row, $options))->handle();
            case 'timestamp':
                return (new Timestamp($request, $slug, $row, $options))->handle();
            case 'coordinates':
                return (new Coordinates($request, $slug, $row, $options))->handle();
            default:
                return (new Text($request, $slug, $row, $options))->handle();
        }
    }
}
