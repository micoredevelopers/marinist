<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\AdminController;
use App\Http\Libs;
use App\Models\Slider;
use App\Models\SliderLang;
use Illuminate\Http\Request;

class SliderController extends AdminController
{
	private $name = 'Слайдеры';
	protected $tb;

	public function __construct()
	{
		parent::__construct();
		$this->tb = 'sliders';
		$this->tbId = $this->tb . '_id';
		$this->tbLang = $this->tb . '_lang';
		$this->tb_photos = $this->tb . '_photo';
		$this->addBreadCrumb($this->name, route($this->tb . '.index'));
	}

	/**
	 * @param Request $request
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index()
	{
		$data = [];
		$vars['controller'] = $this->tb;
		$title = $this->name;
		$data['cardTitle'] = $title;
		$this->setTitle($title);
		$vars['list'] = Slider::GetLang($this->langAdmin)->paginate();
		if (view()->exists('admin.' . $this->tb . '.index')) {
			$data['content'] = view('admin.' . $this->tb . '.index', $vars);
		}

		return $this->main($data);
	}

	/**
	 * @param Request $request
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
	 */
	public function create()
	{
		$data = [];
		$vars['controller'] = $this->tb;
		$title = 'Создание слайдера';
		$data['cardTitle'] = $title;
		$this->addBreadCrumb($title);
		$this->setTitle($title);
		if (view()->exists('admin.' . $this->tb . '.create')) {
			$data['content'] = view('admin.' . $this->tb . '.create', $vars);
		}

		return $this->main($data);
	}

	public function store(Request $request)
	{
		$message = __('generic.successfully_added_new');
		$langArr = $request->only(['name', 'description']);
		if (($slider = new Slider())->fillExisting(['active' => 1])->save()) {
			foreach ($this->languagesList as $language) {
				/** @var $language \App\Models\Language */
				$langArr[$slider->getForeignKey()] = $slider->id;
				$langArr[$language->getForeignKey()] = $language->id;
				(new SliderLang())->fillExisting($langArr)->save();
			}
		}
		if ($request->has('createOpen')) {
			return redirect(route($this->tb . '.edit', $slider->id))->with('success', $message);
		}

		return redirect(route($this->tb . '.index'))->with('message', $message);
	}

	/**
	 * @param Request $request
	 * @param         $id
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function edit(Request $request, $id)
	{
		$data = [];
		$vars['edit'] = Slider::GetLang($this->langAdmin)->find($id);
		if (!$vars['edit']) {
			return redirect(route($this->tb . '.index'))->with('message', Libs::alertMessage('Такой записи не существует!', 2));
		}
		$vars['photo_table'] = $this->tb;
		$vars['photosList'] = $vars['edit']->photos()->orderBy('sort', 'asc')->get();
		$title = 'Редактирование ' . $vars['edit']['name'];
		$data['cardTitle'] = $title;
		$this->addBreadCrumb($title);
		$this->setTitle($title);
		if (view()->exists('admin.' . $this->tb . '.edit')) {
			$data['content'] = view('admin.' . $this->tb . '.edit', $vars);
		}

		return $this->main($data);
	}

	public function update(Request $request, Slider $slider)
	{
		$message = __('generic.update_failed');
		$input = $request->except(['photo', '_token']);
		$input['photo'] = $slider['photo'];
		$input['active'] = (int)$request->get('active');
		if ($request->hasFile('photo')) {
			$input['photo'] = $this->photoModel
				->setData('photo', $request->file('photo'))
				->setData('table', $this->tb)
				->setData('directory', $this->tb)
				->setData('id', $slider->id)
				->saveImage();
		}
		$slider->fillExisting($input);
		if ($slider->save()) {
			$message = __('generic.successfully_updated');
			$slider->langItem($this->langAdmin)->first()->fillExisting($input)->save();
		}
		$this->photoModel->saveAdditionPhotos($slider, $slider->id);
		if ($request->has('saveClose')) {
			return redirect(route($this->tb . '.index'))->with(['success' => $message]);
		}

		return redirect()->back()->with(['success' => $message]);
	}
}