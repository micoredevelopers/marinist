<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ResponseHelper;
use App\Models\Floor;
use App\Models\ProgressGallery;
use App\Models\ProgressSteps;
use App\Models\ProgressStepsLang;
use App\Models\Section;
use App\Traits\Authorizable;
use Illuminate\Http\Request;

class FloorController extends AdminController
{
	use Authorizable;
	private $name = 'Этаж';

	protected $selfModelLang;
	protected $selfModel;
	protected $key;
	protected $table;

	public function __construct()
	{
		parent::__construct();
		$this->key = 'floor';
		$this->table = 'floor';
		$this->selfModel = new Floor();
		$this->setTitle($this->name);
	}

	public function update(Request $request, Floor $floor)
	{
		$message = __('generic.update_failed');
		$input = $request->except(['_token', '_method']);
		//

		$images = [
			'plan',
			'substrate',
			'backlight',
		];
		foreach ($images as $image) {
			if (!$request->hasFile($image)) {
				continue;
			}
			$input[$image] = $this->photoModel
				->setData('table', $this->table)
				->setData('directory', $this->table)
				->setData('id', $floor->id . '_' . $image)
				->setData('photo', $request->file($image))
				->saveImage(false, false);
		}
		$floor->fillExisting($input);
		if ($floor->save()) {
			$message = __('generic.successfully_updated');
		}

//		if ($request->has('saveClose')) {
//			return redirect(route('floor.index'))->with(['success' => $message]);
//		}

		return redirect()->back()->with(['success' => $message]);
	}

	public function store(Request $request)
	{
		$section_id = (int)$request->get('section_id');
		$section = Section::find($section_id);
		if (!$section_id AND !$section) {
			return redirect()->back()->with(['error' => 'Не передана секция']);
		}

		($floor = new Floor())->section()->associate($section)->save();
		return redirect()->back()->with(['success' => 'Запись создана']);
	}

	public function edit(Floor $floor)
	{
		$title = concatWords(
			$this->name, $floor->getAttribute('number'),
			'Секция', $floor->section->number
			);
		$this->addBreadCrumb($title, '');

		$data['content'] = view('admin.floor.edit')->with(['edit' => $floor]);
		return $this->main($data);
	}

	/**
	 * @param Floor $floor
	 * @return \Illuminate\Http\RedirectResponse
	 * @throws \Exception
	 */
	public function destroy(Floor $floor)
	{
		$status = ResponseHelper::ERROR_KEY;
		$message = __('generic.error_deleting');
		if ($floor) {
			if ($floor->delete()) {
				$message = __('generic.successfully_deleted');
				$status = ResponseHelper::SUCCESS_KEY;
			}
		}

		return redirect()->back()->with([$status => $message]);
	}
}
