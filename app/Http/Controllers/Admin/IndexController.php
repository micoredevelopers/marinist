<?php

namespace App\Http\Controllers\Admin;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class IndexController extends AdminController
{
    public function index(Request $request)
    {
        $data['meta'] = 'Админ панель';
        $data['cardTitle'] = 'Дашборд';
        $data['content']=view('admin.index.index');

        return $this->main($data);
    }

    public function clearCache()
    {
        Artisan::call('cache:clear');
        return redirect()->back()->with('success','Кеш успешно очищен!');
    }
    public function makeSitemap()
    {
        Artisan::call('sitemap:generate '.env('APP_URL'));
        return redirect()->back()->with('success','Карта сайта успешно обновлена!');
    }
}
