<?php

namespace App\Http\Controllers\Admin;

use App\Models\Section;
use App\Traits\Authorizable;
use Illuminate\Http\Request;

class SectionController extends AdminController
{
	use Authorizable;
	private $name = 'Секция';

	protected $selfModelLang;
	protected $selfModel;
	protected $key;
	protected $table;

	public function __construct()
	{
		parent::__construct();
		$this->key = 'section';
		$this->table = 'section';
		$this->setTitle($this->name);
	}

	public function update(Request $request, Section $section)
	{
		$message = __('generic.update_failed');
		$input = $request->except(['_token', '_method']);
		//
		if ($request->hasFile('photo')) {
			$input['photo'] = $this->photoModel
				->setData('photo', $request->file('photo'))
				->setData('table', $this->table)
				->setData('directory', $this->table)
				->setData('id', $section->id)
				->saveImage(false, false);
		}
		$section->fillExisting($input);
		if ($section->save()) {
			$message = __('generic.successfully_updated');
		}

//		if ($request->has('saveClose')) {
//			return redirect(route('section.index'))->with(['success' => $message]);
//		}

		return redirect()->back()->with(['success' => $message]);
	}

	public function edit(Section $section)
	{
		$this->addBreadCrumb($this->name . ' ' . $section->getAttribute('number'), '');
		$data['content'] = view('admin.section.edit')->with([
			'edit' => $section,
		]);
		return $this->main($data);
	}
}
