<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ResponseHelper;
use App\Models\Admin\Photo;
use App\Models\ProgressGallery;
use App\Models\ProgressSteps;
use App\Models\ProgressStepsLang;
use App\Traits\Authorizable;
use Illuminate\Http\Request;

class ProgressController extends AdminController
{
	use Authorizable;
	private $name = 'Ход строительства';

	protected $key;
	protected $table;

	public function __construct()
	{
		parent::__construct();
		$this->key = 'progress';
		$this->table = 'progress_steps';
		$this->addBreadCrumb($this->name, route($this->key . '.index'));
	}

	/**
	 * @param Request $request
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index()
	{
		$data = [];
		$vars['controller'] = $this->key;
		$vars['table'] = $this->table;
		$vars['list'] = ProgressSteps::GetLang($this->langAdmin)->SortOrder()->get();
		$vars['galleries'] = ProgressGallery::GetLang($this->langAdmin)->SortOrder()->get();

		$data['cardTitle'] = $this->name;
		$this->setTitle($this->name);
		if (view()->exists('admin.' . $this->key . '.index')) {
			$data['content'] = view('admin.progress.index', $vars);
		}

		return $this->main($data);
	}

	public function update(Request $request)
	{
	    if(!is_null($request->photo))
        {
            foreach ($request->photo as $key => $photo){
                    $photoPath = (new Photo())
                        ->setData('photo', $photo)
                        ->setData('table', 'progress_steps')
                        ->setData('directory', 'progress_steps')
                        ->setData('id', $key)
                        ->saveImage();
                ProgressSteps::where('id',$key)->first()->update(['photo' => $photoPath]);
            }
        }
		$message = __('generic.update_failed');
		$status = ResponseHelper::ERROR_KEY;
		$input = $request->except('_token');

		if ($request->has('progress')) {
			$message = __('generic.successfully_updated');
			$status = ResponseHelper::SUCCESS_KEY;
			foreach ($request->get('progress') as $progress) {
				$step = ProgressSteps::find($progress['id']);
				if (!$step) continue;
				$step->fillExisting($progress)->save();
				$step->langItem($this->langAdmin)->first()->fillExisting($progress)->save();
			}
		}

		return back()->with([$status => $message]);

	}
}
