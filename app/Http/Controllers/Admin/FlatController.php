<?php

namespace App\Http\Controllers\Admin;

use App\Contracts\HasMenuContent;
use App\Helpers\ResponseHelper;
use App\Models\ContentLang;
use App\Models\Flat;
use App\Models\FlatStatus;
use App\Models\Floor;
use App\Models\Section;
use App\Traits\Authorizable;
use Illuminate\Http\Request;

class FlatController extends AdminController implements HasMenuContent
{
	use Authorizable;
	private $name = 'Квартиры';

	protected $selfModelLang;
	protected $selfModel;
	protected $key;
	protected $table;

	public function __construct()
	{
		parent::__construct();
		$this->key = 'flat';
		$this->table = 'flat';
		$this->selfModel = new Floor();
		$this->addBreadCrumb($this->name, route($this->key . '.index'));
	}

	public function index()
	{
		$data['cardTitle'] = $this->name;
		$this->setTitle($this->name);
		$sections = Section::all()->load('floors.flats');
		$statuses = FlatStatus::all();
		$data['content'] = view('admin.flat.index')->with(compact('sections','statuses'));
		return $this->main($data);
	}

	public function store(Request $request)
	{
		$floor_id = (int)$request->get('floor_id');
		$floor = Floor::find($floor_id);
		if (!$floor_id AND !$floor){
			return redirect()->back()->with(['error' => 'Не передан этаж!']);
		}

		($flat = new Flat())->floor()->associate($floor)->save();
		return redirect()->back()->with(['success' => 'Запись создана']);
	}


	public function update(Request $request, Flat $flat)
	{
		$message = __('generic.update_failed');
		$input = $request->except(['_token', '_method']);
		$input['active'] = (int)$request->get('active');
		$input['stock'] = (int)$request->get('stock');
		$input['sea_view'] = (int)$request->get('sea_view');
		$input['terrace'] = (int)$request->get('terrace');
		//
		if ($request->hasFile('photo')) {
			$input['photo'] = $this->photoModel
				->setData('photo', $request->file('photo'))
				->setData('table', $this->table)
				->setData('directory', $this->table)
				->setData('id', $flat->id)
				->saveImage(false, false);
		}
		$flat->fillExisting($input);
		if ($flat->save()) {
			$message = __('generic.successfully_updated');
		}

		if ($request->has('saveClose')) {
			return redirect(route('flat.index'))->with(['success' => $message]);
		}

		return redirect()->back()->with(['success' => $message]);
	}

	public function edit(Flat $flat)
	{
		$flat->load('floor.section');
		$words = [$flat->number, 'Квартира'];
		if ($floor = $flat->floor) {
			$words[] = '[';
			$words[] = $floor->number;
			$words[] = 'этаж';
			if ($section = $floor->section) {
				$words[] = $section->number;
				$words[] = 'секция';
			}
			$words[] = ']';
		}
		$title = implode(' ', $words);
		$this->setTitle($title);
		$data['cardTitle'] = $title;
		$this->addBreadCrumb($title);
        $statuses = FlatStatus::all();
		$data['content'] = view('admin.flat.edit',compact('statuses'))->with([
			'edit' => $flat,
		]);
		return $this->main($data);
	}

	public function getMenuContent(): string
	{
		$settings['ul'][0] = 'drop-block';
		$sections = Section::all()->load('floors.flats');
		$content = view('admin.flat.menu')->with(compact('sections', 'settings'));
		return $content;
	}

	/**
	 * @param Flat $flat
	 * @return \Illuminate\Http\RedirectResponse
	 * @throws \Exception
	 */
	public function destroy(Flat $flat)
	{
		$status = ResponseHelper::ERROR_KEY;
		$message = __('generic.error_deleting');
		if ($flat) {
			if ($flat->delete()) {
				$message = __('generic.successfully_deleted');
				$status = ResponseHelper::SUCCESS_KEY;
			}
		}

		return redirect()->back()->with([$status => $message]);
	}

}
