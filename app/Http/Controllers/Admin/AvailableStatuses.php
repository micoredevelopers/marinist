<?php

namespace App\Http\Controllers\Admin;

use App\Http\Libs;
use App\Models\Model;
use Illuminate\Http\Request;

class AvailableStatuses extends AdminController
{
	private $name = 'Статусы наличия котов';

	public function __construct()
	{
		parent::__construct();
		$this->tb = 'available_statuses';
		$this->tbId = $this->tb . '_id';
		$this->tbLang = $this->tb . '_lang';
		$this->selfModel = new Model([], $this->tb);
		$this->selfModelLang = new Model([], $this->tbLang);
		$this->selfModelLang->setTimestamps(false);
		$this->addBreadCrumb($this->name, route($this->tb));
	}

	public function execute(Request $request)
	{
		$data = [];
		$vars['controller'] = $this->tb;
		// Сохраним данные
		if ($request->isMethod('post')) {
			$successMessage = 'Данные сохранены';
			$input = $request->except('_token');
			if ($count = count($input['id'])) {
				foreach ($input['id'] as $id) {
					// Saving main table data
					$mainArr = [
						'is_available' => isset($input['is_available'][ $id ]) ? (int)$input['is_available'][ $id ] : 0,
						'color_text' => isset($input['color_text'][ $id ]) ? $input['color_text'][ $id ] : null,
						'color_background' => isset($input['color_background'][ $id ]) ? $input['color_background'][ $id ] : null,
					];
					$this->selfModel->whereId($id)->update($mainArr);
					//Saving language table data
					$langArr = ['name' => $input['name'][ $id ]];
					$this->selfModelLang->where($this->tbId, $id )
						->Language($this->langAdmin)->update($langArr);
					$vars['message'] = Libs::alertMessage($successMessage, 1);
				}
			}
		}

		$vars['list'] = $this->selfModel->GetLang($this->langAdmin)->orderBy('id', 'desc')->get();
		if (view()->exists('admin.' . $this->tb . '.list')) {
			$data['content'] = view('admin.' . $this->tb . '.list', $vars);
		}

		return $this->Index($data);
	}

	public function add()
	{
		$message = Libs::alertMessage('Запись не была создана, какая то блин ошибка', 0);

		if ($this->selfModel->save()) {
			$message = Libs::alertMessage('Запись успешно создана', 1);
			foreach ($this->languagesList as $lang) {
				$langArr[ $this->tbId ] = $this->selfModel->id;
				$langArr['language_id'] = $lang->id;
				$this->selfModelLang->insert($langArr);
			}
		}

		return redirect(route($this->tb))->with('message', $message);
	}
}
