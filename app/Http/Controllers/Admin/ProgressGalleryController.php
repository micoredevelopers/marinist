<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ResponseHelper;
use App\Models\Admin\Photo;
use App\Models\ProgressGallery;
use App\Models\ProgressGalleryLang;
use Illuminate\Http\Request;

class ProgressGalleryController extends AdminController
{
	private $name = 'Ход строительства';

	protected $key;
	protected $table;

	public function __construct()
	{
		parent::__construct();
		$this->key = 'progress.galleries';
		$this->table = 'progress_galleries';
		$this->addBreadCrumb('Статус работ', route('progress.index'));
	}

	private function getThumbnailSizes(){
		return [
			getSetting($this->key . '.image.width', 400),
			getSetting($this->key. '.height', 267)
		];
	}

	public function index()
	{
		$this->setTitle($this->name);
		$data['cardTitle'] = $this->name;
		$vars['table'] = $vars['controller'] = $this->table;
		$vars['list'] = ProgressGallery::latest()->GetLang($this->langAdmin)->paginate();
		$data['content'] = view('admin.' . $this->key . '.index', $vars);
		return $this->main($data);
	}

	/**
	 * @param Request $request
	 * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function create()
	{
		$data = [];
		$vars['controller'] = $this->table;
		$title = 'Создание галлереи хода строительства';
		$data['cardTitle'] = $title;
		$this->addBreadCrumb($title);
		$this->setTitle($title);
		$data['content'] = view('admin.progress.galleries.create');
		return $this->main($data);
	}

	public function store(Request $request)
	{
		$message = '';
		$input = $request->except('photo');
		$input['active'] = (int)$request->get('active');
		if (($gallery = new ProgressGallery())->fillExisting($input)->save()) {
			$message = __('generic.successfully_added_new');
			$input[$gallery->getForeignKey()] = $gallery->id;
			foreach ($this->languagesList as $language) {
				/** @var $language \App\Models\Language */
				$input[$language->getForeignKey()] = $language->id;
				(new ProgressGalleryLang())->fillExisting($input)->save();
			}
			if ($request->hasFile('photo')) {
				$gallery->photo = (new Photo())
					->setData('photo', $request->file('photo'))
					->setData('table', $this->table)
					->setData('directory', $this->table)
					->setData('id', $gallery->id)
					->saveImage(...$this->getThumbnailSizes());
				$gallery->save();
			}
		}
		if ($request->has('createOpen')) {
			return redirect(route($this->key . '.edit', $gallery->id))->with('success', $message);
		}

		return redirect(route('progress.index'))->with('message', $message);
	}

	/**
	 * @param $id
	 * @param Request $request
	 * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
	 */
	public function edit($id, Request $request)
	{
		$vars['edit'] = ProgressGallery::GetLang($this->langAdmin)->findOrFail($id);
		$vars['photo_table'] = $this->table;
		$vars['photosList'] = $vars['edit']->photos()->SortOrder()->get();
		$title = 'Редактирование галлереи ' . $vars['edit']['name'];
		$data['cardTitle'] = $title;
		$this->addBreadCrumb($title);
		$this->setTitle($title);

		$data['content'] = view('admin.progress.galleries.edit', $vars);
		return $this->main($data);
	}

	public function update(Request $request, ProgressGallery $gallery)
	{
		$message = __('generic.update_failed');
		$input = $request->except('_token', 'photo');
		$langArr = $request->only(['name', 'description', 'sub_description']);
		$this->validate($request, [
			'name' => 'max:255',
		]);
		$input['active'] = (int)$request->get('active');
		//
		if ($request->hasFile('photo')) {
			$input['photo'] = $this->photoModel
				->setData('photo', $request->file('photo'))
				->setData('table', $this->table)
				->setData('directory', $this->table)
				->setData('id', $gallery->id)
				->saveImage(...$this->getThumbnailSizes());
		}
		$this->photoModel->saveAdditionPhotos($gallery, $gallery->id);
		if ($gallery->fill($input)->save()) {
			$gallery->langItem($this->langAdmin)->first()->fillExisting($langArr)->save();
			$message = __('generic.successfully_updated');
		}

		if ($request->has('saveClose')) {
			return redirect(route( 'progress.index'))->with(['success' => $message]);
		}

		return redirect()->back()->with(['success' => $message]);
	}

	public function destroy(ProgressGallery $gallery)
	{
		$status = ResponseHelper::ERROR_KEY;
		$message = __('generic.error_deleting');
		if ($gallery->delete()) {
			$message = __('generic.successfully_deleted');
			$status = ResponseHelper::SUCCESS_KEY;
		}

		return redirect(route('galleries.index'))->with([$status => $message]);
	}

}
