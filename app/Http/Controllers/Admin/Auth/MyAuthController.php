<?php

namespace App\Http\Controllers\Admin\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class MyAuthController extends Controller
{
    protected $redirectTo = '/';

    //  protected $username = 'name';
    public function showLogin()
    {
        return view('admin.auth.login');
    }

    public function authenticate(Request $request)
    {
        $this->validate(
        	$request,
			[
				'email' => 'required|email|exists:users,email',
				'password' => 'required',
			]
		);
		$array = $request->all();

        //если пользователь активировал чекбокс
         $remember = (bool)$request->has('remember');
        //выполняет аутентификацию пользователя
        //[] - 1) поле для поиска пользователя
        //$remember - второй аргуметнт функции, если активирован флаг
        if (Auth::attempt([
            'email' => $array['email'],
            'password' => $array['password'],
        ], $remember)) {
            //если результат true
            //intended() - перенаправляет на url, на который хотел попасть пользователь
            //если пути не существует передаем запасной путь
            return redirect()->intended(route('admin.index'));
        }
        //withInput() - сохраняет в сессию ранее внесенные данные, перечисляем какие поля сохраняем...без пароля
        //withErrors() - вывод ошибок
        return redirect()->back()
            ->withInput($request->only('email'))
            ->withErrors(['email' => 'Данные аутентификации не верны']);

    }


    public function logout(Request $request)
    {
        Auth::logout();
        return redirect()->route('admin.login');
    }
}
