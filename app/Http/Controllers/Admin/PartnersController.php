<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ResponseHelper;
use App\Models\Admin\Photo;
use App\Models\Partner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class PartnersController extends AdminController
{
    private $name = 'Партнеры';

    protected $tb;

    public function __construct()
    {
        parent::__construct();
        $this->tb = 'admin.partners';
        $this->addBreadCrumb($this->name, route( $this->tb . '.index'));
    }

    public function index()
    {
        $this->setTitle($this->name);
        $data['cardTitle'] = $this->name;
        $vars['controller'] = $this->tb;
        $vars['list'] = Partner::all();
        $data['content'] = view( $this->tb . '.index', $vars);
        return $this->main($data);
    }

    public function create()
    {

        $data =[];
        $data['cardTitle'] = 'Создание партнера';
        $data['content'] = view( $this->tb . '.create');
        return $this->main($data);
    }


    public function store(Request $request)
    {
        $request->validate([
            'photo'=>'required',
            'rating'=>'required|min:1|max:100'
        ]);
        $partner = new Partner();
        $partner->fillExisting($request->all());
        $partner->save();

        if ($request->hasFile('photo')) {
            $photoPath = (new Photo())
                ->setData('photo', $request->file('photo'))
                ->setData('table', 'partners')
                ->setData('directory', 'partners')
                ->setData('id', $partner->id)
                ->saveImage();
            $partner->update(['photo' => $photoPath]);
        }

        if ($request->has('createOpen')) {
            return redirect(route($this->tb . '.edit', $partner->id))->with('success', 'Запись успешно создана!');
        }

        return redirect(route($this->tb . '.index'))->with('message', 'Запись успешно создана!');
    }




    /**
     * @param $id
     * @param Request $request
     * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function edit($id)
    {

        $vars = $data = [];
        $storage = Storage::disk('local');
        $vars['edit'] = Partner::findOrFail($id);
        $vars['photo_table'] = $this->tb;
        $vars['edit']['photoExists'] = $storage->exists('public/' . $vars['edit']['photo']);
        $title = 'Редактирование';
        $data['cardTitle'] = $title;
        $this->addBreadCrumb($title);
        $this->setTitle($title);

        $data['content'] = view('admin.partners.edit', $vars);
        return $this->main($data);
    }

    public function update(Request $request, Partner $partner)
    {
        $message = __('generic.update_failed');
        $input = $request->except('_token','photo');


        //
        if ($request->hasFile('photo')) {
            $input['photo'] = $this->photoModel->setData('photo', $request->file('photo'))
                ->setData('table', 'partners')
                ->setData('directory', 'partners')
                ->setData('id', $partner->id)
                ->saveImage();
        }

        $partner->fillExisting($input);
        if ($partner->save()) {
            $message = __('generic.successfully_updated');
        }

        if ($request->has('saveClose')) {
            return redirect(route($this->tb . '.index'))->with(['success' => $message]);
        }

        return redirect()->back()->with(['success' => $message]);
    }

    public function destroy($id)
    {

        $status = ResponseHelper::ERROR_KEY;
        $message = __('generic.error_deleting');
        if ($delete = Partner::find($id)) {
            if ($delete->delete()) {
                $message = __('generic.successfully_deleted');
                $status = ResponseHelper::SUCCESS_KEY;
            }
        }

        return redirect(route($this->tb.'.index'))->with([$status => $message]);
    }
}
