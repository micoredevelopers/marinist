<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ResponseHelper;
use App\Models\Admin\Photo;
use App\Models\Language;
use App\Models\Page;
use App\Models\PageLang;
use App\Models\ProgressGallery;
use App\Models\ProgressGalleryLang;
use App\Traits\Authorizable;
use Illuminate\Http\Request;

class PageController extends AdminController
{
	use Authorizable;
	private $name = 'Страницы';

	protected $key;
	protected $table;

	public function __construct()
	{
		parent::__construct();
		$this->key = 'pages';
		$this->table = 'pages';
		$this->addBreadCrumb('Страницы', route('pages.index'));
	}

	private function getThumbnailSizes(){
		return [
			getSetting($this->key . '.image.width', 400),
			getSetting($this->key. '.height', 267)
		];
	}

	public function index()
	{
		$this->setTitle($this->name);
		$data['cardTitle'] = $this->name;
		$vars['table'] = $vars['controller'] = $this->table;
		$vars['list'] = Page::latest()->GetLang($this->langAdmin)->paginate();
		$data['content'] = view('admin.pages.index', $vars);
		return $this->main($data);
	}

	/**
	 * @param Request $request
	 * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function create()
	{
		$data = [];
		$title = 'Создание страницы';
		$data['cardTitle'] = $title;
		$this->addBreadCrumb($title);
		$this->setTitle($title);
		$data['content'] = view('admin.pages.create');
		return $this->main($data);
	}

	public function store(Request $request)
	{
		$this->validate($request, [
			'url' => 'unique:pages,url'
		]);
		$message = '';
		$status = 'error';
		$input = $request->all();
		$input['active'] = (int)$request->get('active');
		if (($page = new Page())->fillExisting($input)->save()) {
			$status = 'success';
			$message = __('generic.successfully_added_new');
			$input[$page->getForeignKey()] = $page->id;
			foreach ($this->languagesList as $lang) {
				/** @var $lang Language */
				$input[$lang->getForeignKey()] = $lang->id;
				($pageLang = new PageLang())->fillExisting($input)->save();
			}
			if ($request->hasFile('photo')) {
				$page->photo = (new Photo())
					->setData('photo', $request->file('photo'))
					->setData('table', $this->table)
					->setData('directory', $this->table)
					->setData('id', $page->id)
					->saveImage(...$this->getThumbnailSizes());
				$page->save();
			}
		}

		$redirectTo = $request->has('createOpen')
			? route('pages.edit', $page->id)
			: route('pages.index');

		return redirect($redirectTo)->with($status, $message);
	}

	public function show(Page $page)
	{
		return redirect(route('pages.edit', $page->id));
	}

	/**
	 * @param Page $page
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function edit(Page $page)
	{
		$page->lang =  $page->langItem($this->langAdmin)->first();
		$vars['edit'] = $page;
		$vars['photo_table'] = $this->table;
//		$vars['photosList'] = $vars['edit']->photos()->SortOrder()->get();
		$title = 'Редактирование страницы ' . $vars['edit']['name'];
		$data['cardTitle'] = $title;
		$this->addBreadCrumb($title);
		$this->setTitle($title);

		$data['content'] = view('admin.pages.edit', $vars);
		return $this->main($data);
	}

	public function update(Request $request, Page $page)
	{
		$message = __('generic.update_failed');
		$input = $request->except('_token');
		$langArr = $request->only(['name', 'description', 'sub_description']);
		$this->validate($request, [
			'name' => 'max:255',
		]);
		$input['active'] = (int)$request->get('active');
		//
		if ($request->hasFile('photo')) {
			$input['photo'] = $this->photoModel
				->setData('photo', $request->file('photo'))
				->setData('table', $this->table)
				->setData('directory', $this->table)
				->setData('id', $page->id)
				->saveImage(...$this->getThumbnailSizes());
		}
		$this->photoModel->saveAdditionPhotos($page, $page->id);
		$page->fill($input);
		if ($page->save()) {
			$page->langItem($this->langAdmin)->first()->fill($langArr)->save();
			$message = __('generic.successfully_updated');
		}

		if ($request->has('saveClose')) {
			return redirect(route( 'progress.index'))->with(['success' => $message]);
		}

		return redirect()->back()->with(['success' => $message]);
	}

	public function destroy(ProgressGallery $page)
	{
		$status = ResponseHelper::ERROR_KEY;
		$message = __('generic.error_deleting');
		if ($page->delete()) {
			$message = __('generic.successfully_deleted');
			$status = ResponseHelper::SUCCESS_KEY;
		}

		return redirect(route('galleries.index'))->with([$status => $message]);
	}

}
