<?php

namespace App\Http\Controllers\Admin;

use App\Http\Libs;
use App\Models\Infoblock;
use App\Models\InfoblocksLang;
use Illuminate\Http\Request;

class InfoblockController extends AdminController
{
	private $name = 'Инфоблоки';

	protected $selfModelLang;
	protected $selfModel;
	protected $tb;

	public function __construct()
	{
		parent::__construct();
		$this->tb = 'infoblocks';
		$this->tbId = $this->tb . '_id';
		$this->tbLang = $this->tb . '_lang';
		$this->selfModel = new Infoblock();
		$this->selfModelLang = new InfoblocksLang();
		$this->addBreadCrumb($this->name, route($this->tb . '.index'));
	}

	/**
	 * @param Request $request
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index(Request $request)
	{
		$data = [];
		$vars['controller'] = $this->tb;
		// Сохраним данные
		$data['cardTitle'] = $this->name;
		$vars['list'] = $this->selfModel->GetLang($this->langAdmin)->paginate();
		if (view()->exists('admin.' . $this->tb . '.index')) {
			$data['content'] = view('admin.' . $this->tb . '.index', $vars);
		}

		return $this->main($data);
	}

	/**
	 * @param Request $request
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
	 */
	public function store(Request $request)
	{

		$message = __('generic.successfully_added_new');
		$langArr = $request->only(['name', 'description', 'sub_description']);
		if (($infoblock = new Infoblock())->fillExisting(['active' => 1])->save()) {
			foreach ($this->languagesList as $language) {
				/** @var $language \App\Models\Language */
				$langArr[$infoblock->getForeignKey()] = $infoblock->id;
				$langArr[$language->getForeignKey()] = $language->id;
				(new InfoblocksLang())->fillExisting($langArr)->save();
			}
		}
		if ($request->has('createOpen')) {
			return redirect(route($this->tb . '.edit', $infoblock->id))->with('success', $message);
		}

		return redirect(route($this->tb . '.index'))->with('message', $message);
	}

	public function create()
	{
		$data = [];
		$vars['controller'] = $this->tb;
		$title = 'Создание инфоблока';

		$data['cardTitle'] = $title;
		$this->addBreadCrumb($title);
		if (view()->exists('admin.' . $this->tb . '.create')) {
			$data['content'] = view('admin.' . $this->tb . '.create', $vars);
		}

		return $this->main($data);
	}

	public function update(Request $request, Infoblock $infoblock)
	{
		$message = __('generic.update_failed');
		$input = $request->except(['_token']);
		$input['active'] = (int)$request->get('active');
		$infoblock->fillExisting($input);
		if ($infoblock->save()) {
			$message = __('generic.successfully_updated');
			$infoblock->langItem($this->langAdmin)->first()->fillExisting($input)->save();
		}
		if ($request->has('saveClose')) {
			return redirect(route($this->tb . '.index'))->with(['success' => $message]);
		}

		return redirect()->back()->with(['success' => $message]);
	}

	/**
	 * @param Request $request
	 * @param         $id
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function edit(Request $request, $id)
	{
		$data = [];
		//Оперируем с сохранением формы
		$vars['edit'] = $this->selfModel->GetLang($this->langAdmin)->findOrFail($id);
		$title = 'Инфоблок ' . $vars['edit']['name'];
		$this->addBreadCrumb($title);
		$data['cardTitle'] = $title;
		$this->setTitle($title);
		if (view()->exists('admin.' . $this->tb . '.edit')) {
			$data['content'] = view('admin.' . $this->tb . '.edit', $vars);
		}

		return $this->main($data);
	}
}
