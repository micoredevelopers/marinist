<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ResponseHelper;
use App\Http\Libs;
use App\Models\Meta;
use App\Models\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Route;

class MetaController extends AdminController
{
	private $name = 'SEO';

	public function __construct()
	{
		parent::__construct();
		$this->tb = 'meta';
		$this->tbId = $this->tb . '_id';
		$this->tbLang = $this->tb . '_lang';
		$this->tb_photos = $this->tb . '_photo';
		$this->selfModel = new Meta();
		$this->selfModelLang = new Model([], $this->tbLang);
		$this->selfModelLang->setTimestamps(false);
		$this->addBreadCrumb($this->name, route($this->tb . '.index'));
	}

	public function index()
	{
		$this->setTitle($this->name);
		$data = [];
		$vars['table'] = $vars['controller'] = $this->tb;
		$vars['list'] = $this->selfModel->GetLang($this->langAdmin)->latest()->paginate();
		$data['cardTitle'] = $this->name;
		$data['content'] = view('admin.meta.index', $vars);

		return $this->main($data);
	}

	/**
	 * @param Request $request
	 *
	 * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function create(Request $request)
	{
		$title = 'Создание мета данных';
		$this->setTitle($title);
		$data = [];
		$vars['controller'] = $this->tb;
		$this->addBreadCrumb($title);
		$data['content'] = view('admin.meta.create');
		$data['cardTitle'] = $title;

		return $this->main($data);
	}

	public function store(Request $request)
	{
		$url = getUrlWithoutHost(getNonLocaledUrl($request->get('url')));
		$request->merge(['url' => $url]);
		$this->validate($request, [
			'url' => 'unique:meta,url|max:160',
		]);
		$input = $request->only(['active', 'url']);
		$langArr = $request->only([
			'h1',
			'meta_title',
			'meta_keywords',
			'meta_description',
			'text_top',
			'text_bottom',
		]);
		$input['active'] = (int)$request->get('active');
		$this->validate($request, [
			'url' => 'max:160|unique:' . $this->tb . ',url',
		]);
		$message = __('generic.successfully_added_new');
		if ($this->selfModel->fill($input)->save()) {
			foreach ($this->languagesList as $lang) {
				$langArr[$this->tbId] = $this->selfModel->id;
				$langArr['language_id'] = $lang->id;
				$this->selfModelLang->insert($langArr);
			}
		}
		if ($request->has('createOpen')) {
			return redirect(route($this->tb . '.edit', $this->selfModel->id))->with('success', $message);
		}

		return redirect(route($this->tb . '.index'))->with('message', $message);
	}

	/**
	 * @param         $id
	 * @param Request $request
	 *
	 * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
	 */
	public function edit($id)
	{
		$data = [];
		$edit = $this->selfModel->GetLang($this->langAdmin)->findOrFail($id);
		if ($edit) {
			$data['cardTitle'] = $title = 'Редактирование "' . $edit->url . '"';
			$this->setTitle($title);
			$this->addBreadCrumb($title);
		}
		$data['content'] = view('admin.meta.edit', compact('edit'));

		return $this->main($data);
	}

	public function update(Request $request, $id)
	{
		$message = __('generic.update_failed');
		$meta = Meta::findOrFail($id);
		$input = $request->except('_token');
		$langArr = $request->only([
			'h1',
			'meta_title',
			'meta_keywords',
			'meta_description',
			'text_top',
			'text_bottom',
		]);
		$input['active'] = (int)$request->get('active');
		//
		$meta->fill($input);
		if ($meta->update(['id' => $id])) {
			$this->selfModelLang->where($this->tbId, $id)->Language($this->langAdmin)->update($langArr);
			$message = __('generic.successfully_updated');
		}
		if ($request->has('saveClose')) {
			return redirect(route($this->tb . '.index'))->with(['message' => $message]);
		}

		return redirect()->back()->with(['message' => $message]);
	}

	public function destroy($id)
	{
		$status = ResponseHelper::ERROR_KEY;
		$message = __('generic.error_deleting');
		if ($delete = Meta::find($id)) {
			if ($delete->delete()) {
				$message = __('generic.successfully_deleted');
				$status = ResponseHelper::SUCCESS_KEY;
			}
		}

		return redirect(route('meta.index'))->with([$status => $message]);
	}
}
