<?php

	namespace App\Http\Controllers\Admin;

	use App\Http\Requests\UserProfileRequest;
	use App\Permission;
	use App\Role;
	use App\Traits\Authorizable;
	use App\User;
	use Illuminate\Http\Request;
	use \Auth;

	class UserController extends AdminController {
		use Authorizable;

		public function __construct() {
			parent::__construct();
			$this->tb = 'users';
			$this->addBreadCrumb('Пользователи', route('users.index'));
		}

		public function index() {
			$title = 'Пользователи';
			$data['cardTitle'] = $title;
			$this->addBreadCrumb($title);
			$this->setTitle($title);
			$result = User::latest()->NotSuperAdmin()->paginate(30);
			$data['content'] = view('admin.user.index', compact('result'));

			return $this->main($data);
		}

		public function create() {
			$title = 'Создание нового пользователя';
			$data['cardTitle'] = $title;
			$this->setTitle($title);
			$roles = Role::pluck('name', 'id');
			$data['content'] = view('admin.user.create', compact('roles'));

			return $this->main($data);
		}

		public function store(Request $request) {
			$this->validate($request, [
				'name'     => 'bail|required|min:2',
				'email'    => 'required|email|unique:users',
				'password' => 'required|min:6',
			]);
			$message = __('generic.successfully_added_new');
			// hash password
			$request->merge([ 'password' => bcrypt($request->get('password')) ]);
			// Create the user
			if ( $user = User::create($request->except('roles', 'permissions')) ) {
				$this->syncPermissions($request, $user);
			}
			if ( $request->has('createOpen') ) {
				return redirect(route($this->tb . '.edit', $user->id))->with('success', $message);
			}

			return redirect(route($this->tb . '.index'))->with('message', $message);
		}

		public function edit($id) {
			$user = User::whereId($id)->NotSuperAdmin()->firstOrFail();
			$title = 'Редактирование ' . $user->name;
			$data['cardTitle'] = $title;
			$this->addBreadCrumb($title);
			$this->setTitle($title);
			$roles = Role::pluck('name', 'id');
			$permissions = Permission::all('name', 'id');
			$data['content'] = view('admin.user.edit', compact('user', 'roles', 'permissions'));

			return $this->main($data);
		}

		public function update(Request $request, $id) {
			$message = __('generic.successfully_updated');
			$this->validate($request, [
				'name'  => 'bail|required|min:2',
				'email' => 'required|email|unique:users,email,' . $id,
			]);
			// Get the user
			$user = User::whereId($id)->NotSuperAdmin()->firstOrFail();
			// Update user
			$user->fill($request->except('roles', 'permissions', 'password'));
			// check for password change
			if ( $request->get('password') ) {
				$user->password = bcrypt($request->get('password'));
			}
			// Handle the user roles
			$this->syncPermissions($request, $user);
			$user->save();

			if ( $request->has('saveClose') ) {
				return redirect(route($this->tb . '.index'))->with([ 'success' => $message ]);
			}

			return redirect()->back()->with(['success' => $message]);
		}

		public function destroy($id) {
			if ( Auth::user()->id == $id ) {
				flash()->warning('Deletion of currently logged in user is not allowed :(')->important();

				return redirect()->back();
			}
			if ( User::whereId($id)->NotSuperAdmin()->firstOrFail()->delete() ) {
				flash()->success('User has been deleted');
			} else {
				flash()->success('User not deleted');
			}

			return redirect()->back();
		}

		private function syncPermissions(Request $request, $user) {
			// Get the submitted roles
			$roles = $request->get('roles', []);
			$permissions = $request->get('permissions', []);
			// Get the roles
			$roles = Role::find($roles);
			// check for current role changes
			if ( ! $user->hasAllRoles($roles) ) {
				// reset all direct permissions for user
				$user->permissions()->sync([]);
			} else {
				// handle permissions
				$user->syncPermissions($permissions);
			}
			$user->syncRoles($roles);

			return $user;
		}

		protected function dropCache() {
			\Artisan::call('permission:cache-reset');
		}

		public function profile()
		{
			$this->dropLastBreadCrumb();
			$title = 'Редактирование профиля';
			$this->addBreadCrumb($title);
			$this->setTitle( $title );
			$data['cardTitle'] = $title;
			$data['content'] =  view('admin.user.profile', [
				'user' => \Illuminate\Support\Facades\Auth::user()
			]);
			return $this->main($data);
		}

		public function profileUpdate(UserProfileRequest $request)
		{
			$user = \Auth::user();
			if(\Hash::check($request->password, $user->password)){
				$user->password = \Hash::make($request->password_new);
				$user->name = $request->get('name');
				$user->save();
				return redirect()->back()->with('success', 'Пароль успешно обновлен');
			} else {
				$error = 'Пожалуйста, введите корректный текущий пароль';
				return redirect()->back()->with('error', $error);
			}
		}
	}
