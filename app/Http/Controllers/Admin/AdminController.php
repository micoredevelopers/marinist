<?php

namespace App\Http\Controllers\Admin;

use App\Contracts\HasMenuContent;
use App\Http\Controllers\BaseController;
use App\Models\Admin\Admin_menu;
use App\Models\Admin\Photo;
use App\Models\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;

class AdminController extends BaseController
{
	protected $langAdmin = 1;
	protected $data = [];

	protected $photoModel;
	protected $languagesList;
	protected $languages;

	public function __construct()
	{
        
		parent::__construct();
		$this->languages = new Language();
		$this->languagesList = Language::all();
		$this->checkLanguage();
		$this->photoModel = new Photo();
		$this->checkForStylesAndScripts();
		$this->addBreadCrumb('Админ панель', route('admin.index'));
	}

	public function main($data)
	{
		$this->setTitle(' - ' . getSetting('global.sitename') . ' - Админ панель');
		$data['menu'] = $this->getNestedMenu();
		$data['styles'] = $this->getStylesString();
		$data['scripts'] = $this->getScriptsString($this->getScripts());
		$data['scriptsDefer'] = $this->getScriptsString($this->getScripts(true), 'defer');
		$data['sections'] = [];
		$data['languages'] = Language::Active()->get();
		if (!\Arr::has($data, 'breadcrumbs')) {
			$data['breadcrumbs'] = $this->getBreadCrumbs();
		}
		//Получаем секции дочерних шаблонов, поскольку шаблоны не наследуют друг друга
		if (\Arr::get($data, 'content') AND ($data['content'] instanceof \Illuminate\View\View)) {
			$data['sections'] = $data['content']->renderSections();
		}

		return view('admin.index', $data);
	}

	protected function checkLanguage()
	{
		Session::put(['langAdmin' => getLang()]);
		$this->langAdmin = getLang();
	}

	public function checkForStylesAndScripts()
	{
		$styles = [
			'css/lib/jquery.fancybox.min.css',
			'css/bootstrap-select.css',
			'css/admin/styles.css',
		];
		$jsLibs = [
			'js/lib/bootstrap-filestyle.min.js',
			'js/lib/bootstrap-select.js',
			'js/lib/Sortable.min.js',
			'js/lib/jquery.fancybox.min.js',
		];
		$scripts = [
			'js/ckeditor/ckeditor.js',
			'js/globals.js',
			'js/functions.js',
			'js/functions-admin.js',
		];
		$this->addCss($styles);
		$this->addScripts($jsLibs);
		$this->addScripts($scripts);
		$this->addScriptsDefer([
			'js/admin-triggers.js',
			'js/admin-init.js',
			'js/init.js',
			'js/admin.js',
		]);

		return $this;
	}

	protected function getNestedMenu()
	{
		$menus = objectKeyBy(Admin_menu::Active(1)->ParentMenu()->SortOrder()->get());
//		if (count($menus)) {
//			foreach ($menus as &$menu) {
//				$nested = Admin_menu::where('parent_id', $menu->id)->Active(1)->get();
//				if (count($nested)) {
//					$menu->childrens = containObjectInHisId($nested);
//				}
//			}
//		}
		$menus = $this->checkMenuValid($menus);
		$menus = $this->getMenuSubContent($menus);

		return $menus;
	}

	private function checkMenuValid($menus)
	{
		foreach ($menus as $num => &$menu) {
			$valid = 1;
			if (!$menu->url) {
				$valid = 0;
			}
			if ($menu->gate_rule AND \Gate::denies($menu->gate_rule)) {
				$valid = 0;
			}
			if (isMenuActiveByUrl($menu->url)) {
				$menu->current = 1;
			}
			if (!$valid) unset($menus[$num]);
		}
		return $menus;
	}

	private function getMenuSubContent($menus)
	{
		foreach ($menus as $num => &$menu) {
			$menu->content = $this->getMenuContentProvider($menu);
		}
		return $menus;
	}

	private function getMenuContentProvider(Admin_menu $menu)
	{
		$content = '';
		if ($menu->content_provider) {
			$class = $menu->content_provider;
			try {
				$interface = HasMenuContent::class;
				if (class_exists($class) AND classImplementsInterface($class, $interface)) {
					$instance = new $class;
					$content = $instance->getMenuContent();
				}
			} catch (\Error $e) {
				//
			}
		}
		return $content;
	}

	public function upload(Request $request)
	{
		$fullFilename = null;
		$resizeWidth = 1800;
		$resizeHeight = null;
		$slug = $request->input('type_slug');
		$file = $request->file('image');
		$path = $slug . '/' . date('F') . date('Y') . '/';
		$filename = basename($file->getClientOriginalName(), '.' . $file->getClientOriginalExtension());
		$filename_counter = 1;
		// Make sure the filename does not exist, if it does make sure to add a number to the end 1, 2, 3, etc...
		while (Storage::disk()->exists($path . $filename . '.' . $file->getClientOriginalExtension())) {
			$filename = basename($file->getClientOriginalName(), '.' . $file->getClientOriginalExtension()) . (string)($filename_counter++);
		}
		$fullPath = $path . $filename . '.' . $file->getClientOriginalExtension();
		$ext = $file->guessClientExtension();
		if (in_array($ext, ['jpeg', 'jpg', 'png', 'gif'])) {
			$image = Image::make($file)
				->resize($resizeWidth, $resizeHeight, function (Constraint $constraint) {
					$constraint->aspectRatio();
					$constraint->upsize();
				})
				->encode($file->getClientOriginalExtension(), 75);
			// move uploaded file from temp to uploads directory
			if (Storage::disk()->put($fullPath, (string)$image, 'public')) {
				$status = __('media.success_uploading');
				$fullFilename = $fullPath;
			} else {
				$status = __('media.error_uploading');
			}
		} else {
			$status = __('media.uploading_wrong_type');
		}

		// echo out script that TinyMCE can handle and update the image in the editor
		return "<script> parent.helpers.setImageValue('" . image($fullFilename) . "'); </script>";
	}
}
