<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SeoController extends AdminController
{
    private $name = 'Партнеры';

    protected $tb;

    public function __construct()
    {
        parent::__construct();
        $this->tb = 'seo';
        $this->addBreadCrumb($this->name, route( 'admin.'.$this->tb . '.index'));
    }

    public function index(){
        $vars['list'] = [
            [
                'name' => 'Карта сайта',
                'url' => ''
            ],
            [
                'name' => 'Robots.txt',
                'url' => route('admin.seo.robots.index')
            ]

        ];
        $data['cardTitle'] = $this->name;
        $data['content'] = view('admin.' . $this->tb . '.index',$vars);
        return $this->main($data);
    }

    public function robots(){
        $vars = [];
        $vars['data'] = file_get_contents('robots.txt');
        $data['cardTitle'] = $this->name;
        $data['content'] = view('admin.seo.robots', $vars);
        return $this->main($data);
    }

    public function robotsUpdate(Request $request)
    {
        $robots = $request->get('robots');
        file_put_contents('robots.txt',$robots);

        if ($request->has('saveClose')) {
            return redirect(route('admin.seo.index'))->with(['success' => 'robots.txt успешно изменен!']);
        }

        return redirect()->back()->with(['success' => 'robots.txt успешно изменен!']);
    }
}
