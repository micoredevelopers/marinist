<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ResponseHelper;
use App\Models\Admin\Photo;
use App\Models\GalleriesLang;
use App\Models\Gallery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class GalleryController extends AdminController
{
	private $name = 'Галлерея';

	protected $tb;

	public function __construct()
	{
		parent::__construct();
		$this->tb = 'galleries';
		$this->tbId = $this->tb . '_id';
		$this->tbLang = $this->tb . '_lang';
		$this->tb_photos = $this->tb . '_photo';
		$this->addBreadCrumb($this->name, route($this->tb . '.index'));
	}

	public function index()
	{
		$this->setTitle($this->name);
		$data['cardTitle'] = $this->name;
		$vars['table'] = $vars['controller'] = $this->tb;
		$vars['list'] = Gallery::latest()->GetLang($this->langAdmin)->paginate();
		$data['content'] = view('admin.' . $this->tb . '.index', $vars);
		return $this->main($data);
	}

	/**
	 * @param Request $request
	 * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function create()
	{
		$data = [];
		$vars['controller'] = $this->tb;
		$title = 'Создание галлереи';
		$data['cardTitle'] = $title;
		$this->addBreadCrumb($title);
		$this->setTitle($title);
		$data['content'] = view('admin.galleries.create');
		return $this->main($data);
	}

	public function store(Request $request)
	{
		$message = '';
		if (!$request->get('url')) $request->merge(['url' => \Str::slug($request->get('name'))]);
		$input = $request->except('_token');
		$input['active'] = (int)$request->get('active');
		if (isset($input['photo'])) unset($input['photo']);
		$this->validate($request, [
			'name' => 'max:255',
			'url'  => 'unique:galleries,url',
		]);
		if (($gallery = new Gallery())->fillExisting($input)->save()) {
			$message = __('generic.successfully_added_new');
			$input[$gallery->getForeignKey()] = $gallery->id;
			foreach ($this->languagesList as $language) {
				/** @var $language \App\Models\Language */
				$input[$language->getForeignKey()] = $language->id;
				(new GalleriesLang())->fillExisting($input)->save();
			}
			//сохранить в базе имя фото если есть в реквесте
			if ($request->hasFile('photo')) {
				$photoPath = (new Photo())
					->setData('photo', $request->file('photo'))
					->setData('table', $this->tb)
					->setData('directory', $this->tb)
					->setData('id', $gallery->id)
					->saveImage();
				$gallery->update(['photo' => $photoPath]);
			}
		}
		if ($request->has('createOpen')) {
			return redirect(route($this->tb . '.edit', $gallery->id))->with('success', $message);
		}

		return redirect(route($this->tb . '.index'))->with('message', $message);
	}

	/**
	 * @param $id
	 * @param Request $request
	 * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
	 */
	public function edit($id, Request $request)
	{
		$vars = $data = [];
		$storage = Storage::disk('local');
		$vars['edit'] = Gallery::GetLang($this->langAdmin)->findOrFail($id);
		$vars['photo_table'] = $this->tb;
		$vars['photosList'] = $vars['edit']->photos()->SortOrder()->get();
		$vars['edit']['photoExists'] = $storage->exists('public/' . $vars['edit']['photo']);
		$title = 'Редактирование ' . $vars['edit']['name'];
		$data['cardTitle'] = $title;
		$this->addBreadCrumb($title);
		$this->setTitle($title);

		$data['content'] = view('admin.galleries.edit', $vars);
		return $this->main($data);
	}

	public function update(Request $request, Gallery $gallery)
	{
		$message = __('generic.update_failed');
		$input = $request->except('_token');
		$input['photo'] = $gallery['photo'];
		$this->validate($request, [
			'name' => 'max:255',
		]);
		$input['active'] = (int)$request->get('active');
		//
		if ($request->hasFile('photo')) {
			$input['photo'] = $this->photoModel
				->setData('photo', $request->file('photo'))
				->setData('table', $this->tb)
				->setData('directory', $this->tb)
				->setData('id', $gallery->id)
				->saveImage(false, false);
		}
		$this->photoModel->saveAdditionPhotos($gallery, $gallery->id);
		$gallery->fill($input);
		if ($gallery->save()) {
			$gallery->langItem($this->langAdmin)->first()->fillExisting($input)->save();
			$message = __('generic.successfully_updated');
		}

		if ($request->has('saveClose')) {
			return redirect(route($this->tb . '.index'))->with(['success' => $message]);
		}

		return redirect()->back()->with(['success' => $message]);
	}

	public function destroy($id)
	{
		$status = ResponseHelper::ERROR_KEY;
		$message = __('generic.error_deleting');
		if ($delete = Gallery::find($id)) {
			if ($delete->delete()) {
				$message = __('generic.successfully_deleted');
				$status = ResponseHelper::SUCCESS_KEY;
			}
		}

		return redirect(route('galleries.index'))->with([$status => $message]);
	}
}
