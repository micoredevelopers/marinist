<?php

namespace App\Http\Controllers;

use App\Models\Gallery;
use Illuminate\Http\Request;


class GalleryController extends SiteController
{

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->setTitle(getTranslate('gallery.gallery'));
		$vars = $data = [];
		$vars['galleries'] = Gallery::GetLang($this->lang)->Active(1)->get();
        $this->addBreadCrumb(getTranslate('gallery.gallery'));
        $vars['breadcrumbs'] = $this->getBreadCrumbs();
		$data['content'] = view('public.gallery.index', $vars);
		return $this->main($data);
	}
	public  function  getGalleryData(Request $request)
    {
        $request->validate([
            'galleryId'=>'required'
        ]);

        $id = $request->get('galleryId');
        $gallery = Gallery::with('photos')->GetLang($this->lang)->where('id',$id)->first();
        if(is_null($gallery))
        {
            return response([
                'status'=>'Not found'
            ],404);
        }
        $gallery = $gallery->toArray();
        $response = [
            'name'=>$gallery['name']??'',
            'description'=>$gallery['description']??'',
            'sub_description'=>$gallery['sub_description']??'',
            'videos'=>$gallery['video']??[],
            'photos'=>[]
        ];
        if(count($gallery['photos'])){
            foreach ($gallery['photos'] as $photo){
                array_push($response['photos'],checkImage(imgOrigin($photo['photo'])));
            }
        }
        return response($response,200);
    }
}
