<?php

namespace App\Http\Controllers;

use App\Helpers\FlatParametersHelper;
use App\Models\Filters\FlatFilter;
use App\Models\Flat;
use App\Models\Floor;
use App\Models\Section;
use App\Repositories\FloorsRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;

class FlatController extends SiteController
{

	public function __construct()
	{
		parent::__construct();
	}

	public function index(Request $request)
	{

		$this->setTitle(getTranslate('flat.params.by-params'));
		$sections = Section::select(['*', 'id as section_id'])->get();
		$data['content'] = view('public.flat.index')->with(compact('sections'));
		return $this->main($data);
	}

	public function sections()
	{
		$sections = Section::getAll();
		$sections->map(function ($item) {
			/** @var $item \App\Models\Section */
			$item->addLocalizedData();
		});
		return $sections;
	}

	public function floor(Request $request)
	{

		$res = null;
		if ($floor_id = (int)$request->get('floor_id')) {
			$floor = Floor::with(['flats', 'flats' => function ($query) {
				$query->active(1);
			},
			])->find($floor_id)
			;
			if ($floor) {
				$floor->section->addLocalizedData();
				$section = $floor->section;
				$floor->addLocalizedData($section);
				$floor->flats->map(function ($flat) use ($floor, $section) {
					/** @var $flat Flat */
					$flat->addLocalizedData($section, $floor);
				});
				$res = $floor;

			}
		}


		return $res;
	}

	public function floors(Request $request)
	{
		$url = getUrlWithoutHost(url()->current());
		$sectionId = $request->get('section_id');
		$cacheKey = md5(implode('.', [$url, $sectionId, 'floors']));
		return FloorsRepository::getFloors($cacheKey, $request->get('section_id'));
	}

	public function flats(Request $request)
	{
		$res = [];
		if ($floor_id = (int)$request->get('floor_id')) {
			if ($floor = Floor::find($floor_id)) {
				$section = $floor->section;
				$res = $floor->flats()->active(1)->get();
				$res->map(function ($item) use ($section, $floor) {
					/** @var $item Flat */
					return $item->addLocalizedData($section, $floor);
				});
			}
		}

		return $res;
	}

	public function flat(Request $request)
	{
		$res = [];
		if ($flat_id = (int)$request->get('flat_id')) {
			if ($flat = Flat::with('floor.section')->whereId($flat_id)->active(1)->first()) {

				return $flat->addLocalizedData($flat->floor->section, $flat->floor);
			}
		}
		return $res;
	}

	public function parameters(Request $request, FlatFilter $filter, FlatParametersHelper $flatParametersHelper)
	{
		$this->setTitle(getTranslate('flat.params.by-params'));
		$vars = $data = [];
		$vars['request'] = $request;
		$vars['sections'] = Section::getMinMax('number');
		$vars['floors'] = Floor::getMinMax('number');
		$vars['squares'] = Flat::getMinMax('square');
		$vars['rooms'] = Flat::getMinMax('room');
		$vars['seaView'] = $flatParametersHelper->markSeaView();
		$vars['terrace'] = $flatParametersHelper->markTerrace();
		$vars['stock'] = $flatParametersHelper->markStock();
		$vars['result'] = $this->getParametersList($filter);
		$vars['result']->appends(Input::except('page'));

		$vars['sectionsSelect'] = $request->get('sections') ?? 0;
		$vars['floorsSelect'] = $request->get('floors') ?? 0;
		$vars['squaresSelect'] = $request->get('squares') ?? 0;
		$vars['roomsSelect'] = $request->get('rooms') ?? 0;


		$cacheKey = $request->fullUrl();
		if (!$view = Cache::get($cacheKey) or true) {
			$view = view('public.flat.parameter', $vars)->render();
			Cache::set($cacheKey, $view);
		}
		$data['content'] = $view;

		return $this->main($data);
	}

	private function getParametersList(FlatFilter $filter)
	{
		$select = [
			'flats.*',
			'sections.number as sections_number',
			'floors.number as floors_number',
		];
		/** @var  $result Builder */
		$result = Flat::select($select)
			->active(1)
			->joinParents()
			->filter($filter)
			->orderBy('flats.number')
			->orderBy('sections.number')
			->orderBy('floors.number')
			->orderBy('flats.square')
			->paginate()
		;
		return $result;
	}

	public function getParamsData()
	{

		try {
			$response = ['url' =>
							 [
								 'sections' => route('flat.data.sections'),
								 'floors'   => route('flat.data.floors'),
								 'floor'    => route('flat.data.floor'),
								 'flats'    => route('flat.data.flats'),
								 'flat'     => route('flat.data.flat'),
							 ],
			];
		} catch (\Exception $e) {
			return response([
				'error' => $e->getMessage(),
			], Response::HTTP_UNAUTHORIZED);
		}
		return response($response, Response::HTTP_OK);

	}
}
