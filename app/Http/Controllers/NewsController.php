<?php

namespace App\Http\Controllers;

use App\Models\News;

class NewsController extends SiteController
{
	private $tb = 'news';

	public function __construct()
	{
		parent::__construct();

	}

	public function index()
	{
        $this->addBreadCrumb(getTranslate('news.news'));
		$this->setTitle(getTranslate('modules.news'));
		$data = [];
		$vars['news'] = News::Active(1)->WhereIsPublished()->GetLang($this->lang)->SortOrder()->paginate();
		$vars['breadcrumbs'] = $this->getBreadCrumbs();
		$data['content'] = view('public.news.news', $vars);
		return $this->main($data);
	}

	public function show($url)
	{

		$data = [];
		$news = News::Active(1)->WhereIsPublished()->WhereUrl($url)->GetLang($this->lang)->firstOrFail();
        $this->addBreadCrumb(getTranslate('news.news'),route('news'));
        $this->addBreadCrumb($news->name);
		$news->getPrev();
		$news->getNext();
		$this->setTitle($news->name);
		$vars['news'] = $news;
        $vars['breadcrumbs'] = $this->getBreadCrumbs();
		$data['content'] = view('public.news.single', $vars);
		return $this->main($data);
	}

	public function getMore()
	{
		$result = [];
		$result['content'] = '';
		$news = News::Active(1)->WhereIsPublished()->GetLang($this->lang)->SortOrder()->paginate();
		if ($news->isNotEmpty()) {
			$result['content'] = view('public.news.loop')->with(['news' => $news])->render();
		}
		$result['hasMorePages'] = $news->hasMorePages();
		$result['next'] = $news->nextPageUrl();
		return $result;
	}


}
