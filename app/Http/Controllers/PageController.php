<?php declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends SiteController
{
	public function about()
	{
		$this->setTitle(getTranslate('about.about'));
		$data['content'] = view('public.page.about');

		return $this->main($data);
	}
	
	
	public function curl1($method, $url, $data = null) {

	if ($method == "GET" ) $url .= '?' . http_build_query($data);

	$curlOptions = [
	    CURLOPT_RETURNTRANSFER => true,
	    CURLOPT_HTTPHEADER => ['Accept: application/json'],
	    CURLOPT_URL => $url,
	    CURLOPT_HEADER => false,
		CURLOPT_CONNECTTIMEOUT => 10,
		CURLOPT_TIMEOUT => 20,
		CURLOPT_SSL_VERIFYPEER => 0,
	];

	  //echo $url;

	if ($method == "POST") {

	    $curlOptions[CURLOPT_CUSTOMREQUEST] = 'POST';
	    $curlOptions[CURLOPT_POSTFIELDS] = json_encode($data);
	    $curlOptions[CURLOPT_HTTPHEADER] = array('Content-Type: application/json');
	  }

	  $ch = curl_init();

	  curl_setopt_array($ch, $curlOptions);

	  $result = curl_exec($ch);
	//   $result = false;
	if ($result === false) {
		throw new \Exception('Ошибка curl:'.curl_error($ch));
		return 'Ошибка curl:'.curl_error($ch);
	}
	
	  $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	
	if ($code != 200) {
		throw new \Exception('Код ответа:'.$code);
		return 'Код ответа:'.$code;
	}
	
	  curl_close($ch);

	  return $result;
	}

	public  $getFilterAccesToken = 'snV0hF0VUhvnCqrU136QkbID3DmK5uaavtXxK07Hwj9k1DdC0IdHC6c7mhFArrw0RroRxIaKZzAIHyAELuEYwRt8EX67xxl3PS6eJbGEUtVqIu1O8Q==';
	public function filter()
	{
		$this->setTitle(getTranslate('about.about'));
		$this->addBreadCrumb(getTranslate('flat.by-params'));

		$getME = $this->curl1('GET','https://base-api.marinist.com.ua/v1/apartments', 
		['access_token'=>$this->getFilterAccesToken,
		'option'=> ['project_id'=>4, 'type_object'=>1, 'sale'=>1 ]
	]);

		$vars['curl'] = $getME;
		$vars['styles'] = $this->getStylesString();
		$vars['breadcrumbs'] = $this->getBreadCrumbs();
		$data['content'] = view('public.filter.index', $vars);

		return $this->main($data);
	}

	public function getFlatPdf() {
		$pdf = $this->curl1('GET', 'https://base-api.marinist.com.ua/v1/pdf',  ['access_token' => $this->getFilterAccesToken, 'lang'=>'ua','template'=>'pdf', 'manager'=>1, 'id'=> $_GET['id']]);
		return response()->json($pdf, 200);
	}
	public function singleFlat()
	{
		$this->setTitle(getTranslate('about.about'));
		
		$getME = $this->curl1('GET','https://base-api.marinist.com.ua/v1/apartments', 
			['access_token'=>$this->getFilterAccesToken,
			'option'=> ['project_id'=>4, 'type_object'=>1, 'id' => $_GET['id']]
		]);

		$getME = json_decode($getME);

		$number = $getME->data[0]->number;
		$flatData = $getME->data[0];
		$this->addBreadCrumb(getTranslate('flat.by-params'), 'filter');
		$this->addBreadCrumb('Квартира '.$number);
		// $pdf = json_decode($pdf);
		// $vars['pdf'] = $pdf;
		$vars['flat'] = $flatData;
		$vars['styles'] = $this->getStylesString();
		$vars['breadcrumbs'] = $this->getBreadCrumbs();
		$data['content'] = view('public.single-flat.single-flat', $vars);

		return $this->main($data);
	}

	public function aboutDeveloper()
	{
		$this->setTitle(getTranslate('about-developer.title'));
		$this->addBreadCrumb(getTranslate('about-developer.title'));
		$vars['breadcrumbs'] = $this->getBreadCrumbs();
		$data['content'] = view('public.page.about-developer', $vars);

		return $this->main($data);
	}

	public function contact()
	{
		$this->addBreadCrumb(getTranslate('contact.contact'));
		$this->setTitle(getTranslate('contact.contact'));
		$vars['breadcrumbs'] = $this->getBreadCrumbs();
		$data['content'] = view('public.page.contact', $vars);

		return $this->main($data);
	}

	public function club()
	{
		$with = compact(array_keys(get_defined_vars()));
		$data['content'] = view('public.page.club')->with($with);
		return $this->main($data);
	}
}
