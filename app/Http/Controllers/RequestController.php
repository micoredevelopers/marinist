<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseHelper;
use App\Http\Requests\AbstractRequest;
use App\Http\Requests\ContactRequest;
use App\Http\Requests\FeedbackRequest;
use App\Mail\SignView;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class RequestController extends SiteController
{

	public function signView(ContactRequest $request)
	{
		return $this->request($request);
	}

	private function request(AbstractRequest $request)
	{
		$data = $request->getFillableFields();
		$data['language_id'] = $this->lang;
		$data['referer'] = Str::limit($request->server('HTTP_REFERER'), 900);

		($feedback = new \App\Models\Request())->fillExisting($data)->save();
		Mail::send(new SignView($feedback, $request));

		return [
			ResponseHelper::STATUS_KEY => ResponseHelper::SUCCESS_KEY,
			ResponseHelper::MESSAGE_KEY => getTranslate('request.send-success'),
		];
	}

	public function feedback(FeedbackRequest $request)
	{
		return $this->request($request);
	}
}
