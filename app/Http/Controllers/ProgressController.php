<?php

    namespace App\Http\Controllers;


    use App\Models\ProgressGallery;
    use App\Models\ProgressSteps;
    use Illuminate\Http\Request;


    class ProgressController extends SiteController
    {
        public function __construct()
        {
            parent::__construct();
        }

        public function index()
        {
            $vars = $data = [];
            $vars['steps'] = ProgressSteps::GetLang($this->lang)->SortOrder()->get();
            $vars['galleries'] = ProgressGallery::with('photos')->GetLang($this->lang)->Active(1)->SortOrder()->get();
            $this->addBreadCrumb(getTranslate('progress.steps'));
            $vars['breadcrumbs'] = $this->getBreadCrumbs();
            $data['content'] = view('public.progress.index', $vars);
            return $this->main($data);
        }

        public function getProgressData(Request $request)
        {
            $request->validate([
                'progress' => 'required',
            ]);
            $id = $request->get('progress');
            $data = ProgressGallery::with('photos')->where('id', $id)->GetLang($this->lang)->first()->toArray();
            $response = [
                'title'       => (string)$data['name'],
                'description' => (string)$data['description'],
                'photos'      => [],
            ];
            foreach ($data['photos'] as $photo) {
                if (!storageFileExists(imgOrigin($photo['photo']))){
                    continue;
                }
                array_push($response['photos'], checkImage(imgOrigin($photo['photo'])));
            }

            return response($response, 200);
        }

    }
