<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Redirect;

class SeoMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $url =$request->url();
        $new_url = preg_replace('~///+~', '/', $url);
        if($url != $new_url)
        {
            return redirect($new_url,301);
        }
        if(boolval(strpos($url,'://www.')))
        {
            $replace = str_replace('://www.','://',$url);
            return redirect($replace,301);
        }
        if(boolval(strpos($url,'/index.php')))
        {
            $replace = str_replace('/index.php','',$url);
            return redirect($replace,301);
        }



        return $next($request);
    }
}
