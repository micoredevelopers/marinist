<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
//use Mcamara\LaravelLocalization\LaravelLocalization;


class Language
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //substr($request->server('HTTP_ACCEPT_LANGUAGE'), 0, 2)
        $locale =null;
        if (Session::has('locale')) {
            $locale = getCurrentLocale();
        } else {
            $locale = substr($request->server('HTTP_ACCEPT_LANGUAGE'), 0, 2);

            if ($locale !== 'uk' && $locale !== 'ru') {
                $locale = 'en';
            }
        }

    	$languageList = \App\Models\Language::all();
		if ($locale) {
		    Session::put('locale',$locale);
			$language = $languageList->where('key', $locale)->first();
			if ($language) setLang((int)$language->id);
		}
		if (!getLang()) {
			$language = $languageList->where('default', 1)->firstOrFail();
		} else {
			$language = $languageList->where('id', getLang())->first();
		}
		setLang((int)$language->id);
		if($locale !== getCurrentLocale()){
		    return redirect(langUrl($request->getUri(),$locale));
        }


//		\LaravelLocalization::setLocale('en');
//		dd(getCurrentLocale());

        return $next($request);
    }
}
