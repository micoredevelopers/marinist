<?php

namespace App\Http\Middleware;

use App\Http\Libs;
use App\Role;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class AdminAuthenticated extends Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,...$guards)
    {
        if(!Auth::check()){
            return redirect()->route('admin.login');
        }
        if (!isAdmin() AND !isSuperAdmin()) {
            return redirect()->route('admin.login');
        }

		return $next($request);
    }
}
