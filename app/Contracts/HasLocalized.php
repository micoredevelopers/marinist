<?php
/**
 * Created by PhpStorm.
 * User: aljajazva
 * Date: 2019-05-22
 * Time: 16:48
 */

namespace App\Contracts;


interface HasLocalized
{
	public function langItem(int $language_id);
}