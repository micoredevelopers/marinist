<?php


namespace App\Contracts;


interface GetMinMaxValue
{

    static function getMinMax($name);
}
