<?php

namespace App\Observers;

use App\Models\Translate;

class TranslateObserver
{
    /**
     * Handle the translate "created" event.
     *
     * @param  \App\Models\Translate  $translate
     * @return void
     */
    public function creating(Translate $translate)
    {
        if (!\Arr::get($translate, 'group') AND \Str::contains($translate->key, '.')){
            $parts = explode('.', $translate->key);
            $translate->group = reset($parts);
        }
    }
    public function created(Translate $translate)
    {
        //
    }

    /**
     * Handle the translate "updated" event.
     *
     * @param  \App\Models\Translate  $translate
     * @return void
     */
    public function updated(Translate $translate)
    {
        //
    }

    /**
     * Handle the translate "deleted" event.
     *
     * @param  \App\Models\Translate  $translate
     * @return void
     */
    public function deleted(Translate $translate)
    {
        //
    }

    /**
     * Handle the translate "restored" event.
     *
     * @param  \App\Models\Translate  $translate
     * @return void
     */
    public function restored(Translate $translate)
    {
        //
    }

    /**
     * Handle the translate "force deleted" event.
     *
     * @param  \App\Models\Translate  $translate
     * @return void
     */
    public function forceDeleted(Translate $translate)
    {
        //
    }
}
