<?php

namespace App\Observers;

use App\Models\News;

class NewsObserver
{
    public function creating(News $news)
    {
        if (!$news->url){
            $news->url =  \Str::slug( \Str::random(4));
        }
        if ($new = News::where('url', $news->url)->first()){
            $news->url .= \Str::random(2);
        }
    }
    /**
     * Handle the news "created" event.
     *
     * @param  \App\Models\News  $news
     * @return void
     */
    public function created(News $news)
    {
        //
    }


    public function updating(News $news)
    {
        if (!$news->url){
            $news->url =  \Str::slug( \Str::random(4));
        }

        if ($news->url !== $news->getOriginal('url') AND News::where('url', $news->url)->first()){
            $news->url .= \Str::random(2);
        }
    }

    /**
     * Handle the news "updated" event.
     *
     * @param  \App\Models\News  $news
     * @return void
     */
    public function updated(News $news)
    {

    }

    /**
     * Handle the news "deleted" event.
     *
     * @param  \App\Models\News  $news
     * @return void
     */
    public function deleted(News $news)
    {
        //
    }

    /**
     * Handle the news "restored" event.
     *
     * @param  \App\Models\News  $news
     * @return void
     */
    public function restored(News $news)
    {
        //
    }

    /**
     * Handle the news "force deleted" event.
     *
     * @param  \App\Models\News  $news
     * @return void
     */
    public function forceDeleted(News $news)
    {
        //
    }
}
