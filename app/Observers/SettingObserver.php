<?php

namespace App\Observers;

use App\Models\Setting;

class SettingObserver
{
	/**
	 * Handle the Setting "created" event.
	 *
	 * @param  \App\Models\Setting $setting
	 * @return void
	 */
	public function creating(Setting $setting)
	{
		if (!Setting::isStaff($setting->key)) {
			if (\Str::contains($setting->key, '.')) {
				$parts = explode('.', $setting->key);
				$setting->group = reset($parts);
			}
			if (!\Arr::get($setting, 'group')) {
				$setting->group = Setting::DEFAULT_GROUP;
			}
		}
	}

	public function created(Setting $Setting)
	{
		//
	}

	/**
	 * Handle the Setting "updated" event.
	 *
	 * @param  \App\Models\Setting $Setting
	 * @return void
	 */
	public function updated(Setting $Setting)
	{
		//
	}

	/**
	 * Handle the Setting "deleted" event.
	 *
	 * @param  \App\Models\Setting $Setting
	 * @return void
	 */
	public function deleted(Setting $Setting)
	{
		//
	}

	/**
	 * Handle the Setting "restored" event.
	 *
	 * @param  \App\Models\Setting $Setting
	 * @return void
	 */
	public function restored(Setting $Setting)
	{
		//
	}

	/**
	 * Handle the Setting "force deleted" event.
	 *
	 * @param  \App\Models\Setting $Setting
	 * @return void
	 */
	public function forceDeleted(Setting $Setting)
	{
		//
	}
}
