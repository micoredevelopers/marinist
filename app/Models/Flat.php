<?php

namespace App\Models;


use App\Contracts\GetMinMaxValue;
use App\Models\Filters\AbstractFilter;
use App\Scopes\FlatsScope;
use App\Traits\MinMaxValueParametrs;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

/**
 * App\Models\Flat
 *
 * @property int $id
 * @property int $floor_id
 * @property int $active
 * @property string|null $photo
 * @property int $number № квартиры
 * @property int $price Цена на м2
 * @property int $sum Цена всей квартиры
 * @property int $square Площадь
 * @property int $room Кол-во комнат
 * @property int $stock Акция
 * @property int $sea_view Вид на море
 * @property int $terrace Терасса
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Floor $floor
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Flat filter($filters)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Flat joinParents()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model language($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Flat newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Flat newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Flat query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Flat whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Flat whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Flat whereFloorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Flat whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'date_pub')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Flat whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Flat wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Flat wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Flat whereRoom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Flat whereSeaView($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Flat whereSquare($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Flat whereStock($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Flat whereSum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Flat whereTerrace($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Flat whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereUrl($url)
 * @mixin \Eloquent
 */
class Flat extends Model implements GetMinMaxValue
{

    protected $guarded = [
        'id',
    ];
    use MinMaxValueParametrs;
    protected $casts = [
        'number'  => 'string',
        'active'  => 'integer',
        'square'  => 'float',
        'room'    => 'integer',
        'stock'   => 'integer',
        'terrace' => 'integer',
    ];

    public static function boot()
    {
        parent::boot();

        self::bootGlobalScopes();
    }

    public static function bootGlobalScopes()
    {

        self::addGlobalScope(new FlatsScope);

    }

    public static function dropGlobalScopes($scope, \Closure $implementation = null)
    {
        self::removeGlobalScope($scope, $implementation);
    }



    public function __construct(array $attributes = [], $table = false)
    {

        parent::__construct($attributes, $table);
    }

    public function floor()
    {
        return $this->belongsTo(Floor::class);
    }

    public function scopeFilter($builder, AbstractFilter $filters)
    {
        return $filters->apply($builder);
    }

    public static function scopeJoinParents($query)
    {
        return $query
            ->leftJoin('floors', 'floors.id', 'flats.floor_id')
            ->leftJoin('sections', 'sections.id', 'floors.section_id');
    }


    public function getImagePath($column = 'photo', $default = null)
    {
        $image = \Arr::get($this, $column);
        return ($image AND storageFileExists($image)) ? getStorageFilePath($image) : $default;
    }

    public function getCharacteristicsFloors(Section $section, Floor $floor): array
    {
        $chars = [
            getTranslate('flat.section')        => $section->number,
            getTranslate('flat.floor')          => $floor->number,
            getTranslate('flat.rooms-quantity') => $this->room,
            getTranslate('flat.square')         => translateFormat('global.sq-meter', [(string)\Arr::get($this, 'square')]),
            getTranslate('flat.sea-view')       => translateYesNo($this->sea_view),
            getTranslate('flat.terrace')        => translateYesNo($this->terrace),
        ];

        return $chars;
    }

    public function status()
    {

        return $this->belongsTo(FlatStatus::class, 'status_id', 'id');
    }

    public function addLocalizedData(Section $section, Floor $floor)
    {
        \Arr::forget($this, 'price');
        \Arr::forget($this, 'sum');
        \Arr::forget($this, 'active');
        $this->section_id = (int)$section->id;
        $this->flat_id = $this->id;
        $this->characteristics = $this->getCharacteristicsFloors($section, $floor);
        $this->apartaments = translateFormat('flat.apartment-number', [$this->number]);
        $this->photo = $this->getImagePath('photo');
        return $this;
    }

    public static function getSquaresForParameters()
    {
        $squares = self::active(1)->orderBy('square')->get('square');
        $squares = $squares->map(function ($item) {
            $item->square = (int)$item->square;
            return $item;
        })
            ->unique('square', true)
            ->values();
        return $squares;
    }

    public static function getRoomsForParameters()
    {
        $rooms = self::active(1)->distinct()->get('room');
        $rooms = $rooms->sortBy('room');
        return $rooms;
    }

    public static function getMinMaxSquares(){
        return Collection::make([
            'min'=>self::min('square'),
            'max'=>self::max('square')
        ]);
    }
    public static function getMinMaxRooms(){
        return Collection::make([
            'min'=>self::min('room'),
            'max'=>self::max('room')
        ]);
    }
}
