<?php

namespace App\Models;


/**
 * App\Models\GalleriesPhoto
 *
 * @property int $id
 * @property int $galleries_id
 * @property string|null $date_pub
 * @property int $active
 * @property string|null $name
 * @property string|null $photo
 * @property int|null $sort
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Gallery $news
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model language($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GalleriesPhoto newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GalleriesPhoto newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GalleriesPhoto query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GalleriesPhoto whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GalleriesPhoto whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GalleriesPhoto whereDatePub($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GalleriesPhoto whereGalleriesId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GalleriesPhoto whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'date_pub')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GalleriesPhoto whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GalleriesPhoto wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GalleriesPhoto whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GalleriesPhoto whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereUrl($url)
 * @mixin \Eloquent
 */
class GalleriesPhoto extends Model
{
    protected $table = 'galleries_photo';

    public function news()
    {
        return $this->belongsTo(Gallery::class);
    }
}
