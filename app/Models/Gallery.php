<?php

namespace App\Models;


/**
 * App\Models\Gallery
 *
 * @property int $id
 * @property string|null $date_pub
 * @property int $active
 * @property string|null $photo
 * @property string|null $url
 * @property int|null $sort
 * @property array|null $video
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\GalleriesPhoto[] $photos
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model language($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gallery newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gallery newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gallery query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gallery whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gallery whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gallery whereDatePub($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gallery whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'date_pub')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gallery wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gallery whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gallery whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gallery whereUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gallery whereVideo($value)
 * @mixin \Eloquent
 */
class Gallery extends Model
{
    protected $table = 'galleries';

	protected $fillable = [
		'active',
		'url',
		'photo',
		'sort',
		'date_pub',
		'video',
	];
	protected $casts = [
		'video' => 'array'
	];

    public function photos()
    {
        return $this->hasMany(GalleriesPhoto::class);
	}

	/**
	 * @param $language_id
	 * @return $this
	 */
	public function langItem($language_id)
	{
		return $this->hasOne(GalleriesLang::class)->Language($language_id);
	}
}
