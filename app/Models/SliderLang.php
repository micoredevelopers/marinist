<?php

namespace App\Models;

use App\Traits\EloquentMultipleForeignKeyUpdate;

/**
 * App\Models\SliderLang
 *
 * @property int $sliders_id
 * @property int $language_id
 * @property string|null $name
 * @property string|null $description
 * @property string|null $sub_description
 * @property string|null $video
 * @property-read \App\Models\Slider $lang
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model language($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SliderLang newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SliderLang newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SliderLang query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SliderLang whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereId($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'date_pub')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SliderLang whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SliderLang whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SliderLang whereSlidersId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SliderLang whereSubDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereUrl($url)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SliderLang whereVideo($value)
 * @mixin \Eloquent
 */
class SliderLang extends Model
{
	use EloquentMultipleForeignKeyUpdate;

	protected $guarded = [];

    protected $table = 'sliders_lang';

	public $timestamps = false;

	public $incrementing = false;

	protected $primaryKey = ['slider_id', 'language_id'];

}
