<?php

namespace App\Models;

use App\Traits\EloquentMultipleForeignKeyUpdate;

/**
 * App\Models\NewsLang
 *
 * @property int $news_id
 * @property int $language_id
 * @property string|null $name
 * @property string|null $description
 * @property string|null $sub_description
 * @property array|null $video
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model language($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsLang newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsLang newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsLang query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsLang whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereId($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'date_pub')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsLang whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsLang whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsLang whereNewsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsLang whereSubDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereUrl($url)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NewsLang whereVideo($value)
 * @mixin \Eloquent
 */
class NewsLang extends Model
{
	use EloquentMultipleForeignKeyUpdate;

	protected $guarded = [];

	protected $casts = [
		'video' => 'array'
	];

    protected $table = 'news_lang';

	public $incrementing = false;

	public $timestamps = false;

	protected $primaryKey = ['news_id', 'language_id'];


}
