<?php

namespace App\Models;

use App\Contracts\HasLocalized;

/**
 * App\Models\ProgressGallery
 *
 * @property int $id
 * @property array|null $video
 * @property int $active
 * @property string|null $photo
 * @property int $sort
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProgressGalleryPhoto[] $photos
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model language($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgressGallery newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgressGallery newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgressGallery query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgressGallery whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgressGallery whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgressGallery whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'date_pub')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgressGallery wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgressGallery whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgressGallery whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereUrl($url)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgressGallery whereVideo($value)
 * @mixin \Eloquent
 */
class ProgressGallery extends Model
{

	protected $fillable = [
		'photo',
		'sort',
		'video',
	];
	protected $casts = [
		'video' => 'array'
	];
	public $table = 'progress_galleries';

	/**
	 * @param int $language_id
	 * @return $this
	 */
	public function langItem(int $language_id)
	{
		return $this->hasOne(ProgressGalleryLang::class)->Language($language_id);
    }

	public function photos()
	{
		return $this->hasMany(ProgressGalleryPhoto::class);
    }
}
