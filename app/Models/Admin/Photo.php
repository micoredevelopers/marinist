<?php

namespace App\Models\Admin;

use App\Http\Libs;
use App\Models\Model;
use App\Models\Setting;
use App\Traits\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use Storage;
use DB;
use Image;

/**
 * App\Models\Admin\Photo
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model language($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Photo newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Photo newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin\Photo query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereId($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'date_pub')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereUrl($url)
 * @mixin \Eloquent
 */
class Photo extends Model
{
	use Builder;
	protected $watermarkConfig = null;
	const DEFAULT_EXT = 'jpg';
	const WATERMARK_POSITION = 'center';
	protected static $extensionAllowed = [
		'jpg',
		'jpeg',
		'png',
		'gif',
		'webp',
	];

	protected function extensionAllowed($ext)
	{
		return in_array($ext, self::$extensionAllowed);
	}

	/**
	 * @param $file
	 * @param $directory
	 * @param $id
	 * @return string
	 */
	private function getConfigWatermark()
	{
		if (!$this->watermarkConfig) {
			$this->watermarkConfig = Setting::getSetting('_watermark');
			$this->watermarkConfig = json_decode($this->watermarkConfig, true);
		}
		return $this->watermarkConfig;
	}

	public function saveImage($cols = 250, $rows = 250)
	{
		$file = $this->getData('photo');
		$directory = $this->getData('directory');
		$id = md5($this->getData('id'));
		$table = $this->getData('table');
		//
		$extension = $file->getClientOriginalExtension();
		$pathMove = storage_path('app/public/' . $directory . '/');
		$fileNameThumb = $id . '_s.' . $extension;
		$fileName = $id . '.' . $extension;
		$filePath = $directory . '/' . $fileName;
		if ($file->move($pathMove, $fileName)) {
			//todo переделать тамнейлы и ватермарк на
			// ...->withThumbnail(['width' => 1, 'height' => 1])->withWaterMark()->saveImage()
			// todo добавить в управление позицию водного знака
			if ($this->extensionAllowed($extension)) {
				$watermarked = $this->setWaterMark($pathMove . '/' . $fileName, $table);
				if ($watermarked) Storage::put($directory . '/' . $fileName, $watermarked);
				//
				if ($cols AND $rows) {
					copy($pathMove . $fileName, $pathMove . $fileNameThumb);
					$image = Libs::thumbnailImage($pathMove . $fileNameThumb, $cols, $rows);
					file_put_contents($pathMove . $fileNameThumb, $image);
					$filePath = $directory . '/' . $fileNameThumb;
				}
			}
		}
		$this->clearBuilderData();
		return $filePath;
	}

	public function saveImageFromBase64($cols = 250, $rows = 250)
	{
		$filenamePath = false;
		$base64 = $this->getData('photo');
		$directory = $this->getData('directory');
		$id = md5($this->getData('id'));
		$table = $this->getData('table');
		//
		$base64 = $this->getClearBase64($base64);
		$base64 = base64_decode($base64);
		$extension = self::DEFAULT_EXT;
		$fileNameThumb = $id . '_s.' . $extension;
		$fileName = $id . '.' . $extension;
		if (Storage::put($directory . '/' . $fileName, $base64)) {
			$filenamePath = $directory . '/' . $fileName;
			$watermarked = $this->setWaterMark($base64, $table);
			if ($watermarked) Storage::put($directory . '/' . $fileName, $watermarked);
			$image = Libs::thumbnailImage($base64, $cols, $rows);
			if (Storage::put($directory . '/' . $fileNameThumb, $image)) {
				$filenamePath = $directory . '/' . $fileNameThumb;
			}
		}
		return $filenamePath;
	}

	public function setWaterMark($image, $table, $position = false)
	{
		$config = $this->getConfigWatermark();
		if (isset($config['tables'][$table]) AND $config['tables'][$table] == 1) {
			$pathToWaterMark = asset('images/watermark.png');
			$position = $position ?: self::WATERMARK_POSITION;
			$img = Image::make($image);
//			$imageHeight = $img->height();
			$imageWidth = $img->width();
			if ($img) {
				$watermark = Image::make($pathToWaterMark);
				if ($watermark) {
//					$watermarkHeight = $watermark->height();
					$watermarkWidth = $watermark->width();
					if ($watermarkWidth > $imageWidth) $watermark->resize($imageWidth, null, function ($constraint) {
						$constraint->aspectRatio();
					});
					$img->insert($watermark, $position);
				}
				return $img->stream();
			}
		}
		return false;
	}

	/**
	 * @return bool
	 * Метод является оберткой для saveImageFromBase64, и в общем то пока не имеет смысла в своем существовании
	 */
	public function saveAdditionalImageFromBase64()
	{
		$photo = $this->getData('photo');
		$table = $this->getData('table');
		$primary_id = $this->getData('primary_id');
		if (!$photo) return false;
		$directory = $table . '/' . $primary_id;
		$this->setData('directory', $directory);
		return $this->saveImageFromBase64();
	}

	/**
	 * @param Model $belongToModel
	 * @param $primaryId
	 * @param string $inputName
	 * @return bool
	 */
	public function saveAdditionPhotos(Model $belongToModel, $primaryId, $inputName = 'photos')
	{
		$status = false;
		$table = $belongToModel->getTable();
		if (!Input::file($inputName)) return false;
		$foreignModel = new Model([], $table . '_photo');
		$photos = Input::file('photos');
		foreach ($photos as $photo) {
			$PhotoId = $foreignModel->insertGetId([
				$belongToModel->getForeignKey() => $primaryId,
				'created_at'                    => date('Y-m-d H:i:s'),
				'name'                          => (string)$photo->getClientOriginalName(),
			]);
			if ($PhotoId) $PhotoPath = (new self())
				->setData('photo', $photo)
				->setData('directory', $table . '/' . $primaryId)
				->setData('table', $table . '_photo')
				->setData('id', $PhotoId)
				->saveImage();
			$foreignModel->where(['id' => $PhotoId])->update(['photo' => $PhotoPath]);
			$this->clearBuilderData();
			$status = true;
		}
		return $status;
	}

	public function deletePhoto($id, $table, $primaryId = false)
	{
		// primaryId признак дополнительного фото
		if ($primaryId) $table .= '_photo';
		//this is additional photo, lets delete record from database
		if (!\Schema::hasTable($table)) return false;
		$photo = DB::table($table)->where('id', (int)$id)->first();
		if ($photo AND $photo->photo) {
			$this->deleteImageStorage($photo->photo);
			// Delete record if that additional image
			$query = DB::table($table)->where('id', $id);
			if ($primaryId) {
				$query->delete();
			} else {
				$query->update(['photo' => '']);
			}
			return true;
		}
		return false;
	}

	public function getClearBase64($base64)
	{
		$base64 = str_replace('data:image/jpeg;base64,', '', $base64);
		$base64 = str_replace('data:image/png;base64,', '', $base64);
		$base64 = str_replace(' ', '+', $base64);
		return $base64;
	}

	public function updateImagePath(Request $request, $imagePath, $column = 'photo')
	{
		$table = $request->get('table');
		$id = $request->get('id');
		$columnId = 'id';
		if (Schema::hasTable($table) AND Schema::hasColumn($table, $column)) {
			if ($id AND $record = DB::table($table)->where($columnId, $id)->first()) {
//				DB::table($table)->where($columnId, $id)->update([$column => $imagePath]);
				if ($oldPhoto = $record->$column AND $oldPhoto !== $imagePath) {
					$this->deleteImageStorage($oldPhoto);
				}
			}
		}
	}

	public function deleteImageStorage($imagePath)
	{
		if (storageFileExists($imagePath)) {
			storageDelete($imagePath);
		}
		if (storageFileExists(imgOrigin($imagePath))) {
			storageDelete(imgOrigin($imagePath));
		}
	}
}

