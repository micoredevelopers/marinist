<?php

namespace App\Models;


/**
 * App\Models\SliderPhotos
 *
 * @property int $id
 * @property int $sliders_id
 * @property int $active
 * @property string|null $name
 * @property string|null $photo
 * @property int $sort
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model language($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SliderPhotos newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SliderPhotos newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SliderPhotos query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SliderPhotos whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SliderPhotos whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SliderPhotos whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'date_pub')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SliderPhotos whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SliderPhotos wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SliderPhotos whereSlidersId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SliderPhotos whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SliderPhotos whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereUrl($url)
 * @mixin \Eloquent
 */
class SliderPhotos extends Model
{
    protected $table = 'sliders_photo';

	protected $fillable = [
		'active',
		'name',
		'date_pub',
		'photo',
	];
}
