<?php

namespace App\Models;


use App\Traits\Singleton;

/**
 * App\Models\Menu
 *
 * @property int $id
 * @property int $parent_id
 * @property int $active
 * @property string $url
 * @property string|null $icon Шрифтовые изображения
 * @property string|null $photo обычная картинка, впринципе любого типа
 * @property int $sort
 * @property string|null $options
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model language($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'date_pub')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereOptions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereUrl($value)
 * @mixin \Eloquent
 */
class Menu extends Model
{
	use Singleton;

	protected $table = 'menu';
	protected $guarded = ['id'];

	/**
	 * @param int $language_id
	 * @return $this
	 */
	public function langItem(int $language_id)
	{
		return $this->hasOne(MenuLang::class, 'menu_id')->Language($language_id);
	}

	public function getUrl()
	{
		$url = $this->url;
		if (!isStringUrl($url)) $url = langUrl($url);
		return $url;
	}

	public function isExternal()
	{
		return isStringUrl($this->url);
	}

	public function getRel()
	{
		return $this->isExternal() ? 'rel="nofollow"' : '';
	}
	public function getTarget()
	{
		return $this->isExternal() ? 'targer="_blank"' : '';
	}
}
