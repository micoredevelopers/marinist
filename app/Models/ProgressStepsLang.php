<?php

namespace App\Models;

use App\Traits\EloquentMultipleForeignKeyUpdate;

/**
 * App\Models\ProgressStepsLang
 *
 * @property int $progress_steps_id
 * @property string|null $name
 * @property int $language_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model language($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgressStepsLang newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgressStepsLang newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgressStepsLang query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereId($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'date_pub')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgressStepsLang whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgressStepsLang whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgressStepsLang whereProgressStepsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereUrl($url)
 * @mixin \Eloquent
 */
class ProgressStepsLang extends Model
{
	use EloquentMultipleForeignKeyUpdate;

	protected $guarded = [];

	public $timestamps = false;

	protected $table = 'progress_steps_lang';

	public $incrementing = false;

	protected $primaryKey = ['progress_steps_id', 'language_id'];
}
