<?php

namespace App\Models;

use App\Contracts\HasLocalized;

/**
 * App\Models\ProgressSteps
 *
 * @property int $id
 * @property int $sort
 * @property int $percent
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model language($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgressSteps newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgressSteps newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgressSteps query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgressSteps whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgressSteps whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'date_pub')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgressSteps wherePercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgressSteps whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgressSteps whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereUrl($url)
 * @mixin \Eloquent
 */
class ProgressSteps extends Model implements HasLocalized
{
	protected $fillable = ['percent','photo'];
	protected $guarded = ['id'];

    public $table = 'progress_steps';

	/**
	 * @param int $language_id
	 * @return $this
	 */
	public function langItem(int $language_id)
	{
		return $this->hasOne(ProgressStepsLang::class)->Language($language_id);
    }
}
