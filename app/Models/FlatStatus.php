<?php

namespace App\Models;



class FlatStatus extends Model
{
    protected $table='flat_status';
    public $timestamps=false;
    protected $guarded = ['id'];
}
