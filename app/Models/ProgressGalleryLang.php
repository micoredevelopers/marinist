<?php

namespace App\Models;

use App\Traits\EloquentMultipleForeignKeyUpdate;

/**
 * App\Models\ProgressGalleryLang
 *
 * @property int $progress_galleries_id
 * @property int $language_id
 * @property string|null $name
 * @property string|null $description
 * @property string|null $sub_description
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model language($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgressGalleryLang newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgressGalleryLang newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgressGalleryLang query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgressGalleryLang whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereId($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'date_pub')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgressGalleryLang whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgressGalleryLang whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgressGalleryLang whereProgressGalleriesId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgressGalleryLang whereSubDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereUrl($url)
 * @mixin \Eloquent
 */
class ProgressGalleryLang extends Model
{
	use EloquentMultipleForeignKeyUpdate;

	public $incrementing = false;

	protected $guarded = [];

	public $timestamps = false;

	protected $table = 'progress_galleries_lang';

	protected $primaryKey = ['progress_gallery_id', 'language_id'];
}
