<?php
/**
 * Created by PhpStorm.
 * User: aljajazva
 * Date: 2019-05-31
 * Time: 09:11
 */

namespace App\Models\Filters;


class FlatFilter extends AbstractFilter
{
	public function sections($value)
	{
	    $data = explode(',',$value);

		$sectionFrom = (int)$data[0];
		$sectionTo = (int)$data[1];
		$this->builder->whereBetween('sections.number', [$sectionFrom, $sectionTo]);
	}

	public function floors($value)
	{
        $data = explode(',',$value);
		$floorFrom = (int)$data[0];
		$floorTo =(int)$data[1];
		if ($floorFrom AND $floorTo) {
			$this->builder->whereBetween('floors.number', [$floorFrom, $floorTo]);
		}
	}

	public function squares($value)
	{
        $data = explode(',',$value);
		$squareFrom = (int)$data[0];
		$squareTo = (int)$data[1];
		if ($squareTo AND $squareFrom) {
			$this->builder->whereBetween('flats.square', [$squareFrom, $squareTo]);
		}
	}

	public function rooms($value)
	{
        $data = explode(',',$value);
		$roomsFrom = (int)$data[0];
		$roomsTo = (int)$data[1];
		if ($roomsTo AND $roomsFrom) {
			$this->builder->whereBetween('flats.room', [$roomsFrom, $roomsTo]);
		}
	}

	public function seaView($value)
	{

		$this->builder->where('flats.sea_view', (int)$value);
	}

	public function terrace($value)
	{
		$this->builder->where('flats.terrace', (int)$value);
	}

	public function stock($value)
	{
		$this->builder->where('flats.stock', (int)$value);
	}

}
