<?php

namespace App\Models;

use App\Traits\EloquentExtend;
use App\Traits\EloquentScopes;
use Illuminate\Database\Eloquent\Model as BaseModel;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Support\Arr;

/**
 * App\Models\Model
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model language($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereId($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'date_pub')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereUrl($url)
 * @mixin \Eloquent
 */
class Model extends BaseModel
{
	use EloquentScopes;
	use EloquentExtend;


	public function __construct(array $attributes = [], $table = false)
	{
		if ($table) $this->setTable($table);
		parent::__construct($attributes);
	}

    protected static function removeGlobalScope($scope, \Closure $implementation = null)
    {
        $key = false;
        if (is_string($scope) && !is_null($implementation)) {
            $key = $implementation;
        } else if ($scope instanceof \Closure) {
            $key = spl_object_hash($scope);
        } else if ($scope instanceof Scope) {
            $key = get_class($scope);
        }
        if ($key !== false) {
            Arr::forget(static::$globalScopes[static::class], $key);
        }
    }

	public static function updateNested(array $nested, $parent_id = 0)
	{
		foreach ($nested as $num => $nest) {
			if ($finded = static::find($nest['id'])) {
				if ($finded->sort != $num) $finded->sort = $num;
				$finded->parent_id = (int)$parent_id;
				$finded->save();
				if (isset($todo['children'])) static::updateNested($nest['children'], $nest['id']);
			}
		}
	}


	public function getPrimaryValue()
	{
		$key = $this->getKeyName();
		return $this->getAttribute($key);
	}

}
