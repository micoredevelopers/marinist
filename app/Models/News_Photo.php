<?php

namespace App\Models;


/**
 * App\Models\News_Photo
 *
 * @property int $id
 * @property int $news_id
 * @property int $active
 * @property string|null $name
 * @property string|null $photo
 * @property int $sort
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\News $news
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model language($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News_Photo newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News_Photo newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News_Photo query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News_Photo whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News_Photo whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News_Photo whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'date_pub')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News_Photo whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News_Photo whereNewsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News_Photo wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News_Photo whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News_Photo whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereUrl($url)
 * @mixin \Eloquent
 */
class News_Photo extends Model
{
    protected $table = 'news_photo';


    public function news()
    {
        return $this->belongsTo('App\Models\News');
    }
}
