<?php

namespace App\Models;



use App\Scopes\PartnersScope;

class Partner extends Model
{
    protected $table='partners';
    protected $fillable = ['rating','photo'];
    protected $guarded = ['id'];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new PartnersScope());
    }
}
