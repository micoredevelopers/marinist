<?php

namespace App\Models;


use App\Contracts\GetMinMaxValue;
use App\Traits\MinMaxValueParametrs;
use Illuminate\Support\Collection;

/**
 * App\Models\Section
 *
 * @property int $id
 * @property string|null $photo
 * @property int $number
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Floor[] $floors
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model language($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Section newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Section newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Section query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Section whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Section whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'date_pub')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Section whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Section wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Section whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereUrl($url)
 * @mixin \Eloquent
 */
class Section extends Model implements GetMinMaxValue
{


    protected $guarded = [
        'id',
    ];
    use MinMaxValueParametrs;


    public function floors()
    {
        return $this->hasMany(Floor::class);
    }

    public static function getAll()
    {
        return self::select(['*', 'id as section_id'])->get();
    }

    public function addLocalizedData()
    {
        $this->attributes['name_section'] = getTranslate('flat.section') . ' ' . $this->getPrimaryValue();
    }

    public static function getSectionsForParameters(): Collection
    {
        return Section::get('number')->sortBy('number');
    }


}
