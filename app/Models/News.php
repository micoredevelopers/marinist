<?php

namespace App\Models;


/**
 * App\Models\News
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $date_pub
 * @property string $active
 * @property string $url
 * @property string|null $photo
 * @property int $sort
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\News_Photo[] $photos
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model language($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereDatePub($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'date_pub')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereUrl($value)
 * @mixin \Eloquent
 */
class News extends Model
{
	public $next;
	public $prev;
	protected $table = 'news';
	protected $casts = [
		'date_pub' => 'datetime',
		'video'    => 'array',
	];

	protected $guarded = ['id'];

	public function photos()
	{
		return $this->hasMany('App\Models\News_Photo');
	}

	/**
	 * @param $language_id
	 * @return $this
	 */
	public function langItem($language_id)
	{
		return $this->hasOne(NewsLang::class, 'news_id')->Language($language_id);
	}

	public function getPrev()
	{
		if (is_null($this->prev)) {
			$res = false;
			$id = $this->id;
			$news = (new self())->Active(1)->WhereIsPublished()->GetLang(getLang())->SortOrder()->get('id')->pluck('id')->toArray();
			$offset = array_search($id, $news) - 1;
			$find = \Arr::get($news, $offset);
			if ($find) {
				$res = self::GetLang(getLang())->whereId($find)->first();
			}
			$this->prev = $res;
		}

		return $this->prev;
	}

	public function getNext()
	{
		if (is_null($this->next)) {
			$res = false;
			$id = $this->id;
			$news = (new self())->Active(1)->WhereIsPublished()->GetLang(getLang())->SortOrder()->get('id')->pluck(['id'])->toArray();
			$offset = array_search($id, $news) + 1;
			$find = \Arr::get($news, $offset);
			if ($find) {
				$res = self::GetLang(getLang())->whereId($find)->first();
			}
			$this->next = $res;
		}

		return $this->next;
	}
}
