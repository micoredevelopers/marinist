<?php
namespace App\Models;
use App\Traits\EloquentMultipleForeignKeyUpdate;

/**
 * App\Models\InfoblocksLang
 *
 * @property int $infoblocks_id
 * @property string|null $name
 * @property string|null $description
 * @property string|null $sub_description
 * @property int $language_id
 * @property-read \App\Models\Infoblock $lang
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model language($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InfoblocksLang newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InfoblocksLang newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InfoblocksLang query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InfoblocksLang whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereId($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InfoblocksLang whereInfoblocksId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'date_pub')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InfoblocksLang whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InfoblocksLang whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InfoblocksLang whereSubDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereUrl($url)
 * @mixin \Eloquent
 */
class InfoblocksLang extends Model
{
	use EloquentMultipleForeignKeyUpdate;

	public $incrementing = false;

	protected $primaryKey = ['infoblock_id', 'language_id'];

	protected $table = 'infoblocks_lang';
	public $timestamps = false;
	protected $guarded = [];

	public function lang()
	{
		return $this->belongsTo('App\Models\Infoblock');
	}
}
