<?php

namespace App\Models;


use App\Traits\EloquentMultipleForeignKeyUpdate;

/**
 * App\Models\TranslateLang
 *
 * @property int $translate_id
 * @property string|null $value
 * @property int $language_id
 * @property-read \App\Models\Translate $lang
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model language($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TranslateLang newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TranslateLang newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TranslateLang query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereId($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'date_pub')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TranslateLang whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TranslateLang whereTranslateId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereUrl($url)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TranslateLang whereValue($value)
 * @mixin \Eloquent
 */
class TranslateLang extends Model
{
	use EloquentMultipleForeignKeyUpdate;

	protected $table = 'translate_lang';

	public $timestamps = false;

	public $incrementing = false;

	protected $guarded = [];

	protected $primaryKey = ['translate_id', 'language_id'];

	public function lang(){
		return $this->belongsTo('App\Models\Translate');
	}
}
