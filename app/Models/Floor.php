<?php

namespace App\Models;

use App\Contracts\GetMinMaxValue;
use App\Traits\MinMaxValueParametrs;
use Illuminate\Database\Eloquent\Builder;

/**
 * App\Models\Floor
 *
 * @property int $id
 * @property int $section_id
 * @property int $number
 * @property string|null $photo
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Flat[] $flats
 * @property-read \App\Models\Section $section
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model language($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Floor newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Floor newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Floor query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Floor whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Floor whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'date_pub')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Floor whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Floor wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Floor whereSectionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Floor whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereUrl($url)
 * @mixin \Eloquent
 */
class Floor extends Model implements GetMinMaxValue
{
	protected $guarded = [
		'id',
	];
    use MinMaxValueParametrs;


    public function section()
	{
		return $this->belongsTo(Section::class);
	}

	public function flats()
	{
		return $this->hasMany(Flat::class);
	}

	public function getCharacteristicsFloors(Section $section): array
	{
		$flatsInSale = $this->flats()->active(1)->count();
		$chars = [];
		$chars[getTranslate('flat.section')] = $section->number;
		$chars[getTranslate('flat.on-sale')] = $flatsInSale . ' ' . getTranslate('flat.flat-plural');
		$chars[getTranslate('flat.square')] = translateFormat('global.sq-meter', [(int)\Arr::get($this, 'square')]) ;

		return $chars;
	}

	public function getImagePath($column = 'plan', $default = null)
	{
		$image = \Arr::get($this, $column);
		return ($image AND storageFileExists($image)) ? getStorageFilePath($image) : $default;
	}

	protected static function boot()
	{
		parent::boot();

		static::addGlobalScope('SortOrder', function (Builder $builder) {
			$builder->SortOrder();
		});
	}

	public function addLocalizedData($section)
	{
		$this->floor_id = $this->id;
		$this->name_floor = concatWords(getTranslate('flat.floor') , $this->number);
		$this->characteristics = $this->getCharacteristicsFloors($section);
		$this->plan = $this->getImagePath('plan');
		$this->substrate = $this->getImagePath('substrate');
		$this->backlight = $this->getImagePath('backlight');
		$this->name_section_floor = translateFormat('flat.floor-section', [$section->number, $this->number]);
		$this->name_section_floor_plan = translateFormat('flat.layout-floor-of-section', [$this->number, $section->number]);
		return $this;
	}

    public static function getFloorsForParameters()
    {
//        $floors = self::distinct('number')->get('number')->sortBy('number');

        //todo delete this, and uncomment up
        $floors = self::all('number')->sortBy('number')->unique('number');
        return $floors;
	}
}
