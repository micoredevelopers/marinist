<?php

namespace App\Models;

use App\Traits\Singleton;
use Illuminate\Support\Collection;

/**
 * App\Models\Setting
 *
 * @property int $id
 * @property string $key
 * @property string|null $display_name
 * @property string|null $value
 * @property string|null $details
 * @property string|null $type
 * @property int $sort
 * @property string|null $group
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model language($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereDetails($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereDisplayName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'date_pub')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereUrl($url)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereValue($value)
 * @mixin \Eloquent
 */
class Setting extends Model
{
	use Singleton;
	const DEFAULT_GROUP = 'global';
	public $timestamps = false;
	private static $settings = null;
	private $valueDecoded = false;
	protected $table = 'settings';
	//что разрешаем править
	protected $fillable = [
		'type',
		'key',
		'value',
		'display_name',
		'group',
		'sort',
		'details',
	];

	public static function getSettings($key = false)
	{
		if (is_null(self::$settings)) {
			$settings = self::getInstance()->SortOrder()->get();
			self::$settings = $settings;
		}
		if (!$key) {
			return self::$settings;
		}
		if (self::$settings->where('key', $key)) {
			return self::$settings->where('key', $key)->first();
		}
	}

	public static function getSetting($key, $field = 'value')
	{
		$setting = self::getSettings($key);
		if ($field) {
			return \Arr::get($setting, $field);
		}

		return $setting;
	}

	/**
	 * @return mixed
	 * Получить список доступных для публичного редактирования
	 * настройки начинающиеся с '_' служебные
	 */
	public static function getSettingsList(): Collection
	{
		$settings = self::getSettings();
		return $settings->filter(function (Setting $item) {
			return !Setting::isStaff($item->key);
		});
	}

	/**
	 * @param $key
	 * @return mixed
	 * Является ли переданный ключ, служебной настройкой не доступной для публичного редактирования
	 */
	public static function isStaff($key)
	{
		return \Str::startsWith($key, '_');
	}

	public function isFile()
	{
		$types = [
			'file_multiple',
			'file',
			'image',
		];
		return in_array($this->type, $types);
	}

	public function isFileValid()
	{
		$isFile = $this->isFile();
		$value = \Arr::get($this, 'value');
		return $isFile AND $value;
	}

	public function isTypeImage()
	{
		return $this->isFile() AND $this->type === 'image';
	}

	public function isTypeFile()
	{
		return $this->isFile() AND $this->type === 'file';
	}

	public function isTypeFileMultiple()
	{
		return $this->isFile() AND $this->type === 'file_multiple';
	}


	public function isMultiFile()
	{
		$isFile = $this->isFile();
		$type = $this->type === 'file_multiple';
		return $isFile AND $type;
	}

	public function getValue($key = false)
	{
		if (!$this->valueDecoded AND isJson($this->value)){
			$this->value = \json_decode($this->value);
			$this->valueDecoded = true;
		}
		return $key ? \Arr::get($this->value, $key) : $this->value;
	}

    public function getTypeAttribute()
    {
        return $this->attributes['type'];
    }

    public function getPrepend(): ?string
    {
        $prepend = null;
        switch ($this->getTypeAttribute()) {
            case 'file':
                $prepend = "settingFile('";
                break;
            case 'file_multiple':
                $prepend = "settingFiles('";
                break;
            default:
                $prepend = "getSetting('";
        }
        return $prepend;
    }

    public function getAppend(): ?string
    {
        $append = "')";
        return $append;
    }

}
