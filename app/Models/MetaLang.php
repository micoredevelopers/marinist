<?php

namespace App\Models;


/**
 * App\Models\MetaLang
 *
 * @property int $meta_id
 * @property string|null $h1
 * @property string|null $meta_title
 * @property string|null $meta_keywords
 * @property string|null $meta_description
 * @property string|null $text_top
 * @property string|null $text_bottom
 * @property int $language_id
 * @property-read \App\Models\Meta $lang
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model language($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MetaLang newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MetaLang newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MetaLang query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MetaLang whereH1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereId($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'date_pub')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MetaLang whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MetaLang whereMetaDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MetaLang whereMetaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MetaLang whereMetaKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MetaLang whereMetaTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MetaLang whereTextBottom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MetaLang whereTextTop($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereUrl($url)
 * @mixin \Eloquent
 */
class MetaLang extends Model
{
	protected $table = 'meta_lang';
	public $timestamps = false;
	protected $fillable = [
		'meta_id',
		'h1',
		'meta_title',
		'meta_keywords',
		'meta_description',
		'text_top',
		'text_bottom',
		'language_id',
	];

	public function lang(){
		return $this->belongsTo(Meta::class);
	}
}
