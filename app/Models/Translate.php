<?php

namespace App\Models;

use App\Traits\Singleton;
use Illuminate\Support\Arr;


/**
 * App\Models\Translate
 *
 * @property int $id
 * @property string|null $key
 * @property string|null $comment
 * @property int $module_id
 * @property string|null $group
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model language($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate whereGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'date_pub')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate whereModuleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereUrl($url)
 * @mixin \Eloquent
 */
class Translate extends Model
{
	use Singleton;
	private static $translates = null;
	//
	protected $table = 'translate';
	protected $guarded = ['id'];


	public function langItem( $language_id = null)
	{
		$language_id = $language_id ?: getLang();
		return $this->hasOne(TranslateLang::class)->Language($language_id);
	}

	public static function getTranslates($langId, $key = false)
	{
		if (!Arr::has(self::$translates, $langId)) {
			$translates = self::GetLang($langId)->get()->keyBy('key');
			Arr::set(self::$translates, $langId, $translates);
		}
		if (!$key) {
			return Arr::get(self::$translates, $langId);
		}
		if (Arr::get(self::$translates, $langId)->where('key', $key)) {
			return Arr::get(self::$translates, $langId)->where('key', $key)->first();
		}
	}

	public static function getTranslate($key, $asObject = false)
	{
		$res = $asObject
			? static::getTranslates(getLang(), $key)
			: Arr::get(static::getTranslates(getLang(), $key), 'value');
		return $res;
	}

	public function format()
	{
		return sprintf($this->value, ...func_get_args());
	}

	public function isTextarea()
	{
		return $this->type === 'textarea';
	}
    public function isCKeditor()
    {
        return $this->type === 'ckeditor';
    }
	public function isText()
	{
		return $this->type === 'text';
	}

	public static function getGroups()
	{
		return Translate::distinct('group')->pluck('group');
	}
}
