<?php

namespace App\Models;


use App\Traits\EloquentMultipleForeignKeyUpdate;

/**
 * App\Models\GalleriesLang
 *
 * @property int $galleries_id
 * @property int $language_id
 * @property string|null $name
 * @property string|null $description
 * @property string|null $sub_description
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model language($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GalleriesLang newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GalleriesLang newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GalleriesLang query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GalleriesLang whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GalleriesLang whereGalleriesId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereId($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'date_pub')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GalleriesLang whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GalleriesLang whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GalleriesLang whereSubDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereUrl($url)
 * @mixin \Eloquent
 */
class GalleriesLang extends Model
{
	use EloquentMultipleForeignKeyUpdate;

	protected $guarded = [];

	protected $table = 'galleries_lang';

	public $incrementing = false;

	protected $primaryKey = ['gallery_id', 'language_id'];

	public $timestamps = false;

}
