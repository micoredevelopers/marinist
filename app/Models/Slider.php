<?php

namespace App\Models;


use App\Traits\Singleton;

/**
 * App\Models\Slider
 *
 * @property int $id
 * @property int $active
 * @property string|null $photo
 * @property string|null $options
 * @property int $sort
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model language($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slider newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slider newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slider query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slider whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slider whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slider whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'date_pub')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slider whereOptions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slider wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slider whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slider whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereUrl($url)
 * @mixin \Eloquent
 */
class Slider extends Model
{
	use Singleton;
    protected $table = 'sliders';

	protected $fillable = [
		'active',
		'date_pub',
		'photo',
	];

	public function photos()
	{
		return $this->hasMany(SliderPhotos::class);
	}

	/**
	 * @param $language_id
	 * @return $this
	 */
	public function langItem($language_id)
	{
		return $this->hasOne(SliderLang::class)->Language($language_id);
	}

}
