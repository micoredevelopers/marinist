<?php

namespace App\Models;


use App\Traits\EloquentMultipleForeignKeyUpdate;

/**
 * App\Models\MenuLang
 *
 * @property int $menu_id
 * @property string|null $name
 * @property int $language_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model language($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuLang newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuLang newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuLang query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereId($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'date_pub')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuLang whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuLang whereMenuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuLang whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereUrl($url)
 * @mixin \Eloquent
 */
class MenuLang extends Model
{
	use EloquentMultipleForeignKeyUpdate;

	public $incrementing = false;
	protected $table = 'menu_lang';
	protected $primaryKey = ['menu_id', 'language_id'];

	public $timestamps = false;

	protected $guarded = [];
}
