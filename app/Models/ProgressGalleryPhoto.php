<?php

namespace App\Models;

/**
 * App\Models\ProgressGalleryPhoto
 *
 * @property int $id
 * @property int $progress_galleries_id
 * @property int $active
 * @property string|null $name
 * @property string|null $photo
 * @property int $sort
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model language($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgressGalleryPhoto newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgressGalleryPhoto newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgressGalleryPhoto query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgressGalleryPhoto whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgressGalleryPhoto whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgressGalleryPhoto whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'date_pub')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgressGalleryPhoto whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgressGalleryPhoto wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgressGalleryPhoto whereProgressGalleriesId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgressGalleryPhoto whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProgressGalleryPhoto whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereUrl($url)
 * @mixin \Eloquent
 */
class ProgressGalleryPhoto extends Model
{
	public $table = 'progress_galleries_photo';
}
