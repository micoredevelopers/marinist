<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailAbstract extends Mailable
{
	use Queueable, SerializesModels;
	protected $request;

	/**
	 * Create a new message instance.
	 *
	 * @return void
	 */
	public function __construct(Request $request)
	{
		$this->request = $request;
	}

	protected function getEmailFrom(): string
	{
		$emailFrom = 'info@' . $this->request->getHost();
		return $emailFrom;
	}

	protected function getNameFrom(): string
	{
		$nameFrom = getSetting('global.sitename');
		return $nameFrom;
	}

	protected function getEmailTo(): string
	{
		$emailTo = getSetting('global.email');
		return $emailTo;
	}

	protected function getNameTo(): string
	{
		$nameTo = '';
		return $nameTo;
	}

}
