<?php


namespace App\Mail;


use Illuminate\Http\Request as IlluminateRequest;

class NewsLetterView extends MailAbstract
{
    protected $data;

    /**
     * NewsLetterView constructor.
     * @param IlluminateRequest $illuminateRequest
     * Create a new message instance.
     * @param array $data
     * @return void
     */
    public function __construct( $data ,IlluminateRequest $illuminateRequest)
    {
        parent::__construct($illuminateRequest);
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->getEmailFrom(), $this->getNameFrom())
            ->to($this->getEmailTo())
            ->view('mail.newsletter')->with(['item' => $this->data]);
    }
}
