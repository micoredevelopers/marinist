<?php

namespace App\Mail;

use Illuminate\Http\Request as IlluminateRequest;

class SignView extends MailAbstract
{
	protected $feedback;

	/**
	 * SignView constructor.
	 * @param IlluminateRequest $illuminateRequest
	 * @param \App\Models\Request $request
	 *
	 * Create a new message instance.
	 *
	 * @return void
	 */
	public function __construct(\App\Models\Request $request, IlluminateRequest $illuminateRequest)
	{
		parent::__construct($illuminateRequest);
		$this->feedback = $request;
	}

	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build()
	{
		$data = null;
		if ($this->request->has('section') && $this->request->has('floor') && $this->request->has('flat')) {
			$data = [
				'section' => $this->request->get('section'),
				'floor' => $this->request->get('floor'),
				'flat' => $this->request->get('flat'),
			];
		}

		parse_str(parse_url($this->feedback->getAttribute('referer'), PHP_URL_QUERY), $queries);
		$data['utms'] = $queries;

		return $this->from($this->getEmailFrom(), $this->getNameFrom())
			->to($this->getEmailTo())
			->view('mail.sign-view')->with(['feedback' => $this->feedback, 'data' => $data])
			;
	}
}
