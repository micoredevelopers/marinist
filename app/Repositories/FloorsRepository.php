<?php


namespace App\Repositories;


use App\Models\Flat;
use App\Models\Floor;
use App\Models\Section;
use Illuminate\Support\Facades\Cache;

class FloorsRepository
{
	public static function getFloors($key, int $section_id)
	{
		$res = Cache::get($key, function() use ($key, $section_id){
			if ($section = Section::with(['floors', 'floors.flats' => function ($query) {
				$query->active(1);
			},
			])->find($section_id)) {
				$res = $section->floors;
				$res->map(function ($item) use ($section) {
					/** @var $item Floor */
					$floor = $item;
					$item->flats->map(function ($item) use ($section, $floor) {
						/** @var $item Flat */
						return $item->addLocalizedData($section, $floor);
					});
					return $item->addLocalizedData($section);
				});
			}
			Cache::set($key, $res);
		});

		return $res;


	}
}
