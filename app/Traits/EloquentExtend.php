<?php
/**
 * Created by PhpStorm.
 * User: aljajazva
 * Date: 2019-05-22
 * Time: 13:34
 */

namespace App\Traits;


trait EloquentExtend
{

	private static $schema = [];

	public function getTableColumns()
	{
		if (!\Arr::has(self::$schema, $this->getTable())) {
			$columns = $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
			\Arr::set(self::$schema, $this->getTable(), $columns);
		}
		return \Arr::get(self::$schema, $this->getTable());
	}

	/**
	 * @param array $attributes
	 * @return $this
	 */
	public function fillExisting(array $attributes)
	{
		$schema = array_flip($this->getTableColumns());
		$attributes = array_filter($attributes, function ($value, $column) use ($schema){
			$exists = \Arr::exists($schema, $column);
			$isGuarded = $this->isGuarded($column);
			return ( $exists AND !$isGuarded );
		}, ARRAY_FILTER_USE_BOTH);
		return $this->fill($attributes);
	}
}