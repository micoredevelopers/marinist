<?php
/**
 * Created by PhpStorm.
 * User: aljajazva
 * Date: 2019-06-06
 * Time: 09:52
 */

namespace App\Traits;



trait HasContent
{

	public function contents()
	{
		return $this->morphMany(\App\Models\Content::class, 'contentable');
	}

	public function content()
	{
		return $this->morphOne(\App\Models\Content::class, 'contentable');
	}

}