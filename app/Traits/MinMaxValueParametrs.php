<?php


namespace App\Traits;


use Illuminate\Support\Collection;

trait MinMaxValueParametrs
{



    public static function getMinMax($name){
        return Collection::make([
            'min'=>intval(self::min($name)),
            'max'=>intval(self::max($name))
        ]);
    }

}
