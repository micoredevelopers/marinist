<?php

namespace App\Traits\Models;


use App\Models\Language;

trait BelongsToLanguage
{

	public function getLanguage()
	{
		return $this->belongsTo(Language::class, 'language_id', 'id');
	}
}