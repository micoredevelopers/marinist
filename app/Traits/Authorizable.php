<?php
/**
 * Created by PhpStorm.
 * User: aljajazva
 * Date: 2019-04-23
 * Time: 10:59
 */

namespace App\Traits;


trait Authorizable
{
    private $abilities = [
        'index' => 'view',
        'edit' => 'edit',
        'show' => 'view',
        'update' => 'edit',
        'create' => 'add',
        'store' => 'add',
        'destroy' => 'delete'
    ];

    /**
     * Override of callAction to perform the authorization before
     *
     * @param $method
     * @param $parameters
     * @return mixed
     */
    public function callAction($method, $parameters)
    {
        if ($this->abilityExists($method)){
            if( $ability = $this->getAbility($method) ) {
                $this->authorize($ability);
            }
        }

        return parent::callAction($method, $parameters);
    }

    public function getAbility($method)
    {
        $routeName = explode('.', \Request::route()->getName());
        $parts = array_filter($routeName, function($name){
            return $name != 'admin';
        });
        $action = \Arr::get($this->getAbilities(), $method);

        return $action ? $action . '_' . reset($parts) : null;
    }

    private function getAbilities()
    {
        return $this->abilities;
    }

    public function setAbilities($abilities)
    {
        $this->abilities = $abilities;
    }

    public function abilityExists($method)
    {
        return in_array($method, array_keys($this->getAbilities()));
    }
}