<?php

namespace App\Traits;

use App\Models\Setting;
use App\Models\Translate;
use \SEOMeta;

trait Helpers
{
    protected $data;

    protected $breadcrumbsData;
    /**
     * @param $breadName
     * @param $breadUrl
     *
     * @return $this
     */
    public function addBreadCrumb($breadName, $breadUrl = '')
    {
        $this->createBreadArr();
        $this->breadcrumbsData[] = [ 'name' => $breadName, 'url' => $breadUrl ];

        return $this;
    }

    public function dropLastBreadCrumb()
    {
        if($this->breadcrumbsData) array_pop($this->breadcrumbsData);
    }

    /**
     * @return $this
     */
    public function createBreadArr()
    {
        if (!isset($this->breadcrumbsData)) $this->breadcrumbsData = [];

        return $this;
    }

    /**
     * @param bool $name
     * @return mixed
     */
    public function getBreadCrumbs($name = false)
    {
        $this->createBreadArr();
        if ($name) $this->addBreadCrumb($name);

        return $this->breadcrumbsData;
    }

    /**
     * @param $key
     * @param $value
     *
     * Метод для добавления данных в массив data для данных контроллера
     */
    public function setData($key, $value)
    {
        if ($key) $this->data[ $key ] = $value;
    }

    /**
     * @param $key
     *
     * @return bool|mixed
     * Метод для получения данных в массив data для данных контроллера
     */
    public function getData($key = '*')
    {
        $return = false;
        if ($key === '*') $return = $this->data;
        else if (array_key_exists($key, $this->data)) $return = $this->data[ $key ];

        return $return;
    }

    //  CSS  //
    public function checkForStylesArr($destination)
    {
        if (!isset($this->styles[$destination])) $this->styles[$destination] = [];

        return $this->styles[$destination];
    }

    public function addCss($style, $destination = 'header')
    {
        $this->checkForStylesArr($destination);
        if (is_array($style)) {
            foreach ($style as $css) {
                $this->addCss($css, $destination);
            }
        } else {
            if (!isStringUrl($style)) {
                if (assetFileExists($style)) {
                    $filemtime = assetFilemtime($style) ?? false;
                    if ($filemtime) $style .= '?' . $filemtime;
                }
                $style = asset($style, env('HTTPS'));
            }
            $this->styles[ $destination ][] = $style;
        }
    }

    public function getCss($destination)
    {
        return $this->checkForStylesArr($destination);
    }

    public function getStylesString($destination = 'header')
    {
        $return = '';
        if ($styles = $this->getCss($destination)) {
            foreach ($styles as $style) {
                $return .= ' <link rel="stylesheet" href="' . $style . '">' . PHP_EOL;
            }
        }

        return $return;
    }

    //  JS  //
    public function checkForScriptsArr($defer = false)
    {
        $var = ($defer) ? 'scriptsDefer' : 'scripts';
        if (!isset($this->$var)) $this->$var = [];

        return $this->$var;
    }

    public function dropListScripts($defer = false)
    {
        $var = ($defer) ? 'scriptsDefer' : 'scripts';
        $this->$var = [];

        return $this->$var;
    }

    public function addScripts($scripts)
    {
        $this->checkForScriptsArr();
        if (is_array($scripts)) {
            foreach ($scripts as $script) {
                $this->addScripts($script);
            }
        } else {
            if (!isStringUrl($scripts)) {
                if (assetFileExists($scripts)) {
                    $filemtime = assetFilemtime($scripts) ?? false;
                    if ($filemtime) $scripts .= '?v=' . $filemtime;
                }
                $scripts = asset($scripts, env('HTTPS'));
            }
            $this->scripts[] = $scripts;
        }
    }

    public function getScripts($defer = false)
    {
        return $this->checkForScriptsArr($defer);
    }

    public function getScriptsString($scripts, $attributes = '')
    {
        $return = '';
        if ($scripts) {
            foreach ($scripts as $script) {
                $return .= ' <script type="text/javascript" ' . $attributes . ' src="' . $script . '"></script>' . PHP_EOL;
            }
        }

        return $return;
    }

    // JS DEFER //
    public function addScriptsDefer($scripts)
    {
        if (!$scripts) return;
        $this->checkForScriptsArr(true);
        if (is_array($scripts)) {
            foreach ($scripts as $script) {
                $this->addScriptsDefer($script);
            }
        } else {
            if (!isStringUrl($scripts)) {

                if (assetFileExists($scripts)) {
                    $filemtime = assetFilemtime($scripts) ?? now();
                    if ($filemtime) $scripts .= '?' . $filemtime;
                }
                $scripts = asset($scripts, env('HTTPS'));
            }
            $this->scriptsDefer[] = $scripts;
        }
    }

    public function loadSettings()
    {
        $this->settings = Setting::getSettings();
    }

    public function loadTranslates($langId)
    {
        $this->translate = Translate::getTranslates($langId);
    }

    public function setTitle($title = '', $replace = false)
    {
        SEOMeta::setTitle($replace ? $title : SEOMeta::getTitle() . ' ' . $title);
    }

    public function setKeywords($keywords = '', $replace = true)
    {
        SEOMeta::setKeywords($replace ? $keywords : SEOMeta::getKeywords() . ' ' . $keywords);
    }

    public function setDescription($description = '', $replace = true)
    {
        SEOMeta::setDescription($replace ? $description : SEOMeta::getDescription() . ' ' . $description);
    }

    public function getTitle()
    {
        return SEOMeta::getTitle();
    }

    public function getDescription()
    {
        return SEOMeta::getDescription();
    }

    public function getKeywords()
    {
        return SEOMeta::getKeywords();
    }
}