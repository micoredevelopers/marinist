<?php


namespace App\Helpers;


use App\Models\Section;
use Illuminate\Support\Facades\Session;

class PartnersChess
{
   const EMPTY = 0;
   const ONE_COLUMN = 1;
   const TWO_COLUMN = 2;
   const THREE_COLUMN = 3;
   const FOUR_COLUMN = 4;
   const FIVE_COLUMN = 5;
   const SIX_COLUMN = 6;

    private $twoColumnArray = ['1-5-6','1-6-2','2-7-2','5-3-1','5-3-2','5-4-1','5-4-2'];
    private $threeColumnArray = ['2-7-1'];
    private $fourColumnArray = ['5-5-1','5-6-1'];
    private $fifthColumnArray = ['1-6-3'];

    private static $emptyColumn = [
        ['first'=>'','second'=>'1-1-1'],
        ['first'=>'3-1-5','second'=>'3-1-7']
    ];

    private $sections;
//foreach($sections as $section)
//foreach($section->floors as $floor)
//foreach($floor->flats as $flat )
    public function __construct( $sections)
    {
        $this->sections = $sections;
    }

    public function getDataForChess()
    {
        $sectionsData = [];

        foreach ($this->sections as $section) {
            $sectionsDataLoop = [];
            foreach ($section->floors as $floor) {
                $floorsData = [];
                foreach($floor->flats as $flat ){
                    $flatData = [];
                    if(in_array($flat->number,$this->twoColumnArray)){

                        $flatData =[
                            'columnCount' => self::TWO_COLUMN,
                            'dataFlat' => $flat
                        ];
                    }
                    else if(in_array($flat->number,$this->threeColumnArray)){
                        $flatData =[
                            'columnCount' => self::THREE_COLUMN,
                            'dataFlat' => $flat
                        ];
                    }
                    else if(in_array($flat->number,$this->fourColumnArray)){
                        $flatData =[
                            'columnCount' => self::FOUR_COLUMN,
                            'dataFlat' => $flat
                        ];
                    }
                    else if(in_array($flat->number,$this->fifthColumnArray)){
                        $flatData =[
                            'columnCount' => self::FIVE_COLUMN,
                            'dataFlat' => $flat
                        ];
                    }
                    else{
                        $flatData =[
                            'columnCount' => self::ONE_COLUMN,
                            'dataFlat' => $flat
                        ];
                    }
                    array_push($floorsData,$flatData);
                }
                array_push($sectionsDataLoop,$floorsData);
            }
           array_push($sectionsData,$sectionsDataLoop);
        }
        return $sectionsData;
    }

    public static function getHeader(int $sectionNumber)
    {
        $data = [
            ['Cмарт','2 ком.','1 ком.','1 ком.','1 ком.','3 ком.','3 ком.','Cмарт'],
            ['Cмарт','2 ком.','1 ком.','1 ком.','1 ком.','1 ком.','1 ком.','1 ком.','1 ком.'],
            ['Cмарт','1 ком.','1 ком.','2 ком.','1 ком.','1 ком.','1 ком.','2 ком.','1 ком.','Cмарт'],
            ['1 ком.','1 ком.','1 ком.','Cмарт','1 ком.','1 ком.','1 ком.','2 ком.','1 ком.','1 ком.'],
            ['1 ком.','3 ком.','3 ком.','1 ком.','2 ком.','1 ком.','2 ком.','2 ком.'],
        ];
        return $data[$sectionNumber];
    }

    public static function getRow(array $data,$numberFloor)
    {
        $row = '';
        foreach ($data as $key=>$item)
        {
            $checkColSpan = $item['columnCount']>self::ONE_COLUMN?'colspan="'.$item['columnCount'].'"':'';
            $tableData='';
            foreach (self::$emptyColumn as $emptyColumn)
            {
                if($emptyColumn['first']=='' && $emptyColumn['second']==$item['dataFlat']->number){
                    $tableData.='<td></td>';
                }
                else if ($key>1 && $emptyColumn['first']== $data[$key-1]['dataFlat']->number && $emptyColumn['second']==$item['dataFlat']->number)
                {
                    $tableData.='<td></td>';
                }
            }
            $terrace = $item['dataFlat']->terrace?'Терасса':'';
            $tableData.='<td  '.$checkColSpan.' class="'.$item['dataFlat']->status->status.'">
                                    <span class="td-title border-bottom pb-2 max-width-300 mx-auto">№'.$item['dataFlat']->number.' '.$terrace.'</span>
                                    <div class="d-flex justify-content-between border-bottom py-2 max-width-300 mx-auto">
                                        <span class="td-medium-num">'.$item['dataFlat']->square.'м²</span>
                                        <span class="td-medium-num">'.$item['dataFlat']->sum.'</span>
                                    </div>
                                    <div class="border-bottom py-2 max-width-300 mx-auto">
                                        <div class="d-flex justify-content-between ">
                                            <span class="td-light-num">25%</span>
                                            <span class="td-light-num">'.$item['dataFlat']->twenty_five_percent.'</span>
                                        </div>
                                        <div class="d-flex justify-content-between ">
                                            <span class="td-light-num">50%</span>
                                            <span class="td-light-num">'.$item['dataFlat']->fifty_percent.'</span>
                                        </div>
                                        <div class="d-flex justify-content-between ">
                                            <span class="td-light-num">75%</span>
                                            <span class="td-light-num">'.$item['dataFlat']->seventy_five_percent.'</span>
                                        </div>
                                    </div>
                                    <div class="d-flex justify-content-between  max-width-300 mx-auto pt-2">
                                        <span class="td-medium-num">100%</span>
                                        <span class="td-medium-num">'.$item['dataFlat']->one_hundred_percent.'</span>
                                    </div>
                                    
                     </td>';
            $row.=$tableData;
        }

        $outputData = '<tr>
                            <th scope="row" class="text-center align-middle first-col ">'.$numberFloor.'</th>
                            '.$row.'
                        </tr>  ';
        return $outputData;
    }
}
