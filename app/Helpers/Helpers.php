<?php
function isLast($array, $key)
{
    $last_key = key(array_slice($array, -1, 1, true));
    if ($last_key === $key) {
        return true;
    }

    return false;
}

/**
 * @param $array
 * @param $key
 * @param $value
 * Метод генерирует массив ключ значение из переданного массива где ключем является переданный ключ
 * а значением соответственно поле value
 */
function makeRowsKey($array, $key, $value)
{
    $return = [];
    foreach ($array as $item) {
        $return[$item[$key]] = $item[$value];
    }

    return $return;
}

function rowsKeyObject($object, $key, $value)
{
    $return = [];
    foreach ($object as $item) {
        $return[$item->$key] = $item->$value;
    }

    return $return;
}

/**
 * @param        $array
 * @param string $id = 'id'
 *
 * @return array
 * Метод оборачивает каждый вложенный массив укзанным идентификатором
 * $array[0] = ['id' => 10, 'name' => 'John']
 * Превратит в
 * $array[10] = ['id' => 10, 'name' => 'John']
 */
function keyBy($array, $id = 'id')
{
    $return = [];
    foreach ($array as $item) {
        $return[$item[$id]] = $item;
    }

    return $return;
}

function objectKeyBy($object, $id = 'id')
{
    $return = [];
    foreach ($object as $item) {
        $return[$item->$id] = $item;
    }

    return $return;
}

function checkImage($src, $default = 'images/default.png', $disk = null)
{
    return storageFileExists($src, $disk) ? getStorageFilePath($src, $disk) : asset($default);
}

function prePub($path)
{
    return 'public/' . $path;
}

function isStringUrl($string)
{
    $http = \Str::startsWith($string, 'http://');
    $https = \Str::startsWith($string, 'https://');
    $double = \Str::startsWith($string, '//');
    return ($http OR $https OR $double);
}

function br()
{
    return '<br>';
}

function addBrIfTrue($str)
{
    if ($str) {
        $str .= br();
    }

    return $str;
}

function ebr($str = '')
{
    echo $str . br();
}

function imgOrigin($path)
{
    return \App\Http\Libs::imgOrig($path);
}

/**
 * @param $filePath
 *
 * @return string Получить путь к файлу в хранилище
 */
function getStorageFilePath($filePath, $disk = null)
{
    return \App\Helpers\StorageHelper::filePath($filePath, $disk);
}

function storageFileExists($filePath, $disk = null)
{
    return \App\Helpers\StorageHelper::fileExists($filePath, $disk);
}

function storageFilemtime($filePath, $disk = null)
{
    return \App\Helpers\StorageHelper::lastModified($filePath, $disk);
}

function storageDelete($filePath, $disk = null)
{
    return \App\Helpers\StorageHelper::delete($filePath, $disk);
}

function assetFileExists($filePath)
{
    return file_exists(public_path('/' . $filePath));
}

function assetFilemtime($filePath)
{
    if (!assetFileExists($filePath)) {
        return time();
    }

    return filemtime(public_path('/' . $filePath));
}

function getCurrentLocale()
{
    static $lang = null;
    if (is_null($lang)) {
        $lang = \LaravelLocalization::getCurrentLocale();
    }

    return $lang;
}

function isMenuActive($url)
{
    $lang = getCurrentLocale();
    $langUrl = $lang . '/' . $url;
    if (
        \Request::is($url) OR
        \Request::is($url . '/*') OR
        \Request::is($langUrl) OR
        \Request::is($langUrl . '/*')
    ) {
        return true;
    }

    return false;
}

function isAdminMenuActive($url)
{
    $url = 'admin/' . $url;
    $lang = getCurrentLocale();
    $langUrl = $lang . '/' . $url;
    if (
        \Request::is($url) OR
        \Request::is($url . '/*') OR
        \Request::is($langUrl) OR
        \Request::is($langUrl . '/*')
    ) {
        return true;
    }

    return false;
}

function langUrl($url, $locale = false): string
{
    $localeCode = $locale ?: getCurrentLocale();

    return \LaravelLocalization::getLocalizedURL($localeCode, $url, [], false);
}

function getNonLocaledUrl($url = null)
{
    if (is_null($url)) {
        $url = request()->getPathInfo();
    }

    return \LaravelLocalization::getNonLocalizedURL($url);
}

function urlWithoutPublic($url)
{
    return \Str::replaceFirst('/public', '', $url);
}

function getUrlWithoutHost($url)
{
    if (isLocalhost()) {
        $domain = (env('APP_ENV') === 'local') ? env('APP_URL') : request()->getHost();
        $url = urlWithoutPublic($url);
        return \Str::replaceFirst($domain, '', $url);
    }
    $protocol = getProtocol();
    $domain = request()->getHost();
    $url = urlWithoutPublic($url);
    $clean = \Str::replaceFirst($protocol . '://' . $domain, '', $url);
    return $clean;
}

function getHost()
{
    return request()->getHost();
}

function getProtocol()
{
    return request()->isSecure() ? 'https' : 'http';
}

function isAdmin()
{
    return (\Auth::check() AND \Auth::user()->hasAnyRole(\App\Role::all()));
}

function isSuperAdmin()
{
    /** @var $user \App\User */
    $user = \Auth::user();
    return $user ? (bool)$user->IsSuperAdmin() : false;
}


function isMenuActiveByUrl($url)
{
    $langUrl = langUrl($url);
    $nonLang = getNonLocaledUrl($langUrl);

    return (
        (request()->url() === $langUrl)
        OR (request()->url() === $nonLang)
        OR \Str::startsWith(request()->url(), $langUrl . '/')
        OR \Str::startsWith(request()->url(), $nonLang . '/')
    );
}

function isAdminMenuActiveByRoute($route)
{
    return ($route AND \Route::is($route));
}

function isMenuChildrenIsActive($childrens)
{
    if (is_array($childrens)) {
        foreach ($childrens as $children) {
            if ($children->url AND isMenuActiveByUrl($children->url)) {
                return true;
            } else if ($children->route AND isAdminMenuActiveByRoute($children->route)) {
                return true;
            }
        }
    }

    return false;
}

function isLocalhost()
{
    return env('APP_ENV') === 'local';
}

function ip_info($ip = null, $purpose = "location", $deep_detect = true)
{
    $output = null;
    if (filter_var($ip, FILTER_VALIDATE_IP) === false) {
        $ip = $_SERVER["REMOTE_ADDR"];
        if ($deep_detect) {
            if (isset($_SERVER['HTTP_X_FORWARDED_FOR']) AND filter_var($_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP)) {
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            }
            if (isset($_SERVER['HTTP_CLIENT_IP']) AND filter_var($_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP)) {
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            }
        }
    }
    $purpose = str_replace(["name", "\n", "\t", " ", "-", "_"], null, strtolower(trim($purpose)));
    $support = ["country", "countrycode", "state", "region", "city", "location", "address"];
    $continents = [
        "AF" => "Africa",
        "AN" => "Antarctica",
        "AS" => "Asia",
        "EU" => "Europe",
        "OC" => "Australia (Oceania)",
        "NA" => "North America",
        "SA" => "South America",
    ];
    if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {
        $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
        if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
            switch ($purpose) {
                case "location":
                    $output = [
                        "city"           => @$ipdat->geoplugin_city,
                        "state"          => @$ipdat->geoplugin_regionName,
                        "country"        => @$ipdat->geoplugin_countryName,
                        "country_code"   => @$ipdat->geoplugin_countryCode,
                        "continent"      => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
                        "continent_code" => @$ipdat->geoplugin_continentCode,
                    ];
                    break;
                case "address":
                    $address = [$ipdat->geoplugin_countryName];
                    if (@strlen($ipdat->geoplugin_regionName) >= 1) {
                        $address[] = $ipdat->geoplugin_regionName;
                    }
                    if (@strlen($ipdat->geoplugin_city) >= 1) {
                        $address[] = $ipdat->geoplugin_city;
                    }
                    $output = implode(", ", array_reverse($address));
                    break;
                case "city":
                    $output = @$ipdat->geoplugin_city;
                    break;
                case "state":
                    $output = @$ipdat->geoplugin_regionName;
                    break;
                case "region":
                    $output = @$ipdat->geoplugin_regionName;
                    break;
                case "country":
                    $output = @$ipdat->geoplugin_countryName;
                    break;
                case "countrycode":
                    $output = @$ipdat->geoplugin_countryCode;
                    break;
            }
        }
    }

    return $output;
}

function showEditor($id)
{
    return "
    CKEDITOR.replace('" . $id . "', {
    filebrowserBrowseUrl: '/elfinder/ckeditor',
    filebrowserImageBrowseUrl: '/elfinder/ckeditor',
    uiColor: '#9AB8F3',
    height: 300
    });
    ";
}

function toLower($str, $encode = 'UTF-8')
{
    return mb_strtolower($str, $encode);
}

function dumpAdmin()
{
    if (isSuperAdmin()) {
        dump(debug_backtrace()[0]['file'] . ' | line: ' . debug_backtrace()[0]['line']);
        $vars = func_get_args();
        dump(...$vars);
    }
}

function getDefaultLang($column = 'id')
{
    $lang = \App\Models\Language::default()->first();
    return $column ? Arr::get($lang, $column) : $lang;
}

function getLang()
{
    return \App\Helpers\LanguageHelper::getLanguageId() ?? getDefaultLang();
}

function setLang($lang)
{
    \App\Helpers\LanguageHelper::setLanguageId($lang);
}

function htmlLinkString($link, $text = null, $target = '_blank', $class = '')
{
    $text = $text ?? $link;

    return '<a class="' . $class . '" target="' . $target . '" href="' . $link . '">' . $text . '</a>';
}

function isLink($str = null)
{
    return filter_var($str, FILTER_VALIDATE_URL);
}

function getTranslate($key, $defaultText = '', $asObject = false)
{
    return \App\Models\Translate::getTranslate($key, $asObject) ?? $defaultText;
}

function translateFormat($key, array $values)
{
    /** @var $translate \App\Models\Translate */
    $translate = getTranslate($key, false, true);
    if ($translate) {
        return $translate->format(...$values);
    }
    return '';
}

function translateYesNo($condition)
{
    return $condition ? getTranslate('global.yes') : getTranslate('global.no');
}

function getSetting($key, $default = null)
{
    return \App\Models\Setting::getSetting($key) ?? $default;
}

function setting($key, $asValue = false)
{
    return \App\Models\Setting::getSetting($key, $asValue);
}

function settingFile($key)
{
    $path = '';
    $value = Arr::get(setting($key), 'value');
    if ($value AND isJson($value)) {
        $first = Arr::get(\json_decode($value, true), 0);
        $path = Arr::get($first, 'download_link');
        if (storageFileExists($path))
            $path = getStorageFilePath($path);
    }
    return $path;
}

function settingFiles($key, $array = true)
{   
    $files = [];
    $value = Arr::get(setting($key), 'value');
    if ($value AND isJson($value)) {
        $files = collect(\json_decode($value, true));
        $files = $files->map(function ($item) {
            $link = storageFileExists(Arr::get($item, 'download_link'))
                ? getStorageFilePath(Arr::get($item, 'download_link'))
                : '';
            Arr::set($item, 'download_link', $link);
            return $item;
        });
        if ($array)
            $files = $files->toArray();
    }
    return $files;
}

function normalizePath($path)
{
    $replace = [
        '\\'   => '/',
        '\\\\' => '/',
    ];

    return str_replace(array_keys($replace), array_values($replace), $path);
}

function getExtensionByName($filename)
{
    $partsFilename = explode('.', $filename);

    return end($partsFilename);
}

function getFilenameWithoutExtension($filename)
{
    $partsFilename = explode('.', $filename);
    array_pop($partsFilename);

    return implode('.', $partsFilename);
}

function classImplementsInterface($class, $interface): bool
{
    $interfaces = class_implements($class);
    return \Arr::has($interfaces, $interface);
}

function getMeta()
{
    return \App\Models\Meta::getMetaData();
}

function showMeta($value, $field = 'h1')
{
    return \Arr::get(getMeta(), $field, $value);
}

function getYoutubeVideoCodeFromLink($link)
{
    $exploded = explode('watch?v=', $link);

    return end($exploded);
}

function getPreviewYoutubeFromLink($link, $imageSize = 'hqdefault')
{
    $link = getYoutubeVideoCodeFromLink($link);

    return 'https://img.youtube.com/vi/' . $link . '/' . $imageSize . '.jpg';
}

function getYoutubeIframe($link)
{
    $code = getYoutubeVideoCodeFromLink($link);
    return '<iframe width="100%" height="100%" src="https://www.youtube.com/embed/' . $code . '"
			 frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" 
			 allowfullscreen></iframe>';
}

function selectedIfTrue($condition)
{
    return ($condition) ? 'selected="selected"' : '';
}

function checkedIfTrue($condition)
{
    return ($condition) ? 'checked="checked"' : '';
}

function isJson($str)
{

    \json_decode($str);

    if (\json_last_error()) {
        return false;
    }

    return true;
}

function getDevMessage($message)
{
    $str = '';
    if (isLocalhost()) {
        $str = '<span class="badge badge-danger">Данный блок виден только на локалке</span>';
        $str .= $message;
    }
    return $str;
}

function concatWords()
{
    return implode(' ', func_get_args());
}

function implodeComma(array $array, $glue = ',')
{
    return implode($glue, $array);
}

function toNegative($number)
{
    return -1 * abs($number);
}

function d()
{
    if (isLocalhost()) {
        $backtrace = debug_backtrace();
        $trace = array_shift($backtrace);
        $prevTrace = array_shift($backtrace);
        $methodName = Arr::get($prevTrace, 'function');
        $traceText = Arr::get($trace, 'file') . ' ' . $methodName . ' ' . Arr::get($trace, 'line');
        dd($traceText, ...func_get_args());
    }
}

if (!function_exists('debugInfo')) {
	function debugInfo()
	{
		\Debugbar::info(
			...func_get_args()
		);
	}
}
