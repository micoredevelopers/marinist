<?php

namespace App\Helpers;

use App\Contracts\GetMinMaxValue;
use App\Models\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class FlatParametersHelper
{
	protected $request;

	public function __construct(Request $request)
	{
		$this->request = $request;
	}

	public function markSection(Collection $sections)
	{

		$sections->map(function ($item) {
			$item->sectionFrom = ((int)$this->request->get('sectionFrom') === (int)$item->number) ? true : false;
			$item->sectionTo = ((int)$this->request->get('sectionTo') === (int)$item->number) ? true : false;
			return $item;
		});
		return $sections;
	}

	public function markFloors(Collection $floors)
	{
		$floors->map(function ($item) {
			$item->floorFrom = ((int)$this->request->get('floorFrom') === (int)$item->number) ? true : false;
			$item->floorTo = ((int)$this->request->get('floorTo') === (int)$item->number) ? true : false;
			return $item;
		});
		return $floors;
	}

	public function markRooms(Collection $rooms)
	{
		$rooms->map(function ($item) {
			$item->roomFrom = ((int)$this->request->get('roomFrom') === (int)$item->room) ? true : false;
			$item->roomTo = ((int)$this->request->get('roomTo') === (int)$item->room) ? true : false;
			return $item;
		});
		return $rooms;
	}

	public function markSquares(Collection $squares)
	{
        $squares = $squares
            ->map(function ($item) {
			$item->squareFrom = ((int)$this->request->get('squareFrom') === $item->square);
			$item->squareTo = ((int)$this->request->get('squareTo') === (int)$item->square);
			return $item;
		});
		return $squares;
	}

	public function markSeaView()
	{
		return (int)$this->request->get('seaView') === 1;
	}

	public function markTerrace()
	{
		return (int)$this->request->get('terrace') === 1;
	}

	public function markStock()
	{
		return (int)$this->request->get('stock') === 1;
	}

	public function minMaxValue(GetMinMaxValue $item){
	    return Collection::make([
	        'min'=>$item->getMin(),
            'max'=>$item->getMax()
        ]);
    }
}
