<?php
Route::group(
	[
		'prefix'     => LaravelLocalization::setLocale(),
		'middleware' => ['localizationRedirect', 'localeViewPath'],
	], function () {

	Route::get('/', 'IndexController@index')->name('home');
	Route::get('/translates', 'IndexController@translates')->name('translates');
	//
	Route::group(['prefix' => 'news'], function () {
		Route::get('/', 'NewsController@index')->name('news');
		Route::get('/more', 'NewsController@getMore')->name('news.more');
		Route::get('/{news}', 'NewsController@show')->name('news.single');
	});
	Route::get('/progress', 'ProgressController@index')->name('progress');
    Route::get('/progress-item', 'ProgressController@getProgressData')->name('progress.item');
	Route::get('/gallery', 'GalleryController@index')->name('gallery');
    Route::get('/gallery-item', 'GalleryController@getGalleryData')->name('gallery.item');
	//

	Route::group(['prefix' => 'flat'], function () {
		Route::get('/', 'FlatController@index')->name('flat');
		Route::get('/data/sections', 'FlatController@sections')->name('flat.data.sections');
		Route::get('/data/floors', 'FlatController@floors')->name('flat.data.floors');
		Route::get('/data/floor', 'FlatController@floor')->name('flat.data.floor');
		Route::get('/data/flats', 'FlatController@flats')->name('flat.data.flats');
		Route::get('/data/flat', 'FlatController@flat')->name('flat.data.flat');
	});
	//
	Route::get('/parameters', 'FlatController@parameters')->name('parameters');
	Route::get('/about', 'PageController@about')->name('about');
	Route::get('/about-developer', 'PageController@aboutDeveloper')->name('about-developer');
	Route::get('/contact', 'PageController@contact')->name('contact');
	Route::get('/filter', 'PageController@filter')->name('filter');
	Route::get('/get-flat-pdf', 'PageController@getFlatPdf')->name('getFlatPdf');
	Route::get('/single-flat', 'PageController@singleFlat')->name('singleFlat');
	Route::get('/chelseaclub', 'PageController@club')->name('club');

	Route::group(['prefix' => 'request'], function () {
		Route::get('/sign', 'RequestController@signView')->name('request.sign');
		Route::get('/feedback', 'RequestController@feedback')->name('request.feedback');
	});

    Route::get('/login', 'LoginPartnerController@index')->name('partners.login.index');
    Route::post('/login', 'LoginPartnerController@login')->name('partners.login');
    Route::group(['prefix' => 'partners'], function () {
        Route::get('/', 'PartnersController@index')->name('partners.index');
        Route::get('/mail', 'PartnersController@mail')->name('partners.mail');
    });

    //Api url
    Route::group(['prefix'=>'api'],function () {
        Route::get('/get-params-data', 'FlatController@getParamsData')->name('getParamsData');
    });

//Admin
	include_once "web_admin.php";
});
