<?php

Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {
	Route::get('/login', 'Auth\MyAuthController@showLogin')->name('admin.login');
	Route::post('/login', 'Auth\MyAuthController@authenticate');
	Route::post('/logout', 'Auth\MyAuthController@logout')->name('admin.logout');
});
Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'father'], function () {
	Route::get('/', 'IndexController@index')->name('admin.index');
    Route::get('/clear/cache', 'IndexController@clearCache')->name('admin.clear.cache');
    Route::get('/make/sitemap', 'IndexController@makeSitemap')->name('admin.make.sitemap');
	Route::post('upload', ['uses' => 'AdminController@upload', 'as' => 'upload']);
	Route::resource('users', 'UserController');
	Route::get('user/profile', 'UserController@profile')->name('user.profile');
	Route::post('user/profile', 'UserController@profileUpdate')->name('user.profile.update');
	Route::resource('roles', 'RoleController');
	//partners
	Route::resource('/partners','PartnersController',[
        'as' => 'admin',
        'except'=>['show']
    ]);
    Route::resource('/reg-partner','RegisterPartnerController',[
        'as' => 'admin',
        'except'=>['show']
    ]);
	//media
	Route::resource('galleries', 'GalleryController');
	Route::resource('sliders', 'SliderController');
	Route::resource('news', 'NewsController');
	Route::post('/news/normalize-date-pub', 'NewsController@normalizeDatePub')->name('news.normalize-date');
	Route::resource('infoblocks', 'InfoblockController');
	//
	Route::resource('translate', 'TranslateController',['except'=>['show']]);
    Route::get('/translate/ck', 'TranslateController@ckeditor')->name('translate.ckeditor');
	Route::resource('settings', 'SettingController');
	Route::resource('meta', 'MetaController');
	Route::resource('admin-menus', 'AdminMenuController');
	Route::resource('menu', 'MenuController');
	Route::resource('pages', 'PageController');
	Route::resource('floor', 'FloorController', [
		'except' => ['show', 'create'],
	]);
	Route::resource('section', 'SectionController', [
		'only' => ['update', 'edit'],
	]);
	Route::resource('flat', 'FlatController', [
		'except' => ['show', 'create'],
	]);
	Route::group(['prefix' => 'progress'], function () {
		Route::get('/', 'ProgressController@index')->name('progress.index');
		Route::post('/', 'ProgressController@update')->name('progress.update');
		Route::resource('galleries', 'ProgressGalleryController', ['as' => 'progress']);
	});
	Route::group(['prefix' => 'photos'], function () {
		Route::post('/edit', ['uses' => 'PhotosController@edit']);
		Route::post('/delete', ['uses' => 'PhotosController@delete']);
		Route::match(['get', 'post'], '/get-cropper', ['uses' => 'PhotosController@getPhotoCropper']);
	});
	Route::group(['as' => 'settings.', 'prefix' => 'settings',], function () {
		Route::get('{id}/delete_value', [
			'uses' => 'SettingController@delete_value',
			'as'   => 'delete_value',
		]);
	});


    Route::group(['prefix'=>'seo','as'=>'admin.seo.'], function () {
        Route::get('/','SeoController@index')->name('index');
        Route::get('/robots','SeoController@robots')->name('robots.index');
        Route::put('/robots','SeoController@robotsUpdate')->name('robots.update');
        Route::get('/sitemap','SeoController@sitemap')->name('sitemap.index');
	});

	// Служебные роуты, такие как elfinder
	Route::get('glide/{path}', function ($path) {
		$server = \League\Glide\ServerFactory::create([
			'source' => app('filesystem')->disk('public')->getDriver(),
			'cache'  => storage_path('glide'),
		]);
//
		return $server->getImageResponse($path, Input::query());
	})->where('path', '.+');
	Route::group(['prefix' => 'ajax'], function () {
		Route::post('/sort', 'AjaxController@sort')->name('sort');
	});

	Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
});
