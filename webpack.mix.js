const mix = require('laravel-mix');

// mix.setResourceRoot('/marinist');

mix.copyDirectory('resources/images', 'public/images');
mix.copyDirectory('resources/fonts/AvenirNextCyr', 'public/fonts/AvenirNextCyr');
mix.copyDirectory('resources/fonts/Glyphicons', 'public/fonts/Glyphicons');
mix.sass('resources/sass/main.scss', 'public/css')
    .sass('resources/sass/material-dashboard.scss', 'public/css/admin')
    .sass('resources/sass/admin-main.scss', 'public/css/admin')
    .sass('resources/sass/styles.scss', 'public/css/admin')
    .js('resources/js/index.js', 'public/js')
;
