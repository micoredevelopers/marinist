<header class="header-main is-collapsed">
    {!! \Arr::get($sections, 'header-before') !!}
    <section class="mobile-header d-flex d-xl-none">
        {!! \Arr::get($sections, 'header-mobile-header') !!}

        {{--<a href="#" class="documentation-link">--}}
        {{--    <img class="documentation-icon" src="{{ asset('images/pdf-icon.svg') }}" alt="Скачать презентацию">--}}
        {{--    <span class="documentation-text">Скачать презентацию</span>--}}
        {{--</a>--}}
        <a href="/" class="mobile-logo">
            <img data-src="{{ asset('images/logo-default-black.png') }}" class="lazy-img" alt="Маринист">
        </a>
        <div class="phone-wrap">
            <img class="phone-icon" src="{{ asset('images/call.svg') }}" alt="Телефон" uk-svg>
            <a href="tel:{{ getSetting('social.phone_menu') }}" class="phone-link binct-phone-number-1">
                {{ getSetting('social.phone_menu') }}
            </a>
        </div>

        <div class="right-content">
            <?php /** @var $language \App\Models\Language */ ?>
                @isset($languages)
                <div class="dropdown">
                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{ getCurrentLocale() }}
                    </button>
                    <div class="dropdown-menu">
                        @foreach($languages as $language)
                            @if ($language->key !== getCurrentLocale())
                                <a class="dropdown-item" href="{{ langUrl(null, $language->key) }}">{{ $language->key }}</a>
                            @endif
                        @endforeach
                    </div>
                </div>
            @endisset
            <div class="humb-menu humb-menu_mobile">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
    </section>
    <section class="desktop-header d-none d-xl-flex">
        <div class="top-content">
            <div class="top-content__nav d-flex justify-content-end align-items-center">
                <form action="">
                    @isset($languages)
                        <div class="dropdown menu-lang-select-wrap">
                            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ getCurrentLocale() }}
                            </button>
                            <div class="dropdown-menu">
                                @foreach($languages as $language)
                                    @if ($language->key !== getCurrentLocale())
                                        <a class="dropdown-item" href="{{ langUrl(null, $language->key) }}">{{ $language->key }}</a>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    @endisset
                </form>
                <div class="humb-menu humb-menu_desktop">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
            <div class="logo-wrap">
                <a class="logo-link" href="{{ route('home') }}">
                    <img class="logo-image logo-image_minimized lazy-img d-none" data-src="{{ asset('images/logo-min.png') }}" alt="Маринист">
                    <img class="logo-image logo-image_default lazy-img" data-src="{{ asset('images/logo-default.png') }}" alt="Маринист">
                </a>
            </div>
        </div>
        @isset($menus)
            <?php /** @var $menu \App\Models\Menu */ ?>
            <div class="menu-box">
                @foreach($menus as $menu)
                    <a href="{{ $menu->getUrl() }}" class="menu-box-item {!! isMenuActiveByUrl($menu->getUrl())?'active':'' !!}"  data-toggle="tooltip" data-placement="right" title="{{ $menu->name }}" {!! $menu->getRel() !!}
                            {!! $menu->getTarget() !!} data-sidebar-menu-item="{{ $menu->getKey() }}">
                        <img class="menu-box-item-img" src="{{ checkImage($menu->photo) }}" alt="{{ $menu->name }}" uk-svg>
                        <span>{{ $menu->name }}</span>
                    </a>
                @endforeach
            </div>
        @endisset
        <div class="bottom-content">
            @if ( getSetting('social.phone_menu') )
                <div class="phone-wrap">
                    <img class="call-icon" src="{{ asset('images/call.svg') }}" alt="Телефон" uk-svg>
                    <span class="phone-number binct-phone-number-1">{{ getSetting('social.phone_menu') }}</span>
                </div>
            @endif
            <div class="socials-wrap d-flex justify-content-center">
                <a href="{{ getSetting('social.facebook') }}" class="social-link" target="_blank" rel="nofollow">
                    <img src="{{ asset('images/facebook-header.svg') }}" alt="Facebook" uk-svg>
                </a>
                <a href="{{ getSetting('social.instagram') }}" class="social-link" target="_blank" rel="nofollow">
                    <img src="{{ asset('images/instagram-header.svg') }}" alt="Instagram" uk-svg>
                </a>
                <a href="{{ getSetting('social.youtube') }}" class="social-link" target="_blank" rel="nofollow">
                    <img src="{{ asset('images/youtube-white.svg') }}" alt="Youtube" uk-svg>
                </a>
            </div>
            <button class="button-wave" data-toggle="modal" data-target="#modalAppointment">
                <span class="wave-img"></span>
                <span class="button-text">{{ getTranslate('request.sign-to-view') }}</span>
            </button>

            <div class="header-text">
                <p>ТОВ "Маячный"</p>
            </div>
        </div>
        <div class="button-appointment_minified-wrap d-none">
            <button class="button-appointment_minified" data-toggle="modal" data-target="#modalAppointment">
                <span class="iconify" data-icon="simple-line-icons:phone" data-inline="false"></span>
            </button>
        </div>
    </section>

    {!! \Arr::get($sections, 'header-after') !!}
</header>
