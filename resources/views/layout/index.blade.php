<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">

<head>
{!! SEOMeta::generate() !!}
<!-- Google Tag Manager -->
    <script>(function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-MM5SX6N');</script>
    <!-- End Google Tag Manager -->
    <meta charset="utf-8"/>
    <meta property="og:title" content="MARINIST residence - дом, вдохновлённый морем!">
    <meta property="og:type" content="site"/>
    <meta property="og:url" content="http://marinist.com.ua/"/>
    <meta property="og:image" content="http:///marinist.com.ua/images/meta-image.jpg"/>

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.1.5/css/uikit.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/css/swiper.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.0/css/ion.rangeSlider.min.css"/>
    <link rel="stylesheet" href="{{ asset('css/main.css')}}?v={{ assetFilemtime('css/main.css') }}"/>
    <link rel="canonical" href="{{$canonical??''}}">
    <link hreflang="ru" href="{{$localizedUrl['ru']??'#'}}" rel="alternate">
    <link hreflang="en" href="{{$localizedUrl['en']??'#'}}" rel="alternate">
    <link hreflang="uk" href="{{$localizedUrl['uk']??'#'}}" rel="alternate">
    {!! $styles !!}
    {!! \Arr::get($sections, 'css') !!}
    <link rel="icon" type="image/ico" href="{{ asset('favicon.ico') }}">
    <script type="application/ld+json">
		{
			"@context": "http://schema.org",
			"@type": "WebSite",
			"name": "Мarinist Residence",
			"url": "http://marinist.com.ua/"
		}





    </script>
    <script type="application/ld+json">
		{
			"@context": "http://schema.org",
			"@type": "Organization",
			"name": "Мarinist Residence",
			"telephone": "+380487008833",
			"email": "info@marinist.com.ua",
			"address": {
				"@type": "PostalAddress",
				"streetAddress": "пер. Маячный,7",
				"addressLocality": "Одесса",
				"addressRegion": "Одесская область",
				"addressCountry": "UA"
			}
		}
    </script>

    <!-- Meta Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1307506992762075');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=1307506992762075&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Meta Pixel Code -->
</head>

<body>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MM5SX6N" height="0" width="0"
            style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->


<!--AMO CRM-->
{{--<script>--}}
{{--    var amo_social_button = {--}}
{{--        id: "17639", hash: "bac04ba897fd94fc33558459b030f418a6fd4767ffdce629639e7afba115faa0",--}}
{{--        locale: "ru",--}}
{{--        setMeta: function (params) {--}}
{{--            this.params = this.params || [];--}}
{{--            this.params.push(params);--}}
{{--        }--}}
{{--    };--}}
{{--</script>--}}
{{--<script id="amo_social_button_script" async="async" src="https://gso.amocrm.ru/js/button.js"></script>--}}
<script>

    (function (w, d, u) {

        var s = d.createElement('script');
        s.async = true;
        s.src = u + '?' + (Date.now() / 60000 | 0);

        var h = d.getElementsByTagName('script')[0];
        h.parentNode.insertBefore(s, h);

    })(window, document, 'https://crm.marinist.com.ua/upload/crm/site_button/loader_2_5xxbqi.js');

</script>
<!--END AMO CRM-->

<div class="background-waves-box"></div>
@include('layout.header')

{!! $content ?? '' !!}

<div class="mobile-menu d-flex d-xl-none">
    <ul class="mobile-menu__nav-list">

        @isset($menus)
            <?php /** @var $menu \App\Models\Menu */ ?>
            @foreach($menus as $menu)
                <li class="mobile-menu__nav-list__item">
                    <a class="list-link {!! isMenuActiveByUrl($menu->getUrl())?'active':'' !!}"
                       href="{{ $menu->getUrl() }}"
                       {!!$menu->getRel() !!}
                       {!! $menu->getTarget() !!}
                       data-sidebar-menu-item="{{ $menu->getKey() }}"
                       title="{{ $menu->name }}">
                        <img class="list-icon" src="{{ checkImage($menu->photo) }}"
                             alt="{{ $menu->name }}" uk-svg>
                        <span class="list-text">{{ $menu->name }}</span>
                    </a>
                </li>
            @endforeach
        @endisset

        {!! \Arr::get($sections, 'sidebar') !!}
    </ul>
    <div class="mobile-menu__bottom">
        <div class="mobile-menu__bottom__socials">
            <a class="mobile-menu__bottom__socials__link" href="{{ getSetting('social.facebook') }}" target="_blank"
               rel="nofollow">
                <img class="facebook-icon" src="{{ asset('images/facebook-white.svg') }}" alt="Facebook">
            </a>
            <a class="mobile-menu__bottom__socials__link" href="{{ getSetting('social.instagram') }}" target="_blank"
               rel="nofollow">
                <img class="instagram-icon" src="{{ asset('images/instagram-white.svg') }}" alt="Instagram">
            </a>
            <a class="mobile-menu__bottom__socials__link" href="{{ getSetting('social.youtube') }}" target="_blank"
               rel="nofollow">
                <img class="instagram-icon" src="{{ asset('images/youtube-white.svg') }}" alt="Youtube">
            </a>
        </div>
        <button class="button-wave" data-toggle="modal" data-target="#modalAppointment">
            <span class="wave-img"></span>
            <span class="button-text">{{ getTranslate('request.sign-to-view') }}</span>
        </button>
        <div class="mobile-menu-text">
            <p>ТОВ "Маячный"</p>
        </div>
    </div>
</div>

@include('partials.feedback_modal')

<!-- Modal success -->
<div class="modal fade" id="modalSuccess" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-center"></h5>
            </div>
            <div class="modal-body">
                <button type="button" class="button-wave" data-dismiss="modal">
                    <span class="wave-img"></span>
                    <span class="button-text">Окей</span>
                </button>
            </div>
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.0/js/ion.rangeSlider.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/js/swiper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.1.5/js/uikit.js"></script>
<script src="https://code.iconify.design/1/1.0.2/iconify.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>
<!--{!! $styles !!}-->
<!--{!! $scripts !!}-->
<!--{!! $scriptsDefer !!}-->
<script src="{{ asset('js/index.js') }}?v={{ assetFilemtime('js/index.js')}}" defer></script>

@stack('js')

{!! \Arr::get($sections, 'javascript') !!}
@if(!isLocalhost())
    <script type="text/javascript">
        (function (d, w, s) {
            var widgetHash = '3iykxndxs3otdfpyj7er',
                gcw = d.createElement(s);
            gcw.type = 'text/javascript';
            gcw.async = true;
            gcw.src = '//widgets.binotel.com/getcall/widgets/' + widgetHash + '.js';
            var sn = d.getElementsByTagName(s)[0];
            sn.parentNode.insertBefore(gcw, sn);
        })(document, window, 'script');
    </script>

    <script type="text/javascript">
        (function (d, w, s) {
            var widgetHash = 'tusp06h4xzy646rkwheq',
                ctw = d.createElement(s);
            ctw.type = 'text/javascript';
            ctw.async = true;
            ctw.src = '//widgets.binotel.com/calltracking/widgets/' + widgetHash + '.js';
            var sn = d.getElementsByTagName(s)[0];
            sn.parentNode.insertBefore(ctw, sn);
        })(document, window, 'script');
    </script>
@endif
<script src="https://static.thefloors.io/thefloors-utm-source.js"></script>
{{--<!-- thefloors widget -->--}}
{{--<script type='text/javascript'>--}}
{{--    (function(w, d){--}}
{{--        var s = d.createElement('script');s.async = true;s.src = 'https://api.thefloors.io/v1/site/333/widget';--}}
{{--        var tscs = d.getElementsByTagName('script');--}}
{{--        var ts = tscs[tscs.length-1];--}}
{{--        ts.parentNode.insertBefore(s,ts);--}}
{{--    })(window, document);--}}
{{--</script>--}}
{{--<!-- end -->--}}

</body>

</html>
