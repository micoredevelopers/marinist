@can('edit_'.$entity)
    <a href="{{ route($route.'.edit', [Str::singular($route) => $id])  }}" class="dropdown-item">️Редактировать</a>
@endcan

@if(Auth::user()->id != $id)
    @can('delete_'.$entity)
        {!! Form::open( ['method' => 'delete', 'url' => route($route.'.destroy', ['user' => $id]), 'onSubmit' => 'return confirm("Вы уверены что хотите удалить запись?")']) !!}
        <button type="submit" class="dropdown-item">Удалить</button>
        {!! Form::close() !!}
    @endcan
@endif
