<ul class="nav pb-5 sidebar-menu dropdownMenu">
    @if(isset($menu) AND $menu)
        @foreach($menu as $item)
            @php
                $activeUrl = \Arr::has($item, 'current') ? 'active' : '';
            @endphp
            <li class="nav-item {{ $activeUrl }} drop-item" >
                @if(isset($item->childrens))
                    <a href="{{ langUrl($item->url) }}"
                       class="nav-link">
                        {!! \Arr::get($item, 'icon_font') !!}
                        <p>{{$item->name}}</p>
                    </a>
                    {{--<ul class="nav">--}}
                    {{--@include('admin.admin-menu-nested')--}}
                    {{--</ul>--}}
                @else
                    <a href="{{ langUrl($item->url) }}" class="nav-link">
                        {!! \Arr::get($item, 'icon_font') !!}
                        <p>{{$item->name}}</p>
                    </a>
                @endif
                {!! $item->content !!}
            </li>
            {{--@elseif(Route::has($item->route))--}}
            {{--<li role="presentation">--}}
            {{--<a href="{{ route($item->route) }}"--}}
            {{--class="{{ (isAdminMenuActiveByRoute($item->route) AND isMenuChildrenIsActive(!$item->childrens)) ? 'alert-danger' : '' }}">--}}
            {{--{{ $item->name }} {!! $item->icon ?? '' !!}--}}
            {{--</a>--}}
            {{--@if(isset($item->childrens))--}}
            {{--<ul class="nav">--}}
            {{--@include('admin.admin-menu-nested')--}}
            {{--</ul>--}}
            {{--@endif--}}
            {{--</li>--}}
        @endforeach
    @endif
    <li class="nav-item mt-5 text-center">
        <form action="{{ route('admin.logout') }}" method="POST">
            @csrf
            <button class="btn btn-primary">
                <b>Выйти</b>
            </button>
        </form>
    </li>
</ul>