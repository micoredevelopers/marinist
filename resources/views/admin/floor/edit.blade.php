<form action="{{ route('floor.update', $edit->id) }}" method="post" class="form-horizontal"
      enctype="multipart/form-data">
    @csrf
    @method('patch')
    @php
        $images = [
            'plan' => 'План',
            'substrate' => 'Подложка',
            'backlight' => 'Карта подсветки',
        ];
    @endphp
    @foreach ($images as $field => $name)
        @if( storageFileExists(Arr::get($edit, $field)))
            <div class="form-group">
                <label for="" class=" control-label">Проект</label>
                <div class="col">
                    <a href="{{ imgOrigin(checkImage(Arr::get($edit, $field))) }}" class="fancy">
                        <img src="{{ checkImage(Arr::get($edit, $field)) }}?{{storageFilemtime(Arr::get($edit, $field))}}"
                             class="img-responsive" width="150" alt="">
                    </a>
                </div>
            </div>
        @endif
        @if (isSuperAdmin())
            <div class="form-group">
                <label for="photo" class=" control-label">{{ $name }}</label>
                <input type="file" name="{{ $field }}" id="{{ $field }}" class="filestyle"
                       data-buttonText="Выберите изображение"
                       data-placeholder="Файла нет" accept="image/*">
            </div>
        @endif
    @endforeach

    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label for="number" class="control-label">№ Этажа</label>
                <input type="number" name="number" id="number" class=" form-control"
                       value="{{ $edit->number }}">
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="square" class="control-label">Площадь м²</label>
                <input type="number" name="square" id="square" class=" form-control"
                       value="{{ $edit->square }}">
            </div>
        </div>
    </div>


    @include('admin.partials.submit_update_buttons')
</form>
<div class="row">
    <div class="col-3"><h2>Квартиры</h2></div>
    <div class="col-9">
        <form action="{{ route('flat.store') }}" method="post">
            @method('post')
            @csrf
            <input type="hidden" name="floor_id" value="{{ $edit->id }}">
            <button class="btn btn-info"><i class="fa fa-plus" aria-hidden="true"></i>
                Создать квартиру</button>
        </form>
    </div>
</div>
<div class="table-responsive">
    <table class="table table-shopping">
        <thead>
        <tr>
            <th>ID</th>
            <th>Сортировка</th>
            <th>Номер</th>
            <th class="text-right">Действие</th>
        </tr>
        </thead>
        <tbody data-sortable-container="true" data-table="flats">
        @foreach ($edit->flats as $flat)
            <tr class="draggable" data-sort="" data-id="{{ $flat->id }}">
                <td>{{ $flat->id }}</td>
                <td>
                    @include('admin.partials.sort_handle')
                </td>
                <td>{{ $flat->number }}</td>
                <td class="text-primary text-right">
                    <a href="{{ route('flat.edit', $flat->id) }}" class="btn btn-primary">Редактировать</a>
                    <form action="{{ route('flat.destroy', $flat->id) }}" method="post" class="d-inline-block ">
                        @method('delete')
                        @csrf
                        <button class="btn btn-danger" type="submit" data-confirm="">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
