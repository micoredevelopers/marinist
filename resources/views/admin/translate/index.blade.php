@include('admin.partials.flash-message')
<form action="{{route($controller. '.update', '*')}}" method="post">
	<input type="hidden" name="_method" value="PATCH">
	@csrf
	<div class="panel panel-default">
		<div class="panel-body clearfix">
			@can('create_translate')
			<a href="{{route($controller.'.create')}}" class="btn btn-default">Добавить +</a>
			@endcan
			@can('edit_translate')
			<button class="btn btn-primary">Сохранить</button>
			@endcan
		</div>
	</div>

	<ul class="nav nav-tabs my-5" id="translates" role="tablist">
		@foreach($groups as $num => $group)
		<li class="nav-item">
			<a class="nav-link text-dark @if($num == 0) active @endif" id="{{ $group }}-tab" data-toggle="tab"
			   href="#group-{{ $group }}" role="tab"
			   aria-controls="group-{{ $group }}" aria-selected="@if($num == 0) true @endif">{{ $group }}</a>
		</li>
		@endforeach
	</ul>
	<div class="tab-content" id="translatesContent">
		@foreach($groups as $num => $group)
		<div class="tab-pane @if($num == 0) active @endif" id="group-{{ $group }}" role="tabpanel">
			<div class="table-responisve">
				<table class="table table-hover table-condensed table-striped">
					<tr class="active">
						<th>ID</th>
						<th>Ключ</th>
						<th>Значение</th>
						<th>Группа</th>
					</tr>
					@if(isset($list))
					@forelse($list->where('group', $group) as $item)
            <?php /** @var $item \App\Models\Translate */ ?>
					<tr>
						<td>
							{{$item->id}}
							<input type="hidden" name="translate[{{$item->id}}][id]" value="{{$item->id}}">
						</td>
						<td>
							<input type="hidden" name="translate[{{ $item->id }}][key]" value="{{$item->key}}">
							<input type="text" value="{{$item->key}}" class="form-control" readonly=""
							       title="{{ $item->comment }}"
							       onclick="copyClickBoard(this)" autocomplete="off"
							       data-prepend="{{ Str::contains($item->value, ['%d', '%s']) ? " translateFormat('" : "getTranslate('" }}"
							data-append="')">
						</td>
						<td title="{{ $item->comment }}">
                            @if ($item->isCKeditor())
                                <div class="description-wrap" data-id="{{ $item->id }}">
                                    <textarea data-name="translate[{{ $item->id }}][value]" name="translate[{{ $item->id }}][value]" cols="30" rows="6" class="form-control">{!! $item->value !!}</textarea>
                                    <div class="form-group">
                                        <input id="isArea" checked type="checkbox">
                                        <label for="isArea">Выбрать CK Editor</label>
                                    </div>
                                </div>

							@elseif ($item->isTextarea())
							<div class="description-wrap" data-id="{{ $item->id }}">
								<textarea name="translate[{{ $item->id }}][value]" cols="30" rows="6" class="form-control">{!! $item->value !!}</textarea>
								<div class="form-group">
									<input id="isArea" type="checkbox">
									<label for="isArea">Выбрать CK Editor</label>
								</div>
							</div>
							@else
							<input type="text" name="translate[{{$item->id}}][value]" value="{{$item->value}}"
							       class="form-control">
							@endif
						</td>
						<td width="200">
							@superadmin
							<select name="translate[{{$item->id}}][group]" class="form-control group_select">
								@forelse ($groups as $num => $group)
								<option value="{{ $group }}"
								        @if($group== $item->group) selected="selected" @endif>{{ $group }}
								</option>
								@empty
								<option value="">нет</option>
								@endforelse
							</select>
							@endsuperadmin
						</td>
					</tr>
					@empty
					<h3>Пусто</h3>
					@endforelse
					@endif
				</table>
			</div>
		</div>
		@endforeach
	</div>
</form>

@section('javascript')
<script>
	$('.group_select').not('.group_select_new').select2({
		tags: true,
		width: 'resolve'
	});


	$('input[type=checkbox]').on('change', function () {
		const id = $(this).parents('.description-wrap').attr('data-id')
        $.ajax({
			url: '{{route('translate.ckeditor')}}',
			type: 'GET',
			data: `id=${id}`,
			success: () => {
				 window.location.reload()
			},
			error: (e) => {
				console.log(e)
			}
		});
	})

    const textarea = $('textarea');

    textarea.map((index, item) => {
        const dataName = $(item).attr('data-name')
        console.log(dataName);

        if (dataName) {

            CKEDITOR.replace(dataName, {
                filebrowserBrowseUrl: '/elfinder/ckeditor',
                filebrowserImageBrowseUrl: '/elfinder/ckeditor',
                uiColor: '#9AB8F3',
                height: 300
            });
        }
    })


</script>
@stop
