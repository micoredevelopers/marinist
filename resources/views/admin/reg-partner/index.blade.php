<div class="table-responsive">
    <table class="table table-shopping">
        <thead>
        <tr>
            <th>ID</th>
            <th class="">Логин</th>
            <th class="th-description">Пароль</th>
            <th class="text-right">
                <a href="{{ route($controller . '.create') }}" class="btn btn-primary">Создать</a>
            </th>
        </tr>
        </thead>
        <tbody>
        @foreach($list as $item)
            <tr>
                <td>{{ $item->id }}</td>
                <td>
                   {{$item->getUsername()}}
                </td>
                <td>
                    {{ base64_decode($item->getPassword()) }}
                </td>
                <td class="text-primary text-right">
                    <div class="dropdown menu_drop">
                        <button
                            class="btn btn-secondary dropdown-toggle" type="button"
                            id="dropdownMenuButton_{{ $item->id }}" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">menu</i>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton_{{ $item->id }}">
                            {{--<form class="dropdown-item p-0" href="#">--}}
                            {{--<button class="drop_menu_button text-left"--}}
                            {{--type="submit">Отключить/включить</button>--}}
                            {{--</form>--}}
                            @can('edit_' . $controller)
                                <a href="{{ route($controller . '.edit',  $item->id)  }}" class="dropdown-item">️Редактировать</a>
                            @endcan
                            @can('delete_' . $controller)
                                {!! Form::open( ['method' => 'delete', 'url' => route($controller . '.destroy', $item->id), 'onSubmit' => 'return confirm("Вы уверены что хотите удалить запись?")']) !!}
                                <button type="submit" class="dropdown-item">Удалить</button>
                                {!! Form::close() !!}
                            @endcan
                        </div>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

