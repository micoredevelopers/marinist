<form action="{{ route('admin.reg-partner.store') }}" method="post" class="form-horizontal" enctype="multipart/form-data">
    @method('post')
    @csrf
    @include('admin.reg-partner.partials.form')
</form>
