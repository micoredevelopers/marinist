
<div class="row">
    <div class="form-group col-md-6">
        <label for="username" class="control-label">Логин</label>
        <input type="text" class="form-control" name="username" id="username" value="{{isset($edit)?$edit->username:old('username')}}">
    </div>
</div>
<div class="row">
    <div class="form-group col-md-6">
        <label for="password" class="control-label">Пароль</label>
        <input type="text" class="form-control" name="password" id="password" value="{{isset($edit)?base64_decode($edit->password):old('password')}}">
    </div>
</div>
@include('admin.partials.submit_update_buttons')


