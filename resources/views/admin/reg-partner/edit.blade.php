<form action="{{ route('admin.reg-partner.update',['id'=>$edit->id]) }}" method="post" class="form-horizontal" enctype="multipart/form-data">
    @method('put')
    @csrf
    @include('admin.reg-partner.partials.form')
</form>
