<form action="{{ route('admin.partners.store') }}" method="post" class="form-horizontal" enctype="multipart/form-data">
    @method('post')
    @csrf
    @include('admin.partners.partials.form')
</form>
