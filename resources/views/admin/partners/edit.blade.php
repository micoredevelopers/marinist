<form action="{{ route('admin.partners.update',['id'=>$edit->id]) }}" method="post" class="form-horizontal" enctype="multipart/form-data">
    @method('put')
    @csrf
    @include('admin.partners.partials.form')
</form>
