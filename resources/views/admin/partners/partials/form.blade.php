<div class="row">
    @if(storageFileExists($edit->photo??null))
        <div class="form-group col-md-12">
            <label for="" class=" control-label">Текущее фото партнера</label>
            <div class="col">

                <div class="image-actions" data-image-actions="">
                    <img src="{{ checkImage($edit->photo) }}?{{storageFilemtime($edit->photo)}}"
                         class="img-responsive deleteable croppable" width="150"
                         data-photo-id="{{ $edit->id ?? '' }}"
                         data-photo-table="{{'partners' ?? '' }}">
                </div>
            </div>
        </div>
    @endif
    <div class="form-group col-md-12">
        <label for="photo" class=" control-label">Логотип</label>
        <input type="file" name="photo" id="photo" class="filestyle" data-buttonText="Выберите изображение"
               data-placeholder="Файла нет" accept="image/*">
    </div>
</div>
<div class="row">
    <div class="form-group col-md-6">
        <label for="rating" class="control-label">Рейтинг</label>
        <input type="number" class="form-control" name="rating" id="rating" value="{{$edit->rating??old('rating')}}">
    </div>
</div>
@include('admin.partials.submit_update_buttons')


