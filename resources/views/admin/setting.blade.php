@extends('admin.layouts.app_admin')

@section('wrap-class')
        <div id="wrapper-settings">
@endsection

@section('navigation')
    @include('admin.navigation')
@endsection

@section('content')

    <main>
        <div class="section_main">
            <div class="container-fluid p-0">
                <div class="row m-0">
                    <div class="col-12 p-0">
                        <div class="header_search">
                            <div class="title">
                                <h1>Настройки</h1>
                            </div>
                        </div>
                    </div>
                </div>
                {{--@if ($errors->any())--}}
                    {{--<div class="alert alert-danger">--}}
                        {{--<ul>--}}
                            {{--@foreach ($errors->all() as $error)--}}
                                {{--<li>{{$error}}</li>--}}
                            {{--@endforeach--}}
                        {{--</ul>--}}
                    {{--</div>--}}
                {{--@endif--}}
                {{--@if (\Session::has('error'))--}}
                    {{--<div class="alert alert-danger">--}}
                        {{--<ul>--}}
                            {{--@foreach (\Session::get('error') as $error)--}}
                                {{--<li>{{$error}}</li>--}}
                            {{--@endforeach--}}
                        {{--</ul>--}}
                    {{--</div>--}}
                {{--@endif--}}
                {{--@if (\Session::has('success'))--}}
                    {{--<div class="alert alert-success">--}}
                        {{--<ul>--}}
                            {{--@foreach (\Session::get('success') as $success)--}}
                                {{--<li>{{$success}}</li>--}}
                            {{--@endforeach--}}
                        {{--</ul>--}}
                    {{--</div>--}}
                {{--@endif--}}
                <div class="settings_container">
                    <div class="settings_block row m-0">
                        <div class="col-12 p-0">


                            <form class="settings_form" action="{{route('admin.setting.update', $user)}}" method="post">
                                <div class="form_title">
                                    <h1>Изменить пароль</h1>
                                </div>
                                <div class="form_settings_pass">
                                    <div class="row m-0">
                                        <div class="col-4 p-0">
                                            <div class="input_container">
                                                <p class="input_lable">Старый пароль*</p>
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <input type="password" name="current-password">
                                            </div>
                                        </div>
                                        <div class="col-4 p-0">
                                            <div class="input_container">
                                                <p class="input_lable">Новый пароль*</p>
                                                <input type="password" name="password">
                                            </div>
                                        </div>
                                        <div class="col-4 p-0">
                                            <div class="input_container">
                                                <p class="input_lable">Подтверждение*</p>
                                                <input type="password" name="password_confirmation" >
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="btn_container">
                                    <button class="custom_btn save" type="submit">
                                        <span>Сохранить</span>
                                    </button>
                                </div>
                            </form>


                            {{-- Задание региона--}}
                            {{--<form class="settings_region" action="{{route('admin.region.create')}}" method="post">--}}
                                {{--{{ csrf_field() }}--}}

                                {{--<div class="btn_container">--}}
                                    {{--<button class="custom_btn save" type="submit">--}}
                                        {{--<span>Сохранить</span>--}}
                                    {{--</button>--}}
                                {{--</div>--}}

                            {{--</form>--}}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

@endsection
