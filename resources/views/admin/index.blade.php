<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    {!! SEOMeta::generate() !!}
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{--<link rel="apple-touch-icon" sizes="76x76" href="/assets/img/apple-icon.png"/>--}}
    {{--<link rel="icon" type="image/png" href="/assets/img/favicon.png"/>--}}
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no"
          name="viewport"/>
    <link rel="stylesheet" type="text/css"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css"/>
    <link rel="stylesheet"
          href="{{ asset('css/admin/material-dashboard.css') }}?{{ assetFilemtime('css/material-dashboard.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/admin/admin-main.css') }}?{{ assetFilemtime('css/admin-main.css') }}"/>
    <script src="{{asset( 'js/core/jquery.min.js') }}"></script>
    {!! $styles !!}
</head>
<body>

@include('admin.partials.preloader')

<div class="wrapper">
    <div class="sidebar" data-color="purple" data-background-color="white"
         data-image="">
        <div class="logo">
            <a href="{{ route('admin.index')}}" class="simple-text logo-normal">{{ getSetting('global.sitename') }}</a>
        </div>
        <div class="sidebar-wrapper">
            @include('admin.menu')
        </div>
    </div>
    <div class="main-panel">

        @include('admin.header')
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-10 offset-1 ">
                        <div class="card">
                            <div class="card-header card-header-primary">
                                <h4 class="card-title">{{ $cardTitle ?? '' }}</h4>
                            </div>
                            <div class="card-body">
                                {!! $content ?? '' !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('admin.langlist')

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
{!! \Arr::get($sections, 'css') !!}

<script type="text/javascript">
    const APP_URL = '{{ env('APP_URL') }}';
</script>

<script src="{{asset( 'js/core/popper.min.js') }}"></script>
<script src="{{asset( 'js/core/bootstrap-material-design.min.js') }}"></script>
<script src="{{asset( 'js/plugins/bootstrap-selectpicker.js') }}"></script>
<script src="{{asset( 'js/plugins/bootstrap-notify.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
<script src="https://cdn.jsdelivr.net/npm/flatpickr@4.5.7/dist/flatpickr.min.js"></script>

{!! $scripts !!}

{!! $scriptsDefer !!}
<script type="text/javascript" defer>
    $(document).ready(function (e) {
        var $selectpicker = $('.selectpicker');
        if ($selectpicker.length) $selectpicker.selectpicker();
        //
        $sort = sort;
        $sort.url = '{{ route('sort') }}';
        $sort.init();
    });
</script>
{!! \Arr::get($sections, 'javascript') !!}

@include('admin.partials.flash-message')
</body>
</html>















