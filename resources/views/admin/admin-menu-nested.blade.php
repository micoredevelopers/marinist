@foreach($item->childrens as $children)
    @if($children->url)
        <li role="presentation" >
            <a href="{{ langUrl($children->url) }}" class="ml-4 {{ isMenuActiveByUrl($children->url) ? 'alert-info' : '' }}">
                {{$children->name}} {!! $children->icon or '' !!}
            </a>
        </li>
    @elseif(Route::has($children->route))
        <li role="presentation" >
            <a href="{{ route($children->route) }}" class="ml-4 {{ isAdminMenuActiveByRoute($children->route) ? 'alert-info' : '' }}">
                {{ $children->name }} {!! $children->icon or '' !!}
            </a>
    @endif
@endforeach