<div class="form-group">
    <label for="photo" class="col-sm-2 control-label">Дополнительные фотографии</label>
    <div class="col-sm-4">
        <input type="file" name="photos[]" id="photos" class="filestyle" data-buttonText="Выберите изображение"
               data-placeholder="Файла нет" accept="image/*" multiple>
    </div>
</div>
<div data-sortable-container="true" data-table="{{$photo_table}}_photo">
    @if(isset($photosList) AND $photosList)
        @foreach($photosList as $num => $photo)
            @if ($photo->photo AND storageFileExists($photo->photo))
                @php
                    $photoSrc = checkImage($photo->photo);
                    $primaryId = isset($edit->id) ? 'data-primary-id="' . $edit->id .'"' : '';
                @endphp
                <div class="image-actions draggable" data-sort="true" data-id="{{ $photo->id }}" data-image-actions="">
                    <a href="{{ imgOrigin($photoSrc) }}" class="fancy" data-fancybox="additional_photos">
                        <img src="{{ $photoSrc }}?{{storageFilemtime($photo->photo)}}"
                             class="img-fluid croppable deleteable" width="150"
                             data-photo-id="{{ $photo->id ?? '' }}"
                             data-photo-table="{{$photo_table ?? '' }}" {!! $primaryId !!}>
                        @include('admin.partials.sort_handle')
                    </a>
                </div>
            @endif
        @endforeach
    @endif
</div>