<h3>Этапы строительства</h3>
<form action="{{ route('progress.update') }}" method="post" enctype="multipart/form-data">
    @csrf
    @method('post')
    <h4>Общая готовность - <b>{{ (int)$list->avg('percent') }} %</b></h4>
    <div class="row">
        <div class="col-5 offset-1">
            <label>Название этапа</label>
        </div>
        <div class="col-6">
            <label>% Готовности</label>
        </div>
    </div>
    <div class="steps" data-sortable-container="true" data-table="progress_steps">
        @foreach ($list as $item)
            <div class="row draggable" data-sort="" data-id="{{ $item->id }}">
                <div class="col-1 pt-2">
                    <div class="align-middle">
                        @include('admin.partials.sort_handle')
                    </div>
                </div>
                <div class="col-5">
                    <div class="form-group">
                        <input type="hidden" name="progress[{{ $item->id }}][id]" value="{{ $item->id }}">
                        <input type="text" id="progress-step-{{ $item->id }}"
                               value="{{old('name.' . $item->id, $item->name)}}"
                               class="form-control" placeholder="Название этапа" required="required"
                               name="progress[{{ $item->id }}][name]" autocomplete="off">
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <input type="number" id="percent-{{ $item->id }}"
                               value="{{old('percent.' . $item->id, $item->percent)}}"
                               class="form-control" placeholder="% Готовности" required="required" min="0" max="100"
                               name="progress[{{ $item->id }}][percent]" autocomplete="off">
                    </div>
                </div>

                <div class="col-6">
                    <div class="form-group">
                    <textarea rows="5" id="progress-step-description-{{ $item->id }}"
                              class="form-control" placeholder="Описание этапа"
                              name="progress[{{ $item->id }}][text]"
                              autocomplete="off">{{old('text.' . $item->id, $item->text)}}</textarea>
                    </div>
                    @if(storageFileExists($item->photo??null))
                        <div class="form-group col-md-12">
                            <label for="" class=" control-label">Текущее фото</label>
                            <div class="col">

                                <div class="image-actions" data-image-actions="">
                                    <img src="{{ checkImage($item->photo) }}?{{storageFilemtime($item->photo)}}"
                                         class="img-responsive deleteable croppable" width="150"
                                         data-photo-id="{{ $item->id ?? '' }}"
                                         data-photo-table="{{'progress_steps' ?? '' }}">
                                </div>
                            </div>
                        </div>
                    @endif
                    <input type="file" name="photo[{{$item->id}}]" id="photo[{{$item->id}}]" class="filestyle" data-buttonText="Выберите изображение"
                           data-placeholder="Файла нет" accept="image/*">
                </div>
            </div>
            <hr noshade size="10">
        @endforeach
    </div>
    <div class="text-right">
        <button class="btn btn-default">@lang('generic.update')</button>
    </div>
</form>
<h3>Ход строительства</h3>
<div class="table-responsive">
    <table class="table table-shopping">
        <thead>
        <tr>
            <th>ID</th>
            <th>Сортировка</th>
            <th class="">Фото</th>
            <th class="th-description">@lang('generic.title')</th>
            <th class="text-right">
                <a href="{{ route('progress.galleries.create') }}" class="btn btn-primary">Создать</a>
            </th>
        </tr>
        </thead>
        <tbody data-sortable-container="true" data-table="progress_galleries">
        @foreach ($galleries as $gallery)
            <tr class="draggable" data-sort="" data-id="{{ $gallery->id }}">
                <td>{{ $gallery->id }}</td>
                <td>
                    @include('admin.partials.sort_handle')
                </td>
                <td>
                    <div class="img-container">
                        <a href="{{ imgOrigin(checkImage($gallery->photo)) }}" class="fancy"
                           data-fancybox="galleries-photo">
                            <img src="{{ checkImage($gallery->photo) }}" alt="" class="img-fluid" width="80"/>
                        </a>
                    </div>
                </td>
                <td>
                    {{ $gallery->name }}
                </td>
                <td class="text-primary text-right">
                    <div class="dropdown menu_drop">
                        <button
                            class="btn btn-secondary dropdown-toggle" type="button"
                            id="dropdownMenuButton_{{ $gallery->id }}" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">menu</i>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton_{{ $gallery->id }}">

                            <a href="{{ route( 'progress.galleries.edit',  $gallery->id)  }}"
                               class="dropdown-item">️Редактировать</a>
                            {!! Form::open( ['method' => 'delete', 'url' => route('progress.galleries.destroy', $gallery->id), 'onSubmit' => 'return confirm("Вы уверены что хотите удалить запись?")']) !!}
                            <button type="submit" class="dropdown-item">Удалить</button>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@section('javascript')
    <script>
        $(document).ready(function (e) {
        @foreach ($list as $item)

            {!! showEditor('progress-step-description-'. $item->id ) !!}

        @endforeach
        });
    </script>
@endsection
