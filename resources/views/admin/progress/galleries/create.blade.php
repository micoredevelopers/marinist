<form action="{{ route('progress.galleries.store') }}" method="post" class="form-horizontal" enctype="multipart/form-data">
    @method('post')
    @csrf
    <input type="hidden" name="date_update" value="{{ now() }}">
    <input type="hidden" name="date_add" value="{{ now() }}">

    <div class="form-group">
        <label for="name" class=" control-label">@lang('generic.title')</label>
        <input type="text"id="name" name="name" value="{{old('name') ?? ''}}" class="form-control">
    </div>
    <div class="form-group">
        <label for="active" class=" control-label">Включен</label>
        <div class="col">
            <div class="check-styled">
                <input type="checkbox" value="1" id="active" name="active" checked="checked" />
                <label for="active"></label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="photo" class=" control-label">Обложка</label>
        <input type="file" name="photo" id="photo" class="filestyle" data-buttonText="Выберите изображение"
               data-placeholder="Файла нет" accept="image/*">
    </div>
    <div class="form-group">
        <label for="description" class=" control-label">Описание</label>
        <textarea name="description" id="description" cols="30" rows="10">{{ old('description') ?? '' }}</textarea>
    </div>
    <div class="form-group">
        <label for="sub_description" class=" control-label">Краткое описание</label>
        <textarea name="sub_description" id="sub_description" cols="30" rows="10">{{ old('sub_description') ?? '' }}</textarea>
    </div>
    @include('admin.partials.submit_create_buttons')
</form>

@section('javascript')
    <script defer>
        $(document).ready(function () {
            {!! showEditor('description') !!}
            {!! showEditor('sub_description') !!}
        });
    </script>
@stop