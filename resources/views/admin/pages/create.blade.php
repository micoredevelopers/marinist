<form action="{{ route('pages.store') }}" method="post" class="form-horizontal" enctype="multipart/form-data">
    @method('post')
    @csrf

    <div class="form-group">
        <label for="name" class=" control-label">@lang('generic.title')</label>
        <input type="text" id="name" name="name" value="{{old('name') ?? ''}}" class="form-control">
    </div>
    <div class="form-group">
        @if ($errors->has('url')) <p class="text-danger">{{ $errors->first('url') }}</p> @endif
        <label for="url" class=" control-label"
               title="Url адресс используется для создания уникального url">
            Url адресс</label>
        <input type="text" id="url" name="url" value="{{ old('url') }}" class="form-control">
    </div>
    <div class="form-group">
        <label for="active" class=" control-label">Включен</label>
        <div class="check-styled">
            <input type="checkbox" value="1" id="active" name="active" {{ old('active') == 1 ? 'checked' : ''}} />
            <label for="active"></label>
        </div>
    </div>
    <div class="form-group">
        <label for="photo" class=" control-label">Обложка</label>
        <input type="file" name="photo" id="photo" class="filestyle" data-buttonText="Выберите изображение"
               data-placeholder="Файла нет" accept="image/*">
    </div>
    <div class="form-group">
        <label for="description" class=" control-label">Описание</label>
        <textarea name="description" id="description" cols="30" rows="10">{{ old('description') ?? '' }}</textarea>
    </div>
    <div class="form-group">
        <label for="except" class=" control-label">Краткое Описание</label>
        <div class="">
            <textarea name="except" id="except" cols="30" rows="10">{{ old('except')}}</textarea>
        </div>
    </div>
    @include('admin.partials.submit_create_buttons')
</form>

@section('javascript')
    <script defer>
        $(document).ready(function () {
            {!! showEditor('description') !!}
            {!! showEditor('except') !!}
        });
    </script>
@stop