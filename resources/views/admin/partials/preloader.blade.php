<div class="preloader-holder" id="page-preloader">
    <div class="preloader">
        <div></div>
        <div></div>
    </div>
</div>
<script type="text/javascript" defer>
    $(document).ready(function (e) {
        $('#page-preloader').fadeOut(200);
    });
</script>