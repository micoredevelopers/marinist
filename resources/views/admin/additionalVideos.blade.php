<div class="mb-5">
	@if(!empty($edit->video) AND is_array($edit->video))
		@foreach($edit->video as $link)
			<div class="row mt-3 clearfix" data-cloneable-row="">
				<div class="col-8 vertical-align-bottom">
					<input class="form-control" type="text" name="video[]" value="{{ $link }}" placeholder="Ссылка на видео YouTube">
				</div>
				<div class="col-2 vertical-align-bottom">
					<a href="{{$link}}" class="fancy" data-fancybox="cats_video">
						<img src="{{ getPreviewYoutubeFromLink($link) }}" alt="" class="img-fluid" width="85">
					</a>
				</div>
				<div class="col-2 vertical-align-bottom">
					<div class="deleteRow">
						<a href="#" class="btn btn-warning" data-remove-suffix="" title="Удалить запись">
							<i class="fa fa-times"></i>
						</a>
						<button type="button" class="btn btn-success" data-additional-field="true">
							<i class="fa fa-plus-circle"></i>
						</button>
					</div>
				</div>
			</div>
		@endforeach
	@else
		<div class="form-group row" data-cloneable-row="">
			<div class="col-10">
				<input class="form-control" type="text" name="video[]" placeholder="Ссылка на видео YouTube">
			</div>
			<div class="col-2">
				<div class="deleteRow row">
					<a href="#" class="btn btn-dark" data-remove-suffix="" title="Удалить запись">
						<i class="fa fa-minus-circle"></i>
					</a>
					<button type="button" class="btn btn-success" data-additional-field="">
						<i class="fa fa-plus-circle"></i>
					</button>
				</div>
			</div>
		</div>
	@endif
</div>