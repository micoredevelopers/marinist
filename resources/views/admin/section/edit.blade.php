<form action="{{ route('section.update', $edit->id) }}" method="post" class="form-horizontal"
      enctype="multipart/form-data">
    @csrf
    @method('patch')
    @if( storageFileExists($edit->photo))
        <div class="form-group">
            <label for="" class=" control-label">Проект</label>
            <div class="col">
                <a href="{{ imgOrigin(checkImage($edit->photo)) }}" class="fancy">
                    <img src="{{ checkImage($edit->photo) }}?{{storageFilemtime($edit->photo)}}"
                         class="img-responsive" width="150" alt="">
                </a>
            </div>
        </div>
    @endif
    @if (isSuperAdmin())
        <div class="form-group">
            <label for="photo" class=" control-label">План</label>
            <input type="file" name="photo" id="photo" class="filestyle"
                   data-buttonText="Выберите изображение"
                   data-placeholder="Файла нет" accept="image/*">
        </div>
    @endif
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label for="number" class="control-label">№ секции</label>
                <input type="number" name="number" id="number" class=" form-control"
                       value="{{ $edit->number }}">
            </div>
        </div>
    </div>

    @include('admin.partials.submit_update_buttons')
</form>

<div class="row">
    <div class="col-3"><h2>Этажи</h2></div>
    <div class="col-9">
        <form action="{{ route('floor.store') }}" method="post">
            @method('post')
            @csrf
            <input type="hidden" name="section_id" value="{{ $edit->id }}">
            <button class="btn btn-info"><i class="fa fa-plus" aria-hidden="true"></i>
                Создать этаж</button>
        </form>
    </div>
</div>

<div class="table-responsive">
    <table class="table table-shopping">
        <thead>
        <tr>
            <th>ID</th>
            <th>Сортировка</th>
            <th>Номер</th>
            <th class="text-right">Действие</th>
        </tr>
        </thead>
        <tbody data-sortable-container="true" data-table="floors">
        @foreach ($edit->floors as $floor)
            <?php /** @var $floor \App\Models\Floor  */ ?>
            <tr class="draggable" data-sort="" data-id="{{ $floor->id }}">
                <td>{{ $floor->id }}</td>
                <td>
                    @include('admin.partials.sort_handle')
                </td>
                <td>{{ $floor->getAttribute('number') }}</td>
                <td class="text-primary text-right">
                    <a href="{{ route('floor.edit', $floor->id) }}" class="btn btn-primary">Редактировать</a>
                    <form action="{{ route('floor.destroy', $floor->id) }}" method="post" class="d-inline-block ">
                        @method('delete')
                        @csrf
                        <button class="btn btn-danger" type="submit" data-confirm="">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>