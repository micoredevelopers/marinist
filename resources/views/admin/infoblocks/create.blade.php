<form action="{{route($controller.'.store')}}" method="post" class="form-horizontal" enctype="multipart/form-data">
    @method('post')
    @csrf
    <div class="form-group clearfix">
        <label class="control-label">Название</label>
        <input type="text" name="name" value="{{old('name', '')}}" class="form-control">
    </div>
    <div class="form-group">
        <label for="description" class="control-label">Описание</label>
        <textarea name="description" id="description" cols="30"
                  rows="10"></textarea>
    </div>
    <div class="form-group">
        <label for="sub_description" class="control-label">Краткое Описание</label>
        <textarea name="sub_description" id="sub_description" cols="30"
                  rows="10"></textarea>
    </div>
    @include('admin.partials.submit_create_buttons')
</form>

@section('javascript')
    <script defer>
        $(document).ready(function () {
            {!! showEditor('description') !!}
            {!! showEditor('sub_description') !!}
        });
    </script>
@stop