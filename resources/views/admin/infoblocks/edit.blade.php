<form action="{{ route('infoblocks.update', $edit->id) }}" method="post" class="form-horizontal"
      enctype="multipart/form-data">
    @csrf
    @method('PATCH')
    <div class="form-group clearfix">
        <label class="control-label">Описание</label>
        <input type="text" name="name" value="{{$edit->name}}" class="form-control">
    </div>

    <div class="form-group">
        <label for="active" class="control-label">Включен</label>
        <div class="check-styled">
            <input type="checkbox" value="1" id="active"
                   name="active" {{$edit->active == 1 ? 'checked' : ''}} />
            <label for="active"></label>
        </div>
    </div>
    <div class="form-group">
        <label for="description" class="control-label">Описание</label>
        <textarea name="description" id="description" cols="30"
                  rows="10">{{$edit->description}}</textarea>
    </div>
    <div class="form-group">
        <label for="sub_description" class="control-label">Краткое Описание</label>
        <textarea name="sub_description" id="sub_description" cols="30"
                  rows="10">{{$edit->sub_description}}</textarea>
    </div>
    @include('admin.partials.submit_update_buttons')
</form>
@section('javascript')
    <script defer>
        $(document).ready(function () {
            {!! showEditor('description') !!}
            {!! showEditor('sub_description') !!}
        });
    </script>
@stop