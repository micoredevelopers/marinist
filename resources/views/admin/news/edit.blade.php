<div class="text-right">
    <a href="{{ route('news.single', $edit->url) }}" class="btn" target="_blank">Предпросмотр</a>
</div>

<form action="{{ route('news.update', $edit->id) }}" method="post" class="form-horizontal" enctype="multipart/form-data">
    @csrf
    @method('patch')
    <ul class="nav nav-tabs mb-5" role="tablist">
        <li role="presentation">
            <a class="active" href="#dataDefault" aria-controls="home" role="tab" data-toggle="tab">Основное</a>
        </li>
        @superadmin
        <li role="presentation"><a href="#photosList" aria-controls="profile" role="tab"
                                   data-toggle="tab">Фотографии</a></li>

        <li role="presentation"><a href="#videoYoutube" aria-controls="profile" role="tab"
                                   data-toggle="tab">Видео Youtube</a></li>
        @endsuperadmin
    </ul>
    <section class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="dataDefault">
            <div class="panel-body">
                <input type="hidden" name="date_update" value="{{date("Y-m-d H:i:s")}}">
                <div class="form-group">
                    <label for="name" class=" control-label">Название новости</label>
                    <input type="text" id="name" name="name" value="{{$edit->name ?? ''}}" class="form-control"
                           autocomplete="off">
                </div>
                <div class="form-group">
                    <label for="url" class="control-label"
                           title="Url адресс используется для создания уникального url для новости">
                        Url адресс</label>
                    <input type="text" id="url" value="{{$edit->url}}" class="form-control" autocomplete="off" disabled>
                </div>
                @if(storageFileExists($edit->photo))
                    <div class="form-group">
                        <label for="" class=" control-label">Текущая Обложка новости</label>
                        <div class="col">
                            <div class="image-actions" data-image-actions="">
                                <img src="{{ checkImage($edit->photo) }}?{{storageFilemtime($edit->photo)}}"
                                     class="img-responsive deleteable croppable" width="150"
                                     data-photo-id="{{ $edit->id ?? '' }}"
                                     data-photo-table="{{$photo_table ?? '' }}">
                            </div>
                        </div>
                    </div>
                @endif
                <div class="form-group">
                    <label for="photo" class=" control-label">Обложка новости</label>
                    <input type="file" name="photo" id="photo" class="filestyle"
                           data-buttonText="Выберите изображение"
                           data-placeholder="Файла нет" accept="image/*">
                </div>
                <div class="form-group">
                    <label class="bmd-label-floating">Дата публикации</label>
                    <input type="datetime-local" data-flatpickr-type="datetime_local" name="date_pub"
                           value="{{ $edit->date_pub }}" class="form-control" />
                </div>
                <div class="form-group">
                    <label class=" control-label">Включен</label>
                    <div class="col">
                        <div class="check-styled">
                            <input type="checkbox" value="1" id="active"
                                   name="active" {{$edit->active == 1 ? 'checked' : ''}} />
                            <label for="active"></label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="active_modal" class=" control-label">Включить обратную связь</label>
                    <div class="check-styled">
                        <input type="checkbox" value="1" id="active_modal" name="active_modal"  {{$edit->active_modal ? 'checked' : ''}} />
                        <label for="active_modal"></label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="description" class=" control-label">Описание</label>
                    <textarea name="description" id="description" cols="30"
                              rows="10">{{$edit->description}}</textarea>
                </div>
                <div class="form-group">
                    <label for="sub_description" class=" control-label">Краткое Описание</label>
                    <textarea name="sub_description" id="sub_description" cols="30"
                              rows="10">{{$edit->sub_description}}</textarea>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="photosList">
            <div class="panel-body">
                @include('admin.additionalPhotos')
            </div>
        </div>

        <div role="tabpanel" class="panel-body tab-pane" id="videoYoutube">
            @include('admin.additionalVideos')
        </div>
    </section>
    @include('admin.partials.submit_update_buttons')
</form>
@section('javascript')
    <script type="text/javascript" defer>
        $(document).ready(function () {
            {!! showEditor('description') !!}
            {!! showEditor('sub_description') !!}
        });
    </script>
@stop
