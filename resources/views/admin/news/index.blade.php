@if (isSuperAdmin())
    <form action="{{ route('news.normalize-date') }}" method="post">
        @csrf
        <button  type="submit" class="btn btn-danger">Нормализовать дату публикации</button>
    </form>
@endif
<div class="table-responsive">
    <table class="table table-shopping">
        <thead>
        <tr>
            <th class="">Фото</th>
            <th class="th-description">Заголовок</th>
            <th class="th-description">Дата публикации</th>
            <th class="text-right">
                <a href="{{ route('news.create') }}" class="btn btn-primary">Создать</a>
            </th>
        </tr>
        </thead>
        <tbody>
        @foreach($list as $item)
            <tr>
                <td>
                    <div class="img-container">
                        <a href="{{ imgOrigin(checkImage($item->photo)) }}" class="fancy" data-fancybox="news-photo">
                            <img src="{{ checkImage($item->photo) }}" alt="" />
                        </a>
                    </div>
                </td>
                <td>
                    {{ $item->name }}
                </td>
                <td>
                    {{ $item->date_pub }}
                </td>

                <td class="text-primary text-right">
                    <div class="dropdown menu_drop">
                        <button
                                class="btn btn-secondary dropdown-toggle" type="button"
                                id="dropdownMenuButton_{{ $item->id }}" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">menu</i>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton_{{ $item->id }}">
                            {{--<form class="dropdown-item p-0" href="#">--}}
                            {{--<button class="drop_menu_button text-left"--}}
                            {{--type="submit">Отключить/включить</button>--}}
                            {{--</form>--}}
                            @can('edit_news')
                                <a href="{{ route('news.edit',  $item->id)  }}" class="dropdown-item">️Редактировать</a>
                            @endcan
                            @can('delete_news')
                                {!! Form::open( ['method' => 'delete', 'url' => route('news.destroy', ['user' => $item->id]), 'onSubmit' => 'return confirm("Вы уверены что хотите удалить запись?")']) !!}
                                <button type="submit" class="dropdown-item">Удалить</button>
                                {!! Form::close() !!}
                            @endcan
                        </div>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

{{$list->render()}}

@section('javascript')
    <script defer>
        $(document).ready(function () {
            $("a.fancy").fancybox();
        });
    </script>
@stop