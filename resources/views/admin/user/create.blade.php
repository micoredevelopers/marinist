<main>
{!! Form::open(['route' => ['users.store'] ]) !!}
@include('admin.user._form')
<!-- Submit Form Button -->
    @include('admin.partials.submit_create_buttons')
    {!! Form::close() !!}
</main>