<main>
    <div class="ibox float-e-margins">
        <div class="ibox-content">
        {!! Form::model($user, ['method' => 'PUT', 'route' => ['users.update',  $user->id ] ]) !!}
        @include('admin.user._form')
        <!-- Submit Form Button -->
            @include('admin.partials.submit_update_buttons')
            {!! Form::close() !!}
        </div>
    </div>
</main>
