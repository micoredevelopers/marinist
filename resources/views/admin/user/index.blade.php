<main>
    <div class="clearfix header_search">
        <div class="col-md-5">
            <h3 class="modal-title"> Пользователей - {{ $result->total() }}</h3>
        </div>
        <div class="col-md-7 page-action text-right">
            @can('view_roles')
                <a href="{{ route('roles.index') }}"
                   class="btn btn-secondary ">Роли</a>
            @endcan
            @can('create_users')
                <a href="{{ route('users.create') }}" class="btn btn-primary "> <i
                            class="glyphicon glyphicon-plus-sign"></i> Создать</a>
            @endcan
        </div>
    </div>

    <div class="table_main ">
        <div class="table_container">
            <table class="table table-bordered table-hover" id="data-table">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Имя</th>
                    <th>Email</th>
                    <th>Роль</th>
                    <th>Дата создания</th>
                    @can('delete_users')
                        <th class="text-center">Действия</th>
                    @endcan
                </tr>
                </thead>
                <tbody>
                @foreach($result as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->name }}</td>
                        <td>{{ $item->email }}</td>
                        <td>{{ $item->roles->implode('name', ', ') }}</td>
                        <td>{{ $item->created_at->toFormattedDateString() }}</td>

                        @can('delete_users')
                            <th class="text-center">
                                <div class="dropdown menu_drop">
                                    <button
                                            class="btn btn-secondary dropdown-toggle" type="button"
                                            id="dropdownMenuButton_{{ $item->id }}" data-toggle="dropdown"
                                            aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">menu</i>
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton_{{ $item->id }}">
                                        {{--<form class="dropdown-item p-0" href="#">--}}
                                            {{--<button class="drop_menu_button text-left"--}}
                                                    {{--type="submit">Отключить/включить</button>--}}
                                        {{--</form>--}}
                                        @include('shared._actions', [
                                        'entity' => 'users',
                                        'route' => 'users',
                                        'id' => $item->id
                                        ])
                                    </div>
                                </div>
                            </th>
                        @endcan
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="text-center">
            {{ $result->links() }}
        </div>
    </div>
</main>
