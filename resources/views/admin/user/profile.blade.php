<form method="post" action="{{ route('user.profile.update') }}">
    @csrf
    @method('post')
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                @if ($errors->has('name')) <p class="text-danger">{{ $errors->first('name') }}</p> @endif
                <label class="bmd-label-floating">Имя</label>
                <input type="text" name="name" value="{{ old('name', $user->name) }}"
                       class="form-control" required="required" minlength="2">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            @if ($errors->has('password')) <p class="text-danger">{{ $errors->first('password') }}</p> @endif
            <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Старый пароль</label>
                <input type="password" class="form-control" name="password">
            </div>
        </div>
        <div class="col-md-4">
            @if ($errors->has('password_new')) <p class="text-danger">{{ $errors->first('password_new') }}</p> @endif
            <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Новый пароль</label>
                <input type="password" class="form-control" name="password_new">
            </div>
        </div>
        <div class="col-md-4">
            @if ($errors->has('password_new_confirmation')) <p class="text-danger">{{ $errors->first('password_new_confirmation') }}</p> @endif
            <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Подтверждение пароля</label>
                <input type="password" class="form-control"  name="password_new_confirmation">
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-primary">Сохранить</button>
</form>