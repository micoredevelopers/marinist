@section('css')
    <style>
        .panel-actions .voyager-trash {
            cursor: pointer;
        }

        .panel-actions .voyager-trash:hover {
            color: #e94542;
        }

        .settings .panel-actions {
            right: 0px;
        }

        .panel hr {
            margin-bottom: 10px;
        }

        .panel {
            padding-bottom: 15px;
        }

        .sort-icons {
            font-size: 21px;
            color: #ccc;
            position: relative;
            cursor: pointer;
        }

        .sort-icons:hover {
            color: #37474F;
        }

        .voyager-sort-desc {
            margin-right: 10px;
        }

        .voyager-sort-asc {
            top: 10px;
        }

        .page-title {
            margin-bottom: 0;
        }

        .panel-title code {
            border-radius: 30px;
            padding: 5px 10px;
            font-size: 11px;
            border: 0;
            position: relative;
            top: -2px;
        }

        .modal-open .settings .select2-container {
            z-index: 9 !important;
            width: 100% !important;
        }

        .new-setting {
            text-align: center;
            width: 100%;
            margin-top: 20px;
        }

        .new-setting .panel-title {
            margin: 0 auto;
            display: inline-block;
            color: #999fac;
            font-weight: lighter;
            font-size: 13px;
            background: #fff;
            width: auto;
            height: auto;
            position: relative;
            padding-right: 15px;
        }

        .settings .panel-title {
            padding-left: 0px;
            padding-right: 0px;
            display: inline-block;
        }

        .new-setting hr {
            margin-bottom: 0;
            top: 7px;
            width: 90%;
            margin-left: 2%;
        }

        .new-setting .panel-title i {
            position: relative;
            top: 2px;
        }

        .new-settings-options {
            display: none;
            padding-bottom: 10px;
        }

        .new-settings-options label {
            margin-top: 13px;
        }

        .new-settings-options .alert {
            margin-bottom: 0;
        }

        #toggle_options {
            clear: both;
            float: right;
            font-size: 12px;
            position: relative;
            margin-top: 15px;
            margin-right: 5px;
            margin-bottom: 10px;
            cursor: pointer;
            z-index: 9;
            -webkit-touch-callout: none;
            -webkit-user-select: none;
            -khtml-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        .new-setting-btn {
            margin-right: 15px;
            position: relative;
            margin-bottom: 0;
            top: 5px;
        }

        .new-setting-btn i {
            position: relative;
            top: 2px;
        }

        textarea {
            min-height: 120px;
        }

        textarea.hidden {
            display: none;
        }

        .voyager .settings .nav-tabs {
            background: none;
            border-bottom: 0px;
        }

        .voyager .settings .nav-tabs .active a {
            border: 0px;
        }

        .select2 {
            width: 100% !important;
            border: 1px solid #f1f1f1;
            border-radius: 3px;
        }

        .voyager .settings input[type=file] {
            width: 100%;
        }

        .settings .select2 {
            margin-left: 10px;
        }

        .settings .select2-selection {
            height: 32px;
            padding: 2px;
        }

        .voyager .settings .nav-tabs > li {
            margin-bottom: -1px !important;
        }

        .voyager .settings .nav-tabs a {
            text-align: center;
            background: #f8f8f8;
            border: 1px solid #f1f1f1;
            position: relative;
            top: -1px;
            border-bottom-left-radius: 0px;
            border-bottom-right-radius: 0px;
        }

        .voyager .settings .nav-tabs a i {
            display: block;
            font-size: 22px;
        }

        .tab-content {
            background: #ffffff;
            border: 1px solid transparent;
        }

        .tab-content > div {
            padding: 10px;
        }

        .settings .no-padding-left-right {
            padding-left: 0px;
            padding-right: 0px;
        }

        .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover {
            background: #fff !important;
            color: #62a8ea !important;
            border-bottom: 1px solid #fff !important;
            top: -1px !important;
        }

        .nav-tabs > li a {
            transition: all 0.3s ease;
        }

        .nav-tabs > li.active > a:focus {
            top: 0px !important;
        }

        .voyager .settings .nav-tabs > li > a:hover {
            background-color: #fff !important;
        }

        .ace_editor.min_height_400 {
            min-height: 400px;
            width: 100%
        }

        .ace_editor.min_height_200 {
            min-height: 200px;
            width: 100%
        }
    </style>
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-settings"></i> {{ __('generic.settings') }}
    </h1>
@stop

<?php /** @var $setting \App\Models\Setting */ ?>
<form action="{{ route('settings.update', '*') }}" method="POST" enctype="multipart/form-data">
    {{ method_field("PUT") }}
    @csrf
    <input type="hidden" name="setting_tab" class="setting_tab" value="{{ $active }}"/>
    <div class="panel d-none">
        <div class="page-content settings container-fluid">
            {{--<ul class="nav nav-tabs">--}}
            {{--@foreach($settings as $group => $setting)--}}
            {{--<li @if($group == $active) class="active" @endif>--}}
            {{--<a data-toggle="tab" href="#{{ \Str::slug($group) }}">{{ $group }}</a>--}}
            {{--</li>--}}
            {{--@endforeach--}}
            {{--</ul>--}}
        </div>
    </div>
    <ul class="nav nav-tabs mb-5" id="settings" role="tablist">
        @foreach($groups as $num => $group)
            <li class="nav-item">
                <a class="nav-link text-dark @if($num == 0) active @endif" id="{{ $group }}-tab" data-toggle="tab"
                   href="#group-{{ $group }}" role="tab"
                   aria-controls="group-{{ $group }}" aria-selected="@if($num == 0) true @endif">{{ $group }}</a>
            </li>
        @endforeach
    </ul>
    <div class="tab-content" id="settingsContent">
        @foreach($groups as $num => $group)
            <div class="tab-pane @if($num == 0) active @endif" id="group-{{ $group }}" role="tabpanel"
            >
                @foreach($settings->where('group', $group) as $setting)
                    <div class="panel-heading mt-5">
                        <div class="row">
                            <div class="col col-md-8">
                                @superadmin
                                <input type="text" class="form-control max-width-300" onclick="copyClickBoard(this)"
                                       readonly value="{{ $setting->key }}" data-prepend="{{ $setting->getPrepend() }}"
                                 data-append="{{ $setting->getAppend() }}">
                                @endsuperadmin
                                <h6 class="panel-title mt-0">{{ $setting->display_name }}</h6>
                            </div>
                            <div class="col col-md-4">
                                <div class="row">
                                    <div class="col-10">
                                        <div class="px-0">
                                            @superadmin
                                            <select class="form-control group_select"
                                                    name="{{ $setting->key }}_group">
                                                @foreach($groups as $group)
                                                    <option value="{{ $group }}" {!! $setting->group == $group ? 'selected' : '' !!}>{{ $group }}</option>
                                                @endforeach
                                            </select>
                                            @endsuperadmin
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <div class="panel-actions text-right">
                                            @can('delete_settings')
                                                <i class="voyager-trash fa fa-times" aria-hidden="true"
                                                   data-id="{{ $setting->id }}"
                                                   data-display-key="{{ $setting->key }}"
                                                   data-display-name="{{ $setting->display_name }}"></i>
                                            @endcan
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel-body px-0">
                        <div class="col">
                            @if ($setting->type == "text")
                                <input type="text" class="form-control" name="{{ $setting->key }}"
                                       value="{{ \Arr::get($setting, 'value') }}">
                            @elseif($setting->type == "text_area")
                                <textarea class="form-control"
                                          name="{{ $setting->key }}">{{ \Arr::get($setting, 'value') }}</textarea>

                            @elseif($setting->type == "rich_text_box")
                                <textarea class="form-control richTextBox"
                                          name="{{ $setting->key }}">{{ \Arr::get($setting, 'value') }}</textarea>
                            @elseif($setting->type == "ckeditor")
                                <textarea class="form-control richTextBox" id="settings-ckeditor-{{ $setting->id }}"
                                          name="{{ $setting->key }}">{!! \Arr::get($setting, 'value') !!}</textarea>
                                {{--NOT EXISTS--}}
                            @elseif($setting->type == "code_editor")
								<?php $options = json_decode($setting->details); ?>
                                <div id="{{ $setting->key }}" data-theme="{{ @$options->theme }}"
                                     data-language="{{ @$options->language }}"
                                     class="ace_editor min_height_400"
                                     name="{{ $setting->key }}">{{ \Arr::get($setting, 'value') }}</div>
                                <textarea name="{{ $setting->key }}" id="{{ $setting->key }}_textarea"
                                          class="hidden">{{ \Arr::get($setting, 'value') }}</textarea>
                                {{-- END NOT EXISTS--}}

                            @elseif($setting->isFile())
                                @if($setting->isTypeImage() && storageFileExists($setting->getValue()))
                                    <div class="img_settings_container">
                                        <a href="{{ route('settings.delete_value', $setting->id) }}"
                                           class="voyager-x" data-confirm="true">
                                            <i class="fa fa-times" aria-hidden="true"></i>
                                        </a>
                                        <img src="{{ getStorageFilePath($setting->value) }}"
                                             style="max-width:200px; height:auto; padding:2px; border:1px solid #ddd; margin-bottom:10px;">
                                    </div>
                                    <div class="clearfix"></div>
                                @elseif($setting->isFileValid())
                                    <div class="fileType">
                                        @foreach ($setting->getValue() as $file)
                                            @if (storageFileExists($file->download_link))
                                                <a href="{{ getStorageFilePath($file->download_link) }}"
                                                   class="badge badge-secondary" target="_blank">{{ $file->original_name }}</a>
                                            @endif
                                        @endforeach
                                    </div>
                                @endif

                                @if ($setting->isMultiFile())
                                    <input type="file" name="{{ $setting->key }}[]" multiple>
                                @else
                                    <input type="file" name="{{ $setting->key }}">
                                @endif
                            @elseif($setting->type == "select_dropdown")
								<?php $options = json_decode($setting->details); ?>
								<?php $selected_value = (isset($setting->value) && !empty($setting->value)) ? $setting->value : NULL; ?>
                                <select class="form-control" name="{{ $setting->key }}">
									<?php $default = (isset($options->default)) ? $options->default : NULL; ?>
                                    @if(isset($options->options))
                                        @foreach($options->options as $index => $option)
                                            <option value="{{ $index }}" @if($default == $index && $selected_value === null){{ 'selected="selected"' }}@endif @if($selected_value == $index){{ 'selected="selected"' }}@endif>{{ $option }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            @elseif($setting->type == "radio_btn")
								<?php $options = json_decode($setting->details); ?>
								<?php $selected_value = (isset($setting->value) && !empty($setting->value)) ? $setting->value : NULL; ?>
								<?php $default = (isset($options->default)) ? $options->default : NULL; ?>
                                <ul class="radio">
                                    @if(isset($options->options))
                                        @foreach($options->options as $index => $option)
                                            <li>
                                                <input type="radio" id="option-{{ $index }}"
                                                       name="{{ $setting->key }}"
                                                       value="{{ $index }}" @if($default == $index && $selected_value === null){{ 'checked' }}@endif @if($selected_value == $index){{ 'checked' }}@endif>
                                                <label for="option-{{ $index }}">{{ $option }}</label>
                                                <div class="check"></div>
                                            </li>
                                        @endforeach
                                    @endif
                                </ul>
                            @elseif($setting->type == "checkbox")
								<?php $options = json_decode($setting->details); ?>
								<?php $checked = (isset($setting->value) && $setting->value == 1) ? true : false; ?>
                                @if (isset($options->on) && isset($options->off))
                                    <input type="checkbox" name="{{ $setting->key }}" class="toggleswitch"
                                           @if($checked) checked
                                           @endif data-on="{{ $options->on }}"
                                           data-off="{{ $options->off }}">
                                @else
                                    <input type="checkbox" name="{{ $setting->key }}" @if($checked) checked
                                           @endif class="toggleswitch">
                                @endif
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
        @endforeach
    </div>
    @can('edit_settings')
        <button type="submit" class="btn btn-primary pull-right">{{ __('settings.save') }}</button>
    @endcan
</form>
<div style="clear:both"></div>
@can('add_settings')
    <div class="panel" style="margin-top:10px;">
        <div class="panel-heading new-setting">
            <hr>
            <h3 class="panel-title"><i class="voyager-plus"></i> {{ __('settings.new') }}</h3>
        </div>
        <div class="panel-body">
            <form action="{{ route('settings.store') }}" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="setting_tab" class="setting_tab" value="{{ $active }}"/>
                <div class="col-md-3">
                    <label for="key">{{ __('generic.key') }}</label>
                    <input type="text" class="form-control" name="key" placeholder="{{ __('settings.help_key') }}"
                           required="required">
                </div>
                <div class="col-md-3">
                    <label for="display_name">{{ __('generic.name') }}</label>
                    <input type="text" class="form-control" name="display_name"
                           placeholder="{{ __('settings.help_name') }}" required="required">
                </div>
                <div class="col-md-3">
                    <label for="type">{{ __('generic.type') }}</label>
                    <select name="type" class="form-control" required="required">
                        <option value="">{{ __('generic.choose_type') }}</option>
                        <option value="text">{{ __('form.type_textbox') }}</option>
                        <option value="text_area">{{ __('form.type_textarea') }}</option>
{{--                        <option value="rich_text_box">{{ __('form.type_richtextbox') }}</option>--}}
                        {{--<option value="code_editor">{{ __('form.type_codeeditor') }}</option>--}}
                        <option value="ckeditor">{{ __('form.type_richtextbox') }}</option>
                        <option value="checkbox">{{ __('form.type_checkbox') }}</option>
                        <option value="radio_btn">{{ __('form.type_radiobutton') }}</option>
                        <option value="select_dropdown">{{ __('form.type_selectdropdown') }}</option>
                        <option value="file">{{ __('form.type_file') }}</option>
                        <option value="file_multiple">{{ __('form.type_file_multiple') }}</option>
                        <option value="image">{{ __('form.type_image') }}</option>
                    </select>
                </div>
                <div class="col-md-3">
                    <label for="group">{{ __('settings.group') }}</label>
                    <select class="form-control group_select group_select_new" name="group">
                        @foreach($groups as $group)
                            <option value="{{ $group }}">{{ $group }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-12">
                    <a id="toggle_options"><i
                                class="voyager-double-down"></i> {{ mb_strtoupper(__('generic.options')) }}</a>
                    <div class="new-settings-options">
                        <label for="options">{{ __('generic.options') }}
                            <small>{{ __('settings.help_option') }}</small>
                        </label>
                        <div id="options_editor" class="form-control min_height_200" data-language="json"></div>
                        <textarea id="options_textarea" name="details" class="hidden"></textarea>
                        <div id="valid_options" class="alert-success alert"
                             style="display:none">{{ __('json.valid') }}</div>
                        <div id="invalid_options" class="alert-danger alert"
                             style="display:none">{{ __('json.invalid') }}</div>
                    </div>
                </div>
                <div style="clear:both"></div>
                <button type="submit" class="btn btn-info pull-right new-setting-btn">
                    <i class="voyager-plus"></i> {{ __('settings.add_new') }}
                </button>
                <div style="clear:both"></div>
            </form>
        </div>
    </div>
@endcan
@can('delete_settings')
    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('generic.close') }}">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        <i class="voyager-trash"></i> {!! __('settings.delete_question', ['setting' => '<span id="delete_setting_title"></span>']) !!}
                    </h4>
                </div>
                <div class="modal-footer">
                    <form action="#" id="delete_form" method="POST">
                        {{ method_field("DELETE") }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger delete-confirm"
                               value="{{ __('settings.delete_confirm') }}">
                        <button type="button" class="btn btn-default"
                                data-dismiss="modal">{{ __('generic.cancel') }}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endcan

<iframe id="form_target" name="form_target" style="display:none"></iframe>
{{--<form id="my_form" action="{{ route('upload') }}" target="form_target" method="POST"--}}
      {{--enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">--}}
    {{--{{ csrf_field() }}--}}
    {{--<input name="image" id="upload_file" type="file" onchange="$('#my_form').submit();this.value='';">--}}
    {{--<input type="hidden" name="type_slug" id="type_slug" value="settings">--}}
{{--</form>--}}

@section('javascript')
    <script>
        $('document').ready(function () {
            $('#toggle_options').click(function () {
                $('.new-settings-options').toggle();
                if ($('#toggle_options .voyager-double-down').length) {
                    $('#toggle_options .voyager-double-down').removeClass('voyager-double-down').addClass('voyager-double-up');
                } else {
                    $('#toggle_options .voyager-double-up').removeClass('voyager-double-up').addClass('voyager-double-down');
                }
            });
            $('.panel-actions .voyager-trash').click(function () {
                var display = $(this).data('display-name') + '/' + $(this).data('display-key');
                $('#delete_setting_title').text(display);
                $('#delete_form')[0].action = '{{ route('settings.destroy', [ 'id' => '__id' ]) }}'.replace('__id', $(this).data('id'));
                $('#delete_modal').modal('show');
            });
            $('[data-toggle="tab"]').click(function () {
                $(".setting_tab").val($(this).html());
            });
        });
    </script>
    <script type="text/javascript">
        $(".group_select").not('.group_select_new').select2({
            tags: true,
            width: 'resolve'
        });
        $(".group_select_new").select2({
            tags: true,
            width: 'resolve',
            placeholder: '{{ __("generic.select_group") }}'
        });
        $(".group_select_new").val('').trigger('change');
    </script>
    <script>
        var options_editor = ace.edit('options_editor');
        options_editor.getSession().setMode("ace/mode/json");
        var options_textarea = document.getElementById('options_textarea');
        options_editor.getSession().on('change', function () {
            console.log(options_editor.getValue());
            options_textarea.value = options_editor.getValue();
        });
    </script>
    <script type="text/javascript" defer>
        $(document).ready(function (e) {
            @foreach($settings->where('type', 'ckeditor') as $setting)
            {!! showEditor('settings-ckeditor-' . $setting->id) !!}
            @endforeach
        });
    </script>
@stop
