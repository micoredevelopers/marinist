@if($errors->count)
    @foreach ($errors->all() as $error)
        <div class="alert alert-danger">{{ $error }}</div>
    @endforeach
@endif

@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
@if(isset($message))
    <div class="alert alert-success">
        {{$message  }}
    </div>
@endif

<div class="col-xs-12">
    <div class="panel-body">
        <form action="{{route($controller.'.store')}}" method="post" class="form-horizontal" enctype="multipart/form-data">
            @csrf
            <div class="form-group clearfix">
                <label class="col-xs-2 control-label">Ключ</label>
                <div class="col-sm-4">
                    <input type="text" name="key" value="{{old('key', '')}}" class="form-control">
                </div>
            </div>
            <div class="form-group clearfix">
                <label class="col-xs-2 control-label">Значение</label>
                <div class="col-sm-4">
                    <input type="text" name="value" value="{{old('value', '')}}" class="form-control">
                </div>
            </div>
            <div class="form-group clearfix">
                <label class="col-xs-2 control-label">Описание</label>
                <div class="col-sm-4">
                    <input type="text" name="name" value="{{old('name', '')}}" class="form-control">
                </div>
            </div>
            <div class="form-group clearfix">
                <label class="col-xs-2 control-label">Описание - подсказка</label>
                <div class="col-sm-4">
                    <input type="text" name="description" value="{{old('description') ?? ''}}" class="form-control">
                </div>
            </div>
            <div class="text-center col-xs-2">
                <button class="btn btn-danger form-control" type="submit" name="saveClose">Создать и закрыть</button>
            </div>
            <div class="text-center col-xs-2">
                <button class="btn btn-primary form-control" type="submit">Создать</button>
            </div>
        </form>
    </div>
</div>