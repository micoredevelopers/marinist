<form action="{{route('admin-menus.update', '*')}}" method="post">
    <input type="hidden" name="_method" value="PATCH">
    @csrf
    <div class="panel panel-default">
        <div class="panel-body clearfix">
            <a href="{{route($controller.'.create')}}" class="btn btn-default">Добавить +</a>
            <button class="btn btn-primary">Сохранить</button>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-hover table-condensed table-striped">
            <tr class="active">
                <th>ID</th>
                <th>Сортировка</th>
                <th>Название</th>
                <th>Родитель</th>
                <th>Значок шрифт</th>
                <th>Включен</th>
                <th>URL</th>
                <th>route</th>
                <th>Gate</th>
                <th>Контент</th>
            </tr>
            @if(isset($list))
                <tbody data-sortable-container="true" data-table="{{ $table }}">
                @forelse($list as $item)
                    <tr class="draggable" data-id="{{ $item->id }}" data-sort="{{ $item->sort }}">
                        <td>
                            {{$item->id}}
                            <input type="hidden" name="id[{{$item->id}}]" value="{{$item->id}}">
                        </td>
                        <td>
                            @include('admin.partials.sort_handle')
                        </td>
                        <td>
                            <input type="text" name="name[{{$item->id}}]" value="{{$item->name}}" class="form-control">
                        </td>
                        <td>
                            <select name="parent_id[{{$item->id}}]" class="form-control">
                                <option value="0" {{ ($item->parent_id == 0) ? 'selected' : '' }}>-- Нет --</option>
                                @foreach($list as $listItem)
                                    @if ($listItem->id != $item->id)
                                        <option value="{{ $listItem->id }}" {{ ($item->parent_id == $listItem->id) ? 'selected' : '' }}>{{ $listItem->name }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </td>
                        <td>
                            <input type="text" class="form-control" name="icon_font[{{$item->id}}]" value="{{$item->icon_font}}">
                        </td>
                        <td>
                            <div class="check-styled">
                                <input type="checkbox" value="1" id="active-{{$item->id}}"
                                       name="active[{{$item->id}}]" {{$item->active == 1 ? 'checked' : ''}} />
                                <label for="active-{{$item->id}}"></label>
                            </div>
                        </td>
                        <td>
                            <input type="text" name="url[{{$item->id}}]" value="{{$item->url}}" class="form-control">
                        </td>
                        <td>
                            <input type="text" name="route[{{$item->id}}]" value="{{$item->route}}"
                                   class="form-control">
                        </td>
                        <td>
                            <input type="text" name="gate_rule[{{$item->id}}]" value="{{$item->gate_rule}}"
                                   class="form-control">
                        </td>
                        <td>
                            <input type="text" name="content_provider[{{$item->id}}]" value="{{$item->content_provider}}"
                                   class="form-control">
                        </td>
                    </tr>
                @empty
                    <h3>Пусто</h3>
                @endforelse
                </tbody>
            @endif
        </table>
    </div>
</form>