<form action="{{ route('sliders.update', $edit->id) }}" method="post" class="form-horizontal"
      enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <input type="hidden" name="date_update" value="{{ now() }}">
    <ul class="nav nav-tabs mb-5" role="tablist">
        <li role="presentation"><a class="active" href="#dataDefault" aria-controls="home" role="tab" data-toggle="tab">Основное</a>
        </li>
        <li role="presentation"><a href="#photosList" aria-controls="profile" role="tab"
                                   data-toggle="tab">Фотографии</a></li>
    </ul>
    <section class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="dataDefault">
            <div class="panel-body">
                <div class="form-group">
                    <label for="name" class=" control-label">Имя</label>
                    <input type="text" id="name" name="name" value="{{$edit->name ?? ''}}" class="form-control">
                </div>
                @if(storageFileExists($edit->photo))
                    <div class="form-group">
                        <label for="" class=" control-label">Текущая обложка</label>
                        <div class="col">
                            <div class="image-actions" data-image-actions="">
                                <img src="{{ checkImage($edit->photo) }}?{{storageFilemtime($edit->photo)}}"
                                     class="img-responsive deleteable croppable" width="150"
                                     data-photo-id="{{ $edit->id ?? '' }}"
                                     data-photo-table="{{$photo_table ?? '' }}">
                            </div>
                        </div>
                    </div>
                @endif
                <div class="form-group">
                    <label for="active" class=" control-label">Включен</label>
                    <div class="check-styled">
                        <input type="checkbox" value="1" id="active"
                               name="active" {{$edit->active == 1 ? 'checked' : ''}} />
                        <label for="active"></label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="photo" class=" control-label">Обложка</label>
                    <input type="file" name="photo" id="photo" class="filestyle"
                           data-buttonText="Выберите изображение"
                           data-placeholder="Файла нет" accept="image/*">
                </div>
                <div class="form-group">
                    <label for="description" class=" control-label">Описание</label>
                    <textarea name="description" id="description" cols="30"
                              rows="10">{{$edit->description}}</textarea>
                </div>
                <div class="form-group">
                    <label for="sub_description" class=" control-label">Краткое Описание</label>
                    <textarea name="sub_description" id="sub_description" cols="30"
                              rows="10">{{$edit->sub_description}}</textarea>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="photosList">
            <div class="panel-body">
                @include('admin.additionalPhotos')
            </div>
        </div>
    </section>
    @include('admin.partials.submit_update_buttons')
</form>
@section('javascript')
    <script defer>
        $(document).ready(function () {
            {!! showEditor('description') !!}
            {!! showEditor('sub_description') !!}
            $("a.fancy").fancybox();
        });
    </script>
@stop