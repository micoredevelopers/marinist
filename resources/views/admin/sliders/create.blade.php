<form action="{{route($controller.'.store')}}" method="post" class="form-horizontal" enctype="multipart/form-data">
    @csrf
    @method('post')
    <div class="form-group clearfix">
        <label class="control-label">Название</label>
        <input type="text" name="name" value="{{old('name', '')}}" class="form-control">
    </div>
    <div class="form-group clearfix">
        <label class="control-label">Описание</label>
        <input type="text" name="description" value="{{old('description', '')}}" class="form-control">
    </div>
    @include('admin.partials.submit_create_buttons')
</form>