<main class="">
    <div class="modal fade" id="roleModal" tabindex="-1" role="dialog" aria-labelledby="roleModalLabel">
        <div class="modal-dialog" role="document">
            {!! Form::open(['method' => 'post']) !!}

            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="roleModalLabel">Роль</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <!-- name Form Input -->
                    <div class="form-group @if ($errors->has('name')) has-error @endif">
                        {!! Form::label('name', 'Название') !!}
                        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Название роли']) !!}
                        @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>

                    <!-- Submit Form Button -->
                    {!! Form::submit('Создать', ['class' => 'btn btn-primary']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="row m-0 p-4 bg-white">
        <div class="col-md-5">
            <h3>Роли</h3>
        </div>
        <div class="col-md-7 page-action text-right">
            @can('create_roles')
                <a href="#" class="btn btn-sm btn-success pull-right" data-toggle="modal" data-target="#roleModal"> <i
                            class="glyphicon glyphicon-plus"></i> Создать</a>
            @endcan
        </div>
    </div>


    <div class="col">
        <div class="p-4 bg-white mt-4">
            @forelse ($roles as $role)
                {!! Form::model($role, ['method' => 'PUT', 'route' => ['roles.update',  $role->id ], 'class' => 'm-b']) !!}

                @if($role->name === 'Admin')
                    @include('shared._permissions', [
                                  'title' => $role->name .' Привилегии',
                                  'options' => ['disabled'] ])
                @else
                    @include('shared._permissions', [
                                  'title' => $role->name .' Привилегии',
                                  'model' => $role ])
                    {{--                        @can('edit_roles')--}}
                    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
                    {{--@endcan--}}
                @endif

                {!! Form::close() !!}

            @empty
                <p>No Roles defined, please run <code>php artisan db:seed</code> to seed some dummy data.</p>
            @endforelse
        </div>
    </div>


</main>