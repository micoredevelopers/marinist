<form action="{{ route('menu.update', $edit->id) }}" method="post" class="form-horizontal"
      enctype="multipart/form-data">
    @csrf
    @method('patch')
    <ul class="nav nav-tabs mb-5" role="tablist">
        <li role="presentation">
            <a class="active" href="#dataDefault" aria-controls="home" role="tab" data-toggle="tab">Основное</a>
        </li>
    </ul>
    <section class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="dataDefault">
            <div class="panel-body">
                <div class="form-group">
                    <label for="name" class=" control-label">Название меню</label>
                    <input type="text" id="name" name="name" value="{{$edit->name ?? ''}}" class="form-control"
                           autocomplete="off">
                </div>
                <div class="form-group">
                    <label for="url" class="control-label"
                           title="Url адресс используется для создания уникального url для меню">
                        Url адресс</label>
                    <input type="text" id="url" value="{{$edit->url}}" name="url" class="form-control" autocomplete="off">
                </div>
                <div class="form-group">
                    <label for="icon" class="control-label">Значок шрифта</label>
                    <input type="text" id="icon" value="{{$edit->icon}}" class="form-control" autocomplete="off">
                </div>
                @if(storageFileExists($edit->photo))
                    <div class="form-group">
                        <label for="" class=" control-label">Текущая Обложка</label>
                        <div class="col">
                            <div class="image-actions" data-image-actions="">
                                <img src="{{ checkImage($edit->photo) }}?{{storageFilemtime($edit->photo)}}"
                                     class="img-responsive deleteable croppable" width="150"
                                     data-photo-id="{{ $edit->id ?? '' }}"
                                     data-photo-table="{{$photo_table ?? '' }}">
                            </div>
                        </div>
                    </div>
                @endif
                <div class="form-group">
                    <label for="photo" class=" control-label">Обложка меню</label>
                    <input type="file" name="photo" id="photo" class="filestyle"
                           data-buttonText="Выберите изображение"
                           data-placeholder="Файла нет" accept="photo/*">
                </div>
                <div class="form-group">
                    <label class=" control-label">Включен</label>
                    <div class="col">
                        <div class="check-styled">
                            <input type="checkbox" value="1" id="active"
                                   name="active" {{$edit->active == 1 ? 'checked' : ''}} />
                            <label for="active"></label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('admin.partials.submit_update_buttons')
</form>
@section('javascript')
    <script type="text/javascript" defer>
        $(document).ready(function () {
            {!! showEditor('description') !!}
            {!! showEditor('sub_description') !!}
        });
    </script>
@stop