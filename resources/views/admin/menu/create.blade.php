<form action="{{ route('menu.store') }}" method="post" class="form-horizontal" enctype="multipart/form-data">
    @method('post')
    @csrf
    <input type="hidden" name="date_update" value="{{ now() }}">
    <input type="hidden" name="date_add" value="{{ now() }}">

    <div class="form-group">
        @if ($errors->has('name')) <p class="text-danger">{{ $errors->first('name') }}</p> @endif
        <label for="name" class=" control-label">@lang('generic.title')</label>
        <input type="text" id="name" name="name" value="{{old('name') ?? ''}}" class="form-control">
    </div>
    <div class="form-group">
        @if ($errors->has('url')) <p class="text-danger">{{ $errors->first('url') }}</p> @endif
        <label for="url" class=" control-label">Url адресс</label>
        <input type="text" id="url" name="url" value="{{ old('url') }}" class="form-control">
    </div>
    <div class="form-group">
        <label for="icon" class="control-label">Значок шрифта</label>
        <input type="text" id="icon" value="{{ old('icon') }}" class="form-control" autocomplete="off">
    </div>
    <div class="form-group">
        <label for="active" class=" control-label">Включен</label>
        <div>
            <div class="check-styled">
                <input type="checkbox" value="1" id="active" name="active" {{ old('active') == 1 ? 'checked' : ''}}
                autocomplete="off"/>
                <label for="active"></label>
            </div>
        </div>
    </div>
    <div class="form-group">
        @if ($errors->has('photo')) <p class="text-danger">{{ $errors->first('photo') }}</p> @endif
        <label for="photo" class=" control-label">Обложка</label>
        <input type="file" name="photo" id="photo" class="filestyle" data-buttonText="Выберите изображение"
               data-placeholder="Файла нет" accept="image/*">
    </div>
    @include('admin.partials.submit_create_buttons')
</form>
