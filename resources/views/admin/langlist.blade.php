<div class="localisation-block">
    <div class="dropup">
        @if (count($languages) > 1)
        <ul class=" my-dropdown">
            @foreach($languages as $language)
            @if($language->key !== getCurrentLocale())
            <li>
                <a rel="alternate" hreflang="{{ $language->key }}" class="dropdown-item"
                    href="{{ langUrl(null, $language->key) }}">
                    <img src="{{asset('/images/flags/' . $language->key . '.png')}}" alt="" class="langIcon">
                    <span class="d-inline-block ml-1">{{ $language->name}}</span>
                </a>
            </li>
            @endif
            @endforeach
        </ul>
        @endif
        <button class="btn btn-default dropdown-toggle" type="button" >
            <img src="{{asset('/images/flags/' .  getCurrentLocale()  . '.png')}}" alt="">
            <span class="d-inline-block ml-1">{{ LaravelLocalization::getCurrentLocaleNative() }}</span>
            <span class="caret"></span>
         </button>
    </div>
</div>