<form action="{{ route('flat.update', $edit->id) }}" method="post" class="form-horizontal"
      enctype="multipart/form-data">
    @csrf
    @method('patch')
    @if( storageFileExists($edit->photo))
        <div class="form-group">
            <label for="" class=" control-label">Проект</label>
            <div class="col">
                <a href="{{ imgOrigin(checkImage($edit->photo)) }}" class="fancy">
                    <img src="{{ checkImage($edit->photo) }}?{{storageFilemtime($edit->photo)}}"
                         class="img-responsive" width="150" alt="">
                </a>
            </div>
        </div>
    @endif
    @if (isSuperAdmin())
        <div class="form-group">
            <label for="photo" class=" control-label">Обложка галлереи</label>
            <input type="file" name="photo" id="photo" class="filestyle"
                   data-buttonText="Выберите изображение"
                   data-placeholder="Файла нет" accept="image/*">
        </div>
    @endif
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label for="number" class="control-label">№ Квартиры</label>
                <input type="text" name="number" id="number" class=" form-control"
                       value="{{ $edit->number }}">
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="sum" class="control-label">Цена квартиры</label>
                <input type="number" name="sum" id="sum" class=" form-control"
                       value="{{ $edit->sum }}">
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="price" class="control-label">Цена за м²</label>
                <input type="number" name="price" id="price" class=" form-control"
                       value="{{ $edit->price }}">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label for="square" class="control-label">Площадь м²</label>
                <input type="number" name="square" id="square" class=" form-control"
                       value="{{ $edit->square }}" step="0.01" min="0">
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="room" class="control-label">Комнат</label>
                <input type="number" name="room" id="room" class=" form-control"
                       value="{{ $edit->room }}">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label for="twenty_five_percent" class="control-label">25%</label>
                <input type="number" name="twenty_five_percent" id="twenty_five_percent" class=" form-control"
                       value="{{ $edit->twenty_five_percent }}">
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="fifty_percent" class="control-label">50%</label>
                <input type="number" name="fifty_percent" id="fifty_percent" class=" form-control"
                       value="{{ $edit->fifty_percent }}">
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="seventy_five_percent" class="control-label">75%</label>
                <input type="number" name="seventy_five_percent" id="seventy_five_percent" class=" form-control"
                       value="{{ $edit->seventy_five_percent }}">
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="one_hundred_percent" class="control-label">100%</label>
                <input type="number" name="one_hundred_percent" id="one_hundred_percent" class=" form-control"
                       value="{{ $edit->one_hundred_percent }}">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">

                    <label for="status" class="control-label">Статус квартиры</label>
                    <select class="selectpicker" id="status" name="status_id" data-style="btn btn-primary btn-round" title="Статус квартиры">
                        @foreach($statuses as $status)
                            @if($edit->status_id == $status->id)
                                <option selected value="{{$status->id}}">{{$status->status_name}}</option>
                            @else
                                <option value="{{$status->id}}">{{$status->status_name}}</option>
                            @endif
                        @endforeach
                    </select>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label class=" control-label">Включен</label>
                <div class="col">
                    <div class="check-styled">
                        <input type="checkbox" value="1" id="active" autocomplete="off"
                               name="active" {{$edit->active == 1 ? 'checked' : ''}} />
                        <label for="active"></label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label class=" control-label">Акция</label>
                <div class="col">
                    <div class="check-styled">
                        <input type="checkbox" value="1" id="stock" autocomplete="off"
                               name="stock" {{$edit->stock == 1 ? 'checked' : ''}} />
                        <label for="stock"></label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label class=" control-label">Вид на море</label>
                <div class="col">
                    <div class="check-styled">
                        <input type="checkbox" value="1" id="sea_view" autocomplete="off"
                               name="sea_view" {{$edit->sea_view == 1 ? 'checked' : ''}} />
                        <label for="sea_view"></label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label class=" control-label">Терраса</label>
                <div class="col">
                    <div class="check-styled">
                        <input type="checkbox" value="1" id="terrace" autocomplete="off"
                               name="terrace" {{$edit->terrace == 1 ? 'checked' : ''}} />
                        <label for="terrace"></label>
                    </div>
                </div>
            </div>
        </div>
    </div>


    @include('admin.partials.submit_update_buttons')
</form>
