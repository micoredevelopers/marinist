<ul class="{{ $settings['ul'][0] ?? '' }}">
    @foreach($sections as $section)
        @php
            $linkSection = isSuperAdmin() ? route('section.edit', $section->id) : 'javascript:void(0)';
        @endphp
        <li class="drop-item">
            <a href="{{ $linkSection }}"><b>Секция {{ $section->number }}</b></a>
            @if ($section->floors->isNotEmpty())
                <ul class="drop-block">
                    @foreach($section->floors as $floor)
                        @php
                            $linkFloor = isSuperAdmin() ? route('floor.edit', $floor->id) : 'javascript:void(0)';
                        @endphp
                        <li class="drop-item">
                            <a href="{{ $linkFloor }}">Этаж {{ $floor->number ?? '' }}
                                <small>[id:{{ $floor->id }}]</small>
                            </a>
                            <ul class="drop-block">
                                @foreach($floor->flats as $flat)
                                    <li>
                                        <a href="{{ route('flat.edit', $flat->id) }}">Квартира №{{ $flat->number }}
                                            <small>[id:{{ $flat->id }}]</small>
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </li>
                    @endforeach
                </ul>
            @endif
        </li>
    @endforeach
</ul>