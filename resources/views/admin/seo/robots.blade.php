<form method="post" action="{{route('admin.seo.robots.update')}}">
    @csrf
    @method('put')

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
            <label for="robots" class=" control-label">Редактирование robots.txt</label>
            <textarea cols="30" rows="10" id="robots" name="robots" class="form-control">{{$data??old('robots')}}</textarea>
            </div>
        </div>
    </div>
    @include('admin.partials.submit_update_buttons')
</form>
