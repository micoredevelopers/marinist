<div class="table-responsive">
    <table class="table table-shopping">
        <thead>
        <tr>
            <th class="th-description">@lang('form.title')</th>
        </tr>
        </thead>
        <tbody data-sortable-container="" data-table="menu">
        @foreach($list as $item)
            <tr>
                <td>
                    {{ $item['name'] }}
                </td>
                <td class="text-primary text-right">
                    <div class="dropdown menu_drop">
                        <button
                            class="btn btn-secondary dropdown-toggle" type="button"
                            data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">menu</i>
                        </button>
                        <div class="dropdown-menu" >
                            <a href="{{$item['url']}}" class="dropdown-item">Редактировать</a>
                        </div>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
