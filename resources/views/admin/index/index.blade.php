<div class="row">
    <div class="col-md-3">
        <a class="btn btn-danger" href="{{route('admin.clear.cache')}}">Очистить кеш приложения</a>
    </div>
    <div class="col-md-3">
        <a class="btn btn-danger" href="{{route('admin.make.sitemap')}}">Обновить карту сайта</a>
    </div>
</div>
