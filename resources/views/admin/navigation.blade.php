<header>
    <div class="container-fluid p-0">
        <div class="row m-0">
            <div class="col-12 p-0">
                <div class="nav_container">
                    <p class="logo_container">pro100net.ru
                    <!-- <img src="{{ asset('images/admin/logo.png') }}" alt=""> -->
                    </p>
                    <ul class="nav_menu">
                        @can('view_mains')
                            <li class="nav_item @if (Route::currentRouteName() == 'admin.mains.index' or Route::currentRouteName() == 'admin.index') active @endif">
                                <a href="{{ route('admin.mains.index') }}">
                                    <div class="img_container"><img src="{{ asset('images/admin/Shape.svg') }}" alt="">
                                    </div>
                                    <div class="text"><p>Главная</p></div>
                                </a>
                            </li>
                        @endcan
                        @can('view_contacts')
                            <li class="nav_item @if (Route::currentRouteName() == 'admin.contacts.index') active @endif">
                                <a href="{{ route('admin.contacts.index') }}">
                                    <div class="img_container"><img src="{{ asset('images/admin/spape-1.svg') }}"
                                                                    alt="">
                                    </div>
                                    <div class="text"><p>Контакты и заголовки</p></div>
                                </a>
                            </li>
                        @endcan
                        @can('view_providers')
                            <li class="nav_item @if (Route::currentRouteName() == 'admin.providers.index') active @endif">
                                <a href="{{ route('admin.providers.index') }}">
                                    <div class="img_container"><img src="{{ asset('images/admin/man.svg') }}"
                                                                    alt="">
                                    </div>
                                    <div class="text"><p>Провайдеры</p></div>
                                </a>
                            </li>
                        @endcan
                        @can('view_comments')
                            <li class="nav_item @if (Route::currentRouteName() == ' admin.comments.moderation.list') active @endif">
                                <a href="{{ route('admin.comments.moderation.list') }}">
                                    <div class="img_container"><img src="{{ asset('images/admin/man.svg') }}"
                                                                    alt="">
                                    </div>
                                    <div class="text"><p>Комментарии</p></div>
                                </a>
                            </li>
                        @endcan
                        @can('view_articles')
                            <li class="nav_item @if (Route::currentRouteName() == 'admin.article.index') active @endif">
                                <a href="{{ route('admin.articles.index') }}">
                                    <div class="img_container"><img
                                                src="{{ asset('images/admin/settings_icon.png') }}" alt="">
                                    </div>
                                    <div class="text"><p>Статьи</p></div>
                                </a>
                            </li>
                        @endcan
                        @can('view_applications')
                            <li class="nav_item @if (Route::currentRouteName() == 'admin.applications.index') active @endif">
                                <a href="{{ route('admin.applications.index') }}">
                                    <div class="img_container"><img
                                                src="{{ asset('images/admin/settings_icon.png') }}" alt="">
                                    </div>
                                    <div class="text"><p>Заявки</p></div>
                                </a>
                            </li>
                        @endcan
                        @can('view_services')
                            <li class="nav_item @if (Route::currentRouteName() == 'admin.services.index') active @endif">
                                <a href="{{ route('admin.services.index') }}">
                                    <div class="img_container"><img
                                                src="{{ asset('images/admin/settings_icon.png') }}" alt="">
                                    </div>
                                    <div class="text"><p>Сервисы</p></div>
                                </a>
                            </li>
                        @endcan
                        {{--<li class="nav_item @if (Route::currentRouteName() == 'admin.comment.index') active @endif">--}}
                        {{--<a href="{{ route('admin.comment.index') }}">--}}
                        {{--<div class="img_container"><img src="{{ asset('images/admin/settings_icon.png') }}" alt="">--}}
                        {{--</div>--}}
                        {{--<div class="text"><p>Комментарии и отзывы</p></div>--}}
                        {{--</a>--}}
                        {{--</li>--}}
                        @can('view_settings')
                            <li class="nav_item @if (Route::currentRouteName() == 'admin.setting') active @endif">
                                <a href="{{ route('admin.setting') }}">
                                    <div class="img_container"><img
                                                src="{{ asset('images/admin/settings_icon.png') }}"
                                                alt=""></div>
                                    <div class="text"><p>Настройки</p></div>
                                </a>
                            </li>
                        @endcan
                        @can('view_users')
                            <li class="nav_item @if (Route::currentRouteName() == 'admin.users.index') active @endif">
                                <a href="{{ route('admin.users.index') }}">
                                    <div class="img_container"><img
                                                src="{{ asset('images/admin/users.svg') }}"></div>
                                    <div class="text"><p>Пользователи</p></div>
                                </a>
                            </li>
                        @endcan
                        <li class="nav_item">
                            <div class="btn_container">
                                <form id="logout-form" action="{{ route('admin.logout') }}" method="POST">
                                    @csrf
                                    <button type="submit">Выйти</button>
                                </form>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>
