<form action="{{ route('meta.store') }}" method="post" class="form-horizontal" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        @if ($errors->has('url')) <p class="text-danger">{{ $errors->first('url') }}</p> @endif
        <label for="url" class=" control-label"
               title="">Url адресс</label>
        <input type="text" id="url" name="url" value="{{ old('url') }}" class="form-control">
    </div>
    <div class="form-group">
        <label for="active" class=" control-label">Включен</label>
        <div class="check-styled">
            <input type="checkbox" value="1" id="active" name="active" checked/>
            <label for="active"></label>
        </div>
    </div>

    <div class="form-group">
        @if ($errors->has('h1')) <p class="text-danger">{{ $errors->first('h1') }}</p> @endif
        <label for="h1" class=" control-label"
               title="">H1</label>
        <input type="text" id="h1" name="h1" value="{{ old('h1') ?? '' }}" class="form-control">
    </div>
    <div class="form-group">
        @if ($errors->has('meta_title')) <p class="text-danger">{{ $errors->first('meta_title') }}</p> @endif
        <label for="meta_title" class=" control-label"
               title="">Meta Title</label>
        <input type="text" id="meta_title" name="meta_title" value="{{ old('meta_title') ?? '' }}"
               class="form-control">
    </div>
    <div class="form-group">
        @if ($errors->has('meta_keywords')) <p class="text-danger">{{ $errors->first('meta_keywords') }}</p> @endif
        <label for="meta_keywords" class=" control-label"
               title="">Meta Keywords</label>
        <input type="text" id="meta_keywords" name="meta_keywords" value="{{ old('meta_keywords') ?? '' }}"
               class="form-control">
    </div>
    <div class="form-group">
        @if ($errors->has('meta_description')) <p class="text-danger">{{ $errors->first('meta_description') }}</p> @endif
        <label for="meta_description" class=" control-label"
               title="">Meta Description</label>
        <input type="text" id="meta_description" name="meta_description"
               value="{{ old('meta_description') ?? '' }}" class="form-control">
    </div>
    <div class="form-group">
        <label for="text_top" class=" control-label">Текст вверху страницы</label>
        <textarea name="text_top" id="text_top" cols="30" rows="10">{{ old('text_top') ?? '' }}</textarea>
    </div>
    <div class="form-group">
        <label for="text_bottom" class=" control-label">Текст внизу страницы</label>

        <textarea name="text_bottom" id="text_bottom" cols="30"
                  rows="10">{{ old('text_bottom') ?? '' }}</textarea>
    </div>
    @include('admin.partials.submit_create_buttons')
</form>
@section('javascript')
    <script defer>
        $(document).ready(function () {
            {!! showEditor('text_top') !!}
            {!! showEditor('text_bottom') !!}
        });

    </script>
@stop