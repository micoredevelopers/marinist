<?php /** @var $edit \App\Models\Meta  */ ?>
<form action="{{ route('meta.update', $edit->id)}}" method="post" class="form-horizontal"
      enctype="multipart/form-data">
    @csrf
    @method('PATCH')
    <div class="form-group mb-5">
        <div class="col">
            <div class="row">
                <div class=" col-8">
                    @include('admin.partials.submit_update_buttons')
                </div>
                <div class="col-sm-4">
                    <a class="btn btn-default" target="_blank" href="{{ langUrl($edit->url) }}">Перейти <i
                                class="fa fa-external-link"></i></a>
                </div>
            </div>
        </div>
    </div>
    @if ($edit->isDefault())
        <h4 class="badge badge-warning d-inline-block">Мета данные по умолчанию</h4>
    @endif
    <div class="form-group">
        <label for="url" class=" control-label position-static"
               title="">Url адресс</label>
        <input type="text" id="url" name="url" value="{{ $edit->url }}" class="form-control" readonly="">
    </div>
    <div class="form-group">
        <label class=" control-label">Включен</label>
        <div class="check-styled">
            <input type="checkbox" value="1" id="active" name="active"
                   {{ $edit->active == 1 ? 'checked' : '' }}  autocomplete="off"/>
            <label for="active"></label>
        </div>
    </div>
    @if($edit->url !== '*')
        <div class="form-group">
            <label for="h1" class=" control-label" title="">H1</label>
            <input type="text" id="h1" name="h1" value="{{ $edit->h1 }}" class="form-control">
        </div>
    @endif
    <div class="form-group">
        <label for="meta_title" class=" control-label"
               title="">Meta Title</label>
        <input type="text" id="meta_title" name="meta_title" value="{{ $edit->meta_title }}"
               class="form-control">
    </div>
    <div class="form-group">
        <label for="meta_keywords" class=" control-label"
               title="">Meta Keywords</label>
        <input type="text" id="meta_keywords" name="meta_keywords" value="{{ $edit->meta_keywords }}"
               class="form-control">
    </div>
    <div class="form-group">
        <label for="meta_description" class=" control-label"
               title="">Meta Description</label>
        <input type="text" id="meta_description" name="meta_description"
               value="{{ $edit->meta_description }}"
               class="form-control">
    </div>
    @if($edit->url !== '*')
        <div class="form-group">
            <label for="text_top" class=" control-label">Текст вверху страницы</label>
            <textarea name="text_top" id="text_top" cols="30" rows="10">{{ $edit->text_top}}</textarea>
        </div>
        <div class="form-group">
            <label for="text_bottom" class=" control-label">Текст внизу страницы</label>
            <textarea name="text_bottom" id="text_bottom" cols="30"
                      rows="10">{{ $edit->text_bottom }}</textarea>
        </div>
    @endif
    @include('admin.partials.submit_update_buttons')
</form>
@section('javascript')
    <script defer>
        $(document).ready(function () {
            {!! showEditor('text_top') !!}
            {!! showEditor('text_bottom') !!}
        });
    </script>
@stop