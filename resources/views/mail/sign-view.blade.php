<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Новая заявка на просмотр</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<?php /** @var $feedback \App\Models\Request */ ?>
<div class="container">
    <table class="table">
        <tbody>
        <tr>
            <td>Имя:</td>
            <td>{{ $feedback->name ?? false }}</td>
        </tr>
        @if ($feedback->email ?? false)
            <tr>
                <td>Email:</td>
                <td>{{ $feedback->email }}</td>
            </tr>
        @endif
        @if ($feedback->text ?? false)
            <tr>
                <td>Сообщение:</td>
                <td>{{ $feedback->text }}</td>
            </tr>
        @endif
        @if ($feedback->phone ?? false)
            <tr>
                <td>Телефон:</td>
                <td>{{ $feedback->phone }}</td>
            </tr>
        @endif
        @if ($feedback->flat_id AND $feedback->flat)
            <tr>
                <td>Квартира:</td>
                <td>
                    <a href="{{ route('flat.edit', $feedback->flat_id ) }}">{{ $feedback->flat->number }}</a>
                </td>
            </tr>
        @endif
        @if ($feedback->language_id && $feedback->getLanguage)
            <tr>
                <td>Язык:</td>
                <td>
                    {{ $feedback->getLanguage->name }}
                </td>
            </tr>
        @endif
        @if ($feedback->referer ?? false)
            <tr>
                <td colspan="2" class="text-center">
                    <a href="{{ $feedback->referer }}" target="_blank">Страница заявки</a>
                </td>
            </tr>
        @endif
        @if($data)
            <tr>
                @if($data['section'] ?? false)
                    <td colspan="2" class="text-center">
                        {{ $data['section'] }}
                    </td>
                @endif
            </tr>
            <tr>
                @if($data['floor'] ?? false)
                    <td colspan="2" class="text-center">
                        {{ $data['floor'] }}
                    </td>
                @endif
            </tr>
            <tr>
                @if($data['flat'] ?? false)
                    <td colspan="2" class="text-center">
                        {{ $data['flat'] }}
                    </td>
                @endif
            </tr>
        @endif
        @if($data['utms'] ?? false)
            <tr>
                <td colspan="2" class="text-center">
                    <p>UTM</p>
                    <table class="table">
                        @foreach($data['utms'] as $name => $value)
                            <tr>
                                <td>{{ $name }}:</td>
                                <td>{{ $value }}</td>
                            </tr>
                        @endforeach
                    </table>
                </td>
            </tr>
        @endif
        </tbody>
    </table>
</div>

</body>
</html>
