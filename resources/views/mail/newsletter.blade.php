<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Новая заявка на просмотр</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <table class="table">
        <tbody>
        <tr>
            <td>Имя:</td>
            <td>{{ $item['name'] }}</td>
        </tr>
        @if(array_key_exists('surname',$item))
        <tr>
            <td>Фамилия:</td>
            <td>{{ $item['surname'] }}</td>
        </tr>
        @endif
        @if(array_key_exists('email',$item))
        <tr>
            <td>Email:</td>
            <td>{{ $item['email'] }}</td>
        </tr>
        @endif
        @if(array_key_exists('agency',$item))
        <tr>
            <td>Агенство:</td>
            <td>{{ $item['agency'] }}</td>
        </tr>
        @endif
        <tr>
            <td>Филиал:</td>
            <td>{{ $item['branch'] }}</td>
        </tr>
        <tr>
            <td>Телефон:</td>
            <td>{{ $item['phone'] }}</td>
        </tr>
        @if(array_key_exists('text',$item))
            <tr>
                <td>Коментарий:</td>
                <td>{{ $item['text'] }}</td>
            </tr>
        @endif
        </tbody>
    </table>
</div>

</body>
</html>
