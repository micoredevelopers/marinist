<div class="modal fade" id="modalAppointment" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close modal-header-close" data-dismiss="modal" aria-label="Close">
					<span></span>
					<span></span>
				</button>
				<h5 class="modal-title">{{ getTranslate('request.sign-to-view') }}</h5>
			</div>
			<div class="modal-body">
				<form action="{{ route('request.sign') }}" class="modal-form-submit">
                    <input type="hidden" name="section" class="input-flat">
                    <input type="hidden" name="floor" class="input-flat">
                    <input type="hidden" name="flat" class="input-flat">
                    <input type="hidden" name="flat_id" class="input-flat">

                    <input type="hidden" name="link">
					<input type="text" name="name" min="2" placeholder="{{ getTranslate('request.name') }}" class="modal-styled-input" required>
					<input type="email" min="5" name="email" placeholder="{{ getTranslate('request.email') }}" class="modal-styled-input" required>
					<input type="tel" name="phone" min="5" placeholder="{{ getTranslate('request.phone') }}" class="modal-styled-input" required>
					<input type="text" name="text" placeholder="{{ getTranslate('request.message') }}" class="modal-styled-input">
					<button type="submit" class="button-wave">
                        <span class="wave-img"></span>
                        <span class="button-text">{{ getTranslate('request.send') }}</span>
                    </button>
				</form>
			</div>
		</div>
	</div>
</div>
