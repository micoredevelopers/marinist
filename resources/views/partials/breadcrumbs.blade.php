<div class="container">
    <div class="row">
        <div class="col-12">
            <nav aria-label="breadcrumb">
                <ol itemscope itemtype="http://schema.org/BreadcrumbList" class="breadcrumb">
                    @foreach($breadcrumbs as $bread)
                        @if(strlen($bread['url']))
                            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"
                                class="breadcrumb-item">
                                <a itemprop="item" href="{{$bread['url']}}">
                                    <span itemprop="name">{{$bread['name']}}</span>
                                </a>
                                <meta itemprop="position" content="{{$loop->iteration}}"/>
                            </li>
                        @else
                            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="breadcrumb-item active">
                                <link itemprop="item" href="">
                                <span itemprop="name">{{$bread['name']}}</span>
                                <meta itemprop="position" content="{{$loop->iteration}}" />
                            </li>
                        @endif
                    @endforeach
                </ol>
            </nav>
        </div>
    </div>
</div>
