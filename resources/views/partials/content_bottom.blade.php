@if($contentBottom = showMeta('', 'text_bottom'))
    <div class="container">
        {!! $contentBottom !!}
    </div>
@endif