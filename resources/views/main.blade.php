<main class="main-page">
	<section class="section-main">
		<h1 class="hidden">Marinist - Главная</h1>
		<div class="main-image"></div>
		{{--<div class="video-box">--}}
		{{--	<button type="button" class="video-btn video-btn_stop">--}}
		{{--    <span class="play-box">--}}
		{{--      <img class="play-box__icon" src="{{ asset('images/stop.svg') }}" alt="Остановить видео">--}}
		{{--    </span>--}}
		{{--		<span class="btn-text">{{ getTranslate('global.stop-video') }}</span>--}}
		{{--	</button>--}}
		{{--	<div id="iframe-wrap"></div>--}}
		{{--</div>--}}
		<div class="section-title">
			<img class="gif-logo" src="{{ asset('images/logo-gif-new.gif?'.rand(0,1000)) }}" alt="Gif image"/>
			<!--<a href="#" class="stock-link wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.15s"><span>{{ getTranslate('global.stock') }}</span></a>-->
		</div>
		<div class="bottom-info">
			{{--<button type="button" class="video-btn video-btn_play wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s" data-src="https://www.youtube.com/embed/1EjxOVbC_VA">--}}
			{{--	<span class="play-box">--}}
			{{--		<img class="play-box__icon" src="{{ asset('images/play-btn.png') }}" alt="Смотреть видео">--}}
			{{--	</span>--}}
			{{--	<span class="btn-text">{{ getTranslate('global.watch-video') }}</span>--}}
			{{--</button>--}}
			<div class="about-container">
				{{--<p class="about-container__text wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.45s">--}}
				{{--	{{ getTranslate('home.about') }}--}}
				{{--</p>--}}
				<a href="{{ route('about') }}" class="button-wave wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.45s">
					<span class="wave-img"></span>
					<span class="button-text">{{ getTranslate('global.more-info') }}</span>
				</a>
			</div>
		</div>
	</section>
</main>

@section('header-mobile-menu')
<div class="mobile-menu-wrap__documentation">
	<a href="{{ settingFile('main.download') }}">
		<img class="d-block d-lg-none" src="{{ asset('images/pdf-icon-white.svg') }}" alt="PDF download">
		{{ getTranslate('home.download-presentation') }}
		<img class="d-none d-lg-block" src="{{ asset('images/pdf-icon-white.svg') }}" alt="PDF download">
	</a>
</div>
@stop
