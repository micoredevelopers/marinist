<main class="contact-page">
    @include('partials.content_top')
    @include('partials.breadcrumbs')
    <div class="container">
        <div class="row row-contact">
            <div class="col-12">
                <h1 class="section-title">
                    <img src="{{ asset('images/wave.svg') }}" alt="Волна">
                    <span>{{ getTranslate('contact.contact') }}</span>
                </h1>
            </div>
            <div class="col-lg-6 col-12">
                <div class="image-wrap">
                    <img class="sales-department-image" src="{{ asset('images/contacts/sales-department.png') }}" alt="Отдел продаж">
                    <h2 class="sales-department-title">{{ getTranslate('contact.sales-department') }}</h2>
                </div>
            </div>
            <div class="col-lg-5 offset-lg-1 col-12 col-info">
                <h2 class="info-title">{{ getTranslate('contact.central-office') }}</h2>
                <div class="phones-wrap">
                    <img src="{{ asset('images/contacts/contact-phone-icon.svg') }}" alt="Телефон">
                    <span class="phone-number binct-phone-number-1"><span>098</span> 700 88 33</span>
                </div>
                <div class="mail-wrap">
                    <img src="{{ asset('images/wave.svg') }}" alt="Волна" class="wave-img">
                    <p class="mail">{{ getSetting('pages.email') }}</p>
                </div>
                <button class="btn-modal" data-toggle="modal" data-target="#modalFeedback">
                    <span class="btn-text">{{ getTranslate('request.contact-to-manager') }}</span>
                </button>
                <div class="socials-wrap">
                    <a class="social-link" target="_blank" href="{{ getSetting('social.facebook') }}">
                        <img class="social-icon" src="{{ asset('images/facebook-header.svg') }}" alt="Facebook" uk-svg>
                    </a>
                    <a class="social-link" target="_blank" href="{{ getSetting('social.instagram') }}">
                        <img class="social-icon" src="{{ asset('images/instagram-header.svg') }}" alt="Instagram" uk-svg>
                    </a>
                    <a class="social-link" target="_blank" href="{{ getSetting('social.youtube') }}" rel="nofollow">
                        <img class="social-icon" src="{{ asset('images/youtube-white.svg') }}" alt="Youtube" uk-svg>
                    </a>
                </div>
            </div>
            <div class="col-12 col-map">
                <img src="{{ asset('images/image-border.png') }}" alt="Рамка" class="image-border">
                {!! getSetting('pages.contact_map') !!}
            </div>
        </div>
    </div>

    <!-- Modal feedback -->
    <div class="modal fade" id="modalFeedback" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close modal-header-close" data-dismiss="modal" aria-label="Close">
                        <span></span>
                        <span></span>
                    </button>
                    <p class="modal-title">{{getTranslate('modal.call')}}</p>
                </div>
                <div class="modal-body">
                    <form action="{{ route('request.sign') }}" class="modal-form-submit">
                        <input type="hidden" name="link">
                        <input type="text" name="name" placeholder="{{getTranslate('modal.name')}}" class="modal-styled-input" required=""
                               maxlength="50">
                        <input type="text" name="phone" placeholder="{{getTranslate('modal.phone')}}" class="modal-styled-input"
                               required="" maxlength="50">
                        <button type="submit" class="button-wave">
                            <span class="wave-img"></span>
                            <span class="button-text">{{ getTranslate('request.send') }}</span>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @include('partials.content_bottom')
</main>
