<main class="real-estate-developer-page">
    @include('partials.content_top')
    @include('partials.breadcrumbs')
    <div class="container">
        <div class="row about-row">
            <div class="col-xl-6 col-12">
                <div class="info-box">
                    <h1 class="info-box__title d-none d-xl-block">{{ getTranslate('about-developer.title') }}</h1>
                    <p class="info-box__description">
                       {!! getTranslate('about-developer.text-about') !!}
                    </p>
                    <a href="{{ getSetting('pages.developer-site') }}" class="info-box__btn-link"
                    target="_blank" rel="nofollow">{{ getTranslate('about-developer.to-site') }}</a>
                </div>
            </div>
            <div class="col-xl-6 col-12">
                <div class="images-box">
                    <h2 class="images-box__title d-block d-xl-none">{{ getTranslate('about-developer.title') }}</h2>
                    <div class="images-box__circle">
                        <img src="{{ asset('images/logo-default-black.svg') }}" alt="Company logo">
                    </div>
                    <div class="images-box__company-link">
                        <p>{{ getTranslate('about-developer.developer.title') }}</p>
                        <img src="{{ asset('images/real-estate-developer/wave.png') }}" alt="Wave">
                    </div>
                </div>
            </div>
        </div>
        <div class="row developer-row">
            <div class="title-box">
                <h2 class="title-box__title">{{ getTranslate('about-developer.projects') }}</h2>
            </div>
            <div class="projects-row row">
                <div class="projects-row__item col-4 col-lg-3 col-xl-2">
                    <a href="#">
                        <img src="{{ asset('images/real-estate-developer/arcadia.png') }}" alt="Project image">
                    </a>
                </div>
                <div class="projects-row__item col-4 col-lg-3 col-xl-2">
                    <a href="#">
                        <img src="{{ asset('images/real-estate-developer/city-center.png') }}" alt="Project image">
                    </a>
                </div>
                <div class="projects-row__item col-4 col-lg-3 col-xl-2">
                    <a href="#">
                        <img src="{{ asset('images/real-estate-developer/chelsea-logo.svg') }}" alt="Project image">
                    </a>
                </div>
                <div class="projects-row__item col-4 col-lg-3 col-xl-2">
                    <a href="#">
                        <img src="{{ asset('images/real-estate-developer/hotel-de-paris-odessa.png') }}" alt="Project image">
                    </a>
                </div>
                <div class="projects-row__item col-4 col-lg-3 col-xl-2">
                    <a href="#">
                        <img src="{{ asset('images/real-estate-developer/kandinsky_logo.svg') }}" alt="Project image">
                    </a>
                </div>
                <div class="projects-row__item col-4 col-lg-3 col-xl-2">
                    <a href="#">
                        <img src="{{ asset('images/real-estate-developer/logo_lavanda.svg') }}" alt="Project image">
                    </a>
                </div>
            </div>
        </div>
    </div>
    @include('partials.content_bottom')
</main>
