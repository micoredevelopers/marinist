<main class="about-page">
    <section class="aboutPage-intro">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-12">
                    <div class="img-wrapper">
                        <img src="/images/about/header.jpg" alt="О нас">
                    </div>
                </div>
                <div class="col-lg-6 col-12">
                    <div class="content-wrapper">
                        <h2 class="content-title title">
                            <img src="/images/wave.svg" alt="Волна">
                            <span>О НАС</span>
                        </h2>
                        <div class="content-wrapper_desc">
                            <p>
                                ЖК МARINIST residence — малоэтажный дом на 16-й станции Большого Фонтана.
                            </p>
                            <br>
                            <p>МARINIST — релакс-резиденция в первой линии от моря, для людей, которые любят природу и
                                морской пейзаж, ведут
                                стильную, размеренную и комфортную жизнь, ценят тишину в отдалении от городской
                                суеты.</p>
                            <br>
                            <p>
                                Большинство квартир — с видом на море, с просторными балконами, открытыми террасами и
                                солнечными верандами.
                                Собственный парк из многолетних голубых елей украшает территорию.В шаговой доступности —
                                набережная "Золотого Берега"
                                с пляжами и ресторанами. Также есть круглосуточная охрана, подземный паркинг, площадки
                                для игр, отдыха и спорта
                            </p>
                            <br>
                            <p>
                                Жизнь в MARINIST residence — во всех смыслах комфортна и вдохновляет на творчество.
                                Потому что между морем, счастьем и
                                человеком — ничего лишнего. Мы об этом позаботились!
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="beforeAfter">
        <div class="container">
            <div class="beforeAfter-title title">
                <h2>До/После</h2>
            </div>
            <div class="row">
                <div class="col-lg-6 col-12">
                    <div class="beforeAfter_img">
                        <img src="/images/about/before.jpeg" alt="before">
                    </div>
                    <div class="beforeAfter_description">
                        В 2019 году, как мы начинали строиться, MARINIST выглядел именно так
                    </div>
                </div>
                <div class="col-lg-6 col-12">
                    <div class="beforeAfter_img">
                        <img src="/images/about/after1.jpg" alt="after">
                    </div>
                    <div class="beforeAfter_description">
                        Спустя 2 года, мы наблюдаем такую красоту, дом который не оставит никого равнодушным!
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="benefits">
        <div class="container">
            <div class="benefits-title title">
                <h2>наши преимущества</h2>
            </div>
            <div class="row benefits__row">
                <div class="col-lg-6 col-12">
                    <div class="benefits-img">
                        <img src="/images/about/advantage1.jpg" alt="advantage 1">
                    </div>
                </div>
                <div class="col-lg-6 col-12">
                    <div class="benefits__item">
                        <h2 class="benefits__item_title">
                            Первая линия от моря
                        </h2>
                        <p class="benefits__item_desc">
                            Первая линия от моря в MARINIST residence — как в самых лучших отелях для отдыха. Это значит
                            — пешком до побережья, вид на море из окон, с террас и балконов большинства квартир.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row benefits__row flex-lg-row-reverse">
                <div class="col-lg-6 col-12">
                    <div class="benefits-img">
                        <img src="/images/about/advantages4.jpg" alt="advantage 4">
                    </div>
                </div>
                <div class="col-lg-6 col-12">
                    <div class="benefits__item">
                        <h2 class="benefits__item_title">
                            Малоэтажность
                        </h2>
                        <p class="benefits__item_desc">
                            MARINIST residence — малоэтажный дом на 16-й станции Большого Фонтана. Секции состоят из
                            7-ми либо 8-ми этажей, что задает камерную, домашнюю атмосферу жилого комплекса — здесь
                            будут жить «все свои» и «на одной волне»
                        </p>
                    </div>
                </div>
            </div>
            <div class="row benefits__row">
                <div class="col-lg-6 col-12">
                    <div class="benefits-img">
                        <img src="/images/about/advantage3.jpeg" alt="advantage 3">
                    </div>
                </div>
                <div class="col-lg-6 col-12">
                    <div class="benefits__item">
                        <h2 class="benefits__item_title">
                            Паркинг
                        </h2>
                        <p class="benefits__item_desc">
                            Все машины в MARINIST residence находятся в подземном паркинге из 205 мест. Также есть
                            небольшой паркинг для гостей.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row benefits__row flex-lg-row-reverse">
                <div class="col-lg-6 col-12">
                    <div class="benefits-img">
                        <img src="/images/about/header.jpg" alt="advantage 1">
                    </div>
                </div>
                <div class="col-lg-6 col-12">
                    <div class="benefits__item">
                        <h2 class="benefits__item_title">
                            Жилой фонд
                        </h2>
                        <p class="benefits__item_desc">
                            Статус жилого фонда — гордость MARINIST residence. Жилой фонд предполагает закрепление за
                            владельцем более низких коммунальных тарифов и широкую юридическую свободу.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row benefits__row">
                <div class="col-lg-6 col-12">
                    <div class="benefits-img">
                        <img src="/images/about/advantage2.jpg" alt="advantage 2">
                    </div>
                </div>
                <div class="col-lg-6 col-12">
                    <div class="benefits__item">
                        <h2 class="benefits__item_title">
                            Патио
                        </h2>
                        <p class="benefits__item_desc">
                            Патио-зона — это территория ваших возможностей расширить досуг и эмоции. Уютный зелёный
                            уголок с атмосферой умиротворения для полного релакса или островок свободы со свежим морским
                            воздухом для бодрости и воодушевления
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="birthday">
        <div class="container">
            <div class="birthday-title title">
                <h2>День рождения Marinist</h2>
            </div>
            <p class="birthday-subtitle">
                Кстати, недавно, все наши инвесторы отмечали день рождение MARINIST и торжественно получали ключи от
                своих квартир.
            </p>
            <div class="row">
                <div class="col-12">
                    <div class="video">
                        <iframe width="100%" height="100%" src="https://www.youtube.com/embed/XwXji_Ry5P4"
                                title="YouTube video player" frameborder="0"
                                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="welcome">
        <div class="container">
            <div class="welcome-title title">
                <h2>Добро пожаловать к нам в гости</h2>
            </div>
            <div class="row">
                <div class="col-lg-6 offset-lg-3 col-12">
                    <div id="welcomeSlider" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img class="d-block w-100" src="/images/about/sliderItems/photo1.jpg" alt="1">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="/images/about/sliderItems/photo2.jpg" alt="2">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="/images/about/sliderItems/photo3.jpg" alt=3">
                            </div>
                            <div class="carousel-item ">
                                <img class="d-block w-100" src="/images/about/sliderItems/photo4.jpg" alt="4">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="/images/about/sliderItems/photo5.jpg" alt="5">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="/images/about/sliderItems/photo6.jpg" alt=6">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="/images/about/sliderItems/photo7.jpg" alt="7">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="/images/about/sliderItems/photo8.jpg" alt="8">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="/images/about/sliderItems/photo9.jpg" alt=9">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="/images/about/sliderItems/photo10.jpg" alt="10">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="/images/about/sliderItems/photo11.jpg" alt="11">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="/images/about/sliderItems/photo12.jpg" alt="12">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="/images/about/sliderItems/photo13.jpg" alt="13">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="/images/about/sliderItems/photo14.jpg" alt="14">
                            </div>
                        </div>
                        <a class="sliderArrows carousel-control-prev" href="#welcomeSlider" role="button"
                           data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="sliderArrows carousel-control-next" href="#welcomeSlider" role="button"
                           data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="contacts" class="about-contacts">
        <div class="container">
            <div class="row">
                <div class="offset-sm-2 col-sm-8 offset-sm-2 offset-md-0 col-md-6 offset-md-0 col-lg-7">
                    <div class="contacts_data">
                        <div class="contacts_data_header">
                            Контакты
                        </div>
                        <ul class="contacts_data_list">
                            <li class="contacts_data_list_item item1">
                                <a href="tel:+380987008833" class="xtraspc-contacts">+38 (098) 700 88 33</a>
                            </li>
                            <li class="contacts_data_list_item item2">
                                <span class="xtraspc-contacts">10:00 - 19:00, ВС — выходной</span>
                            </li>
                            <li class="contacts_data_list_item item3">
                                <a href="https://www.google.com/maps/place/%D0%9C%D0%B0%D1%8F%D1%87%D0%BD%D1%8B%D0%B9+%D0%BF%D0%B5%D1%80.,+7,+%D0%9E%D0%B4%D0%B5%D1%81%D1%81%D0%B0,+%D0%9E%D0%B4%D0%B5%D1%81%D1%81%D0%BA%D0%B0%D1%8F+%D0%BE%D0%B1%D0%BB%D0%B0%D1%81%D1%82%D1%8C,+65000/@46.3802446,30.7443828,17z/data=!3m1!4b1!4m5!3m4!1s0x40c634cbbcc1bf71:0xb9e1d46081ae3826!8m2!3d46.3802446!4d30.7465715"
                                   class="xtraspc-contacts">{{ getTranslate('contact.address') }}</a>
                            </li>
                            <li class="contacts_data_list_item item4">
                                <a href="mailto:Marinist.info@gmail.com" class="xtraspc-contacts">Marinist.info@gmail.com</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="offset-sm-2 col-sm-8 offset-sm-2 offset-md-0 col-md-6 offset-md-0 col-lg-5">
                    <div class="contacts_form">
                        <div class="contacts_form_header">
                            Связаться с нами
                        </div>
                        <form method="post" action="{{ route('request.feedback') }}"
                              class="contacts_form_input modal-form-submit">
                            <p><input type="text" class="contacts_form_input_item" placeholder="Имя" name="name"></p>
                            <p><input type="email" class="contacts_form_input_item" placeholder="Почта" name="email">
                            </p>
                            <p><input type="text" class="contacts_form_input_item" placeholder="Телефон" name="phone">
                            </p>
                            <p><input type="submit" class="contacts_form_input_submit"></p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
