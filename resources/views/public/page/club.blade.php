<main class="club-page">
    <section id="promo">
      <div class="promoContainer">
        <div class="promo">
            <div class="promo_photo">
                <img src="{{ asset('images/club/main_photo.jpg') }}" alt="building">
            </div>
            <div class="promo_descr">
                <div class="promo_descr_upper">
                    <div class="promo_descr_upper_text">Клубный дом Chelsea</div>
                </div>
                <div class="promo_descr_header">
                    <p>Загородная жизнь в черте города</p>
                </div>
                <div class="promo_descr_lower">
                    <p>Chelsea Club House - Клубный дом в английском стиле.
                        Все преимущества загородной жизни - в черте города.</p>
                </div>
                <div class="promo_descr_consult">
                    <button data-toggle="modal" data-target="#modalFeedback">Получить консультацию</button>
                </div>
            </div>
            </div>
        </div>
    </section>

    <section id="pros">
        <div class="container">
            <div class="advantages">
                <div class="advantages_header">
                    преимущества
                </div>
                <div class="advantages_list">
                    <div class="row">
                        <div class="col-xs-12 offset-sm-2 col-sm-8 offset-sm-2 offset-md-0 col-md-6 offset-md-0 col-lg-4">
                            <div class="advantages_panel">
                                <div class="advantages_panel_icon icon1"></div>
                                <div class="advantages_panel_header">Кирпичный дом</div>
                                <div class="advantages_panel_subheader">Долговечно, экологично, безопасно</div>
                            </div>
                        </div>
                        <div class="col-xs-12 offset-sm-2 col-sm-8 offset-sm-2 offset-md-0 col-md-6 offset-md-0 col-lg-4">
                            <div class="advantages_panel">
                                <div class="advantages_panel_icon icon2"></div>
                                <div class="advantages_panel_header">Индивидуальное отопление</div>
                                <div class="advantages_panel_subheader">2-х контурные котлы в каждой квартире</div>
                            </div>
                        </div>
                        <div class="col-xs-12 offset-sm-2 col-sm-8 offset-sm-2 offset-md-0 col-md-6 offset-md-0 col-lg-4">
                            <div class="advantages_panel">
                                <div class="advantages_panel_icon icon3"></div>
                                <div class="advantages_panel_header">Собственный парк</div>
                                <div class="advantages_panel_subheader">На территории клубного дома</div>
                            </div>
                        </div>
                        <div class="col-xs-12 offset-sm-2 col-sm-8 offset-sm-2 offset-md-0 col-md-6 offset-md-0 col-lg-4">
                            <div class="advantages_panel">
                                <div class="advantages_panel_icon icon4"></div>
                                <div class="advantages_panel_header">Паркинг</div>
                                <div class="advantages_panel_subheader">На охраняемой территории комплекса</div>
                            </div>
                        </div>
                        <div class="col-xs-12 offset-sm-2 col-sm-8 offset-sm-2 offset-md-0 col-md-6 offset-md-0 col-lg-4">
                            <div class="advantages_panel">
                                <div class="advantages_panel_icon icon5"></div>
                                <div class="advantages_panel_header">Узкий круг жильцов</div>
                                <div class="advantages_panel_subheader">2-4 квартиры на каждом этаже</div>
                            </div>
                        </div>
                        <div class="col-xs-12 offset-sm-2 col-sm-8 offset-sm-2 offset-md-0 col-md-6 offset-md-0 col-lg-4">
                            <div class="advantages_panel">
                                <div class="advantages_panel_icon icon6"></div>
                                <div class="advantages_panel_header">Квартиры с двориками</div>
                                <div class="advantages_panel_subheader">Индивидуальный выход
                                    в личный палисадник
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 offset-sm-2 col-sm-8 offset-sm-2 offset-md-0 col-md-6 offset-md-0 col-lg-4">
                            <div class="advantages_panel">
                                <div class="advantages_panel_icon icon7"></div>
                                <div class="advantages_panel_header">Двор без машин</div>
                                <div class="advantages_panel_subheader">Свободный двор для прогулок и отдыха</div>
                            </div>
                        </div>
                        <div class="col-xs-12 offset-sm-2 col-sm-8 offset-sm-2 offset-md-0 col-md-6 offset-md-0 col-lg-4">
                            <div class="advantages_panel">
                                <div class="advantages_panel_icon icon8"></div>
                                <div class="advantages_panel_header">Пентхаусы с террасами</div>
                                <div class="advantages_panel_subheader">2-х уровневые квартиры с открытыми террасами</div>
                            </div>
                        </div>
                        <div class="col-xs-12 offset-sm-2 col-sm-8 offset-sm-2 offset-md-0 col-md-6 offset-md-0 col-lg-4">
                            <div class="advantages_panel">
                                <div class="advantages_panel_icon icon9"></div>
                                <div class="advantages_panel_header">Рассрочка</div>
                                <div class="advantages_panel_subheader">В сданном доме до 2-х лет без переплат</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="style">
        <div class="container">
            <div class="style_header">
                АНГЛИЙСКИЙ СТИЛЬ НА БОЛЬШОМ ФОНТАНЕ
            </div>
            <div class="panel">
                <div class="panel_image image1">
                    <img src="{{ asset('images/club/engstyle1.jpg') }}" alt="Английский стиль, фото">
                </div>
                <div class="panel_text">
                    <div class="panel_text_number">01</div>
                    <div class="panel_text_header">Престижное расположение</div>
                    <div class="panel_text_subheader">Тишина — это современная роскошь.
                        Дом расположен на 13 станции Большого Фонтана, что обеспечивает
                        насыщенную жизнь в сердце города, и в то же время — в его спокойной, приватной части.
                    </div>
                </div>
            </div>
            <div class="panel">
                <div class="panel_image image2">
                    <img src="{{ asset('images/club/engstyle2.jpg') }}" alt="Английский стиль, фото">
                </div>
                <div class="panel_text">
                    <div class="panel_text_number">02</div>
                    <div class="panel_text_header">Приватность и комфорт</div>
                    <div class="panel_text_subheader">Приватность — это статус. В доме спроектировано идеальное
                        пространство для комфортной и спокойной жизни, которой сопутствуют уединенность
                        и ощущение безопасности.
                    </div>
                </div>
            </div>
            <div class="panel">
                <div class="panel_image image3">
                    <img src="{{ asset('images/club/engstyle3.jpg') }}" alt="Английский стиль, фото">
                </div>
                <div class="panel_text">
                    <div class="panel_text_number">03</div>
                    <div class="panel_text_header">Оптимальное окружение</div>
                    <div class="panel_text_subheader">Инфраструктура комфорта повседневной жизни. В шаговой доступности
                        — элитный супермаркет, фитнес-клуб, школа, детский сад, больница, кафе и рестораны.
                        В непосредственной близости — лучшая набережная Одессы.
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="gallery" class="galleryphotos">
        <div class="container">
            <div class="photogallery">
                <div class="photogallery_header">Галерея клубного дома</div>
               <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                      <img class="d-block w-100" src="/images/club/123.jpeg" alt="1 slide">
                    </div>
                    <div class="carousel-item">
                      <img class="d-block w-100" src="/images/club/1234.jpeg" alt="2 slide">
                    </div>
                    <div class="carousel-item">
                      <img class="d-block w-100" src="/images/club/12345.jpeg" alt="3 slide">
                    </div>
                            <div class="carousel-item">
                                          <img class="d-block w-100" src="/images/club/123456.jpeg" alt="4 slide">
                                        </div>
                                        <div class="carousel-item">
                                          <img class="d-block w-100" src="/images/club/1234567.jpeg" alt="5 slide">
                                        </div>
                                                <div class="carousel-item">
                                                              <img class="d-block w-100" src="/images/club/IMG_9199.jpeg" alt="6 slide">
                                                            </div>
                                                            <div class="carousel-item">
                                                              <img class="d-block w-100" src="/images/club/IMG_9201.jpeg" alt="7 slide">
                                                            </div>
                                                            <div class="carousel-item">
                                                              <img class="d-block w-100" src="/images/club/IMG_9215.jpeg" alt="8 slide">
                                                            </div>

                  </div>
                  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>
               </div>
            </div>
            <div class="photogallery_border"></div>
        </div>
    </section>

    <section id="layouts-section">
        <div class="container">
            <div class="layouts-header">
                Планировки
            </div>
            <div class="row">
                <div class="col-xs-12 offset-sm-2 col-sm-8 offset-sm-2 offset-md-0 col-md-6 offset-md-0 col-lg-4">
                    <div class="panel-layouts">
                        <div class="layout">
                            <img src="{{ asset('images/club/layout1.jpg') }}" alt="План 1">
                        </div>
                        <div class="layout-name">
                          Квартира  двухуровневая,  пентхаус  4-4-1
                        </div>
                        <div class="layout-numbers">
                            <ul class="layout-numbers_data">
                                <li class="layout-numbers_data_item1">
                                    <span class="xtraspc">198,8 <span class="sqm">кв.м</span></span>
                                </li>
                                <li class="layout-numbers_data_item2">
                                    <span class="xtraspc">1500$ <span class="sqm">кв.м</span></span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 offset-sm-2 col-sm-8 offset-sm-2 offset-md-0 col-md-6 offset-md-0 col-lg-4">
                    <div class="panel-layouts">
                        <div class="layout">
                            <img src="{{ asset('images/club/layout2.jpg') }}" alt="План 2">
                        </div>
                        <div class="layout-name">
                            Патио квартира </br> 3-1-2
                        </div>
                        <div class="layout-numbers">
                            <ul class="layout-numbers_data">
                                <li class="layout-numbers_data_item1">
                                    <span class="xtraspc">46,50 <span class="sqm">кв.м</span></span>
                                </li>
                                <li class="layout-numbers_data_item2">
                                    <span class="xtraspc">1665 <span class="sqm">кв.м</span></span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 offset-sm-2 col-sm-8 offset-sm-2 offset-md-0 col-md-6 offset-md-0 col-lg-4">
                    <div class="panel-layouts">
                        <div class="layout">
                            <img src="{{ asset('images/club/layout3.jpg') }}" alt="План 3">
                        </div>
                        <div class="layout-name">
                           Квартира </br> 1-1-3
                        </div>
                        <div class="layout-numbers">
                            <ul class="layout-numbers_data">
                                <li class="layout-numbers_data_item1">
                                    <span class="xtraspc">40,60 <span class="sqm">кв.м</span></span>
                                </li>
                                <li class="layout-numbers_data_item2">
                                    <span class="xtraspc">1 590$ <span class="sqm">кв.м</span></span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="contacts">
        <div class="container">
            <div class="row">
                <div class="offset-sm-2 col-sm-8 offset-sm-2 offset-md-0 col-md-6 offset-md-0 col-lg-7">
                    <div class="contacts_data">
                        <div class="contacts_data_header">
                            Контакты
                        </div>
                        <ul class="contacts_data_list">
                            <li class="contacts_data_list_item item1"><span class="xtraspc-contacts">+38 (098) 700 88 33</span></li>
                            <li class="contacts_data_list_item item2"><span class="xtraspc-contacts">10:00 - 19:00, ВС — выходной</span>
                            </li>
                            <li class="contacts_data_list_item item3"><span class="xtraspc-contacts">г. Одесса, пер. Майский, 4</span></li>
                            <li class="contacts_data_list_item item4"><span class="xtraspc-contacts">chelsea.od.info@gmail.com</span></li>
                        </ul>
                    </div>
                </div>
                <div class="offset-sm-2 col-sm-8 offset-sm-2 offset-md-0 col-md-6 offset-md-0 col-lg-5">
                    <div class="contacts_form">
                        <div class="contacts_form_header">
                            Связаться с нами
                        </div>
                        <form method="post" action="{{ route('request.feedback') }}" class="contacts_form_input modal-form-submit">
                            <p><input type="text" class="contacts_form_input_item" placeholder="Имя" name="name"></p>
                            <p><input type="email" class="contacts_form_input_item" placeholder="Почта" name="email"></p>
                            <p><input type="text" class="contacts_form_input_item" placeholder="Телефон" name="phone"></p>
                            <p><input type="submit" class="contacts_form_input_submit"></p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
<div class="modal fade" id="modalFeedback" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close modal-header-close" data-dismiss="modal" aria-label="Close">
                        <span></span>
                        <span></span>
                    </button>
                    <p class="modal-title">Получить консультацию</p>
                </div>
                <div class="modal-body">
                    <form action="{{ route('request.sign') }}" class="modal-form-submit">
                        <input type="hidden" name="link">
                        <input type="text" name="name" placeholder="Имя" class="modal-styled-input" required=""
                               maxlength="50">
                        <input type="text" name="phone" placeholder="Телефон" class="modal-styled-input"
                               required="" maxlength="50">
                        <button type="submit" class="button-wave">
                            <span class="wave-img"></span>
                            <span class="button-text">{{ getTranslate('request.send') }}</span>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</main>
