{{--<main class="apartment-page">--}}
{{--    @include('partials.content_top')--}}

{{--    <div class="main-box">--}}
{{--        <div class="image-box">--}}
{{--            <span class="test-box"></span>--}}
{{--            <div class="tooltip-text"></div>--}}
{{--            <img src="{{ asset('images/apartment/main-image.jpg') }}" class="building-image" alt="Выбор секции"/>--}}
{{--            <img src="{{ asset('images/apartment/building-layer.svg') }}" class="layer-image" alt="Выбор этажа" uk-svg />--}}
{{--        </div>--}}
{{--        <div class="sub-section">--}}
{{--            <div class="container container-top">--}}
{{--                <div class="top-info">--}}
{{--                    <div class="btn-back">--}}
{{--                        <img class="d-none d-xl-block" src="{{ asset('images/apartment/arrow-back.svg') }}" alt="Назад">--}}
{{--                        <img class="d-block d-xl-none" src="{{ asset('images/apartment/arrow-icon.svg') }}" alt="Назад">--}}
{{--                        <span>{{ getTranslate('global.back') }}</span>--}}
{{--                    </div>--}}
{{--                    <div class="right-box">--}}
{{--                        <div class="wave-wrap">--}}
{{--                            <img class="wave-image" src="{{ asset('images/wave.svg') }}" alt="Волна">--}}
{{--                            <span class="wave-title">{{ getTranslate('flat.choosing-section') }}</span>--}}
{{--                        </div>--}}
{{--                        <div class="building-mini-wrap">--}}
{{--                            <img src="{{ asset('images/apartment/building-image.png') }}" alt="Выбор секции">--}}
{{--                            <img class="layer-mini-image" src="{{ asset('images/apartment/building-mini-layer.svg') }}" alt="Выбор секции" uk-svg>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    --}}{{--<a class="download-link d-flex d-xl-none" href="#">--}}
{{--                    --}}{{--    <span>{{ getTranslate('flat.download-layout') }}</span>--}}
{{--                    --}}{{--    <img src="{{ asset('images/pdf-icon-white.svg') }}" alt="Скачать планировку">--}}
{{--                    --}}{{--</a>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="container">--}}
{{--                <div class="row row-section">--}}
{{--                    <div class="col-12 col-section col-section_floor">--}}
{{--                        <div class="row">--}}
{{--                            <div class="col-xl-2 col-12 col-floor-list">--}}
{{--                                <ul class="floor-list"></ul>--}}
{{--                            </div>--}}
{{--                            <div class="col-xl-10 col-12">--}}
{{--                                <div class="floor-name"></div>--}}
{{--                                <div class="plan-wrap">--}}
{{--                                    <div class="floor-plan"></div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="col-12 col-section col-section_flat">--}}
{{--                        <div class="flat-name d-block d-xl-none">--}}
{{--                            <img src="{{ asset('images/wave.svg') }}" alt="Волна">--}}
{{--                            <p></p>--}}
{{--                        </div>--}}
{{--                        <div class="row row-flat-info">--}}
{{--                            <div class=" col-xl-8 col-md-6 col-12">--}}
{{--                                <div class="flat-name d-none d-xl-block">--}}
{{--                                    <p></p>--}}
{{--                                </div>--}}
{{--                                <div class="flat-plan"></div>--}}
{{--                                <div class="flat-bottom-row d-none d-xl-flex">--}}
{{--                                    <button type="button" class="flat-btn flat-btn_plan" data-toggle="modal" data-target="#modalAppointment">--}}
{{--                                        {{ getTranslate('request.sign-to-view') }}--}}
{{--                                    </button>--}}
{{--                                    --}}{{--<a class="download-link" href="#">--}}
{{--                                    --}}{{--    <span>{{ getTranslate('flat.download-layout') }}</span>--}}
{{--                                    --}}{{--    <img src="{{ asset('images/pdf-icon-white.svg') }}" alt="Скачать планировку">--}}
{{--                                    --}}{{--</a>--}}
{{--                                    <div class="nav-image">--}}
{{--                                        <img src="{{ asset('images/apartment/compass-icon.svg') }}" alt="Компас">--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="col-xl-4 col-md-6 col-12 col-flat-info">--}}
{{--                                <div class="flat-info-list-wrap d-none d-xl-flex">--}}
{{--                                    <p class="list-title">{{getTranslate('flat.apartment-params')}}</p>--}}
{{--                                    <ul class="flat-info-list"></ul>--}}
{{--                                    --}}{{--<a class="flat-btn flat-btn_list" href="#">{{ getTranslate('flat.get-price') }}</a>--}}
{{--                                </div>--}}
{{--                                <ul class="flat-info-list d-block d-md-flex d-xl-none"></ul>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="row row-flat-buttons d-flex d-xl-none">--}}
{{--                            --}}{{--<div class="col-6">--}}
{{--                            --}}{{--    <a class="flat-btn" href="#">{{ getTranslate('flat.get-price') }}</a>--}}
{{--                            --}}{{--</div>--}}
{{--                            <div class="col-12">--}}
{{--                                <button type="button" class="flat-btn" data-toggle="modal" data-target="#modalAppointment">--}}
{{--                                    {{ getTranslate('request.sign-to-view') }}--}}
{{--                                </button>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="bottom-nav d-flex d-xl-none">--}}
{{--            <div class="container d-flex">--}}
{{--                <div class="nav-image">--}}
{{--                    <img src="{{ asset('images/apartment/compass-icon.svg') }}" alt="Компас">--}}
{{--                </div>--}}
{{--                <div class="hand-box">--}}
{{--                    <img src="{{ asset('images/apartment/hand-bg.svg') }}" alt="Hand icon">--}}
{{--                </div>--}}
{{--                <div class="floor-slide hide">--}}
{{--                    <div class="floor-slide__arrow-top">--}}
{{--                        <img src="{{ asset('images/apartment/arrow-icon.svg') }}" alt="Arrow top">--}}
{{--                    </div>--}}
{{--                    <p class="floor-slide__text">Этаж 6</p>--}}
{{--                    <div class="floor-slide__arrow-bottom">--}}
{{--                        <img src="{{ asset('images/apartment/arrow-icon.svg') }}" alt="Arrow bottom">--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="call-box">--}}
{{--                    <img src="{{ asset('images/apartment/call-icon.svg') }}" alt="Call">--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <div class="parameters-btn-wrap">--}}
{{--        <a class="parameters-btn" href="{{ route('parameters') }}">{{ getTranslate('flat.by-params') }}</a>--}}
{{--    </div>--}}

{{--    @include('partials.content_bottom')--}}
{{--    @push('js')--}}
{{--    <!-- thefloors widget --><script type='text/javascript'>--}}
{{--            (function(w, d){--}}
{{--                var s = d.createElement('script');s.async = true;s.src = 'https://api.thefloors.io/v1/site/333/widget';--}}
{{--                var tscs = d.getElementsByTagName('script');--}}
{{--                var ts = tscs[tscs.length-1];--}}
{{--                ts.parentNode.insertBefore(s,ts);--}}
{{--            })(window, document);--}}
{{--        </script><!-- end -->--}}
{{--    @endpush--}}
{{--</main>--}}
