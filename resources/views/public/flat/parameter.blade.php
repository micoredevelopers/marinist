<? /** @see \App\Http\Controllers\FlatController::parameters() */?>
<main class="parameters-page">
    @include('partials.content_top')
    <div class="container">
        <div class="top-nav">
            <a href="{{ route('flat') }}" class="back-link">
                <img src="{{ asset('images/parameters/arrow-left-black.svg') }}" alt="Arrow back">
                <span>{{ getTranslate('global.back') }}</span>
            </a>
            <h1 class="title">{{ getTranslate('flat.params.by-params') }}</h1>
            <div class="view-box">
                <span class="view-box__title">{{ getTranslate('global.view') }}</span>
                <div class="type-wrap">
                    <div class="type-box type-box_table active">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                    <div class="type-box type-box_block">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="search-result d-none d-xl-block">
            <h2 class="search-result__title">{{ getTranslate('flat.flat-finded') }} <span>{{ $result->total() }}</span>
            </h2>
        </div>
        <form action="{{ route('parameters') }}" class="filters-form" method="get">
            <div class="row filters-row">
                <div class="col-xl-3 col-md-3 col-6">
                    <?php /** @var $section \App\Models\Section */ ?>
                    @isset($sections)

                        <div class="range-slider-wrap" data-range-select="{{$sectionsSelect}}" data-range-min="{{$sections->get('min')}}" data-range-max="{{$sections->get('max')}}">
                            <label for="sections">{{ getTranslate('flat.section') }}</label>
                            <input id="sections" name="sections" type="text" class="js-range-slider"/>
                        </div>
                    @endisset
                    {{--                    <div class="filters-select">--}}
                    {{--                        @isset($sections)--}}
                    {{--                            --}}
                    {{--                            <select name="sectionFrom" id="sectionSelectMin" class="select-hidden left">--}}
                    {{--                                @foreach($sections as $section)--}}
                    {{--                                    <option value="{{ $section->number }}"--}}
                    {{--                                        {!! selectedIfTrue($section->sectionFrom) !!}>{{ $section->number }}--}}
                    {{--                                    </option>--}}
                    {{--                                @endforeach--}}
                    {{--                            </select>--}}
                    {{--                            <span class="filters-select__title">{{ getTranslate('flat.section') }}</span>--}}
                    {{--                            <select name="sectionTo" id="sectionSelectMax" class="select-hidden right">--}}
                    {{--                                @foreach($sections as $section)--}}
                    {{--                                    <option value="{{ $section->number }}"--}}
                    {{--                                        {!! selectedIfTrue($section->sectionTo) !!}>{{ $section->number }}--}}
                    {{--                                    </option>--}}
                    {{--                                @endforeach--}}
                    {{--                            </select>--}}
                    {{--                        @endisset--}}
                    {{--                    </div>--}}
                </div>
                <div class="col-xl-3 col-md-3 col-6">
                    @isset($floors)
                    <div class="range-slider-wrap" data-range-select="{{$floorsSelect}}" data-range-min="{{$floors->get('min')}}" data-range-max="{{$floors->get('max')}}">
                        <label for="floors">{{ getTranslate('flat.floor') }}</label>
                        <input id="floors" name="floors" type="text" class="js-range-slider"/>
                    </div>
                    @endisset
{{--                    @isset($floors)--}}
{{--                        <div class="filters-select">--}}
{{--                            <select name="floorFrom" id="floorSelectMin" class="select-hidden left">--}}
{{--                                @foreach($floors as $floor)--}}
{{--                                    <option value="{{ $floor->number }}" {!! selectedIfTrue($floor->floorFrom) !!}>--}}
{{--                                        {{ $floor->number }}--}}
{{--                                    </option>--}}
{{--                                @endforeach--}}
{{--                            </select>--}}
{{--                            <span class="filters-select__title">{{ getTranslate('flat.floor') }}</span>--}}
{{--                            <select name="floorTo" id="floorSelectMax" class="select-hidden right">--}}
{{--                                @foreach($floors as $floor)--}}
{{--                                    <option value="{{ $floor->number }}"--}}
{{--                                        {!! selectedIfTrue($floor->floorTo) !!}>{{ $floor->number }}--}}
{{--                                    </option>--}}
{{--                                @endforeach--}}
{{--                            </select>--}}
{{--                        </div>--}}
{{--                    @endisset--}}
                </div>

                <div class="col-xl-3 col-md-3 col-6">
                    @isset($squares)
                    <div class="range-slider-wrap" data-range-select="{{$squaresSelect}}" data-range-min="{{$squares->get('min')}}" data-range-max="{{$squares->get('max')}}">
                        <label for="square">{{ getTranslate('flat.square') }}</label>
                        <input id="square" name="square" type="text" class="js-range-slider"/>
                    </div>
                    @endisset

                </div>
                <div class="col-xl-3 col-md-3 col-6">
                    @isset($rooms)
                    <div class="range-slider-wrap" data-range-select="{{$roomsSelect}}" data-range-min="{{$rooms->get('min')}}" data-range-max="{{$rooms->get('max')}}">
                        <label for="rooms">{{ getTranslate('flat.rooms') }}</label>
                        <input id="rooms" name="rooms" type="text" class="js-range-slider"/>
                    </div>
                    @endisset
{{--                    <div class="filters-select">--}}
{{--                        @isset($rooms)--}}
{{--                            <select name="roomFrom" id="roomsSelectMin" class="select-hidden left">--}}
{{--                                @foreach ($rooms as $room)--}}
{{--                                    <option value="{{ $room->room }}"--}}
{{--                                        {!! selectedIfTrue($room->roomFrom) !!}>{{ $room->room }}--}}
{{--                                    </option>--}}
{{--                                @endforeach--}}
{{--                            </select>--}}
{{--                            <span class="filters-select__title">{{ getTranslate('flat.rooms') }}</span>--}}
{{--                            <select name="roomTo" id="roomsSelectMax" class="select-hidden right">--}}
{{--                                @foreach ($rooms as $room)--}}
{{--                                    <option value="{{ $room->room }}"--}}
{{--                                        {!! selectedIfTrue($room->roomTo) !!}>{{ $room->room }}--}}
{{--                                    </option>--}}
{{--                                @endforeach--}}
{{--                            </select>--}}
{{--                        @endisset--}}
{{--                    </div>--}}
                </div>
            </div>
            <div class="row filters-row">
                <div class="col-xl-3 col-md-3 col-6">
                    <div class="filters-check">
                        <span class="filters-check__title">{{ getTranslate('flat.sea-view') }}</span>
                        <input id="seaView" type="checkbox" name="seaView" value="1" {!! checkedIfTrue($seaView) !!}>
                        <label for="seaView" class="{{ getCurrentLocale() }}">
                            <span class="yes">{{ getTranslate('global.yes') }}</span>
                            <span class="no">{{ getTranslate('global.no') }}</span>
                        </label>
                    </div>
                </div>
                <div class="col-xl-3 col-md-3 col-6">
                    <div class="filters-check">
                        <span class="filters-check__title">{{ getTranslate('flat.terrace') }}</span>
                        <input id="terrace" type="checkbox" name="terrace" value="1" {!! checkedIfTrue($terrace) !!}>
                        <label for="terrace" class="{{ getCurrentLocale() }}">
                            <span class="yes">{{ getTranslate('global.yes') }}</span>
                            <span class="no">{{ getTranslate('global.no') }}</span>
                        </label>
                    </div>
                </div>

                <div class="col-xl-3 col-md-6 col-12">
                    <div class="btn-row">
                        <button type="submit"
                                class="btn-custom btn-custom_submit">{{ getTranslate('flat.accept') }}</button>
{{--                        <a href="{{ route('parameters') }}"--}}
{{--                           class="btn-custom btn-custom_reset">{{ getTranslate('flat.reset') }}</a>--}}
                    </div>
                </div>
                <div class="col-xl-3 col-12">
                    <div class="btn-row actions">
                        <input type="checkbox" id="stock" name="stock" value="1" {!! checkedIfTrue($stock) !!}>
                        <label for="stock" class="stock-label">{{ getTranslate('flat.stock-proposals') }}</label>
                    </div>
                </div>
            </div>
        </form>
        <div class="search-result d-block d-xl-none">
            <h3 class="search-result__title">{{ getTranslate('flat.flat-finded') }} <span>{{ $result->total() }}</span>
            </h3>
        </div>

        <div class="table-box">
            <table class="custom-table">
                <thead>
                <tr>
                    <td>{{ getTranslate('flat.section') }}</td>
                    <td>{{ getTranslate('flat.floor') }}</td>
                    <td>{{ getTranslate('flat.square') }}</td>
                    <td>{{ getTranslate('flat.rooms') }}</td>
                    <td class="d-none d-xl-table-cell">{{ getTranslate('flat.sea-view') }}</td>
                    <td class="d-none d-xl-table-cell">{{ getTranslate('flat.terrace') }}</td>
                    <td>№</td>
                </tr>
                </thead>
                <tbody>
                @forelse($result as $flat)
                    <tr class="@if ((int)$flat->stock) isStock {{ getCurrentLocale() }} @endif">
                        <td>{{ $flat->sections_number }}</td>
                        <td>{{ $flat->floors_number }}</td>
                        <td>{{ translateFormat('global.sq-meter', [(int)\Arr::get($flat, 'square')]) }}</td>
                        <td>{{ $flat->room }}</td>
                        <td class="d-none d-xl-table-cell">{{ translateYesNo((int)$flat->sea_view) }}</td>
                        <td class="d-none d-xl-table-cell">{{ translateYesNo((int)$flat->terrace) }}</td>
                        <td>{{ $flat->number }}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="7" class="text-center">{{ getTranslate('flat.params.no-results') }}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
        <div class="row row-box m-0">
            @forelse($result as $flat)
                <div class="col-6 p-0 flat-col @if ((int)$flat->stock) isStock {{ getCurrentLocale() }} @endif">
                    <div class="row m-0 flat-row">
                        <div class="col-xl-6 col-lg-6 col-12 p-0">
                            <div class="flat-box">
                                <img src="{{$flat->getImagePath('photo',asset('images/parameters/flat-plan.png'))  }}"
                                     alt="Image flat plan">
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-12 p-0">
                            <div class="flat-info">
                                <div class="flat-info__row">
                                    <p class="flat-info__row__text">{{ getTranslate('flat.section') }}
                                        : {{ $flat->sections_number }}</p>
                                    <p class="flat-info__row__text">{{ getTranslate('flat.floor') }}
                                        : {{ $flat->floors_number }}</p>
                                </div>
                                <div class="flat-info__row">
                                    <p class="flat-info__row__text">{{ getTranslate('flat.square') }}:
                                        {{ translateFormat('global.sq-meter', [(int)\Arr::get($flat, 'square')]) }}</p>
                                    <p class="flat-info__row__text">{{ getTranslate('flat.rooms') }}
                                        : {{ $flat->room }}</p>
                                </div>
                                <div class="flat-info__row">
                                    <p class="flat-info__row__text">{{ getTranslate('flat.sea-view') }}
                                        : {{ translateYesNo((int)$flat->sea_view) }}</p>
                                    <p class="flat-info__row__text">{{ getTranslate('flat.terrace') }}
                                        : {{ translateYesNo((int)$flat->terrace) }}</p>
                                </div>
                                <div class="flat-info__row">
                                    <a href="{{ route('flat') }}" data-flat-id="{{(int)$flat->id}}" class="flat-info__row__btn-detailed">{{ getTranslate('global.more-info') }}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @empty
                <div class="text-center">
                    {{ getTranslate('flat.params.no-results') }}
                </div>
            @endforelse
        </div>

        <div class="row m-0">
            <div class="col-12 p-0">
                <div class="pagination-wrap">
                    {!! $result->render() !!}
                </div>
            </div>
        </div>
    </div>

    @include('partials.content_bottom')
</main>
{{--

@section('css')
<link rel="stylesheet" href="{{ asset('libs/swiper/css/swiper.min.css') }}">
@stop
--}}
