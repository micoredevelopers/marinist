@isset($news)
    <?php /** @var $new \App\Models\News */?>
    @forelse($news as $index => $new)
        @php
            $odd = ($index %2 !== 0);
            $row = $odd ? 'reverse' : '';
            $col = $odd ? 'd-flex d-xl-none' : '';
        @endphp
        <div class="col-xl-12 col-md-6 col-12 news-card-wrap">
            <div class="row">
                <div class="col-xl-6 col-12 p-xl-0 {{ $col }}">
                    <div class="image-box">
                        <img class="news-image" src="{{ checkImage($new->photo )}}"
                             alt="{{ translateFormat('news.alt', [$new->name]) }}">
                    </div>
                </div>
                <div class="col-xl-6 col-12 p-xl-0">
                    <div class="news-description">
                        <h2 class="news-description__title">{{ $new->name }}</h2>
                        <div class="news-description__text">
                            {!! $new->sub_description !!}
                        </div>
                        <div class="news-description__bottom-info">
                            <a class="link-about" href="{{ route('news.single', $new->url) }}" class="info-box__bottom__btn-detailed">
                                {{ getTranslate('news.more-info')}}
                                <span class="line-box"></span>
                            </a>
                            <p class="news-data">{{ $new->date_pub->formatLocalized('%d %B, %Y') }}</p>
                        </div>
                    </div>
                </div>
                @if ($odd)
                    <div class="col-xl-6 col-12 p-xl-0 d-none d-xl-flex">
                        <div class="image-box">
                            <img class="news-image" src="{{ checkImage($new->photo) }}" alt="{{ translateFormat('news.alt', [$new->name]) }}">
                        </div>
                    </div>
                @endif
            </div>
        </div>
    @empty
        {{ getTranslate('news.empty') }}
    @endforelse
@endisset
