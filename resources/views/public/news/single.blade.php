<main class="news-selected-page">
    @include('partials.content_top')
    @include('partials.breadcrumbs')
    <div class="news-image-wrap">
        <img class="news-image" src="{{ checkImage(imgOrigin($news->photo)) }}"
             alt="{{ translateFormat('news.alt', [$news->name]) }}">
        <img class="image-border" src="{{ asset('images/image-border.png') }}" alt="Рамка">
    </div>
    <div class="container">
        <div class="row row-news-info">
            <div class="col-12">
                <div class="news-description-wrap">
                    <p class="news-data d-none d-xl-block">
                        <img src="{{ asset('images/wave.svg') }}" alt="Волна">
                        <span>{{ $news->date_pub->formatLocalized('%d %B, %Y') }}</span>
                    </p>
                    <h1 class="news-title">{{ showMeta($news->name) }}</h1>
                    <div class="news-description">{!! $news->description !!}</div>
                    <p class="news-data d-block d-xl-none">{{ $news->date_pub->formatLocalized('%d %B, %Y') }}</p>
                    {{--                    Modal--}}
                    @if(isset($news) && ($news->active_modal??false))
                        <button class="btn-modal" data-toggle="modal" id="news-{{$news->id}}" data-target="#modalFeedback">
                            <span class="btn-text">{{ getTranslate('news.contact-to-manager') }}</span>
                        </button>
                    @endif
                    <div class="news-nav">
                        @if ($prev = $news->getPrev())
                            <a href="{{ route('news.single', $prev->url) }}" class="news-nav__item news-nav__item_prev">
                                <img src="{{ asset('images/news-selected/prev-arrow.png') }}" alt="Arrow prev">
                                {{ getTranslate('news.prev') }}
                            </a>
                        @endif
                        <a href="{{ route('news') }}"
                           class="news-nav__item news-nav__item_all">{{ getTranslate('news.all') }}</a>
                        @if ($next = $news->getNext())
                            <a href="{{ route('news.single', $next->url) }}" class="news-nav__item news-nav__item_next">
                                {{ getTranslate('news.next') }}
                                <img src="{{ asset('images/news-selected/next-arrow.png') }}" alt="Arrow next">
                            </a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal feedback -->
    <div class="modal fade" id="modalFeedback" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close modal-header-close" data-dismiss="modal" aria-label="Close">
                        <span></span>
                        <span></span>
                    </button>
                    <p class="modal-title">{{getTranslate('modal.call')}}</p>
                </div>
                <div class="modal-body">
                    <form action="{{ route('request.sign') }}" id="news-form-{{$news->id}}" class="modal-form-submit">
                        <input type="hidden" name="link">
                        <input type="text" name="name" placeholder="{{getTranslate('modal.name')}}" class="modal-styled-input" required>
                        <input type="text" name="phone" placeholder="{{getTranslate('modal.phone')}}" class="modal-styled-input" required>
                        <button type="submit" class="button-wave">
                            <span class="wave-img"></span>
                            <span class="button-text">{{ getTranslate('request.send') }}</span>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @include('partials.content_bottom')
</main>

