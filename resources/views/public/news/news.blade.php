<main class="news-page">
    @include('partials.content_top')
    @include('partials.breadcrumbs')
    <div class="container">
        <h1 class="section-title">
            <img src="{{ asset('images/wave.svg') }}" alt="Волна">
            <span>{{ showMeta(getTranslate('news.news')) }}</span>
        </h1>
        <div class="row news-list">
            @include('public.news.loop')
        </div>
        @if ($news->hasMorePages())
            <div class="btn-box">
                <a class="btn-box__more d-none d-xl-flex"
                   data-url="{{ route('news.more') }}?page={{ $news->currentPage() + 1 }}"
                   data-current-page="{{ $news->currentPage() }}">{{ getTranslate('global.show-more') }}</a>
                <a class="btn-box__more d-flex d-xl-none"
                   data-url="{{ route('news.more') }}?page={{ $news->currentPage() + 1 }}"
                   data-current-page="{{ $news->currentPage() }}">
                    {{ getTranslate('global.show-more') }}
                    <img src="{{ asset('images/news/arrow-bottom.png') }}" alt="Arrow icon">
                </a>
            </div>
        @endif
    </div>
    @include('partials.content_bottom')
</main>
