{{--@extends('layout.index')--}}

<?php ?>
<main class="building-page">
    @include('partials.breadcrumbs')
    <div class="container-fluid">
        @include('partials.content_top')
        <div class="row row-top">
            <div class="col-xl-6 col-lg-4 col-12">
                <div class="title-wrap">
                    <h1 class="section-title">{{ getTranslate('progress.status') }}</h1>
                    <p class="section-subtitle">{{ getTranslate('progress.exploitation') }}</p>
                </div>
            </div>
            <div class="col-lg-4 col-6 d-flex d-xl-none">
                <div class="percent-wrap">
                    <p class="percent-status"
                       data-percent="{{ (int)$steps->avg('percent') }}">{{ (int)$steps->avg('percent') }}%</p>
                </div>
            </div>
            <div class="col-xl-6 col-lg-4 col-6 pr-0">
                {{--                <div class="camera-wrap">--}}
                {{--                    <img class="camera-wrap__wave-image" src="{{ asset('images/building/wave.png') }}" alt="Wave">--}}
                {{--                    <a class="camera-link" href="#">--}}
                {{--                        <img class="camera-icon" src="{{ asset('images/building/camera-icon.png') }}" alt="Camera {{ getTranslate('global.image') }}">--}}
                {{--                        {{ getTranslate('progress.online-camera') }}--}}
                {{--                    </a>--}}
                {{--                </div>--}}
                <div class="building-image-wrap">
                    <img class="building-image" src="{{ asset('images/building/status-building.png') }}" alt="Статус">
                    <span class="building-status-wave"></span>
                    <p class="status-number d-none d-xl-block"
                       data-percent="{{ (int)$steps->avg('percent') }}">{{ (int)$steps->avg('percent') }}%</p>
                </div>
            </div>
        </div>
        <div class="row row-status">
            <div class="col-12">
                <div class="status-line-wrap">
                    @isset($steps)
                        @foreach ($steps as $step)
                            @php
                                $last = $loop->iteration == $steps->count();
                                $even = $loop->even;
                            @endphp
                            <div
                                class="status-line {!! $even ? 'status-line_even' : 'status-line_odd' !!} {!! $last ? 'last' : '' !!}"
                                data-percent="{{ $step->percent }}">
                                @if ($last)
                                    <div class="line-wrap">
                                        <img class="line-image"
                                             src="{{ asset('images/building/single-circle-bottom.svg') }}" alt="Статус">
                                        <img class="line-image-fill"
                                             src="{{ asset('images/building/single-circle-bottom-fill.svg') }}"
                                             alt="Статус">
                                    </div>
                                @elseif ($even)
                                    <div class="line-wrap">
                                        <img class="line-image"
                                             src="{{ asset('images/building/status-line-bottom.svg') }}" alt="Статус">
                                        <img class="line-image-fill"
                                             src="{{ asset('images/building/status-line-bottom-fill.svg') }}"
                                             alt="Статус строительства">
                                    </div>
                                @else
                                    <div class="line-wrap">
                                        <img class="line-image" src="{{ asset('images/building/status-line-top.svg') }}"
                                             alt="Статус">
                                        <img class="line-image-fill"
                                             src="{{ asset('images/building/status-line-top-fill.svg') }}"
                                             alt="Статус">
                                    </div>
                                @endif
                                <span class="status-icon d-block d-xl-none">
                                    <img src="{{ checkImage($step->photo) }}" alt="Статус">
                                    <div class="status-tooltip">
                                        {!!  $step->text !!}
                                    </div>
                                </span>
                                <div class="status-name d-none d-xl-flex">
                                    <span class="status-icon">
                                        <img src="{{ checkImage($step->photo) }}" alt="Статус">
                                    </span>
                                    <span class="status-text">{{ $step->name }}</span>
                                    <div class="status-tooltip">
                                        {!! $step->text !!}
                                    </div>
                                </div>
                                <span class="status-percent"
                                      data-percent="{{ $step->percent }}">{{ $step->percent }}%</span>
                            </div>
                        @endforeach
                    @endisset
                </div>
            </div>
        </div>
        @if(isset($galleries) && $galleries->isNotEmpty())
            <div class="row row-building">
                <div class="col-12">
                    <div class="title-wrap">
                        <h2 class="section-title">{{ getTranslate('progress.steps') }}</h2>
                    </div>
                </div>
                <div class="col-xl-12 d-none d-xl-flex">
                    <div class="slider-box">
                        <div class="building-slider swiper-container">
                            <div class="swiper-wrapper">
                                @foreach ($galleries as $gallery)
                                    <div class="swiper-slide"
                                         data-url="{{ route('progress.item',['progress'=>$gallery->id]) }}"
                                         data-gallery-id="{{ $gallery->id }}" data-toggle="modal"
                                         data-target="#building-modal">
                                        <div class="image-box">
                                            <img src="{{ checkImage($gallery->photo) }}"
                                                 alt="{{ translateFormat('progress.alt-gallery', [$gallery->name]) }}">
                                        </div>
                                        <div class="info-box">
                                            <p class="info-box__title">{{ $gallery->name }}</p>
                                            <div class="info-box__desc">{!! $gallery->sub_description !!}</div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="swiper-button-prev swiper-arrow prev">
                                <img src="{{ asset('images/gallery/slider-arrow-prev.png') }}" alt="Arrow prev">
                            </div>
                            <div class="swiper-button-next swiper-arrow next">
                                <img src="{{ asset('images/gallery/slider-arrow-next.png') }}" alt="Arrow next">
                            </div>
                            <div class="swiper-pagination"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row row-building-cards d-flex d-xl-none">
                <?php /** @var $galleries \Illuminate\Support\Collection */ ?>
                @foreach($galleries as $gallery)
                    <div class="col-md-6 col-12 col-building">
                        <div class="building-box">
                            <div class="image-box" data-url="{{ route('progress.item',['progress'=>$gallery->id]) }}"
                                 data-gallery-id="{{ $gallery->id }}" data-toggle="modal" data-target="#building-modal">
                                <img src="{{ checkImage($gallery->photo) }}"
                                     alt="{{ translateFormat('progress.alt-gallery', [$gallery->name]) }}">
                            </div>
                            <div class="info-box">
                                <p class="info-box__title">{{ $gallery->name }}</p>
                                <div class="info-box__desc">{!! $gallery->sub_description !!}</div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        @endif
        @include('partials.content_bottom')
    </div>
</main>

@if(isset($galleries) && $galleries->isNotEmpty())
    <div class="modal fade" id="building-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
         aria-hidden="true"
         data-gallery-id="{{ $gallery->id }}">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span></span>
                        <span></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="slider-box">
                        <div class="modal-slider swiper-container">
                            <div class="swiper-wrapper">
                                {{--								@if ($gallery->photos)--}}
                                {{--                      <?php /** @var $photo \App\Models\ProgressGalleryPhoto */ ?>--}}
                                {{--									@foreach ($gallery->photos as $photo)--}}
                                {{--										<div class="swiper-slide">--}}
                                {{--											<div class="image-box">--}}
                                {{--												<img src="{{ checkImage(imgOrigin($photo->photo)) }}" alt="{{ translateFormat('progress.alt-gallery-photo', [$gallery->name, $loop->iteration]) }}">--}}
                                {{--											</div>--}}
                                {{--										</div>--}}
                                {{--									@endforeach--}}
                                {{--								@endif--}}
                            </div>
                            <div class="swiper-button-prev swiper-arrow prev">
                                <img src="{{ asset('images/gallery/left-arrow.svg') }}" alt="Назад">
                            </div>
                            <div class="swiper-button-next swiper-arrow next">
                                <img src="{{ asset('images/gallery/right-arrow.svg') }}" alt="Вперед">
                            </div>
                        </div>
                    </div>
                    <div class="info-box">
                        {{--<div class="info-box__title">{{ $gallery->name }}</div>--}}
                        {{--<div class="info-box__desc">{!! $gallery->description !!}</div>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
