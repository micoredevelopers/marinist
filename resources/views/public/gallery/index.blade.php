{{--@extends('layout.index')--}}
<?php /** @var $gallery \App\Models\Gallery */ ?>

@isset($galleries)
    <main class="gallery-page">
        @include('partials.content_top')
        @include('partials.breadcrumbs')
        <div class="container">
            <h1 class="section-title">{{ showMeta(getTranslate('gallery.gallery')) }}</h1>
            <div class="nav-wrap">
                @foreach($galleries as $gallery)
                    @php
                        $active = ($loop->iteration === 1) ? 'active' : '';
                    @endphp
                    <div data-target="gallery-{{ $gallery->id }}" data-url="{{ route('gallery.item',['galleryId'=>$gallery->id]) }}" class="nav-wrap__link-wrap {{ $active }}">
                        <span class="nav-wrap__link-text">{{ $gallery->name }}</span>
                        <span class="wave-mask"></span>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="slider-wrap">
            @foreach($galleries as $gallery)
                @php
                    $active = ($loop->iteration === 1) ? 'active' : '';
                @endphp
                <div id="gallery-{{ $gallery->id }}" class="gallery-slider-wrap {!! isset($gallery->photos[0]) ? 'photos-slider' : '' !!}  {{ $active }}">
                    @isset ($gallery->photos[0])
                        <div class="gallery-slider swiper-container">
                            <div class="swiper-wrapper"></div>
                            <div class="swiper-button-prev swiper-arrow prev">
                                <img class="d-block d-xl-none" src="{{ asset('images/gallery/slider-arrow-left.svg') }}" alt="Назад">
                                <img class="d-none d-xl-block" src="{{ asset('images/gallery/left-arrow.svg') }}" alt="Назад">
                            </div>
                            <div class="swiper-button-next swiper-arrow next">
                                <img class="d-block d-xl-none" src="{{ asset('images/gallery/slider-arrow-right.svg') }}" alt="Вперед">
                                <img class="d-none d-xl-block" src="{{ asset('images/gallery/right-arrow.svg') }}" alt="Вперед">
                            </div>
                            <div class="swiper-pagination"></div>
                        </div>
                    @endisset
                    @isset ($gallery->video[0])
                        <div class="row row-gallery-videos"></div>
                    @endisset
                </div>
            @endforeach
        </div>
        @include('partials.content_bottom')
    </main>
@endisset


@section('css')
    <link rel="stylesheet" href="{{ asset('libs/swiper/css/swiper.min.css') }}">
@stop
@section('javascript')
    <script src="{{ asset('libs/swiper/js/swiper.min.js') }}"></script>
@stop
