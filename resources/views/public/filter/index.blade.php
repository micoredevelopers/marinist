@php
echo "<script>var qwe = JSON.parse('".$curl."');</script>";
@endphp
<link href="{{ asset('css/filter-page.css') }}" rel="stylesheet">
<main class="contact-page">
    @include('partials.content_top')
    @include('partials.breadcrumbs')
    <div class="choose-flat__filters container">
        <div class="choose-flat__filters-block">
            <div class="choose-flat__filters-title">Фільтр:</div>
            <div class=" filter-labels no-side-padding"><input class="choose-flat__filters-item"
                    data-filter-item="" data-type="checkbox" data-value="1" data-name="rooms" id="rooms1"
                    type="checkbox"><label for="rooms1" class="choose-flat__filters-item">1-кімнатні</label></div>
            <div class=" filter-labels no-side-padding"><input class="choose-flat__filters-item"
                    data-filter-item="" data-type="checkbox" data-value="2" data-name="rooms" id="rooms2"
                    type="checkbox"><label for="rooms2" class="choose-flat__filters-item">2-кімнатні</label></div>
            <div class=" filter-labels no-side-padding"><input class="choose-flat__filters-item"
                    data-filter-item="" data-type="checkbox" data-value="3" data-name="rooms" id="rooms3"
                    type="checkbox"><label for="rooms3" class="choose-flat__filters-item">3-кімнатні</label></div>
            <div class=" filter-labels no-side-padding"><input class="choose-flat__filters-item"
                    data-filter-item="" data-type="checkbox" data-value="4" data-name="rooms" id="rooms4"
                    type="checkbox"><label for="rooms4" class="choose-flat__filters-item">4-кімнатні</label></div>
            <!-- <div class="choose-flat__filters-item">Дворівневі</div>
            <div class="choose-flat__filters-item">Пентхауси</div> -->
        </div>
        <div class="choose-flat__filters-block">
            <div class="choose-flat__filters-title">Секція:</div>
            <div class=" filter-labels no-side-padding"><input class="choose-flat__filters-item"
                    data-filter-item="" data-type="checkbox" data-value="1" data-name="sec" id="section1"
                    type="checkbox"><label for="section1" class="choose-flat__filters-item">1</label></div>
            <div class=" filter-labels no-side-padding"><input class="choose-flat__filters-item"
                    data-filter-item="" data-type="checkbox" data-value="2" data-name="sec" id="section2"
                    type="checkbox"><label for="section2" class="choose-flat__filters-item">2</label></div>
            <div class=" filter-labels no-side-padding"><input class="choose-flat__filters-item"
                    data-filter-item="" data-type="checkbox" data-value="3" data-name="sec" id="section3"
                    type="checkbox"><label for="section3" class="choose-flat__filters-item">3</label></div>
            <div class=" filter-labels no-side-padding"><input class="choose-flat__filters-item"
                    data-filter-item="" data-type="checkbox" data-value="4" data-name="sec" id="section4"
                    type="checkbox"><label for="section4" class="choose-flat__filters-item">4</label></div>
        </div>
    </div>

    <div class="choose-flat__items " data-cards-container data-card-flats-render
        data-empty-mes="За вашими параметрами нічого не знайдено">

    </div>

    <!-- <a href="#" class="choose-flat__btn choose-flat__btn-2">Більше планувань</a> -->

    <script src="{{ asset('js/filter/gsap.min.js') }}"></script>
    <script src="{{ asset('js/filter/filter.js') }}"></script>
    @include('partials.content_bottom')
</main>