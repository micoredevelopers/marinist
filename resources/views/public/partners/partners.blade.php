<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.1.5/css/uikit.min.css">
    <link rel="stylesheet" href="{{ asset('css/main.css')}}?v={{ assetFilemtime('css/main.css') }}"/>

</head>
<body class="body">

<header class="header-partners">
    <div class="container-fluid ">
        <nav class="d-flex justify-content-between align-items-center mobile-nav">
            <a class="menu-logo" href="{{route('home')}}">
                <img src="{{asset('images/logo-default-black.svg')}}" alt="">
            </a>
            <div class="d-lg-flex d-none">
                <nav class="desctop-menu">
                    <a class="desctop-menu__item scroll-link" href="#price-table">Шахматка</a>
                    <a class="desctop-menu__item scroll-link" href="#credit">Расрочка</a>
                    <a class="desctop-menu__item scroll-link" href="#characteristic">Характеристики</a>
                    <a class="desctop-menu__item scroll-link" href="#partners-conditions">Условия сотрудничества</a>
                    <div class="my-dropdown-wrap">
                        <a class="desctop-menu__item my-dropdown-btn d-flex align-items-center">Позвонить
                            <img src="{{asset('images/arrow.svg')}}" uk-svg class="my-dropdown-btn-arrow" alt="">
                        </a>
                        <nav class=" my-dropdown">
                            <a href="">+38 (096) 123 45 67</a>
                            <a href="">+38 (096) 123 45 67</a>
                            <a href="">+38 (096) 123 45 67</a>
                        </nav>
                    </div>

                </nav>

            </div>
            <div class="d-flex justify-content-between align-items-center d-lg-none">
                <a class="call-link" href="#" data-toggle="modal" data-target="#phoneModal">
                    <img src="{{asset('images/phone.svg')}}" alt="">
                </a>
                <span id="nav-icon1">
                        <span></span>
                        <span></span>
                        <span></span>
                    </span>
            </div>
        </nav>
    </div>
    <div class="mob-menu-wrap d-lg-none">
        <ul class="mob-menu">
            <li><img src="{{ asset('images/icon_pack/plan.svg')}}" alt=""><a class="mob-menu__item scroll-link"
                                                                             href="#price-table">Шахматка</a></li>
            <li><img src="{{ asset('images/icon_pack/engineer.svg')}}" alt=""><a class="mob-menu__item scroll-link"
                                                                                 href="#credit">Расрочка</a></li>
            <li><img src="{{ asset('images/icon_pack/building.svg')}}" alt=""><a class="mob-menu__item scroll-link"
                                                                                 href="#characteristic">Характеристики</a>
            </li>
            <li><img src="{{ asset('images/icon_pack/newspaper.svg')}}" alt=""><a class="mob-menu__item scroll-link"
                                                                                  href="#partners-conditions">Условия
                    сотрудничества</a></li>
        </ul>
        <div class="mob-menu-footer">
            <div class="mob-menu-footer-box">
                <a target="_blank" class="fb-icon mr-0" href=""> <img src="{{ asset('images/icon_pack/facebook.svg')}}"
                                                                      alt=""></a>
                <a target="_blank" class="inst-icon mr-0" href=""> <img
                        src="{{ asset('images/icon_pack/instagram.svg')}}" alt=""></a>
                <a target="_blank" class="youtube-icon mr-0" href=""> <img
                        src="{{ asset('images/icon_pack/youtube.svg')}}" alt=""></a>
            </div>
            <a href="" class="mob-menu-footer-link">Вернуться на сайт</a>
        </div>
    </div>
</header>
<main class="partners-main">
    <section class="partners-first">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 d-lg-flex flex-column justify-content-center">
                    <p class="partners-first-title-desc">Заголовок о лидерах продаж</p>
                    <h1 class="partners-first-title">Дополнительная надпись</h1>
                    <p class="partners-first-text">
                        Oftentimes, life would be great if it weren't for other people! Yet,
                        it's through relationships with others that you often obtain the greatest rewards.
                    </p>
                    <button class="castom-btn wave_btn zi-9" data-toggle="modal" data-target="#modalBooking">Записаться на обучение</button>
                </div>
                <div class="offset-lg-1 col-lg-4  d-lg-flex flex-column justify-content-center">
                    <ul class="raiting-list">
                        @foreach($partners as $partner)
                            <li class="raiting-list__item">
                                <span class="raiting-list__logo">
                                    <img src="{{ checkImage($partner->photo) }}?{{storageFilemtime($partner->photo)}}"
                                         alt="">
                                </span>
                                <span class="raiting-list__result-line" style="width:{{$partner->rating}}%">
                                    <span class="raiting-list__result">{{$partner->rating}}кв.</span>
                                </span>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <img class="partners-first-bg" src="{{asset('images/Mask1.png')}}" alt="">
        <div class="partners-first-social-box d-none d-lg-flex justify-content-between align-items-center">
            <nav class="social-box position-relative">
                <div class="social-box__bg d-none d-lg-block"></div>
                <a target="_blank" href="{!! getSetting('social.facebook') !!}"><img class="fb-icon"
                                                                                     src="{{asset('images/fb.svg')}}"
                                                                                     alt="">
                    Facebook
                </a>
                <a target="_blank" href="{!! getSetting('social.instagram') !!}"><img class="inst-icon"
                                                                                      src="{{asset('images/ins.svg')}}"
                                                                                      alt="">
                    Instagram
                </a>
                <a target="_blank" href="{!! getSetting('social.youtube') !!}"><img class="youtube-icon"
                                                                                    src="{{asset('images/yout.svg')}}"
                                                                                    alt="">
                    youtube
                </a>
            </nav>
            <a href="{{route('home')}}" class="my-dropdown-btn social-box__button">Вернуться на сайт</a>
        </div>
    </section>
    <section id="price-table" class="price-table-section">
        <div class="title-box justify-content-center justify-content-lg-end">
            <p class="price-table__description">Сдача дома 4 кв. 2020г.</p>
        </div>
        <div class="title-box">
            <h2 class="price-table-section-title">Шахматка. Цены</h2>
            <div class="my-dropdown-wrap">
                <a class="desctop-menu__item my-dropdown-btn d-flex align-items-center mr-lg-0">
                    <span class="dropdown_section__text">Секция 1</span>
                    <img src="{{asset('images/arrow.svg')}}" uk-svg class="my-dropdown-btn-arrow" alt="">
                </a>
                <nav class="my-dropdown">
                    @for($i=0; $i<count($sections);$i++)
                        <a class="tabs-link" href="">Секция {{$i+1}}</a>
                    @endfor
                </nav>
            </div>
        </div>
        <div class="my-container overflowScroll">
            @foreach($sections as $keySection =>$section)
                <table class="table table-bordered price-table">
                    <thead>
                    <tr>
                        <th class="text-center first-row" scope="col">Этаж</th>
                        @foreach(\App\Helpers\PartnersChess::getHeader($keySection) as $name)
                            <th class="text-center my-col" scope="col">{{$name}}</th>
                        @endforeach

                    </tr>
                    </thead>
                    <tbody>
                    @foreach($section as $floor)
                        @if($keySection==3 || $keySection==4)
                            {!! \App\Helpers\PartnersChess::getRow($floor,$loop->iteration+1) !!}
                        @else
                            {!! \App\Helpers\PartnersChess::getRow($floor,$loop->iteration) !!}
                        @endif
                        {{--                        <tr>--}}
                        {{--                            <th scope="row" class="text-center align-middle first-col ">{{$loop->iteration}}</th>--}}
                        {{--                            @foreach($floor->flats as $flat )--}}
                        {{--                                <td class="{{$flat->status->status}}">--}}
                        {{--                                    --}}{{--                                 @dump($flat->smart)--}}
                        {{--                                    --}}{{--                                    @dump($flat->terrace)--}}
                        {{--                                    <span class="td-title border-bottom pb-2">№{{$flat->number}}</span>--}}
                        {{--                                    <div class="d-flex justify-content-between border-bottom py-2">--}}
                        {{--                                        <span class="td-medium-num">{{$flat->square}}м²</span>--}}
                        {{--                                        <span class="td-medium-num">{{$flat->sum}}</span>--}}
                        {{--                                    </div>--}}
                        {{--                                    <div class="border-bottom py-2">--}}
                        {{--                                        <div class="d-flex justify-content-between ">--}}
                        {{--                                            <span class="td-light-num">25%</span>--}}
                        {{--                                            <span class="td-light-num">{{$flat->twenty_five_percent}}</span>--}}
                        {{--                                        </div>--}}
                        {{--                                        <div class="d-flex justify-content-between ">--}}
                        {{--                                            <span class="td-light-num">50%</span>--}}
                        {{--                                            <span class="td-light-num">{{$flat->fifty_percent}}</span>--}}
                        {{--                                        </div>--}}
                        {{--                                        <div class="d-flex justify-content-between ">--}}
                        {{--                                            <span class="td-light-num">75%</span>--}}
                        {{--                                            <span class="td-light-num">{{$flat->seventy_five_percent}}</span>--}}
                        {{--                                        </div>--}}
                        {{--                                    </div>--}}
                        {{--                                    <div class="d-flex justify-content-between  pt-2">--}}
                        {{--                                        <span class="td-medium-num">100%</span>--}}
                        {{--                                        <span class="td-medium-num">{{$flat->one_hundred_percent}}</span>--}}
                        {{--                                    </div>--}}
                        {{--                                </td>--}}
                        {{--                            @endforeach--}}

                        {{--                        </tr>--}}
                    @endforeach
                    </tbody>
                </table>
            @endforeach


        </div>
        <div class="my-container">
            <div class="desc-box">
                <div class="d-flex flex-column flex-lg-row align-items-lg-center">
                    <div class="d-flex justify-content-between mb-2 mb-lg-0">
                        <span class="desc-box-text"><span class="color-cub bezh"></span>Свободно</span>
                        <span class=" d-flex flex-row-reverse flex-lg-row desc-box-text"><span
                                class="color-cub green"></span>Продано</span>
                    </div>
                    <div class="d-flex justify-content-between mb-20 mb-lg-0">
                        <span class="desc-box-text"><span class="color-cub ultra-bezh"></span>Забронировано</span>
                        <span class=" d-flex flex-row-reverse flex-lg-row desc-box-text"><span
                                class="color-cub breeze"></span>От инвестора</span>
                    </div>
                </div>
                <div class="row min-width-200">
                    <div class="col-6 d-lg-none">
                        <a href="{!! settingFile('global.shahmatka') !!}"
                           class="partners-btn-custom price-table-section__btn price-table-section__btn-white">Поставить
                            бронь</a>
                    </div>
                    <div class="col-6 col-lg-12">
                        <a href="{!! settingFile('global.shahmatka') !!}"
                           class="partners-btn-custom price-table-section__btn">Скачать схему</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid mt-4">
            <h5 class="price-table-section__description text-center">Поэтапная оплата (поквартально / ежемесячно) до
                декабря 2020г.</h5>
        </div>
        <h4 id="credit" class="some-small-title mt-lg-1 mb-lg-5">Длительная рассрочка на 3 года, первоначальный взнос
            15%. Условия в
            отделе продаж.</h4>
    </section>
    <section class="partners-form-section">
        <div class="form-section__box">
            <img src="{{asset('images/feedback_image1.jpg')}}" alt="" class="form-section__box__image">
            <div class="form-section__box__text d-lg-none">
                <p class="form-section__box__title">Подписаться на рассылку</p>
                <p class="form-section__box__description">Заполните форму, чтобы получать обновленную шахматку на
                    почту</p>
            </div>
            <img src="{{asset('images/maskImage.svg')}}" alt="" class="form-section__box__mask">
        </div>
        <div class="container pt-5 pt-lg-0">
            <h3 class="form-section-title d-none d-lg-block">Подписаться на рассылку</h3>
            <form id="modal-form" method="get" class="partners-form" action="{{route('partners.mail')}}">
                @csrf
                <span class="form-section-text d-none d-lg-block">Заполните форму, чтобы получать обновленную шахматку на почту</span>
                <input required type="text" autocomplete="off" name="agency" placeholder="Агентство">
                <input required type="text" autocomplete="off" name="branch" placeholder="Филиал">
                <input required type="text" autocomplete="off" name="name" placeholder="Имя">
                <input required type="text" autocomplete="off" name="surname" placeholder="Фамилия">
                <input required type="text" autocomplete="off" name="phone" placeholder="Телефон">
                <input required type="email" autocomplete="off" name="email" placeholder="Почта">
                <button class="partners-form__btn" type="submit">Подписаться</button>
            </form>
        </div>
    </section>
    <section id="characteristic" class="characteristic">
        <div class="container sub__container">
            <img src="{{ asset('images/char-border.svg') }}" alt="" class="characteristic_border d-none d-lg-block">
            <h3 class="characteristic-title mb-lg-4">Технические характеристики дома</h3>
            <img src="{{ asset('images/tech-span.svg') }}" alt="" class="d-lg-none mx-auto mb-3">
            <div class="row">
                <div class="col-lg-5">
                    <ul class="characteristic-list">
                        <li class="characteristic-list__item">Адрес: г. Одесса, пер. Маячный 7</li>
                        <li class="characteristic-list__item">Этажность: 6, 7</li>
                        <li class="characteristic-list__item">Количество секций: 5</li>
                        <li class="characteristic-list__item">Количество квартир: 260</li>
                        <li class="characteristic-list__item">Высотта потолков: типовой этаж — 3м., 1-й этаж - 3,3м.
                        </li>
                        <li class="characteristic-list__item">Площадь квартир:<br>
                            1-комн. 28,61 — 53,55 кв.м.<br>
                            2-комн. 66,05 — 90,32 кв.м.<br>
                            3-комн. 75,06 — 121,77 кв.м.<br>
                            Свободная планировка — до 220,26 кв.м.
                        </li>
                        <li class="characteristic-list__item">Типы квартир: Smart, с балконом, с верандой, с террасой
                        </li>
                        <li class="characteristic-list__item">Паркинг: подземный, 205 м/м</li>
                        <li class="characteristic-list__item">Год сдачи: 4-й кв. 2020г.</li>
                        <li class="characteristic-list__item">Документы на землю: право собственности</li>
                    </ul>
                </div>
                <div class=" offset-lg-1 col-lg-6">
                    <ul class="characteristic-list">
                        <li class="characteristic-list__item">Площадь земельного участка: 9 383 кв.м.</li>
                        <li class="characteristic-list__item">Площадь застройки: 3 805 кв.м.</li>
                        <li class="characteristic-list__item">Площадь общественной территории: 3 176 кв.м.</li>
                        <li class="characteristic-list__item">Площадь подземного паркинка: 5 965 кв.м.</li>
                        <li class="characteristic-list__item">Конструкция здания: железобетонный монолитный каркас</li>
                        <li class="characteristic-list__item">Наружные стены: ячеистый бетон толщиной 300 мм.</li>
                        <li class="characteristic-list__item">Отделка: вентилируемый фасад</li>
                        <li class="characteristic-list__item">Утепление фасадов: минеральная вата</li>
                        <li class="characteristic-list__item">Межквартирные стены: ячеистый бетон толщиной 200 мм,
                            оштукатуренный звукоизоляционными смесями
                        </li>
                        <li class="characteristic-list__item">Отопление: индивидуальная газовая котельная</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="offset-lg-2 col-lg-8">
                    <h3 id="partners-conditions" class="characteristic-title characteristic-title_mar-top">Условия
                        сотрудничества</h3>
                    <img src="{{ asset('images/tech-span.svg') }}" alt="" class="mx-auto mb-4 mt-2">
                    <div class="row">
                        <div class="col-12 d-flex flex-column">
                            <div class="characteristic_box d-flex">
                                <div class="characteristic_box__image">
                                    <img src="{{asset('images/card__1.svg')}}" alt="">
                                </div>
                                <div class="characteristic_box__description">
                                    <p class="characteristic-text">Привести клиента в ОП для предоставления полной
                                        информации. С этого момента клиент фиксируется за АН сроком на 30 дней.</p>
                                </div>
                            </div>
                            <img src="{{ asset('images/arrow-down__image.svg') }}" alt="" class="characteristic_arrow">
                            <div class="characteristic_box d-flex">
                                <div class="characteristic_box__image">
                                    <img src="{{asset('images/card__2.svg')}}" alt="">
                                </div>
                                <div class="characteristic_box__description">
                                    <p class="characteristic-text">Выбрать и поставить бронь на квартиру. Срок брони – 3
                                        дня. Бронь ставится только при личном присутствии клиента и риелтора
                                        (представителя АН) в ОП.</p>
                                </div>
                            </div>
                            <img src="{{ asset('images/arrow-down__image.svg') }}" alt="" class="characteristic_arrow">
                            <div class="characteristic_box d-flex">
                                <div class="characteristic_box__image">
                                    <img src="{{asset('images/card__3.svg')}}" alt="">
                                </div>
                                <div class="characteristic_box__description">
                                    <p class="characteristic-text">По истечении срока бронирования возможно: <br>
                                        - оформить резерв квартиры, внеся 1 000 у.е. и оформить соответствующий договор.
                                        Срок резерва – 2 недели, по истечение этого срока необходимо выйти на сделку.
                                        Резерв не возвращается. <br>
                                        - выйти на сделку</p>
                                </div>
                            </div>
                            <img src="{{ asset('images/arrow-down__image.svg') }}" alt="" class="characteristic_arrow">
                            <div class="characteristic_box d-flex">
                                <div class="characteristic_box__image">
                                    <img src="{{asset('images/card__4.svg')}}" alt="">
                                </div>
                                <div class="characteristic_box__description">
                                    <p class="characteristic-text">Получить денежное вознаграждение в течение
                                        3 рабочих дней, с дня оформления договора.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <p class="characteristic-text mt-4 mt-lg-5 fs-14">
                        <b>Недопустимо:</b> рекламировать, предоставлять услуги, позиционируя как Отдел продаж
                        компании, экслюзивная продажа и прочее, что может ввести в заблуждение клиента.
                    </p>
                </div>
            </div>
        </div>
    </section>

</main>

<!-- Modal booking -->
<div class="modal fade" id="modalBooking" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content modal-content__feedback">
            <div class="modal-header">
                <button type="button" class="close modal-header-close" data-dismiss="modal" aria-label="Close">
                    <span></span>
                    <span></span>
                </button>
                <h5 class="modal-title modal-content__title">Записаться на обучение</h5>
            </div>
            <div class="modal-body">
                <form id="modal-form" action="{{route('partners.mail')}}" class="modal-form-submit">
                    <input type="text" id="name" name="agency" placeholder="АН" class="modal-styled-input" required>
                    <input type="text" id="name" name="branch" placeholder="Филиал" class="modal-styled-input" required>
                    <input type="text" id="name" name="name" placeholder="Имя директора" class="modal-styled-input" required>
                    <input type="text" id="phone" name="phone" placeholder="Телефон" class="modal-styled-input" required>
                    <textarea name="text" id="comment" rows="5" placeholder="Комментарий"
                              class="modal-styled-input modal-styled-input--area" required=""></textarea>
                    <button type="submit" class="button-wave">
                        <span class="wave-img"></span>
                        <span class="button-text">{{ getTranslate('request.send') }}</span>
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Modal success -->
<div class="modal fade" id="modalSuccess" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-center"></h5>
            </div>
            <div class="modal-body">
                <button type="button" class="button-wave" data-dismiss="modal">
                    <span class="wave-img"></span>
                    <span class="button-text">Окей</span>
                </button>
            </div>
        </div>
    </div>
</div>
<footer class="partners-footer">
    <h4 class="partners-footer-title">Документы для скачивания</h4>
    <div class="partners-footer-items-box mt-lg-5">
        <div class="d-flex partners-footer__box flex-lg-column align-items-center">
            <img src="{{ asset('images/partners__image-1.svg') }}" alt="" class="partners-footer__image">
            <a href="" class="partners-footer-item mb-0">Разрешительные документы</a>
        </div>
        <div class="d-flex partners-footer__box flex-lg-column align-items-center">
            <img src="{{ asset('images/partners__image-2.svg') }}" alt="" class="partners-footer__image">
            <a href="" class="partners-footer-item mb-0">Разрешительные документы</a>
        </div>
        <div class="d-flex partners-footer__box flex-lg-column align-items-center">
            <img src="{{ asset('images/partners__image-3.svg') }}" alt="" class="partners-footer__image">
            <a href="" class="partners-footer-item mb-0">Разрешительные документы</a>
        </div>
    </div>
    <div class="d-flex justify-content-between align-items-center mt-3">
        <span class="partners-footer-text">2019 (с) marinist.com.ua</span>
        <a class="partners-footer-text" target="_blank" href="">by Command+X</a>
    </div>
</footer>


<div class="modal fade" id="phoneModal" tabindex="-1" role="dialog" aria-labelledby="phoneModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered align-items-end" role="document">
        <div class="modal-content">
            <div class="modal-content-box">
                <div class="modal-header d-flex justify-content-center align-items-center p-3">
                    <p class="phone-mobile-title text-center mb-0">Наши номера для связи</p>
                </div>
                <div class="modal-body border-bottom text-center">
                    <a href="" class="phones-box__item flex-row align-items-center justify-content-center">
                        <span class="phone">(095) 095 76 68</span>
                    </a>
                </div>
                <div class="modal-body border-bottom text-center">
                    <a href="" class="phones-box__item flex-row align-items-center justify-content-center">
                        <span class="phone">(095) 095 76 68</span>
                    </a>
                </div>
                <div class="modal-body border-bottom text-center">
                    <a href="" class="phones-box__item flex-row align-items-center justify-content-center">
                        <span class="phone">(095) 095 76 68</span>
                    </a>
                </div>
            </div>
            <div class="modal-footer">
                <span class="mx-auto" data-dismiss="modal">Отмена</span>
            </div>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.1.5/js/uikit.js"></script>
<script src="https://code.iconify.design/1/1.0.2/iconify.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>
<script src="{{ asset('js/index.js') }}?v={{ assetFilemtime('js/index.js')}}" defer></script>


</body>
</html>
