<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('css/main.css')}}?v={{ assetFilemtime('css/main.css') }}">
</head>
<body>

<main class="login-page">
    <div class="bg-box"></div>
    <div class="container">
        <a href="{{ route('home') }}" class="logo-container">
            <img src="{{asset('images/logo.png')}}" alt="Logo">
        </a>
        <div class="form-container">
            <div class="title-box">
                <img src="{{asset('images/Wave-Marinist_1.svg')}}" alt="Wave">
                <h1 class="title-box__title">Авторизируйтесь</h1>
            </div>
            <form action="{{route('partners.login')}}" method="post" class="form-box">
                @csrf
                <input id="login" type="text" placeholder="Логин" name="username">
                <input id="password" type="password" placeholder="Пароль" name="password">
                <button type="submit" class="form-box__btn-send">
                    <span class="form-box__btn-send__wave"></span>
                    <span>Войти</span>
                </button>
            </form>
        </div>
    </div>
</main>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="{{ asset('js/index.js') }}?v={{ assetFilemtime('js/index.js')}}"></script>
</body>
</html>
