import { stringify } from 'query-string'
import { request } from './helper'

$(document).ready(async function() {
  const languages = [{ name: '/en/' }, { name: '/uk/' }]
  let locales = '/'
  languages.map(lang => {
    if (window.location.pathname.search(lang.name) !== -1) {
      locales = lang.name
    }
  })
  const currentUrl = window.location.origin + locales
  const { url } = await request({ url: `${currentUrl}api/get-params-data`, method: 'GET' })
  const apartment = {
    getAllSections: () => request({ url: url.sections, method: 'GET' }),
    getAllFloors: (section_id) => request({ url: `${url.floors}?${stringify({ section_id })}`, method: 'GET' }),
    getFloor: (floor_id) => request({ url: `${url.floor}?${stringify({ floor_id })}`, method: 'GET' }),
    getFlat: (flat_id) => request({ url: `${url.flat}?${stringify({ flat_id })}`, method: 'GET' })
  }
  const btnBack = $('.btn-back')
  const waveWrap = $('.wave-wrap')
  const sections = $('.layer-image>g')
  const tooltipBox = $('.tooltip-text')
	const sectionName = sections.find('.st05')
	const selectedFloor = sections.find('.st00')
  const sectionsMini = $('.layer-mini-image>g')
  const allSections = await apartment.getAllSections()
  
	allSections.map(async (section, index) => {
    const getFloors = await apartment.getAllFloors(section.section_id)
    const floors = $(sections[index]).find('.st00:not([id*=_x])')
    
    $(sectionName[index]).text(section.name_section)
    
    getFloors.map((floor, index) => {
      $(floors[index]).attr('data-floor', floor.floor_id)
      $(floors[index]).attr('data-section', section.section_id)
      $(floors[index]).attr('data-floor-name', floor.name_floor)
    })
  })
  
  function goTo(goToSection) {
    switch (goToSection) {
      case 'main': {
        btnBack.addClass('toMain')
        btnBack.removeClass('toFloor')
        $('.sub-section').removeClass('show')
        $('.main-box').removeClass('isChanged')
        $('.right-box').removeClass('show')
        $('.col-section').removeClass('show')
        break
      }
      case 'floor': {
        btnBack.addClass('toMain')
        $('.right-box').addClass('show')
        $('.col-section_floor').addClass('show')
        $('.sub-section').toggleClass('show')
        $('.main-box').toggleClass('isChanged')
        $('.col-section_flat').removeClass('show')
        break
      }
      case 'flat': {
        $('.sub-section').addClass('show')
        $('.main-box').addClass('isChanged')
        
        btnBack.addClass('toFloor')
        btnBack.removeClass('toMain')
        $('.download-link').addClass('show')
        $('.col-section_flat').addClass('show')
        $('.right-box').removeClass('show')
        $('.col-section_floor').removeClass('show')
        break
      }
      case 'fromFlatToFloor': {
        $('.right-box').addClass('show')
        $('.col-section_floor').addClass('show')
        btnBack.addClass('toMain')
        btnBack.removeClass('toFloor')
        $('.col-section_flat').removeClass('show')
        $('.download-link').removeClass('show')
        break
      }
      default:
        return ''
    }
  }
  
  if (Number(sessionStorage.getItem('isParameter')) === 1) {
    const flatID = sessionStorage.getItem('flatID')
    const link = sessionStorage.getItem('link')
    
    $('#modalAppointment').find('input[name=link]').val(link)
    goTo('flat')
    await flatSelectedStart(flatID)
    
    sessionStorage.removeItem('isParameter')
  }
  
  async function floorSectionsStart(sectionId) {
    sectionsMini.removeClass('active')
    allSections.map((section, index) => {
      if (Number(sectionId) === Number(section.section_id)) {
        $(sectionsMini[index]).addClass('active')
      }
    })
  }
  
  async function floorListStart(sectionId, floorId) {
    const floorList = $('.floor-list')
    const floors = await apartment.getAllFloors(sectionId)
    
    floorList.html('')
    floors.map(floor => {
      floorList.append(`<li class="floor-list__item ${Number(floor.floor_id) === Number(floorId) && 'active'}" data-floor-id=${floor.floor_id}>${floor.name_floor.split(' ')[1]}</li>`)
    })
    
    $('.floor-list__item').on('click', function() {
      const floorId = $(this).attr('data-floor-id')
      
      $('.floor-list__item').removeClass('active')
      $(this).addClass('active')
      
      floorSelectedStart(floorId)
    })
  }
  
  function floorInfoStart(floor) {
    const floorName = $('.floor-name')
    
    floorName.html('')
    floorName.append(`<p>${floor.name_section_floor_plan}</p>`)
  }
  
  function floorPlanStart(floor) {
    const flats = floor.flats
    const floorPlan = $('.floor-plan')
    const infoPlan = {
      backlight: floor.backlight,
      plan: floor.plan,
      substrate: floor.substrate
    }
    
    floorPlan.html('')
    for (let i in infoPlan) {
      floorPlan.append(`<img class="svg-${i}" src=${infoPlan[i]} alt="" uk-svg>`)
    }
    floorPlan.append(`<img class="svg-backlight-transparent" src=${infoPlan.backlight} alt="" uk-svg>`)
    
    setTimeout(() => {
      const flatPlanTransparent = $('.floor-plan .svg-backlight-transparent>g')
      const flatPlanColor = $('.floor-plan .svg-backlight>g')
      
      flats.map((flat, index) => {
        $(flatPlanTransparent[index]).attr('data-flat-id', flat.flat_id)
        $(flatPlanColor[index]).attr('data-flat-id', flat.flat_id)
      })
      
      flatPlanTransparent.hover(function() {
        const transparentId = $(this).attr('data-flat-id')
        
        flatPlanColor.map((index, item) => {
          const colorId = $(item).attr('data-flat-id')
          
          if (transparentId === colorId) {
            $(item).addClass('more-visible')
          }
        })
      }, function() {
        flatPlanColor.removeClass('more-visible')
      })
      
      flatPlanTransparent.on('click', function() {
        const id = Number($(this).attr('data-flat-id'))
        
        goTo('flat')
        flatSelectedStart(id)
      })
    }, 500)
  }
  
  function flatInfoStart(flat) {
    const flatName = $('.flat-name p')
    const infoList = $('.flat-info-list')
    const characteristics = flat.characteristics
    
    infoList.html('')
    flatName.text(flat.apartaments)
    for (let i in characteristics) {
      const listItem = `<li class="flat-info-list__item"><p class="title">${i}:</p> ${characteristics[i]}</li>`
      
      infoList.append(listItem)
    }
  }
  
  function flatPlanStart(image) {
    const planWrap = $('.flat-plan')
    const planImg = `<img src=${image} alt=""/>`
    
    planWrap.html('')
    planWrap.append(planImg)
  }
  
  async function floorSelectedStart(floorId) {
    const floor = await apartment.getFloor(floorId)
    
    floorInfoStart(floor)
    floorPlanStart(floor)
  }
  
  async function flatSelectedStart(flatId) {
    const flat = await apartment.getFlat(flatId)
    
    $('.input-flat[name=section]').val(`Секиця ${flat.section_id}`)
    $('.input-flat[name=floor]').val(`Этаж ${flat.characteristics['Этаж']}`)
    $('.input-flat[name=flat_id]').val(flatId)
    $('.input-flat[name=flat]').val(flat.apartaments)
    
    flatInfoStart(flat)
    flatPlanStart(flat.photo)
  }
  
  selectedFloor.on('click', function() {
    const floorId = $(this).attr('data-floor')
    const sectionId = $(this).attr('data-section')
    
    if ($(this).index() === $(this).parent().find('[data-floor-name]').length) {
      return false
    }
    
    goTo('floor')
    floorSelectedStart(floorId)
    floorListStart(sectionId, floorId)
    
    floorSectionsStart(sectionId)
    
    var elem = $('.test-box')
    var offset = elem.offset().left - elem.parent().offset().left
    
    $('.sub-section').css({
      'margin-left': `${offset}px`
    })
  })
  btnBack.on('click', function() {
    if ($(this).hasClass('toMain')) {
      goTo('main')
    } else {
      goTo('fromFlatToFloor')
    }
  })
  
  selectedFloor.on('mouseover', function(e) {
    const sectionLayer = $(this).parent().find('path[id*=_x]')
    const floorName = $(this).attr('data-floor-name')
    const position = {
      top: e.offsetY + 50,
      left: e.offsetX + 50
    }
    
    if ($(this).index() === $(this).parent().find('[data-floor-name]').length) {
      $(this).css('cursor', 'not-allowed')
    }
    
    sectionLayer.addClass('visible')
    tooltipBox.addClass('show')
    tooltipBox.text(floorName)
    tooltipBox.css({
      'left': position.left,
      'top': position.top
    })
  })
  
  selectedFloor.on('mouseleave', function() {
    const sectionLayer = $(this).parent().find('path[id*=_x]')
    
    if ($(this).index() === $(this).parent().find('[data-floor-name]').length) {
      $(this).css('cursor', 'not-allowed')
    }
    
    sectionLayer.removeClass('visible')
    tooltipBox.removeClass('show')
  })
  
  waveWrap.on('click', function() {
    goTo('main')
  })
})
