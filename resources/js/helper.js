$(document).ready(function () {
  $('[data-toggle="tooltip"]').tooltip()
  // $('input[name=phone]').mask('+380999999999')
  
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  })
})

$(window).on('load', function () {
  if ($('.bingc-phone-button') && window.innerWidth > 991) {
    $('.bingc-phone-button').css({ 'left': '340px' })
  }
})

const request = async ({url, data, method = 'post'}) => {
  try {
    const apiCall = await fetch(url, {
      method,
      body: JSON.stringify(data)
    })
    return await apiCall.json()
  } catch (e) {
    console.log('Request_ERROR', e)
  }
}

$('.amo-button__link').on('click', function () {
  fbq('track', 'Contact');
})

$('.humb-menu_mobile').click(function () {
  $(this).toggleClass('is-opened')
  $('html, body').toggleClass('isMenuOpened')
  $('main').toggleClass('isMenuOpened')
  $('.mobile-menu').toggleClass('open')
})

$('.humb-menu_desktop').on('click', function () {
  $(this).toggleClass('is-opened')
  $(this).parents('header').toggleClass('is-collapsed')
  
  if ($(this).hasClass('is-opened')) {
    $('.bingc-phone-button').css({ 'left': '340px' })
  } else {
    $('.bingc-phone-button').css({ 'left': '130px' })
  }
})

$('.modal-form-submit').on('submit', function (e) {
  const modalSuccessTitle = $('#modalSuccess .modal-title')
  const modalSubmit = $('#modalAppointment')
  const modalSuccess = $('#modalSuccess')
  const modalFeedback = $('#modalFeedback')
  const modalBooking = $('#modalBooking')
  const inputs = $(this).find('input, textarea')
  const url = $(this).attr('action')
  const data = {}
  const modal = $(this).parents('.modal')
  
  e.preventDefault()
  inputs.map((key, input) => {
    const name = $(input).attr('name')
    const value = $(input).val()
    
    data[name] = value
  })
  $.ajax({
    url,
    data,
    method: 'GET'
  })
    .done(function (res) {
      if (res && res.status === 'success') {
        modalFeedback.modal('hide')
        modalSubmit.modal('hide')
        modalBooking.modal('hide')
        modalSuccessTitle.text(res.message)
        modalSuccess.modal('show')
        inputs.val('')
  
        fbq('track', 'Lead');
        
        if ($(modal).attr('id') === 'modalFeedback') {
          dataLayer.push({'event': 'svyazatsya_s_rukovodstvom'})
        } else {
          dataLayer.push({'event': 'zapisatsya_na_prosmotr'})
        }
      }
    })
    .fail(function (res) {
      alert(res.responseJSON.message)
    })
})

$('img[data-src]').each((index, img) => {
  img.setAttribute('src', img.getAttribute('data-src'))
  img.onload = function () {
    img.removeAttribute('data-src')
  }
})

export { request }
