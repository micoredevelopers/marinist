import { request } from '../js/helper'

const navLink = $('.nav-wrap__link-wrap')

function InitGallerySlider(sliderId) {
	new Swiper(`#${sliderId} .gallery-slider`, {
		spaceBetween: 0,
		speed: 800,
		loop: true,
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev'
		},
		pagination: {
			el: '.swiper-pagination',
			clickable: true
		}
	})
}

function InsertGalleryPhotos(photos, id) {
	const sliderInnerWrap = $(`#${id} .swiper-wrapper`)

	sliderInnerWrap.html('')
	photos.map((imageSrc, key) => {
		const slideImage = `<div class="swiper-slide"><img class="slider-image" src=${imageSrc} alt="Слайд номер ${key}" /></div>`

		sliderInnerWrap.append(slideImage)
	})
}
function InsertGalleryVideos(videos, id) {
	const videosInnerWrap = $(`#${id} .row-gallery-videos`)

	videosInnerWrap.html('')
	videos.map(videoId => {
		const videoFrame = `
			<div class="col-12">
				<div class="video-wrap">
					<iframe 
						width="100%" 
						height="100%" 
						src=https://www.youtube.com/embed/${videoId} 
						allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" 
						allowfullscreen></iframe>
				</div>
			</div>
		`

		videosInnerWrap.append(videoFrame)
	})
}

$(document).ready(async function () {
	const firstUrl = $(navLink[0]).attr('data-url')
	const firstId = $(navLink[0]).attr('data-target')
	const apiCall = await request({ url: firstUrl, method: 'GET' })
	const { photos } = apiCall

	navLink.on('click', async function () {
		const id = $(this).attr('data-target')
		const url = $(this).attr('data-url')
		const apiCall = await request({ url, method: 'GET' })
		const { photos, videos } = apiCall

		if (photos.length > 0) {
			InsertGalleryPhotos(photos, id)
			InitGallerySlider(id)
		} else if (videos.length > 0) {
			InsertGalleryVideos(videos, id)
		}

		navLink.removeClass('active')
		$(this).addClass('active')

		$('.gallery-slider-wrap').each((index, item) => {
			if ($(this).index() === index) {
				$('.gallery-slider-wrap').removeClass('active')
				$(item).addClass('active')
			}
		})
	})

	InsertGalleryPhotos(photos, firstId)
	InitGallerySlider(firstId)
})
