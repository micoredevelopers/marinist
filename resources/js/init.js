const body = document.querySelector('main')
const pageClass = body.getAttribute('class')

switch (pageClass) {
  case 'main-page':
    require('./main')
    break
  case 'about-page':
    require('./about')
    break
  case 'gallery-page':
    require('./gallery')
    break
  case 'apartment-page':
    require('./apartment')
    break
  case 'building-page':
    require('./building')
    break
  case 'real-estate-developer-page':
    require('./real-estate-developer')
    break
  case 'news-selected-page':
    require('./news-selected')
    break
  case 'contact-page':
    require('./contacts')
    break
  case 'parameters-page':
    require('./parameters')
    break
  case 'news-page':
    require('./news')
    break
  case 'partners-main':
    require('./partners')
    break
  case 'partners-login':
    require('./partnersLogin')
    break
  case 'club-page':
    require('./club')
    break
  default:
    break
}
