import axios from "axios";


// $('body').css('background-color', '#f3f3f3');

$('.btn-box__more').click(function () {
    let url = $(this).attr('data-url');
    axios.get(url)
        .then((res) => {
            if (res.status === 200) {
                $('.news-list').append(res.data.content);
                if (res.data.hasMorePages) {
                    $('.btn-box__more').attr('data-url', res.data.next);
                } else {
                    $('.btn-box').addClass('hide');
                }
            }
        })
});
