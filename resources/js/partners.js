$(document).ready(function() {
    $("#nav-icon1,#nav-icon2,#nav-icon3,#nav-icon4").click(function() {
        $(this).toggleClass("open");
        $(".mob-menu-wrap").toggleClass("open");
    });

    $(".my-dropdown-wrap").click(function() {
        const that = $(this);
        that.find(".my-dropdown").slideToggle(300);
    });
    $(".tabs-link").click(function(e) {
        e.preventDefault();
        const that = $(this);
        const thisInd = that.index();
        const text = that.html()
        $('.dropdown_section__text').html(text)
        that.closest("my-dropdown").slideUp();
        that.closest(".title-box")
            .next()
            .find(".price-table")
            .eq(thisInd)
            .show()
            .siblings()
            .hide();
    });

    $(".scroll-link").click(function() {
        $('html, body').animate({
            scrollTop: $( $.attr(this, 'href') ).offset().top - 70
        }, 500);
        $(".mob-menu-wrap").removeClass("open");
        return false;
    });
});
