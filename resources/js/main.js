const wow = new WOW({
	boxClass: 'wow',
	animateClass: 'animated',
	offset: 0,
	mobile: true,
	live: true
})
const header = $('header')
const logo = $('.gif-logo')
const videoCont = $('.video-box')
const btnPlay = $('.video-btn_play')
const btnStop = $('.video-btn_stop')
const bottomInfoCont = $('.bottom-info')
const iframe = $('.video-box .iframe-wrap iframe')

wow.init()

function startVideo() {
	logo.addClass('hide')
	videoCont.addClass('show')
	bottomInfoCont.addClass('hide')
	onYouTubeIframeAPIReady()
}
function stopVideo() {
	logo.removeClass('hide')
	videoCont.removeClass('show')
	bottomInfoCont.removeClass('hide')
	player.stopVideo()
}

btnPlay.on('click', function () {
	const iframeSrc = $(this).attr('data-src')

	startVideo(iframeSrc)
})
btnStop.on('click', function () {
	stopVideo()
})

header.addClass('shadow-none')
header.addClass('is-main')
header.removeClass('is-collapsed')
$('.humb-menu_desktop').addClass('is-opened')

let player
let tag = document.createElement('script')
tag.src = "https://www.youtube.com/iframe_api"
let firstScriptTag = document.getElementsByTagName('script')[0]
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag)

function onYouTubeIframeAPIReady() {
	player = new YT.Player('iframe-wrap', {
		width: '100%',
		height: '100%',
		videoId: '1EjxOVbC_VA',
		playerVars: { 'autoplay': 1, 'controls': 1, 'loop': 1, 'rel': 0 },
		events: {
			'onStateChange': onPlayerStateChange,
		}
	});
}

function onPlayerStateChange(e) {
	if (e.data === 2) {
		btnStop.addClass('show')
	} else {
		btnStop.removeClass('show')
	}
}

