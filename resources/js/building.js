import { request } from './helper'

function getPercent(box) {
	return Number(box.attr('data-percent'))
}

function recountNumbers(numberCont, maxValue) {
	let count = 0
	numberCont.text('0%')

	const incNum = setInterval(() => {
		count++
		numberCont.text(`${count}%`)
		count >= maxValue && clearInterval(incNum)
	}, 8)
}

function setStatusBuilding() {
	const statusBar = $('.status-number')
	const statusBuilding = $('.building-status-wave')
	const statusPercent = getPercent(statusBar)

	statusBuilding.addClass('started')
	statusBuilding.css('height', `${statusPercent}%`)
}

function setStatus() {
	const statusLine = $('.status-line')

	statusLine.map((lineIndex, lineWrap) => {
		const statusPercent = getPercent($(lineWrap))
		const fillLine = $(lineWrap).find('.line-image-fill')

		if (statusPercent > 0) {
			fillLine.addClass('started')
			fillLine.css('width', `${statusPercent}%`)
		}
	})
}

setStatus()
setStatusBuilding()

const buildingSlider = new Swiper('.building-slider', {
	spaceBetween: 30,
	slidesPerView: 3,
	speed: 800,
	navigation: {
		nextEl: '.swiper-button-next',
		prevEl: '.swiper-button-prev'
	},
	pagination: {
		el: '.swiper-pagination',
		clickable: true
	}
})

function InitGallerySlider() {
	new Swiper(`.modal-slider`, {
		spaceBetween: 30,
		slidesPerView: 1,
		speed: 800,
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev'
		}
	})
}

$('.humb-menu_desktop').on('click', function () {
	setTimeout(function () {
		buildingSlider.update()
	}, 1100)
})

$('.row-building .building-slider .swiper-slide, .row-building-cards .building-box .image-box').on('click', async function () {
	const url = $(this).attr('data-url')
	const sliderWrap = $('.modal-slider .swiper-wrapper')
	const infoBox = $('#building-modal .info-box')
	const apiCall = await request({ method: 'GET', url: `${url}` })
	const infoContent = `<div class="info-box__title">${apiCall.title}</div><div class="info-box__desc">${apiCall.description}</div>`

	sliderWrap.html('')
	infoBox.html('')
	apiCall.photos.map(photo => {
		const slideHTML =
			`<div class="swiper-slide">
				<div class="image-box">
					<img src=${photo} alt="Слайдер ход строительства">
				</div>
			</div>`

		sliderWrap.append(slideHTML)
	})
	infoBox.append(infoContent)
	InitGallerySlider()
})

$('.building-box').map(index => {
	if (index < 4) {
		$('.row-building-cards .btn-box').addClass('d-none')
	}
})
