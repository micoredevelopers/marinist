setTimeout(function () {
	$('.select-styled').map(function (index, item) {
		let $hiddenSelect = $(this).parent().find('.select-hidden option[selected="selected"]').val();
		$(item).text($hiddenSelect);
	});
}, 500);

function customizeSelect() {
	$('.filters-form select').each(function () {
		var $this = $(this), numberOfOptions = $(this).children('option').length;
		let $selectId = $this.attr('id');
		let $selectName = $this.attr('name');
		let $selectClass = $this.attr('class');

		$this.addClass('select-hidden');
		$this.wrap(`<div id=${$selectId} data-name=${$selectName} class="select ${$selectClass}"></div>`);
		$this.after('<div class="select-styled"></div>');

		var $styledSelect = $this.next('div.select-styled');
		$styledSelect.text($this.children('option').eq(0).text());

		var $list = $('<ul/>', {
			'class': 'select-options'
		}).insertAfter($styledSelect);

		for (var i = 0; i < numberOfOptions; i++) {
			$('<li/>', {
				text: $this.children('option').eq(i).text(),
				rel: $this.children('option').eq(i).val()
			}).appendTo($list);
		}

		var $listItems = $list.children('li');

		$styledSelect.click(function (e) {
			e.stopPropagation();
			$('div.select-styled.active').not(this).each(function () {
				$(this).removeClass('active').next('ul.select-options').hide();
			});
			$(this).toggleClass('active').next('ul.select-options').toggle();
		});

		$listItems.click(function (e) {
			e.stopPropagation();
			$styledSelect.text($(this).text()).removeClass('active');
			$this.val($(this).attr('rel'));
			$list.hide();
		});

		$(document).click(function () {
			$styledSelect.removeClass('active');
			$list.hide();
		});
	});
}

customizeSelect();

function sendData([...dataCont], object) {
	$(dataCont).each((index, item) => {
		if ($(item).hasClass('select')) {
			object[$(item).attr('data-name')] = $(item).find('.select-styled').text();
		} else {
			object[$(item).attr('name')] = item.checked;
		}
	});
}

const selectOptions = $('.select-options')
const options = $('.select-options>li')

selectOptions.map((key, select) => {
	const options = $(select).find('li')

	if (options.length > 9) {
		options.prevObject.addClass('scrollable')
	}
})

$('.type-box').click(function () {
	$('.type-box').removeClass('active');
	$(this).addClass('active');
	if ($('.type-box_table').hasClass('active')) {
		$('.table-box').addClass('show');
		$('.row-box').removeClass('show');
		localStorage.setItem('type', 'table')
	} else {
		$('.table-box').removeClass('show');
		$('.row-box').addClass('show');
		localStorage.setItem('type', 'row')
	}
});

localStorage.setItem('type', 'row')

function setTable() {
	$('.table-box').addClass('show');
	$('.row-box').removeClass('show');
	$('.type-box_table').addClass('active');
	$('.type-box_block').removeClass('active');
}

function setRow() {
	$('.row-box').addClass('show');
	$('.table-box').removeClass('show');
	$('.type-box_block').addClass('active');
	$('.type-box_table').removeClass('active');
}

if (localStorage.getItem('type') === 'row') {
	setRow()
} else {
	setTable()
}

options.on('click', function () {
	let $select = $(this).parents('.select');
	let $selectVal = Number($select.find('.select-styled').text());
	let $anotherSelect;

	if ($select.hasClass('left')) {
		$anotherSelect = $(this).parents('.filters-select').find('div.right');
	} else {
		return false;
	}

	$anotherSelect.find('.select-options li').each((index, item) => {
		let $optVal = Number($(item).text());
		if ($optVal < $selectVal) {
			$(item).hide();
		} else {
			$(item).show();
		}
		$anotherSelect.find('.select-styled').text(`${$selectVal}`);
	});
});

// $('.filters-form').on('submit', function (e) {
// 	let $sendSelects = $('.select');
// 	let $sendChecks = $('input[type=checkbox]');
// 	e.preventDefault();
// 	sendData([...$sendSelects, ...$sendChecks], dataObject);
// });
$('.btn-reset').click(function () {
	let $sendSelects = $('.select');
	let $sendChecks = $('input[type=checkbox]');
	$sendSelects.find('.select-styled').text('0');
	$sendSelects.find('.select-options li').show();
	$sendChecks.prop('checked', false);
});

const rangeWrap = $('.range-slider-wrap')

rangeWrap.map((index, item) => {
	const rangeMin = Number($(item).attr('data-range-min'));
	const rangeMax = Number($(item).attr('data-range-max'));

	const rangeFrom = Number($(item).attr('data-range-select').split(',')[0])
	const rangeTo = Number($(item).attr('data-range-select').split(',')[1])

	$(item).find('.js-range-slider').ionRangeSlider({
		type: 'double',
		skin: 'round',
		min: rangeMin,
		max: rangeMax,
		input_values_separator: ',',
		from: rangeFrom === 0 ? rangeMin : rangeFrom,
		to: rangeTo === 0 ? rangeMax : rangeTo,
		force_edges: true,
		extra_classes: 'custom-range-slider'
	});
})

$('.flat-info__row__btn-detailed').on('click', function () {
	const flatId = $(this).attr('data-flat-id')
    const currentUrl = window.location.href

    sessionStorage.setItem('flatID', flatId)
    sessionStorage.setItem('link', currentUrl)
	sessionStorage.setItem('isParameter', '1')
})
