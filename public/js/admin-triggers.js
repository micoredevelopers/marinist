$(document).ready(function (e) {
	$(document).on('click', '.image-actions .delete-image-btn', function (e) {
		if (confirm('Удалить фото?')) {
			$image = $(this).parent().find('.deleteable')[0];
			deletePhoto($image);
		}
	});
	$(document).on('click', '.image-actions .get-crop-btn', function (e) {
		$image = $(this).parent().find('.croppable')[0];
		getEditPhotoForCrop($image);
	});
	$('[data-flatpickr-type="datetime_local"]').flatpickr({
		enableTime: true,
		dateFormat: "Y-m-d H:i",
	});
	$('[data-flatpickr-type="default"]').flatpickr();
	//
	const $fancy = $("a.fancy");
	if ($fancy.length) {
		$fancy.fancybox();
	}
});
