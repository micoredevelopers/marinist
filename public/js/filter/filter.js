/* eslint-disable camelcase */
/* eslint-disable no-use-before-define */
/* eslint-disable no-undef */
/* eslint-disable prefer-destructuring */
/* eslint-disable no-param-reassign */
const currentLangForUrl = (()=>{
    if (window.location.href.match(/\/uk\//)) return '/uk/';
    if (window.location.href.match(/\/en\//)) return '/en/';
    return '/';
  })()
const currentLang = (()=>{
    if (window.location.href.match(/\/uk\//)) return 'ua';
    if (window.location.href.match(/\/en\//)) return 'en';
    return 'ru';
  })()
  const langs = {
    'floors': {
      ua:'Поверхи',
      ru:'Этажи',
      en:'Floors',
    },
    'flat': {
      ua:'Квартирa',
      ru:'Квартирa',
      en:'Flat',
    },
    'toFloor': {
      ua:'РџРµСЂРµР№С‚Рё РґРѕ РїРѕРІРµСЂС…Сѓ',
      ru:'РџРµСЂРµР№С‚Рё Рє СЌС‚Р°Р¶Сѓ',
      en:'Go to floor',
    },
    'total': {
      ua:'Всього',
      ru:'Всего',
      en:'Total square',
    },
    'all__room': {
      ua:'Загальна площа',
      ru:'Общая площадь',
      en:'Total square',
    },
    'living': {
      ua:'Житлова площа',
      ru:'Жилая площадь',
      en:'Living square',
    },
    'rooms': {
      ua:'Кімнат',
      ru:'Комнат',
      en:'Rooms',
    },
    'floor': {
      ua:'Поверх',
      ru:'Этаж',
      en:'Floor',
    },
    'type': {
      ua:'Тип',
      ru:'Тип',
      en:'Type',
    },
    'show': {
      ua:'Дивитись планування',
      ru:'Смотреть планировку',
      en:'show',
    },
    'section': {
      ua:'Секція',
      ru:'Секция',
      en:'Section',
    },
    'house': {
      ua:'Будинок',
      ru:'Дом',
      en:'Build',
    },
    'm': {
      ua:'м',
      ru:'м',
      en:'m',
    },
  }
  const preloader = `
  <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
  `;
  const filterNav = document.querySelector('[data-filter-nav]');
  gsap.to(filterNav, { filter: 'grayscale(1)', pointerEvents: 'none'});
//   filterNav.insertAdjacentHTML('beforeend', preloader);
  const data = new FormData();
  data.append('action', 'flatsGet');
  
  // fetch('./static/flats-test.json', {
  //   method: 'POST',
  //   body: data,
  // })
  // fetch('https://base-api.marinist.com.ua/v1/apartments?access_token=snV0hF0VUhvnCqrU136QkbID3DmK5uaavtXxK07Hwj9k1DdC0IdHC6c7mhFArrw0RroRxIaKZzAIHyAELuEYwRt8EX67xxl3PS6eJbGEUtVqIu1O8Q%3D%3D&option%5Bproject_id%5D=4', {
  //   // body: data,
  //   mode: 'no-cors',
  //   headers: {
  //     'Content-Type': 'text/plain',
  //     'Accept-Encoding': '*',
  //     'Access-Control-Allow-Origin': 'https://base-api.marinist.com.ua/',
  //     'Accept': '*',
  //     'Host': 'base-api.marinist.com.ua',
  //     'Sec-Fetch-Mode': 'cors',
  //     'Sec-Fetch-Site': 'cross-site',
  //   }
  // })
  // .then(el => {
  //   console.log(el);
  //   return el.json();
  // })
  // .then(flats => {
  //   console.log(flats);
  //   initPage(JSON.parse(flats));

  //   // gsap.to(filterNav, { filter: 'grayscale(0)', pointerEvents: 'all'});
  //   // document.querySelector('.lds-ring').remove();
  // })
  // console.log(qwe);
  initPage(qwe.data);
  
 
  
  function initPage(appartments) {
   
    function prepareValidFlatsArray(filterDataValues, objectOfFlats, cb = () => {}) {
      const validationDataArray = Object.entries(filterDataValues);
      const validationDataLength = validationDataArray.length;
      const validFlats = [];
      objectOfFlats.forEach((flat) => {
        let validFieldsCount = 0;
        validationDataArray.forEach((filterPoint) => {
          const name = filterPoint[0];
          const value = filterPoint[1];
          const constructor = Object.getPrototypeOf(value).constructor.name;
          switch (constructor) {
            case 'Object':
              if (value.min === '' && value.max === '') {
                validFieldsCount += 1;
                break;
              }
              if (value.min <= +flat[name]
                && value.max >= +flat[name]) validFieldsCount += 1;
              break;
            case 'Set':
              if (value.size === 0) {
                validFieldsCount += 1;
                break;
              }
              if (value.has(flat[name])) validFieldsCount += 1;
              break;
            case 't':
              if (value.size === 0) {
                validFieldsCount += 1;
                break;
              }
              if (value.has(flat[name])) validFieldsCount += 1;
              break;
            case 'String':
              if (value === '') {
                validFieldsCount += 1;
                break;
              }
              if (value === flat[name]) validFieldsCount += 1;
              break;
            case 'Array':
              validFieldsCount += 1;
              break;
            default:
              break;
          }
        });
        if ((validFieldsCount === validationDataLength)) {
          validFlats.push(flat);
        }
        cb();
      });
      return validFlats;
    }
    function filterFlats(filterDataValues, cb = () => {}) {
      const objectsToFilter = document.querySelectorAll('[data-filter-me]');
      const validationDataArray = Object.entries(filterDataValues);
      const validationDataLength = validationDataArray.length;
      objectsToFilter.forEach((flat) => {
        let validFieldsCount = 0;
        validationDataArray.forEach((filterPoint) => {
          const name = filterPoint[0];
          const value = filterPoint[1];
          const constructor = Object.getPrototypeOf(value).constructor.name;
          switch (constructor) {
            case 'Object':
              if (value.min === '' && value.max === '') {
                validFieldsCount += 1;
                break;
              }
              if (value.min <= +flat.dataset[name]
                && value.max >= +flat.dataset[name]) validFieldsCount += 1;
              break;
            case 'Set':
              if (value.size === 0) {
                validFieldsCount += 1;
                break;
              }
              if (value.has(flat.dataset[name])) validFieldsCount += 1;
              break;
            case 'String':
              if (value === '') {
                validFieldsCount += 1;
                break;
              }
              if (value === flat.dataset[name]) validFieldsCount += 1;
              break;
            case 'Array':
              validFieldsCount += 1;
              break;
            default:
              break;
          }
        });
        if ((validFieldsCount === validationDataLength)) {
          flat.style.display = '';
          flat.dataset.isValid = '1';
        } else {
          flat.style.display = 'none';
          flat.dataset.isValid = '';
        }
        // filterDataValues.validFlatsCount[0] = validFlatsCount;
        cb();
      });
    }
    function handleFilterItemsChanges(filterArg) {
      const filter = filterArg;
      const filterItems = document.querySelectorAll('[data-filter-item]');
      filterItems.forEach((item) => {
        const { type, name, value } = item.dataset;
        item.addEventListener('change', () => {
          if (type === 'checkbox' && item.checked) {
            filter[name].add(value);
          } else if (type === 'radio' && item.checked) {
            filter[name] = value;
          } else if (type === 'checkbox' && !item.checked) {
            filter[name].delete(value);
          }
        });
      });
    }
    function initFilterItems() {
      const filterItems = document.querySelectorAll('[data-filter-item]');
      const object = {};
      filterItems.forEach((item) => {
        const { type, name } = item.dataset;
        const { id } = item;
    
        if (type === 'checkbox') {
          object[name] = new Set();
        } else if (type === 'radio') {
          object[name] = '';
        } else {
          object[id] = {
            min: '',
            max: '',
          };
        }
      });

      // console.log(object);
      return object;
    }
    const filter = initFilterItems();
    window.filter = filter;
    handleFilterItemsChanges(filter);
    function setFilteringValues(dataObjectArg, key, digit) {
      const dataObject = dataObjectArg;
      dataObject[key].min = digit[0];
      dataObject[key].max = digit[1];
    }
    
    function initBoxRange({ range, onChange, onFinish }) {
      let minV = 0;
      //   let maxV = 0;
      const minArr = [];
      const maxArr = [];
      appartments.forEach((flat) => {
        minArr.push(flat[range.getAttribute('id')] >= 0 ? parseFloat(flat[range.getAttribute('id')]) : null);
        maxArr.push(flat[range.getAttribute('id')] >= 0 ? parseFloat(flat[range.getAttribute('id')]) : null);
      });
      minV = Math.min.apply(null, minArr);
      //   maxV = Math.max.apply(null, maxArr);
      range.dataset.max = range.getAttribute('id');
      range.dataset.max = Math.max.apply(null, minArr) + 1;
      range.dataset.min = Math.floor(minV);
      range.dataset.to = Math.max.apply(null, minArr) + 1;
      range.dataset.from = Math.floor(minV);
      const {
        max, min, from, to,
      } = range.dataset;
      $(range).ionRangeSlider({
        type: 'double',
        values_separator: '-',
        min,
        max,
        from,
        to,
        hide_min_max: true,
        onChange(/* ionRange */) {
          onChange();
          // setValue(range, ionRange, ['from', 'to']);
          // console.log(filter);
        },
    
        onFinish(ionRange) {
          onFinish(ionRange);
        },
      });
      // after init set filter state
      const dataRange = $(range).data('ionRangeSlider');
      //   setFilter(dataRange.options, dataRange.input);
      return dataRange;
    }
    
    const RANGES = document.querySelectorAll('.js-range-init');
    
    RANGES.forEach((elArgs) => {
      const el = elArgs;
      const range = initBoxRange({
        range: el,
        onChange: (/* ionRange */) => {
        },
        onStart: (/* ionRange */) => {},
        onFinish: (/* ionRange */) => {
        //   setFilter(ionRange, ionRange.input[0]);
          setFilteringValues(
            filter,
            el.getAttribute('id'),
            [range.old_from, range.old_to],
          );
        },
      });
      el.range = range;
    });
    
    const filterNav = document.querySelector('[data-filter-nav]');
  
    
  
    
    
    
    
    
    const tableFlatsRenderContainer = document.querySelector('[data-table-flats-render]');
    const cardFlatsRenderContainer = document.querySelector('[data-card-flats-render]');
    
    // tableFlatsRenderContainer.innerHTML = '';
    cardFlatsRenderContainer.innerHTML = '';
    portionedRenderFlats(appartments, 0, 10);
    // appartments.forEach((appart) => {
    // //   tableFlatsRenderContainer.innerHTML += tableFlatTemplate(appart);
    //   cardFlatsRenderContainer.innerHTML += cardFlatTemplate(appart);
    // });
    
    const filterButtons = document.querySelectorAll('[data-filter-button]');
    filterFlats(filter);
    filterButtons.forEach((button) => {
      button.dataset.countResults = document.querySelectorAll('[data-is-valid="1"]').length / 2;
      button.addEventListener('click', () => {
        const validFlats = prepareValidFlatsArray(filter, appartments);
        window.dispatchEvent(new Event('filtering'));
        // filterFlats(filter, () => {
        //   locoScroll.update();
        // });
        portionedRenderFlats(validFlats, 0, 9, () => {
          // locoScroll.update();
        });
        button.dataset.countResults = `
        (${validFlats.length})`;
      });
    
      if (document.documentElement.clientWidth < 576) {
        button.addEventListener('click', () => {
          filterNav.classList.add('rolled-up');
        });
      }
    });
    document.querySelectorAll('[data-filter-item]').forEach(el => {
      el.addEventListener('change',function(evt){
        const validFlats = prepareValidFlatsArray(filter, appartments);
        window.dispatchEvent(new Event('filtering'));
        // filterFlats(filter, () => {
        //   locoScroll.update();
        // });
        portionedRenderFlats(validFlats, 0, 9, () => {
          // locoScroll.update();
        });
      });
    })
    function portionedRenderFlats(flats, startPoint, endPoint, cb = () => {}) {
      if (startPoint === 0) {
        cardFlatsRenderContainer.innerHTML = '';
      }
      if (flats[startPoint] === undefined) return;
      const portion = 9;
      const options = {
        rootMargin: '0px',
        threshold: 0.1,
      };
      for (let i = startPoint; i < endPoint; i += 1) {
        const flat = flats[i];
        if (flat !== undefined) {
          cardFlatsRenderContainer.innerHTML += cardFlatTemplate(flat);
        } else {
          break;
        }
      }
      const lastFlat = cardFlatsRenderContainer.lastElementChild;
      const callback = (entries) => {
        /* Content excerpted, show below */
        entries.forEach((entry) => {
          if (entry.isIntersecting) {
            // document.querySelector('.contact-page').classList.add('pending');
            
            portionedRenderFlats(flats, endPoint, endPoint + portion, () => {
              setTimeout(() => {
                // document.querySelector('.contact-page').classList.remove('pending');
              }, 1000);
            });
            observer.unobserve(lastFlat);
          }
        });
      };
      const observer = new IntersectionObserver(callback, options);
      const target = lastFlat;
      observer.observe(target);
      cb();
    }
    
    /* mobile tolltip for floor plan */
    function createTippyContent(path) {
      const some = document.createElement('div');
      const {
        ceil, flats, commerce, floors,
      } = path.dataset;
      // const some = document.querySelector('[data-clone-node-for-tippy]').cloneNode(true);
      some.innerHTML = `
        <table>
          <tr class="section-info-row">
            <td class="section-info-row-subtitle">${langs['floors'][currentLang]}:</td>
            <td class="section-info-row-val" data-render="floors"> ${floors}</td>
          </tr>
          <tr class="section-info-row">
            <td class="section-info-row-subtitle">${langs['flats'][currentLang]}:</td>
            <td class="section-info-row-val" data-render="flats">${flats} </td>
          </tr>
          <tr>
            <td>
              <a class="underlined-link-wrap" href="${path.parentElement.getAttribute('href')}">
                <div class="underlined-link">  ${langs['toFloor'][currentLang]}</div>
                <svg class="icon--build-arrow" role="presentation">
                  <use xlink:href="#icon-build-arrow"></use>
                </svg>
              </a>
            </td>
          </tr>
        </table>
      `;
      return some;
    }
    
    const pathesForTooltip = document.querySelectorAll('[data-info-path]');
    if (document.documentElement.clientWidth < 769) {
      pathesForTooltip.forEach((path) => {
        const pathTippy = tippy(path, {
          content: createTippyContent(path),
          // interactive: true,
          placement: 'bottom',
          allowHTML: true,
          theme: 'light',
        });
        path.parentElement.addEventListener('click', (evt) => {
          evt.preventDefault();
          pathTippy.show();
        });
      });
    }
    
    
    
    
  }
  
  
  /* filter help functions*/
  function cardFlatTemplate(flatData) {

    const { life_room, all_room, img_big, sec, id, rooms, build,number } = flatData;
    const datasetsToRender = {
      life_room, all_room, img_big, sec, id, rooms, build,number
    }
    let dataset = '';
    Object.entries(datasetsToRender).forEach((filterValue) => {
      dataset += ` data-${filterValue[0]}='${filterValue[1]}'`;
    });
    // const type = img.split('_').pop().replace(/flat|\.png/g, '');
    return `
    <a class="choose-flat__item fade-in-fwd" href="${currentLangForUrl}single-flat?id=${id}" ${dataset}>
            <div class="choose-flat__title">${langs.flat[currentLang]} ${number}</div>
            <img
              src="//base.marinist.com.ua/${img_big}"
              alt="foto"
              onerror="this.style.opacity = 0"
            />
            <div class="choose-flat__wrap">
              <div class="choose-flat__block">
                <div class="choose-flat__name">${langs.house[currentLang]}:</div>
                <div class="choose-flat__value">${build}</div>
              </div>
              <div class="choose-flat__block">
                <div class="choose-flat__name">${langs.rooms[currentLang]}:</div>
                <div class="choose-flat__value">${rooms}</div>
              </div>
              <div class="choose-flat__block">
                <div class="choose-flat__name">${langs.section[currentLang]}:</div>
                <div class="choose-flat__value">${sec}</div>
              </div>
              <div class="choose-flat__block">
                <div class="choose-flat__name">${langs.all__room[currentLang]}:</div>
                <div class="choose-flat__value">${all_room} ${langs.m[currentLang]}<sup>2</sup></div>
              </div>
              <!--<div class="choose-flat__block">
                <div class="choose-flat__name">${langs.living[currentLang]}:</div>
                <div class="choose-flat__value">${life_room} ${langs.m[currentLang]}<sup>2</sup></div>
              </div> -->
              <div class="btn choose-flat__btn">
                <span>${langs.show[currentLang]}</span>
              </div>
            </div>
        </a>
    `;
  }
  
  function renderPathesInfo(path) {
    path.addEventListener('mouseenter', () => {
      const dataInfo = Object.entries(path.dataset);
      dataInfo.forEach((item) => {
        const renderEl = document.querySelector(`[data-render='${item[0]}']`);
        if (renderEl !== null) {
          renderEl.textContent = item[1];
        }
      });
    });
  }
  const floorLinkPathes = document.querySelectorAll('[data-info-path]');
  floorLinkPathes.forEach(renderPathesInfo);
  
  gsap.defaults({ duration: 0.25 });
  function changeScreenWithEffects(toOpenElement, toCloseElement) {
    gsap.timeline()
      .to(toCloseElement, { y: 25, autoAlpha: 0, clearProps: 'all' })
      .set(toCloseElement, { display: 'none' })
      .set(toOpenElement, { display: '' })
      .fromTo(toOpenElement, { y: -25, autoAlpha: 0 }, { y: 0, autoAlpha: 1 }, '<')
  }
  
  
  /**РџРµСЂРµРєР»СЋС‡РµРЅРёРµ С”РєСЂР°РЅРѕРІ */
  const filterWrapper = document.querySelector('[data-filter-wrapper]');
  const planWrapper = document.querySelector('[data-build-plan-wrapper]');
  const switchScreenItems = document.querySelectorAll('[data-switch-screen]');
  let currentPageState = 'planWrapper';
  const screensSwitcher = {
    filterWrapper: () => changeScreenWithEffects(filterWrapper, planWrapper),
    planWrapper: () => changeScreenWithEffects(planWrapper, filterWrapper),
  };
  screensSwitcher[currentPageState]();
  switchScreenItems.forEach((el) => {
    el.addEventListener('click', () => {
      if (currentPageState === el.dataset.switchScreen) return;
      currentPageState = el.dataset.switchScreen;
      screensSwitcher[currentPageState]();
    });
  });
  
  const cardsContainer = document.querySelector('[data-cards-container]');
  const tablesContainer = document.querySelector('[data-tables-container]');
  const tableViewImage = document.querySelector('[data-table-image-view]');
  const switchFilterScreenItems = document.querySelectorAll('[data-switch-filter-screen]');
  // const classNameActiveInFilterScreen = 'filter'
//   switchFilterScreenItems[0].classList.add('active');
  let currentFilterState = 'cardsContainer';
  const filterScreensSwitcher = {
    cardsContainer: () => changeScreenWithEffects(cardsContainer, tablesContainer),
    tablesContainer: () => changeScreenWithEffects(tablesContainer, cardsContainer),
  };
  
  filterScreensSwitcher[currentFilterState]();
  switchFilterScreenItems.forEach((el) => {
    el.addEventListener('click', () => {
      if (currentFilterState === el.dataset.switchFilterScreen) return;
      switchFilterScreenItems.forEach(elem => elem.classList.remove('active'));
      currentFilterState = el.dataset.switchFilterScreen;
      el.classList.add('active');
      filterScreensSwitcher[currentFilterState]();
    });
  });
  /**РџРµСЂРµРєР»СЋС‡РµРЅРёРµ С”РєСЂР°РЅРѕРІ END */
  
  
  /* РЎРѕСЂС‚РёСЂРѕРІРєР° РєРІР°СЂС‚РёСЂ РІ СЃРїРёСЃРєРµ */
  const filterTableTriggers = document.querySelectorAll('[data-filter]');
  let currentSortEl = '';
  filterTableTriggers.forEach((item) => {
    item.addEventListener('click', () => {
      const filterTablesContainerItems = document.querySelectorAll('[data-table-flats-render]>*');
      const container = document.querySelector('[data-table-flats-render]');
      if (currentSortEl !== item) {
        filterTableTriggers.forEach(t => t.classList.remove('current-sorted'));
        item.classList.add('current-sorted');
      }
      item.classList.toggle('ascent');
      currentSortEl = item;
      if (item.classList.contains('ascent')) {
        Array.from(filterTablesContainerItems)
          .sort((first, second) => (
            +first.dataset[item.dataset.filter] - +second.dataset[item.dataset.filter]
          ))
          .forEach(el => container.insertAdjacentElement('beforeend', el));
      } else {
        Array.from(filterTablesContainerItems)
          .sort((first, second) => (
            +second.dataset[item.dataset.filter] - +first.dataset[item.dataset.filter]
          ))
          .forEach(el => container.insertAdjacentElement('beforeend', el));
      }
    });
  });
  
  window.addEventListener('filtering', () => {
    filterTableTriggers.forEach(t => t.classList.remove('current-sorted'));
    filterTableTriggers.forEach(t => t.classList.remove('ascent'));
  });
  
  /* РЎРѕСЂС‚РёСЂРѕРІРєР° РєРІР°СЂС‚РёСЂ РІ СЃРїРёСЃРєРµ  END*/