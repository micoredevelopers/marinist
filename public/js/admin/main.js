$(document).ready(function() {
    // ВСПЛЫВАЮЩИЕ МЕНЮ РЕДАКТИРОВАНИЯ В ТАБЛИЦЕ
    $(".order_button").on("click", function() {
        var toggleBlock = $(".detailed_block");
        if (
            $(toggleBlock).hasClass("active") &&
            $(this)
                .find(toggleBlock)
                .hasClass("active")
        ) {
            $(toggleBlock).removeClass("active");
        } else {
            $(toggleBlock).removeClass("active");
            $(this)
                .find(toggleBlock)
                .addClass("active");
        }
    });

    // ЗАКРЫТИЕ МЕНЮ ПРИ НАЖАТИИ НА ЛЮБОЕ МЕСТО
    $(".table_container").on("click", function(e) {
        if (
            e.target.className !== "detailed_block" &&
            e.target.className !== "btn_show"
        ) {
            $(".detailed_block").removeClass("active");
        }
    });

    //CKEDITOR
    $(document).ready(function() {
        if ($("textarea").length) {
            $("textarea").ckeditor();
        }
    });

    function readURL(input, img) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $(img).attr("src", e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#provider-file1").change(function() {
        readURL(this, $("#provider-first-img"));
        $("#provider-first-img").css({
            zIndex: "1"
        });
    });

    $("#file1").change(function() {
        readURL(this, $("#first-img"));
        $("#first-img").css({
            zIndex: "1"
        });
    });

    $("#provider-file2").change(function() {
        readURL(this, $("#provider-second-img"));
        $("#provider-second-img").css({
            zIndex: "1",
            width: "100%"
        });
    });
    function changeSrc(input, img) {
        $(input).change(function() {
            readURL(this, $(img));
            $(img).css({
                zIndex: "1",
                width: "100%"
            });
        });
    }

    changeSrc($("#file0"), $("#first-img0"));
    changeSrc($("#file1"), $("#first-img1"));
    changeSrc($("#file2"), $("#first-img2"));
    changeSrc($("#file3"), $("#first-img3"));
    changeSrc($("#file4"), $("#first-img4"));
    changeSrc($("#file5"), $("#first-img5"));
    changeSrc($("#file6"), $("#first-img6"));
    changeSrc($("#file7"), $("#first-img7"));

    $(".banner-cross").on("click", function() {
        $("#provider-file2").val("");
        $("#provider-second-img").attr("src", "");
        $("#provider-second-img").css({
            zIndex: "-1"
        });
    });

    $(".tariff-create-star-input:checked")
        .siblings(".tariff-star")
        .addClass("tariff-star-active");

    $(".tariff-label").on("click", function() {
        $(this)
            .find(".tariff-star")
            .addClass("tariff-star-active")
            .closest(".pos-rel")
            .siblings()
            .find(".tariff-star")
            .removeClass("tariff-star-active");
    });

    $("form").on("submit", function(e) {
        const inpArr = $(".contacts-field");
        // const contactsTitle= $('.contacts-field');

        inpArr.each(function(i, item) {
            const itemVal = $(item).val();
            const mail = $("#e-mail");

            let reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/g;
            if (itemVal === "" || itemVal === null) {
                e.preventDefault();
                $(item).addClass("error");
            } else if (!reg.test(mail.val())) {
                e.preventDefault();
                mail.addClass("error");
            } else {
                $(item).removeClass("error");
            }
        });
    });

    $("#phone1").mask("+7(999)-999-99-99", {
        placeholder: "+7(XXX)-XXX-XX-XX"
    });
    $("#phone2").mask("+7(999)-999-99-99", {
        placeholder: "+7(XXX)-XXX-XX-XX"
    });
    $("#phone3").mask("+7(999)-999-99-99", {
        placeholder: "+7(XXX)-XXX-XX-XX"
    });

    function showOption(arr, elem, selectTariff) {
        if (selectTariff !== undefined) {
            let optionArr = [];
            arr.each(function(i, item) {
                if (elem === $(item).attr("data-id")) {
                    optionArr.push($(item));
                    optionArr[0].prop("selected", true);
                    $(item).show();
                } else {
                    $(item).hide();
                }
            });
        } else {
            arr.each(function(i, item) {
                if (elem === $(item).attr("data-id")) {
                    $(item).show();
                } else {
                    $(item).hide();
                }
            });
        }
    }

    function showStock(provider, tariff) {
        const provVal = provider.val();
        const options = tariff;
        showOption(options, provVal);
        //provider.on('change', function() {showOption(options, provVal)})
    }

    showStock($("#provider_1"), $("#tariff_1 option"));
    showStock($("#provider_2"), $("#tariff_2 option"));
    showStock($("#provider_3"), $("#tariff_3 option"));
    showStock($("#provider_4"), $("#tariff_4 option"));

    $(".stock-providers").on("change", function() {
        let thisSelectVal = $(this).val();
        let optionsArr = $(this)
            .closest(".col-3")
            .find(".stock-tariff option");
        let bottomSelect = $(this)
            .closest(".col-3")
            .find(".stock-tariff");
        showOption(optionsArr, thisSelectVal, bottomSelect);
    });

    function showSelect(select, options) {
        const selectVal = $(select).val();
        const nextSelectOptions = $(options).children();
        showOption(nextSelectOptions, selectVal);
    }
    showSelect($("#region"), $("#city"));
    showSelect($("#city"), $("#street"));
    showSelect($("#street"), $("#house"));

    $("#region").on("change", function() {
        showSelect($("#region"), $("#city"));
    });
    $("#city").on("change", function() {
        showSelect($("#city"), $("#street"));
    });
    $("#street").on("change", function() {
        showSelect($("#street"), $("#house"));
    });

    function valid(form, arr) {
        $(form).on("submit", function(e) {
            const $inputArr = $(arr);

            $inputArr.each((key, el) => {
                if ($(el).val() === "" || $(el).val() === null) {
                    $(el).addClass("error");
                    e.preventDefault();
                } else {
                    $(el).removeClass("error");
                    $(this)
                        .unbind("submit")
                        .submit();
                }
            });
        });
    }
    valid($("#login-form"), $("#login-form .prov-edit-input"));

    $(".section_main").click(function() {
        $(".alert").remove();
    });
});


