<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$settings = [
			[
				'key'          => 'global.sitename',
				'value'        => 'Marinist',
				'display_name' => 'Имя сайта',
				'type'         => 'text',
			],
			[
				'key'          => 'social.facebook',
				'value'        => 'https://www.facebook.com/',
				'display_name' => 'Аккаунт facebook',
				'type'         => 'text',
			],
			[
				'key'          => 'social.instagram',
				'value'        => 'https://www.instagram.com/',
				'display_name' => 'Аккаунт instagram',
				'type'         => 'text',
			],
			[
				'key'          => 'main.download',
				'value'        => '',
				'display_name' => 'Файл презентации на главной',
				'type'         => 'file',
			],
			[
				'key'          => 'pages.contact_files',
				'value'        => '',
				'display_name' => 'Файлы для скачивания в контактах',
				'type'         => 'file_multiple',
			],
			[
				'key'          => 'pages.developer-site',
				'value'        => '',
				'display_name' => 'Ссылка на сайт застройщика',
				'type'         => 'text',
			],
			[
				'key'          => 'pages.contact_map',
				'value'        => '',
				'display_name' => 'Код карты',
				'type'         => 'text_area',
			],
			[
				'key'          => 'global.email',
				'value'        => 'info@marinist.com.ua',
				'display_name' => 'e-mail администратора сайта',
				'type'         => 'text',
			],
		];
		foreach ($settings as $setting) {
			if(!\App\Models\Setting::where('key', $setting['key'])->first()){
				(new \App\Models\Setting())->fill($setting)->save();
			}
		}
	}
}
