<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UsersTableSeeder::class);
         $this->call(RolesAndPermissionsSeeder::class);
         $this->call(LanguageTableSeeder::class);
         $this->call(AdminMenuSeeder::class);
         $this->call(MetaSeeder::class);
         $this->call(SettingsTableSeeder::class);
         $this->call(ProgressTableSeeder::class);
         $this->call(SectionFloorFlatSeeder::class);
         $this->call(MenuTableSeeder::class);
         $this->call(TranslateTableSeeder::class);
    }
}
