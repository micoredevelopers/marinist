<?php

use Illuminate\Database\Seeder;

class MetaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr = [
            'url' => '*'
        ];
        $meta = \App\Models\Meta::create($arr);
        foreach (\App\Models\Language::all() as $language) {
        	/** @var $language \App\Models\Language */
            DB::table('meta_lang')->insert([
				$language->getForeignKey() => $language->id,
				$meta->getForeignKey() => $meta->id,
            ]);
        }
    }
}
