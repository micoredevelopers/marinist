<?php

use Illuminate\Database\Seeder;

class ProgressTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$steps = [
			'Cтарт',
			'Фундамент',
			'Остекление',
			'Строительство',
			'Фасадные работы',
			'Освещение',
			'Оборудование двора',
			'Паркинг',
			'Трубы',
			'Озеленение',
			'Сдача',
		];
		$steps = array_reverse($steps);
		$languages = \App\Models\Language::all();
		foreach ($steps as $step) {
			$progressStep = \App\Models\ProgressSteps::create();
			foreach ($languages as $language) {
				/** @var $language \App\Models\Language */
				$lang = [
					$language->getForeignKey() => $language->id,
					'name' => $step,
					$progressStep->getForeignKey() => $progressStep->id
				];
				(new \App\Models\ProgressStepsLang)->fill($lang)->save();
			}
		}
	}
}
