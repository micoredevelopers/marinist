<?php

use Illuminate\Database\Seeder;

class MenuTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$arr = [
			['name' => 'Партнерам', 'url' => '/partners', 'active' => 0, 'photo' => 'images/partners.svg'],
			['name' => 'Контакты', 'url' => '/contact', 'photo' => 'images/4-layers.svg'],
			['name' => 'Новости', 'url' => '/news', 'photo' => 'images/12-layers.svg'],
			['name' => 'О застройщике', 'url' => '/about-developer', 'photo' => 'images/28-layers.svg'],
			['name' => 'Ход строительства', 'url' => '/progress', 'photo' => 'images/8-layers.svg'],
			['name' => 'Выбрать квартиру', 'url' => '/flat', 'photo' => 'images/2-layers.svg'],
			['name' => 'Галлерея', 'url' => '/gallery', 'photo' => 'images/9-layers.svg'],
			['name' => 'О проекте', 'url' => '/about', 'photo' => 'images/about-project.svg'],
		];
		$default = ['active' => 1];
		$arr = array_map(function ($menu) use ($default) {
			return array_merge($default, $menu);
		}, $arr);
		$languages = \App\Models\Language::all();
		foreach ($arr as $item) {
			($menu = new \App\Models\Menu())->fillExisting($item)->save();
			foreach ($languages as $language) {
				$lang = array_merge($item, [$menu->getForeignKey() => $menu->id, $language->getForeignKey() => $language->id]);
				($menuLang = new \App\Models\MenuLang())->fillExisting($lang)->save();
			}
		}
	}
}
