<?php

use Illuminate\Database\Seeder;

class AdminMenuSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$menus = [
			[
				'name'      => 'Настройки',
				'url'       => '/admin/settings',
				'gate_rule' => 'view_settings',
				'icon_font' => '<i class="material-icons">settings</i>',
			],
			[
				'name'      => 'Локализация',
				'url'       => '/admin/translate',
				'gate_rule' => 'view_translate',
				'icon_font' => '<i class="fa fa-language" aria-hidden="true"></i>',
			],
			[
				'name'      => 'Инфоблоки',
				'url'       => '/admin/infoblocks',
				'gate_rule' => 'view_infoblocks',
				'icon_font' => '',
			],
			[
				'name'      => 'Новости',
				'url'       => '/admin/news',
				'gate_rule' => 'view_news',
				'icon_font' => '<i class="fa fa-newspaper-o" aria-hidden="true"></i>',
			],
			[
				'name'      => 'Слайдеры',
				'url'       => '/admin/sliders',
				'gate_rule' => 'view_sliders',
				'icon_font' => '',
			],
			[
				'active'    => 0,
				'name'      => 'SEO',
				'url'       => '/admin/meta',
				'gate_rule' => 'view_meta',
				'icon_font' => '<i class="fa fa-google-plus" aria-hidden="true"></i>',
			],
			[
				'active'    => 0,
				'name'      => 'Пользователи',
				'url'       => '/admin/users',
				'gate_rule' => 'view_users',
				'icon_font' => '<i class="fa fa-user" aria-hidden="true"></i>',
			],
			[
				'active'    => 0,
				'name'      => 'Роли',
				'url'       => '/admin/roles',
				'gate_rule' => 'view_roles',
				'icon_font' => '<i class="fa fa-user-plus" aria-hidden="true"></i>',
			],
			[
				'name'      => 'Галлерея',
				'url'       => '/admin/galleries',
				'gate_rule' => 'view_galleries',
				'icon_font' => '<i class="fa fa-file-image-o" aria-hidden="true"></i>',
			],
			[
				'name'      => 'Админ меню',
				'url'       => '/admin/admin-menus',
				'gate_rule' => 'view_admin-menus',
				'icon_font' => '<i class="fa fa-bars" aria-hidden="true"></i>',
			],
			[
				'name'      => 'Меню',
				'url'       => '/admin/menu',
				'gate_rule' => 'view_menu',
				'icon_font' => '<i class="fa fa-bars" aria-hidden="true"></i>',
			],
			[
				'name'      => 'Ход строительства',
				'url'       => '/admin/progress',
				'gate_rule' => 'view_progress',
				'icon_font' => '<i class="fa fa-cubes" aria-hidden="true"></i>',
			],
			[
				'name'             => 'Квартиры',
				'url'              => '/admin/flat',
				'gate_rule'        => 'view_flat',
				'icon_font'        => '<i class="material-icons">business</i>',
				'content_provider' => '\App\Http\Controllers\Admin\FlatController',
			],
		];
		$default = [
			'active' => 1,
		];
		foreach ($menus as $menu) {
			$menu = array_merge($default, $menu);
			\App\Models\Admin\Admin_menu::create($menu);
		}
	}
}
