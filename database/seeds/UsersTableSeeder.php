<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class, 'main', 1)->create();
        factory(App\User::class, 'moderator', 1)->create();
        factory(App\User::class, 10)->create();
    }

}
