<?php

use Illuminate\Database\Seeder;
use \App\Models\Translate;
use \App\Models\TranslateLang;

class TranslateTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$groups = [
			'global'          => [
				['key' => 'watch-video', 'value' => 'Смотреть видео'],
				['key' => 'stop-video', 'value' => 'Остановить видео'],
				['key' => 'more-info', 'value' => 'Подробнее'],
				['key' => 'stock', 'value' => 'Акция'],
				['key' => 'stocks', 'value' => 'Акции'],
				['key' => 'show-more', 'value' => 'Показать еще'],
				['key' => 'image', 'value' => 'картинка'],
				['key' => 'photo', 'value' => 'фото'],
				['key' => 'text', 'value' => 'текст'],
				['key' => 'back', 'value' => 'назад'],
				['key' => 'yes', 'value' => ' Да'],
				['key' => 'no', 'value' => 'Нет'],
				['key' => 'view', 'value' => 'Вид'],
				['key' => 'download', 'value' => 'Скачать'],
				['key' => '404', 'value' => 'Страница не найдена'],
				['key' => 'sq-meter', 'value' => '%d м²'],
				['key' => '404-description', 'value' => 'Страница не найдена, попробуйте другой адрес', 'type' => 'textarea'],
			],
			'home'            => [
				['key' => 'about', 'value' => 'О проекте'],
				['key' => 'stop-video', 'value' => 'Остановить видео'],
				['key' => 'download-presentation', 'value' => 'Скачать презентацию'],
			],
			'request'         => [
				['key' => 'sign-to-view', 'value' => 'Записаться на просмотр'],
				['key' => 'contact-to-manager', 'value' => 'Связаться с руководством'],
				['key' => 'send', 'value' => 'Отправить'],
				['key' => 'cancel', 'value' => 'Отмена'],
				['key' => 'message', 'value' => 'Сообщение'],
				['key' => 'send-success', 'value' => 'Заявка успешно отправлена, мы скоро с вами свяжемся, спасибо.'],
				['key' => 'send-error', 'value' => 'Заявка не была отправлена, пожалуйста попробуйте немного позже.'],
				['key' => 'name', 'value' => 'Ваше имя'],
				['key' => 'email', 'value' => 'e-mail'],
				['key' => 'phone', 'value' => 'Номер телефона'],
			],
			'news'            => [
				['key' => 'news', 'value' => 'Новости'],
				['key' => 'single', 'value' => 'новость'],
				['key' => 'empty', 'value' => 'Новостей нет'],
				['key' => 'alt', 'value' => 'Новость %s, фото'],
				['key' => 'all', 'value' => 'Все новости'],
				['key' => 'photo-alt', 'value' => 'Новость %s, фото № %d'],
				['key' => 'next', 'value' => 'Следующая новость'],
				['key' => 'prev', 'value' => 'Предыдущая новость'],
			],
			'progress'        => [
				['key' => 'status', 'value' => 'Статус работ'],
				['key' => 'exploitation', 'value' => 'Введение в эксплуатацию - 2 квартал 2019 года'],
				['key' => 'online-camera', 'value' => 'Онлайн камера'],
				['key' => 'steps', 'value' => 'Ход строительства'],
				['key' => 'alt-gallery', 'value' => 'Обложка %s, фото', 'comment' => 'Альтернативный текст обложки галлереи'],
				['key' => 'alt-gallery-photo', 'value' => '%s фото № %d', 'comment' => 'Альтернативный текст фотографий внутри галлереи (в модалке)'],
			],
			'gallery'         => [
				['key' => 'gallery', 'value' => 'Галерея'],
				['key' => 'alt-photo', 'value' => 'галерея %s слайд № %d', 'comment' => 'Альтернативный текст фотографий галлереи'],
			],
			'flat'            => [
				['key' => 'choose-section', 'value' => 'Выберите секцию'],
				['key' => 'choosing-section', 'value' => 'Выбор секции'],
				['key' => 'section', 'value' => 'Секция'],
				['key' => 'back-to-section', 'value' => 'Вернутся к выбору секции'],
				['key' => 'choose-floor', 'value' => 'Выберите этаж'],
				['key' => 'choose-apartments', 'value' => 'Выберите апартаменты'],
				['key' => 'floor', 'value' => 'Этаж'],
				['key' => 'layout-floor', 'value' => 'Показать планировку этажа'],
				['key' => 'flat', 'value' => 'Квартира'],
				['key' => 'flats', 'value' => 'Квартиры'],
				['key' => 'flat-plural', 'value' => 'квартир'],
				['key' => 'square', 'value' => 'Площадь'],
				['key' => 'rooms', 'value' => 'Комнаты'],
				['key' => 'rooms-quantity', 'value' => 'Кол-во комнат'],
				['key' => 'sea-view', 'value' => 'Вид на море'],
				['key' => 'terrace', 'value' => 'Терраса'],
				['key' => 'flat-finded', 'value' => 'Найдено квартир:'],
				['key' => 'get-price', 'value' => 'Узнать цену'],
				['key' => 'apartment-params', 'value' => 'Параметры апартаментов'],
				['key' => 'apartment-number', 'value' => 'Апартаменты %d'],
				['key' => 'download-layout', 'value' => 'Скачать планировку'],
				['key' => 'layout-floor-of-section', 'value' => 'План %d этажа %d секции'],
				['key' => 'layout-floor', 'value' => 'План %d этажа %d секции'],
				['key' => 'floor-section', 'value' => 'Секция %d / %d Этаж'],
				['key' => 'by-params', 'value' => 'Подбор по параметрам'],
				['key' => 'on-sale', 'value' => 'В продаже'],
				['key' => 'stock-proposals', 'value' => 'Акционные предложения'],
				['key' => 'params.by-params', 'value' => 'Подбор квартир по параметрам'],
				['key' => 'params.no-results', 'value' => 'По заданым параметрам квартир не найдено'],
				['key' => 'accept', 'value' => 'Принять'],
				['key' => 'reset', 'value' => 'Сбросить'],
			],
			'contact'         => [
				['key' => 'contact', 'value' => 'Контакты'],
				['key' => 'sales-department', 'value' => 'Отдел продаж'],
				['key' => 'address', 'value' => 'Одесса, пер. Маячный 23 <br> 1 этаж, офис 3 '],
				['key' => 'manager-first', 'value' => 'Менеджер Лилия'],
				['key' => 'manager-second', 'value' => 'Менеджер Михаил'],
				['key' => 'central-office', 'value' => 'Центральный офис'],
			],
			'about'           => [
				['key' => 'about', 'value' => 'О нас'],
				['key' => 'about-project', 'value' => 'О проекте'],
				['key' => 'download-documents', 'value' => 'Разрешительные документы'],
				['key' => 'our-advantages', 'value' => 'Наши преимущества'],
				['key' => 'head-name', 'value' => 'Елена цветкова'],
				['key' => 'head-position', 'value' => 'Руководитель проекта'],
				['key' => 'watch-apartments', 'value' => 'Смотреть апартаменты'],
				['key' => 'text-about', 'value' => 'Текст о нас ', 'type' => 'textarea'],
				['key' => 'text-about-project', 'value' => 'Текст о проекте ', 'type' => 'textarea'],
				['key' => 'select', 'value' => 'Подобрать'],
			],
			'about-developer' => [
				['key' => 'title', 'value' => 'О застройщике'],
				['key' => 'to-site', 'value' => 'На сайт застройщика'],
				['key' => 'projects', 'value' => 'Проекты застройщика'],
				['key' => 'text-about', 'value' => 'Текст о проекте ', 'type' => 'textarea'],
				['key' => 'developer.title', 'value' => 'Застройщик', 'type' => 'textarea'],
			],
		];
		$languages = \App\Models\Language::all();
		foreach ($groups as $groupName => $group) {
			foreach ($group as $item) {
				$item['group'] = $groupName;
				$item['key'] = implode('.', [$groupName, $item['key']]);
				if ($translate = Translate::where('key', $item['key'])->first()) {
					continue;
				}
				($translate = new Translate())->fillExisting($item)->save();
				$item[$translate->getForeignKey()] = $translate->id;
				foreach ($languages as $lang) {
					$item[$lang->getForeignKey()] = $lang->id;
					($translateLang = new TranslateLang())->fillExisting($item)->save();
				}
			}
		}
	}
}
