<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesAndPermissionsSeeder extends Seeder
{
	public function run()
	{
// Reset cached roles and permissions
		app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

// create permissions
		$create = [
			'comments',
			'articles',
			'requests',
			'users',
			'roles',
			'settings',
			'sliders',
			'news',
			'menu',
			'galleries',
			'meta',
			'infoblocks',
			'translate',
			'progress',
			'flat',
		];
		foreach ($create as $route) {
			Permission::create(['name' => 'add_' . $route]);
			Permission::create(['name' => 'view_' . $route]);
			Permission::create(['name' => 'edit_' . $route]);
			Permission::create(['name' => 'delete_' . $route]);
		}

// create roles and assign created permissions
		$roleWriter = Role::create(['name' => 'writer']);
		$roleWriter->givePermissionTo('edit_articles', 'view_articles');

// or may be done by chaining
		$roleModerator = Role::create(['name' => 'moderator'])->givePermissionTo($this->getRolesModerator());
		if (\App\User::MODERATOR_USER_ID AND $moderator = \App\User::find(\App\User::MODERATOR_USER_ID)) {
			$moderator->assignRole($roleModerator);
		}

		$role = Role::create(['name' => 'admin'])->givePermissionTo(Permission::all());
		if (\App\User::SUPER_ADMIN_ID) {
			\App\User::find(\App\User::SUPER_ADMIN_ID)->assignRole($role);
		}
	}

	private function getRolesModerator()
	{
		return [
			'view_settings',
			'edit_settings',
			'add_news',
			'view_news',
			'edit_news',
			'delete_news',
			'add_galleries',
			'view_galleries',
			'edit_galleries',
			'delete_galleries',
			'view_translate',
			'edit_translate',
			'add_progress',
			'view_progress',
			'edit_progress',
			'add_flat',
			'view_flat',
			'edit_flat',
		];
	}
}