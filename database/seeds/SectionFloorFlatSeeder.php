<?php

use Illuminate\Database\Seeder;

class SectionFloorFlatSeeder extends Seeder
{
	private $numbers;
	private $squareSmart = 32; // число площади, ниже которого, квартира является "смарт"

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$this->initData();
		foreach ($this->numbers as $sectionNumber => $floors) {
			($section = new \App\Models\Section())->fillExisting(['number' => (int)$sectionNumber])->save();
			foreach ($floors as $floorNumber => $flats) {
				$flats = collect($flats);
				$dataFloor = [
					'number' => $floorNumber,
					'square' => $flats->sum('square'),
					'section_id' => $section->id
				];
				($floor = new \App\Models\Floor())->fillExisting($dataFloor)->save();
				foreach ($flats as $flat) {
					$dataFlat = [
						'floor_id' => $floor->id,
						'smart' => $this->getSmartBySquare($flat['square'])
					];
					$flat = array_merge($flat, $dataFlat);
					(new \App\Models\Flat())->fillExisting($flat)->save();
				}
			}
		}
	}

	public function initData()
	{
		$firstSection = [
				/** number floor*/
				8 => [
					['number' => 15, 'room' => 1, 'square' => 31.65],
					['number' => 23, 'room' => 1, 'square' => 31.65],
					['number' => 31, 'room' => 1, 'square' => 31.65],
				],
				7 => [
					['number' => 7, 'room' => 1, 'square' => 31.98],
					['number' => 14, 'room' => 3, 'square' => 88.53],
					['number' => 22, 'room' => 3, 'square' => 88.32],
					['number' => 30, 'room' => 2, 'square' => 66.06],
					['number' => 38, 'room' => 1, 'square' => 31.65],
				],
				6 => [
					['number' => 6, 'room' => 3, 'square' => 92.27],
					['number' => 13, 'room' => 3, 'square' => 116.97],
					['number' => 21, 'room' => 3, 'square' => 116.82],
					['number' => 29, 'room' => 3, 'square' => 87.20],
					['number' => 37, 'room' => 3, 'square' => 110.60],
				],
				5 => [
					['number' => 5, 'room' => 3, 'square' => 116.89],
					['number' => 12, 'room' => 1, 'square' => 48.88],
					['number' => 20, 'room' => 1, 'square' => 48.88],
					['number' => 28, 'room' => 1, 'square' => 48.88],
					['number' => 36, 'room' => 1, 'square' => 48.88],
				],
				4 => [
					['number' => 4, 'room' => 1, 'square' => 51.00],
					['number' => 11, 'room' => 1, 'square' => 50.24],
					['number' => 19, 'room' => 1, 'square' => 50.24],
					['number' => 27, 'room' => 1, 'square' => 50.24],
					['number' => 35, 'room' => 1, 'square' => 50.24],
				],
				3 => [
					['number' => 3, 'room' => 1, 'square' => 52.33],
					['number' => 10, 'room' => 1, 'square' => 49.94],
					['number' => 18, 'room' => 1, 'square' => 49.91],
					['number' => 26, 'room' => 1, 'square' => 49.84],
					['number' => 34, 'room' => 1, 'square' => 49.79],
					['number' => 41, 'room' => 3, 'square' => 216.26],
				],
				2 => [
					['number' => 2, 'room' => 1, 'square' => 52.40],
					['number' => 9, 'room' => 2, 'square' => 71.83],
					['number' => 17, 'room' => 2, 'square' => 71.68],
					['number' => 25, 'room' => 2, 'square' => 71.58],
					['number' => 33, 'room' => 2, 'square' => 71.52],
					['number' => 40, 'room' => 2, 'square' => 71.89],
				],
				1 => [
					['number' => 1, 'room' => 2, 'square' => 74.81],
					['number' => 8, 'room' => 1, 'square' => 31.74],
					['number' => 16, 'room' => 1, 'square' => 31.70],
					['number' => 24, 'room' => 1, 'square' => 31.64],
					['number' => 32, 'room' => 1, 'square' => 31.58],
					['number' => 39, 'room' => 1, 'square' => 32.02],
				],
		];
		foreach (range(1, 5) as $numberSec) {
			$this->numbers[$numberSec] = $firstSection;
		}
	}

	private function getSmartBySquare(float $square){
		return (int) ($square <= $this->squareSmart);
	}
}
