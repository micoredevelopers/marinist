<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSectionsAndFloors extends Migration
{
	protected $table = 'sections';
	protected $secondary = 'floors';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->string('photo', 255)->nullable();
            $table->smallInteger('number')->default(0);
            $table->timestamps();
        });
        Schema::create($this->secondary, function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->smallInteger('section_id');
			$table->smallInteger('number')->default(0);
			$table->smallInteger('sort')->default(0);
			$table->float('square')->default(0);
            $table->string('plan', 255)->nullable()->comment('План');
            $table->string('substrate', 255)->nullable()->comment('Подложка');
            $table->string('backlight', 255)->nullable()->comment('Карта подсветки');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
        Schema::dropIfExists($this->secondary);
    }
}
