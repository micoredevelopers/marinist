<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlatStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flat_status', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('status')->nullable();
            $table->string('status_name')->nullable();
        });
        $statuses = [
            [
                'status'=>null,
                'status_name'=>'Свободно',
            ],
            [
                'status'=>'saled',
                'status_name'=>'В продаже',
            ],
            [
                'status'=>'booked',
                'status_name'=>'Забронировано',
            ],

            [
                'status'=>'investor',
                'status_name'=>'От инвестора',
            ]
        ];
        foreach ($statuses as $status){
            (new \App\Models\FlatStatus())->fillExisting($status)->save();
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flat_status');
    }
}
