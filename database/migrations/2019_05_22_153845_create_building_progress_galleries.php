<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBuildingProgressGalleries extends Migration
{
	protected $table = 'progress_galleries';
	protected $foreign = 'progress_gallery_id';
	protected $tableLang = 'progress_galleries_lang';
	protected $tablePhoto = 'progress_galleries_photo';

	public function up()
	{
		Schema::create($this->table, function (Blueprint $table) {
			$table->increments('id');
			$table->text('video')->nullable();
			$table->tinyInteger('active')->default(1);
			$table->string('photo', 255)->nullable();
			$table->smallInteger('sort')->default(0);
			$table->timestamps();
		});
		//
		Schema::create($this->tableLang, function (Blueprint $table) {
			$table->integer($this->foreign)->unsigned();
			$table->integer('language_id');
			$table->string('name', 255)->nullable();
			$table->longText('description')->nullable();
			$table->longText('sub_description')->nullable();
			//
			$table->index($this->foreign);
			$table->index('language_id');
			//
			$table->foreign($this->foreign)
				->references('id')->on($this->table)
				->onUpdate('cascade')->onDelete('cascade');
		});
		//
		Schema::create($this->tablePhoto, function (Blueprint $table) {
			$table->increments('id');
			$table->integer($this->foreign)->unsigned();
			$table->tinyInteger('active')->default(1);
			$table->string('name', 255)->nullable();
			$table->string('photo', 255)->nullable();
			$table->tinyInteger('sort')->default(0);
			$table->timestamps();
			//
			$table->index($this->foreign);
			//
			$table->foreign($this->foreign)
				->references('id')->on($this->table)
				->onUpdate('cascade')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists($this->tablePhoto);
		Schema::dropIfExists($this->tableLang);
		Schema::dropIfExists($this->table);
	}
}
