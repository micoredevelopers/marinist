<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentsTable extends Migration
{
	protected $table = 'contents';
	protected $foreignKey = 'content_id';
	protected $tableLang = 'contents_lang';
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create($this->table, function (Blueprint $table) {
			$table->increments('id');
			$table->tinyInteger('active')->default(1);
			$table->string('photo', 255)->nullable();
			$table->tinyInteger('sort')->default(0);
			$table->char('type')->nullable();

			$table->timestamps();
		});
		//
		Schema::create($this->tableLang, function (Blueprint $table) {
			$table->integer($this->foreignKey)->unsigned();
			$table->integer('language_id')->unsigned();
			$table->string('name', 255)->nullable();
			$table->longText('description')->nullable();
			$table->text('except')->nullable();
			$table->text('video')->nullable();
			$table->text('options')->nullable();
			//
			$table->index($this->foreignKey);
			$table->index('language_id');
			$table->foreign($this->foreignKey)
				->references('id')->on($this->table)
				->onUpdate('cascade')->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists($this->tableLang);
		Schema::dropIfExists($this->table);
	}
}
