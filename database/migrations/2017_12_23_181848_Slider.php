<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Slider extends Migration
{
	protected $table = 'sliders';
	protected $foreign = 'slider_id';
    protected $tableLang = 'sliders_lang';
    protected $tablePhoto = 'sliders_photo';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create($this->table, function (Blueprint $table) {
		    $table->increments('id');
		    $table->tinyInteger('active')->default(1);
		    $table->string('photo', 255)->nullable();
		    $table->text('options')->nullable();
            $table->tinyInteger('sort')->default(0);
		    $table->timestamps();

		    $table->index('id');
	    });
	    //
        Schema::create($this->tableLang, function (Blueprint $table) {
            $table->integer($this->foreign)->unsigned();
            $table->integer('language_id');
            $table->string('name', 255)->nullable();
            $table->longText('description')->nullable();
            $table->longText('sub_description')->nullable();
            $table->text('video')->nullable();
            //
            $table->index($this->foreign);
            $table->index('language_id');
            $table->foreign($this->foreign)
                ->references('id')->on($this->table)
                ->onUpdate('cascade')->onDelete('cascade');
        });
        //
        Schema::create($this->tablePhoto, function (Blueprint $table) {
            $table->increments('id');
            $table->integer($this->foreign)->unsigned();
            $table->tinyInteger('active')->default(1);
            $table->string('name', 255)->nullable();
            $table->string('photo', 255)->nullable();
            $table->tinyInteger('sort')->default(0);
            $table->timestamps();
            //
            $table->index($this->foreign);
            $table->foreign($this->foreign)
                ->references('id')->on($this->table)
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tablePhoto);
        Schema::dropIfExists($this->tableLang);
        Schema::dropIfExists($this->table);
    }
}
