<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Languages extends Migration
{
	protected $table = 'languages';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create($this->table, function (Blueprint $table) {
		    $table->increments('id');
		    $table->tinyInteger('active')->default(1);
		    $table->tinyInteger('default')->default(0);
		    $table->char('name', 255)->nullable();
		    $table->char('key', 255);
		    $table->char('icon', 255)->nullable();

		    $table->timestamps();

		    $table->index('id');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::dropIfExists($this->table);
    }
}
