<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFlatsAddPercentValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('flats', function (Blueprint $table){
            $table->integer('twenty_five_percent')->nullable()->default(0);
            $table->integer('fifty_percent')->nullable()->default(0);
            $table->integer('seventy_five_percent')->nullable()->default(0);
            $table->integer('one_hundred_percent')->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
