<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Infoblocks extends Migration
{
    protected $table = 'infoblocks';
    protected $foreign = 'infoblock_id';
    protected $tableLang = 'infoblocks_lang';

    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->increments('id');
            $table->smallInteger('active')->default(1);
            $table->timestamps();

            $table->index('id');
        });

        Schema::create($this->tableLang, function (Blueprint $table) {
            $table->integer($this->foreign)->unsigned();

            $table->string('name', 255)->nullable();
            $table->mediumText('description')->nullable();
            $table->text('sub_description')->nullable();
            $table->smallInteger('language_id');
            //
            $table->index($this->foreign);
            $table->index('language_id');

            $table->foreign($this->foreign)
                ->references('id')->on($this->table)
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::dropIfExists($this->tableLang);
	    Schema::dropIfExists($this->table);
    }
}
