<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRooms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flats', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->smallInteger('floor_id');
            $table->tinyInteger('active')->default(1);
			$table->smallInteger('sort')->default(0);
            $table->string('photo')->nullable();
            $table->smallInteger('number')->default(0)->comment('№ квартиры');
            $table->smallInteger('price')->default(0)->comment('Цена на м2');
            $table->smallInteger('sum')->default(0)->comment('Цена всей квартиры');
            $table->double('square')->default(0)->comment('Площадь');
            $table->smallInteger('room')->default(1)->comment('Кол-во комнат');
            $table->boolean('stock')->default(0)->comment('Акция');
            $table->boolean('sea_view')->default(0)->comment('Вид на море');
            $table->boolean('smart')->default(0)->comment('Смарт квартира');
            $table->boolean('terrace')->default(0)->comment('Терасса');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flats');
    }
}
