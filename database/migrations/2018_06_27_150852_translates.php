<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Translates extends Migration
{
	protected $table = 'translate';
	protected $tableLang = 'translate_lang';

	public function up()
	{
		Schema::create($this->table, function (Blueprint $table) {
			$table->increments('id');
			$table->char('key')->nullable();
			$table->string('comment')->nullable();
			$table->tinyInteger('module_id')->default(0);
            $table->string('group', 254)->nullable()->default('global');
            $table->string('type', 255)->default('text');
            $table->timestamps();

			$table->index('id');
		});

		Schema::create($this->tableLang, function (Blueprint $table) {
			$table->integer($this->table.'_id')->unsigned();

			$table->text('value')->nullable();
			$table->smallInteger('language_id');
			//
			$table->index($this->table.'_id');
			$table->index('language_id');

			$table->foreign($this->table.'_id')
				->references('id')->on($this->table)
				->onUpdate('cascade')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists($this->tableLang);
		Schema::dropIfExists($this->table);
	}
}
