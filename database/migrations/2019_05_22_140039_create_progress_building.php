<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgressBuilding extends Migration
{
	protected $table = 'building_progress';
	protected $secondary = 'progress_steps';
	protected $secondaryForeign = 'progress_steps_id';
	protected $secondaryLang = 'progress_steps_lang';

	public function up()
	{
		Schema::create($this->table, function (Blueprint $table) {
			$table->increments('id');
			$table->smallInteger('percent')->default(0);
			$table->timestamps();
		});
		//
		Schema::create($this->secondary, function (Blueprint $table) {
			$table->increments('id');
			$table->smallInteger('sort')->default(0);
			$table->smallInteger('percent')->default(0);
			$table->timestamps();
		});

		Schema::create($this->secondaryLang, function (Blueprint $table) {
			$table->integer($this->secondaryForeign)->unsigned();
			$table->string('name', 255)->nullable();
			$table->smallInteger('language_id');
			//
			$table->index($this->secondaryForeign);
			$table->index('language_id');

			$table->foreign($this->secondaryForeign)
				->references('id')->on($this->secondary)
				->onUpdate('cascade')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists($this->secondaryLang);
		Schema::dropIfExists($this->secondary);
		Schema::dropIfExists($this->table);
	}
}
