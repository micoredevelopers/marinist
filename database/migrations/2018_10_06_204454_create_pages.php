<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePages extends Migration
{
	protected $table = 'pages';
	protected $foreignKey = 'page_id';
	protected $tableLang = 'pages_lang';
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create($this->table, function (Blueprint $table) {
			$table->increments('id');
			$table->tinyInteger('active')->default(1);
			$table->tinyInteger('sort')->default(0)->nullable();
			$table->char('url')->unique();
			$table->string('photo', 255)->nullable();
			$table->text('options')->nullable();
			$table->timestamps();

			$table->index('id');
		});
		//
        Schema::create($this->tableLang, function (Blueprint $table) {
            $table->integer($this->foreignKey)->unsigned();
            $table->integer('language_id');
            $table->string('name', 255)->nullable();
            $table->mediumText('description')->nullable();
            $table->text('except')->nullable();
            //
            $table->index($this->foreignKey);
            $table->index('language_id');
            $table->foreign($this->foreignKey)
                ->references('id')->on($this->table)
                ->onUpdate('cascade')->onDelete('cascade');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists($this->tableLang);
		Schema::dropIfExists($this->table);
	}
}
