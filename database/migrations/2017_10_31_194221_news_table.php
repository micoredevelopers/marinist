<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewsTable extends Migration
{
    protected $table = 'news';
    protected $foreign = 'news_id';
    protected $tableLang = 'news_lang';
    protected $tablePhoto = 'news_photo';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('date_pub')->nullable();
            $table->char('active', 4)->default(0);
            $table->string('url', 128)->unique();
            $table->string('photo', 255)->nullable();
            $table->tinyInteger('sort')->default(0);

            $table->timestamps();
        });
        //
        Schema::create($this->tableLang, function (Blueprint $table) {
            $table->integer($this->foreign)->unsigned();
            $table->integer('language_id');
            $table->string('name', 255)->nullable();
            $table->longText('description')->nullable();
            $table->longText('sub_description')->nullable();
            $table->text('video')->nullable();
            //
            $table->index($this->foreign);
            $table->index('language_id');
            $table->foreign($this->foreign)
                ->references('id')->on($this->table)
                ->onUpdate('cascade')->onDelete('cascade');
        });
        //
        Schema::create($this->tablePhoto, function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer($this->foreign);
            $table->tinyInteger('active')->default(1);
            $table->string('name', 255)->nullable();
            $table->string('photo', 255)->nullable();
            $table->tinyInteger('sort')->default(0);
            $table->timestamps();
            //
            $table->index($this->foreign);
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tablePhoto);
        Schema::dropIfExists($this->tableLang);
        Schema::dropIfExists($this->table);
    }
}
